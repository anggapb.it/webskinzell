<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_mutama extends Controller {
    public function __construct()
    {
        parent::Controller();
              $this->load->library('session');
    }
  
function getTree(){
	$strTree = "";
        $idparent = 0;
		//get Menu
        $q = $this->getMenuFields($idparent);        
        $row = array();
		
        if ($q->num_rows() > 0) {
            $row = $q->result();
            //$r = count($row);
        
			$strTree = "[";
			
			foreach($row as $rr){
				$strTree .= "{";
				//get submenu
				$qc = $this->getMenuFields($rr->idmenu); 
				$rowc = array();
				
				if ($qc->num_rows() > 0) {
					$rowc = $qc->result();					
					$strTree .= $this->getStrTreeProperties($rr, true);
					$strTree .= ",children:["; //Open children
					
					foreach($rowc as $rrc){					
						$strTree .= "{";
						$strTree .= $this->getStrTreeProperties($rrc, false);
						$strTree .= "},";
					}
					$strTree .="]";	 //Close children
				}
				else {
					$strTree .= $this->getStrTreeProperties($rr, false);
				}
				$strTree .= "},";
			}       
			
			//$strTree .= $this->getStrTree($row, $strTree);  //for recursive, not finished
			$strTree .="]";
		}
        echo $strTree;
    }
	
	private function getMenuFields($idparent){
         $otorisasi=$this->input->post("OTOR");
		$this->db->select("*");
//        $this->db->from("v_menu");
//        $this->db->order_by('kdmenu'); 
//		$this->db->where('idsubmenu',$idparent);
//		$this->db->where('idstatus',1);
                
            $this->db->from("v_otoritas");
             $this->db->order_by('kdmenu'); 
		$this->db->where('idsubmenu',$idparent);
		$this->db->where('idstatusklppengguna',1);
                $this->db->where('nmlengkap',$otorisasi);
                
        $q = $this->db->get();
		return $q;
	}
	
	private function getStrTree($row, $strTree){
		$str = $strTree;
		foreach($row as $rr){
			$str .= "{";
			//get submenu
			$q = $this->getMenuFields($rr->idmenu); 
			$rowc = array();
			
			if ($q->num_rows() > 0) {
				$rowc = $q->result();					
				$str .= $this->getStrTreeProperties($rr, true);
				$str .= ",children:["; //Open children
				
				//recursive call getStrTree()
				$str .= $this->getStrTree($rowc, $str);
				
				$str .="]";	 //Close children
			}
			else {
				$str .= $this->getStrTreeProperties($rr, false);
			}
			$str .= "},";
		}
		return $str;
	}
	
	private function getStrTreeProperties($row, $isHasChild){
		$id = "id:'".$row->idmenu."'";
		$kode ="kode:'".$row->kdmenu."'";
		$text ="text:'".$row->nmmenu."'";
		$expanded = "expanded:false";  //($isHasChild)? "expanded:false" : "expanded:false";
		$leaf = ($isHasChild)? "leaf:false" : "leaf:true";
		$parent = "parent:".$row->idsubmenu;
		$url = "url:'".$row->url."'";
		$icon = ($row->gambar != null)? "iconCls:'".$row->gambar."'" : "iconCls:''";
		$ret = $id.",".$kode.",".$text.",".$expanded.",".$leaf.",".$parent.",".$url.",".$icon;
		//$ret = ($isHasChild)? "{".$ret.",children:[" : "{".$ret."},";
		return $ret;
	}
	
	function getNumCols($table,$column,$field,$value){
        $q= "select count(".$column.") as numCols from ".$table." where ".$field."='".$value."'";
        $query  = $this->db->query($q);
        $numCols = 0; 
                    
        if ($query->num_rows() != 0)
        {
            $row = $query->row();
            $numCols=$row->numCols;
        }
        return $numCols;
    }

//function getTree(){
//              
//        $parentid = $this->input->post("node");
//        $xnode =substr($parentid, 0, 5);
//            
//            $this->db->select("*");
//            $this->db->from("v_menu");
//            $this->db->order_by('kdmenu');
//        if($xnode!='xnode'){
//            $parentid=$parentid;
//            $this->db->where('idsubmenu',$parentid);
//            $q = $this->db->get(); 
//            
//        }else{//xnode
//            $parentid=0;
//            $this->db->where('idsubmenu',$parentid);
//            $q = $this->db->get(); 
//            
//        }   
//        
//        $row = array();
//        if ($q->num_rows() > 0) {
//            $row = $q->result();
//            $r = count($row);
//        }
//           
//        $i = 1;
//        $print="[";
//            foreach($row as $rr){
//                if($i < $r){
//                    $c=",";
//                }else{
//                    $c="";
//                }
//                
//                $print .="{id:'".$rr->idmenu."',kode:'".$rr->kdmenu."',text:'".$rr->nmmenu."',";
//                if($rr->nmsubmenu==null){
//                        $print .= "expanded:false,iconCls:''";
//                }else{
//                            $print .= "leaf:true,";
//                            $print .= "parent:".$rr->idsubmenu.",";
//                            $print .= "url:'".$rr->url."',iconCls:''";
//
//                }
//                    
//                $print .= "}".$c;
//                $i++;
//            }
//       
//        $print .="]";
//        
//      //  $printx ="[{id:'11',text:'Menu Backend',leaf:true,parent:10,url:'admin/tools/module.do',iconCls:''}]";
//        echo $print;
//
//    }
//
//    function countx($column,$tbl,$whereb,$wherea){
//        $q= "select count(".$column.") as max from ".$tbl." where ".$whereb."='".$wherea."'";
//        $query  = $this->db->query($q);
//        $max = ''; 
//                    
//        if ($query->num_rows() != 0)
//        {
//            $row = $query->row();
//            $max=$row->max;
//        }
//        return $max;
//    }  
}
