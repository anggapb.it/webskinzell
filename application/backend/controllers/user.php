<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends Controller {
var $stat;

    public function __construct()
    {
        parent::Controller();
    }

    
    public function index()
    {
        if ($this->my_usessionpublic->logged_in)
        {
                $data['title'] = 'Owner Area';
                $this->load->view('user/index-unla', $data);
        }
        else
        {
				redirect('user/login');
           
        }
    }
    
	function autoNumber($column,$tbl){
        $q = "SELECT max(".$column.")+1 as max FROM ".$tbl."" ;
        $query  = $this->db->query($q);
        $max = ''; 
                    
        if ($query->num_rows() != 0)
        {
            $row = $query->row();
            $max=$row->max;
        }
        if ($max == null){
            $max=0;
        }
        return $max;
    }
	
	 function nm_field($column,$tbl,$whereb, $wherea){
        $q = "SELECT ".$column." as nm FROM ".$tbl." where ".$whereb." = '".$wherea."' " ;
        $query  = $this->db->query($q);
        $nm= ''; 
                    
        if ($query->num_rows() != 0)
        {
            $row = $query->row();
            $nm=$row->nm;
        }
        return $nm;
    }  
     
        
    public function login()
    {
        if ($this->my_usessionpublic->logged_in)
        {
            redirect('user/index');
        }
        else
        {
            $data['title'] = 'User Login';
			$data['pesan']="";
            $this->load->view('user/login', $data);
        }
    }

    public function ext_is_unique_username()
    {
        $cond = array('username' => $_POST['username']);
        $query = $this->db->get_where('tbl_admin', $cond);
        if ($query->num_rows() != 0)
        {
            echo 0;
        }
        else
        {
            echo 1;
        }
    }
    
    public function ext_is_unique_email()
    {
        $cond = array('email' => $_POST['email']);
        $query = $this->db->get_where('tbl_admin', $cond);
        if ($query->num_rows() != 0)
        {
            echo 0;
        }
        else
        {
            echo 1;
        }
    }

    public function ext_logout()
    {
	
		//$this->updatelog();
	
		$this->my_usessionpublic->unset_userdata("idlog1unla");
        $this->my_usessionpublic->unset_userdata("user_id1unla");
        $this->my_usessionpublic->unset_userdata('status_aktifasi1unla');
        $this->my_usessionpublic->unset_userdata('username1unla');
        $this->my_usessionpublic->unset_userdata('level_member1unla');
		$this->my_usessionpublic->unset_userdata('nm_klp1unla');
        $this->session->sess_destroy();    
        echo "{success:true}";
    }

    public function ext_login()
    {
        $cond = array(
            'userid' => $_POST['logUsername'],
            'password' => $_POST['logPassword']
        );
        $query = $this->db->get_where('pengguna', $cond);
        if ($query->num_rows() != 0)
        {
            $row = $query->row();
            $this->my_usessionpublic->set_userdata('status_aktifasi1unla', $row->idstatus);
            if($this->my_usessionpublic->userdata('status_aktifasi1unla') == 1){
                $this->my_usessionpublic->set_userdata('user_id1unla', $row->userid);
                $this->my_usessionpublic->set_userdata('username1unla', $row->nmlengkap);
                $this->my_usessionpublic->set_userdata('level_member1unla', $row->idklppengguna);
				$this->my_usessionpublic->set_userdata('nm_klp1unla', $this->nm_field('nmklppengguna','klppengguna','idklppengguna',$row->idklppengguna));
				//$this->savelog();
				
              redirect('user/index');
            }else{
				$data['pesan']="User Tidak Aktif!";
				$this->load->view('user/login', $data);
            }
            
        }
		else if (!$cond['userid'] && !$cond['password'])
		{
				$data['pesan']="Masukkan User ID dan Password!";
				$this->load->view('user/login', $data);
		}
        else
        {
				$data['pesan']="User ID atau Password Salah!";
				$this->load->view('user/login', $data);
        }
    }
	
	function savelog(){      // ISTRA
	  $ipaddress = $_SERVER['REMOTE_ADDR'];
	  $idlog = $this->autoNumber('idlog','logpengguna');
	  
         $data = array(
			 'idlog'=> $idlog,
			 'userid'=> $_POST['logUsername'],
			 'ipaddress'=> $ipaddress,
              );

        $this->db->query("CALL SP_insertlog (?,?,?)", $data);
		
        $this->my_usessionpublic->set_userdata('idlog1unla', $idlog);
		
      if($this->db->trans_status()=== FALSE)
        {
            $this->db->trans_rollback();
            $ret["success"]=false;
            $ret["message"]="Simpan Data gagal";
        }
        else
        {
            $this->db->trans_commit();
            $ret["success"]=true;
            $ret["message"]="Simpan Data Berhasil";
        }
        return $ret;
    }
	
	function updatelog(){      // ISTRA

         $data = array(
             'idlog'=>  $this->my_usessionpublic->userdata('idlog1unla'),
              );

        $this->db->query("CALL SP_updatelog (?)", $data);
        	
      if($this->db->trans_status()=== FALSE)
        {
            $this->db->trans_rollback();
            $ret["success"]=false;
            $ret["message"]="Simpan Data gagal";
        }
        else
        {
            $this->db->trans_commit();
            $ret["success"]=true;
            $ret["message"]="Simpan Data Berhasil";
        }
        return $ret;
    }

}
