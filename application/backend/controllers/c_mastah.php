<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_mastah extends Controller {
var $stat;

    public function __construct()
    {
        parent::Controller();
        
    }

    // START HELPER ===========================================================
    function gridM_status(){ //ISTRA
        $q="SELECT * from status";
        
        $query  = $this->db->query($q);
        $data = array();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        }
        
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
        echo json_encode($build_array);
    }
    
        function gridM_jkerja(){ //Jenis Kerjasama
        $q="SELECT * from jkerjasama";
        
        $query  = $this->db->query($q);
        $data = array();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        }
        
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
        echo json_encode($build_array);
    }

        function gridM_jabatan(){ //REG Jabatan
        $q="SELECT * from jabatan";
        
        $query  = $this->db->query($q);
        $data = array();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        }
        
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
        echo json_encode($build_array);
    }

        function gridM_negaratujuan(){ //REG Negara Tujuan
        $q="SELECT * from negaratujuan";
        
        $query  = $this->db->query($q);
        $data = array();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        }
        
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
        echo json_encode($build_array);
    }

        function gridM_periode(){ //REG Periode
        $q="SELECT * from periode";
        
        $query  = $this->db->query($q);
        $data = array();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        }
        
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
        echo json_encode($build_array);
    }
	
	    function gridM_provinsi(){ //REG Provinsi
        $q="SELECT * from provinsi";
        
        $query  = $this->db->query($q);
        $data = array();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        }
        
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
        echo json_encode($build_array);
    }
	
    function gridM_stposting(){ //REG Provinsi
        $q="SELECT * from stposting";
        
        $query  = $this->db->query($q);
        $data = array();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        }
        
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
        echo json_encode($build_array);
    }

	function gridM_jhewan(){ //Jenis Kerjasama
        $q="SELECT * from jhewan";
        
        $query  = $this->db->query($q);
        $data = array();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        }
        
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
        echo json_encode($build_array);
    }

    function gridM_klshewan(){ //Jenis 
        $q="SELECT * from klshewan";
        
        $query  = $this->db->query($q);
        $data = array();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        }
        
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
        echo json_encode($build_array);
    }

    function gridM_program(){ //Jenis 
        $q="SELECT * from program";
        
        $query  = $this->db->query($q);
        $data = array();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        }
        
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
        echo json_encode($build_array);
    }

    function gridM_dash(){ //ISTRA
        $q="SELECT * from jdashboard";
        
        $query  = $this->db->query($q);
        $data = array();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        }
        
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
        echo json_encode($build_array);
    }
    
    function gridM_hierarki(){ //ISTRA
        $q="SELECT * from jhirarki";
        
        $query  = $this->db->query($q);
        $data = array();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        }
        
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
        echo json_encode($build_array);
    }
    
    function gridS_menu(){ //ISTRA
        $q="SELECT * from menu";
        
        $query  = $this->db->query($q);
        $data = array();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        }
        
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
        echo json_encode($build_array);
    }
	
	function gridS_menuweb(){ //ISTRA
        $q="SELECT * from menuweb where idjnshirarki=1";
        
        $query  = $this->db->query($q);
        $data = array();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        }
        
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
        echo json_encode($build_array);
    }
    
    function gridL_pengguna(){ //ISTRA
	 $rmasuk                  = $this->input->post("rtglmasuk");
     $rkeluar                 = $this->input->post("rtglkeluar");
	 
	 $tgl1                  = $this->input->post("tglmasuk");
     $tgl2                  = $this->input->post("tglkeluar");
	 
	 if ($rmasuk == 1){
		$q="SELECT * from logpengguna where date(tglmasuk) between '". $tgl1 ."' and '". $tgl2 ."'";	
	 } else if ($rkeluar == 1){
		$q="SELECT * from logpengguna where date(tglkeluar) between '". $tgl1 ."' and '". $tgl2 ."'";	
	 }
	 else {
        $q="SELECT * from logpengguna";
	 }
        
        $query  = $this->db->query($q);
        $data = array();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        }
        
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
        echo json_encode($build_array);
    }
    
    function grid_stpublish(){ //ISTRA
        $q="SELECT * from stpublish";
        
        $query  = $this->db->query($q);
        $data = array();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        }
        
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
        echo json_encode($build_array);
    }
    
     function grid_kategorihalaman(){ //ISTRA
        $q="SELECT * from ktghalaman";
        
        $query  = $this->db->query($q);
        $data = array();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        }
        
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
        echo json_encode($build_array);
    }
    
    function grid_jgaleri(){ //ISTRA
        $q="SELECT * from jgaleri";
        
        $query  = $this->db->query($q);
        $data = array();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        }
        
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
        echo json_encode($build_array);
    }
    
    function grid_klpgaleri(){ //ISTRA
        $q="SELECT * from klpgaleri";
        
        $query  = $this->db->query($q);
        $data = array();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        }
        
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
        echo json_encode($build_array);
    }
    
    function grid_klptautan(){ //ISTRA
        $q="SELECT * from klptautan";
        
        $query  = $this->db->query($q);
        $data = array();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        }
        
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
        echo json_encode($build_array);
    }
    
    function grid_stsemester(){ //ISTRA
        $q="SELECT * from stsemester";
        
        $query  = $this->db->query($q);
        $data = array();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        }
        
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
        echo json_encode($build_array);
    }
    
    function grid_vstsemester(){ //ISTRA
        $KEDUA                         = $this->load->database('second', TRUE);
        
        $q="SELECT kdstsemester, kdthnakademik, idjnssemester, nmjnssemester from vv_status_semester WHERE idstatus = 1";
        
        $query  = $KEDUA->query($q);
        if ($query->num_rows() > 0) {
            $data = $query->row();
        }
        
        $ttl = count($data);
        
        if($ttl>0){
            $build_array='{"kdstsemester":"'.$data->kdstsemester
                    .'", "kdthnakademik":"'.$data->kdthnakademik
                    .'", "idjnssemester":"'.$data->idjnssemester
                    .'", "nmjnssemester":"'.$data->nmjnssemester.'"}';
        }
        echo $build_array;
    }
    
    function grid_vstsemester2(){ //ISTRA
        $KEDUA                         = $this->load->database('second', TRUE);
        
        $q="SELECT kdstsemester, kdthnakademik, idjnssemester, nmjnssemester from vv_status_semester WHERE idstatus = 1";
        
        $query  = $KEDUA->query($q);
         if ($query->num_rows() > 0) {
            $data = $query->result();
        }
        
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
        echo json_encode($build_array);
    }
    
    function grid_vstsemester3(){ //ISTRA
        $KEDUA                         = $this->load->database('second', TRUE);
        
        $q="SELECT kdstsemester, kdthnakademik, idjnssemester, nmjnssemester from vv_status_semester";
        
        $query  = $KEDUA->query($q);
         if ($query->num_rows() > 0) {
            $data = $query->result();
        }
        
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
        echo json_encode($build_array);
    }
    
    function grid_stmskmhs(){ //ISTRA
        $q="SELECT idstmskmhs, nmstmskmhs from stmskmhs";
        
        $query  = $this->db->query($q);
        $data = array();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        }
        
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
        echo json_encode($build_array);
    }
	
	function grid_jusm(){ //ISTRA
        $q="SELECT idjnsusm, nmjnsusm from jusm";
        
        $query  = $this->db->query($q);
        $data = array();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        }
        
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
        echo json_encode($build_array);
    }
    
    function grid_stmskmhs_pmb(){ //ISTRA
        $q="SELECT idstmskmhs, nmstmskmhs from stmskmhs where idstmskmhs<=2";
        
        $query  = $this->db->query($q);
        $data = array();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        }
        
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
        echo json_encode($build_array);
    }
    
    function grid_jadwalpmb(){ //ISTRA
        $q="SELECT * from jadwalpmb where idstatus=1";
        
        $query  = $this->db->query($q);
        $data = array();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        }
        
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
        echo json_encode($build_array);
    }
    
    function grid_prodi(){ //ISTRA

        $q="SELECT nourutprodi
     , nmprodi
     , nmjenjangstudi
     , kdprodi
FROM
  v_prodi
WHERE
  nmjenjangstudi <> 'S-2'
  AND kdprodi IN (
SELECT kdprodi
FROM
  penggunaprodi
WHERE
  userid = '".$this->my_usessionpublic->userdata("user_id1unla")."')";
    
        $query  = $this->db->query($q);
        $data = array();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        }
        
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
           foreach($data as $row) {
                array_push($build_array["data"],array(
                    'kdprodi'=>$row->kdprodi,
                    'nmprodi'=>$row->nourutprodi.".".$row->nmprodi." ".$row->nmjenjangstudi
                ));
            }
        }
        echo json_encode($build_array);
    }
	
	function grid_prodiall(){ //ISTRA

        $q="SELECT nourutprodi
     , nmprodi
     , nmjenjangstudi
     , kdprodi
FROM
  v_prodi
WHERE
  nmjenjangstudi <> 'S-2'";
    
        $query  = $this->db->query($q);
        $data = array();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        }
        
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
           foreach($data as $row) {
                array_push($build_array["data"],array(
                    'kdprodi'=>$row->kdprodi,
                    'nmprodi'=>$row->nourutprodi.".".$row->nmprodi." ".$row->nmjenjangstudi
                ));
            }
        }
        echo json_encode($build_array);
    }
    
    function grid_prodiasal(){ //ISTRA
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        $fields                 = $this->input->post("fields");
        $query                  = $this->input->post("query");
       
        
        $this->db->select("kdpsttbpst, nmpsttbpst");
        $this->db->from("tbpst"); 
        
        //$q="SELECT nmkabtbpro, nmprotbpro,kdprotbpro,kdkabtbpro from tbpro";
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(50,0);
        }
        
        if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
    //        $a[explode(',', $r)];
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }

           // $this->db->bracket('open','like');
             $this->db->or_like($d, $query);
           // $this->db->bracket('close','like');
        }
        
        $query2  = $this->db->get(); //$this->db->query($q);
        $data = array();
        if ($query2->num_rows() > 0) {
            $data = $query2->result();
        }
            
        $ttl =$this->db->count_all('tbpst');
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
           foreach($data as $row) {
                array_push($build_array["data"],array(
                    'kdprodi'=>$row->kdpsttbpst,
                    'nmprodi'=>$row->nmpsttbpst
                ));
            }
        }
        echo json_encode($build_array);
    }
    
    function grid_fieldpmb()
    {
        $query="SELECT COLUMN_NAME AS kolom FROM information_schema.`COLUMNS` WHERE TABLE_NAME = 'pmb' AND TABLE_SCHEMA = 'siak-unla-public'";
        $exec=$this->db->query($query);
        
        $data['data']=$exec->result();
        echo json_encode($data);
    }
    
    function grid_klsmhs(){ //ISTRA
        $dimana                   = "";
        $kdprodi                  = $this->input->post("kdprodi");
        
        //////////
        $idpilih = $this->input->post("idnom");
        if ($idpilih){
            if($idpilih == '3'){
                $q="SELECT idklsmhs, kdklsmhs, nmklsmhs from klsmhs";
            } elseif ($idpilih == '1' || $idpilih == '2') {
                $q="SELECT idklsmhs, kdklsmhs, nmklsmhs from klsmhs WHERE idklsmhs = '1' OR idklsmhs = '2'";
            }
        }
        
        else {
        
            if($kdprodi !=''){
                $dimana = "Where kdprodi=".$kdprodi;
            }
                // $q="SELECT idklsmhs, kdklsmhs, nmklsmhs from v_klsprodi";
                $q="SELECT idklsmhs, kdklsmhs, nmklsmhs from klsmhs";
        }
        
        
        $query  = $this->db->query($q);
        $data = array();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        }
        
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
        echo json_encode($build_array);
    }
    
    
    function grid_kotkabasal(){ //ISTRA
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        $fields                 = $this->input->post("fields");
        $query                  = $this->input->post("query");
       
        
        $this->db->select("nmkabtbpro, nmprotbpro,kdprotbpro,kdkabtbpro");
        $this->db->from("tbpro"); 
        
        //$q="SELECT nmkabtbpro, nmprotbpro,kdprotbpro,kdkabtbpro from tbpro";
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(50,0);
        }
        
        if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
    //        $a[explode(',', $r)];
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }

           // $this->db->bracket('open','like');
             $this->db->or_like($d, $query);
           // $this->db->bracket('close','like');
        }
        
        $query2  = $this->db->get(); //$this->db->query($q);
        $data = array();
        if ($query2->num_rows() > 0) {
            $data = $query2->result();
        }
            
        $ttl =$this->db->count_all('tbpro');
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            foreach($data as $row) {
                array_push($build_array["data"],array(
                    'nmkabtbpro'=>$row->nmkabtbpro,
                    'nmprotbpro'=>$row->nmprotbpro,
                    'kdkabtbpro'=>$row->kdkabtbpro.$row->kdprotbpro,
                                ));
            }
        }
        echo json_encode($build_array);
    }//
    //
    function grid_pekerjaanortu(){ //ISTRA
        $q="SELECT idpekerjaanortu, nmpekerjaanortu from pekerjaanortu";
        
        $query  = $this->db->query($q);
        $data = array();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        }
        
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
        echo json_encode($build_array);
    }
	
	function grid_pekerjaanmhs(){ //ISTRA
        $q="SELECT idpekerjaanmhs, nmpekerjaanmhs from pekerjaanmhs";
        
        $query  = $this->db->query($q);
        $data = array();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        }
        
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
        echo json_encode($build_array);
    }
    
    function grid_ptasal(){ //ISTRA
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        $fields                 = $this->input->post("fields");
        $query                  = $this->input->post("query");
       
        
        $this->db->select("nmptitbpti, kdptitbpti");
        $this->db->from("tbpti"); 
        
        //$q="SELECT nmkabtbpro, nmprotbpro,kdprotbpro,kdkabtbpro from tbpro";
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(50,0);
        }
        
        if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
    //        $a[explode(',', $r)];
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }

           // $this->db->bracket('open','like');
             $this->db->or_like($d, $query);
           // $this->db->bracket('close','like');
        }
        
        $query2  = $this->db->get(); //$this->db->query($q);
        $data = array();
        if ($query2->num_rows() > 0) {
            $data = $query2->result();
        }
            
        $ttl =$this->db->count_all('tbpti');
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
        echo json_encode($build_array);
    }//
    //
    function grid_jenpdkpendaftar(){ //ISTRA
        $q="SELECT idjenpdkpendaftar, kdjenpdkpendaftar, nmjenpdkpendaftar from jenpdkpendaftar where idjenpdkpendaftar>3";
        
        $query  = $this->db->query($q);
        $data = array();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        }
        
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
        echo json_encode($build_array);
    } //grid_stakreditasi
    
    function grid_stakreditasi(){ //ISTRA
        $q="SELECT idstakreditasi, nmstakreditasi from stakreditasi";
        
        $query  = $this->db->query($q);
        $data = array();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        }
        
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
        echo json_encode($build_array);
    } //grid_sbrinfo
    //
    function grid_sbrinfo(){ //ISTRA
        $q="SELECT idsbrinfo, nmsbrinfo from sbrinfo";
        
        $query  = $this->db->query($q);
        $data = array();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        }
        
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
        echo json_encode($build_array);
    }
    
    function grid_rekpmb(){ //ISTRA
        $q="SELECT idrekpmb, nmrekpmb from rekpmb";
        
        $query  = $this->db->query($q);
        $data = array();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        }
        
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
        echo json_encode($build_array);
    }
    
    function grid_alasanpendaftar(){ //ISTRA
        $q="SELECT idalasanpendaftar, nmalasanpendaftar from alasanpendaftar";
        
        $query  = $this->db->query($q);
        $data = array();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        }
        
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
        echo json_encode($build_array);
    }
    
    function grid_carabyr(){ //ISTRA
        $q="SELECT idcarabyr, nmcarabyr from carabyr where idcarabyr<>'1'";
        
        $query  = $this->db->query($q);
        $data = array();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        }
        
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
        echo json_encode($build_array);
    }
    
    function gridM_stusm(){ 
        $q="SELECT idstusm, nmstusm from stusm";
        
        $query  = $this->db->query($q);
        $data = array();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        }
        
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
        echo json_encode($build_array);
    }
    
    function grid_jpengguna(){ //ISTRA
        $q="SELECT idjnspengguna, nmjnspengguna from jpengguna";
        
        $query  = $this->db->query($q);
        $data = array();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        }
        
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
        echo json_encode($build_array);
    }
    
    function griddosen(){ //ISTRA
        $KEDUA                         = $this->load->database('second', TRUE);
        $q="SELECT nidu, nmdosdgngelar from dosen";
        
        $query  = $KEDUA->query($q);
        $data = array();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        }
        
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
        echo json_encode($build_array);
    }
    
    function grid_thnakademik(){ //ISTRA
        $KEDUA                         = $this->load->database('second', TRUE);
        $q="SELECT idthnakademik, kdthnakademik from thnakademik order by idthnakademik";
        
        $query  = $KEDUA->query($q);
        $data = array();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        }
        
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
        echo json_encode($build_array);
    }
	
	 function gridS_halaman(){ //ISTRA
        $q="SELECT * from halaman";
        
        $query  = $this->db->query($q);
        $data = array();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        }
        
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
        echo json_encode($build_array);
    }
	
	function grid_setbiayapmb(){ //ISTRA
       $q="SELECT idsetbiayapmb, nmsetbiayapmb from setbiayapmb where idstatus=1";
        
        $query  =  $this->db->query($q);
        $data = array();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        }
        
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
        echo json_encode($build_array);
    }
	
	
	function grid_perusahaan(){ //ISTRA
        $q="SELECT * from perusahaan";
        
        $query  = $this->db->query($q);
        $data = array();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        }
        
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
        echo json_encode($build_array);
    }
	
	function grid_posisi(){ //ISTRA
        $q="SELECT * from posisi";
        
        $query  = $this->db->query($q);
        $data = array();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        }
        
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
        echo json_encode($build_array);
    }
	
	function grid_matakuliah(){ //ISTRA
        $q="SELECT * from matakuliah";
        
        $query  = $this->db->query($q);
        $data = array();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        }
        
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
        echo json_encode($build_array);
    }
	
	function grid_sesi(){ //ISTRA
        $q="SELECT * from sesi";
        
        $query  = $this->db->query($q);
        $data = array();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        }
        
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
        echo json_encode($build_array);
    }
	
	function grid_thnakademik2(){ //ISTRA
        $q="SELECT * from v_tahunakademik";
        
        $query  = $this->db->query($q);
        $data = array();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        }
        
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
        echo json_encode($build_array);
    }
	
	function grid_jmlbiaya(){ //ISTRA
        $q="SELECT * from setbiayapmb GROUP BY idpilihprodi";
        
        $query  = $this->db->query($q);
        $data = array();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        }
        
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
        echo json_encode($build_array);
    }
	
	function grid_tahun(){ //ISTRA
        $q="SELECT tahun as kode, tahun as nama FROM tahun where tahun<>''
			ORDER BY tahun DESC";
        
        $query  = $this->db->query($q);
        $data = array();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        }
        
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
        echo json_encode($build_array);
    }
	
	function grid_jkls(){ //ISTRA
        $q="SELECT idjnskls,kdjnskls, nmjnskls,deskripsi
			FROM jkls";
        
        $query  = $this->db->query($q);
        $data = array();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        }
        
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
        echo json_encode($build_array);
    }
	
	//katalog//pengarang buku//
	function grid_pengarang(){ //ISTRA
        $q="SELECT * FROM pengarang_buku";
        
        $query  = $this->db->query($q);
        $data = array();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        }
        
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
        echo json_encode($build_array);
    }
	
	//katalog//kategori buku//
	function grid_kategori(){ //ISTRA
        $q="SELECT * FROM kategori_buku";
        
        $query  = $this->db->query($q);
        $data = array();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        }
        
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
        echo json_encode($build_array);
    }
	
	function grid_pengguna_mhs_dos(){ //ISTRA
        $q="SELECT * FROM klppengguna WHERE idklppengguna IN (2,3)";
        
        $query  = $this->db->query($q);
        $data = array();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        }
        
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
        echo json_encode($build_array);
    }
	
	function gridstkrs(){ //ISTRA
        $KEDUA                         = $this->load->database('second', TRUE);
        $q="SELECT * from stkrs";
        
        $query  = $KEDUA->query($q);
        $data = array();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        }
        
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
        echo json_encode($build_array);
    }
	
	function grid_tbpro(){ //ISTRA
        $q="SELECT CONCAT(rtrim(kdkabtbpro),'',ltrim(kdprotbpro)) as kdkotkabasal,nmkabtbpro FROM tbpro";
        
        $query  = $this->db->query($q);
        $data = array();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        }
        
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
        echo json_encode($build_array);
    }
	
	function grid_jnsjurnal(){ //ISTRA
        $q="SELECT * FROM jnsjurnal";
        
        $query  = $this->db->query($q);
        $data = array();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        }
        
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
        echo json_encode($build_array);
    }
	
	function grid_jnsbuku(){ //ISTRA
        $q="SELECT * FROM jenis_buku";
        
        $query  = $this->db->query($q);
        $data = array();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        }
        
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
        echo json_encode($build_array);
    }
	
	function grid_fakultas(){ //ISTRA
        $q="SELECT * FROM siak_unla_private.fakultas WHERE kdfakultas <> 6";
        
        $query  = $this->db->query($q);
        $data = array();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        }
        
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
        echo json_encode($build_array);
    }
	
	function grid_prodi_jurnal(){ //ISTRA
        $q="SELECT * FROM siak_unla_private.prodi WHERE kdfakultas='".$_POST['kdfakultas']."'";
        
        $query  = $this->db->query($q);
        $data = array();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        }
        
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
        echo json_encode($build_array);
    }
	
	function get_url_private(){ //ISTRA
        $q="SELECT nilai from setting where idklpset='4'";
        
        $query  = $this->db->query($q);
        $data = array();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        }
        
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
        echo json_encode($build_array);
    }
    // END HELPER =============================================================
  
}
