<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start();
class Content extends Controller {
        function __construct() {
            parent::__construct();
            $this->load->model('m_website_unlaacid');
			$this->load->model(array('Mediatutorialcaptcha'));
			$this->load->helpers(array('form_helper','url_helper'));
			//$this->load->libraries(array('form_validation'));
            $this->load->database();
        }
        
	function Content()
	{
		parent::Controller();	
	}

	function index($id=NULL)
	{       
                 $jml = $this->db->get('v_beritanpengumuman');

                 //pengaturan pagination
                 $config['base_url'] = base_url().'index.php/web/content/index';
                 $config['total_rows'] = $jml->num_rows();
                 $config['per_page'] = '9';
                 $config['page_galeri'] = '9';
                 $config['per_pagehead'] = '1';
                 $config['page_poster'] = '1';
                 $config['per_page_post'] = '3';
                 $config['per_page_pop'] = '5';
				 $config['per_page_testi'] = '5';
				 $config['footerpost'] = '5';
				 $config['num_links'] = '5';
				 $config['uri_segment'] = 4;
                 $config['page_service'] = '4';
                 $config['page_product'] = '4';
                 $config['berita_foot'] = '2';
                 $config['per_promo'] = '4';

                /*  $config['first_page'] = 'Awal';
                 $config['last_page'] = 'Akhir';
                 $config['next_page'] = '&laquo;';
                 $config['prev_page'] = '&raquo;'; */
	
                //inisialisasi config
                $this->pagination->initialize($config);

                //buat pagination
                $data['halaman'] = $this->pagination->create_links();

				//slider
				$data['slider']=$this->m_website_unlaacid->getByid('slideratas',1,'galeri');
                
				//tamplikan data
                $data['service'] = $this->m_website_unlaacid->getfPage('v_service',$config['page_service'], $id);
                $data['product'] = $this->m_website_unlaacid->getfPage('v_product',$config['page_product'], $id);
                $data['promo'] = $this->m_website_unlaacid->getfPage('v_promo',$config['per_promo'], $id);
                $data['menu_meals'] = $this->m_website_unlaacid->getfPage('v_loker',$config['per_page'], $id);
                $data['menu_cookies'] = $this->m_website_unlaacid->getfPage('v_menu_cookies',$config['per_page'], $id);
                $data['menu_desserts'] = $this->m_website_unlaacid->getfPage('v_menu_desserts',$config['per_page'], $id);
                $data['package'] = $this->m_website_unlaacid->getfPage('v_package',$config['per_page'], $id);
                $data['queryhead'] = $this->m_website_unlaacid->getfPage('v_beritanpengumuman',$config['per_pagehead'], $id);
				$data['berita'] = $this->m_website_unlaacid->getfPage('v_berita',$config['per_page_post'], $id);
				$data['galeri']=  $this->m_website_unlaacid->getfPage('v_galeri_index',$config['page_galeri'], $id);
                $data['postpopular'] = $this->m_website_unlaacid->getfPage('v_popularpost',$config['per_page_pop'], $id);
                $data['postrecent'] = $this->m_website_unlaacid->getfPage('v_recentpost',$config['per_page_pop'], $id);
                $data['postakademik'] = $this->m_website_unlaacid->getfPage('v_post_kegiatanakademik',$config['per_page_post'], $id);
                $data['postpenelitian'] = $this->m_website_unlaacid->getfPage('v_post_penelitian',$config['per_page_post'], $id);
                $data['postpengabdian'] = $this->m_website_unlaacid->getfPage('v_post_pengabdian',$config['per_page_post'], $id);
                $data['postberita'] = $this->m_website_unlaacid->getfPage('v_post_berita',$config['per_page_post'], $id);
                $data['databank'] = $this->m_website_unlaacid->getAll('rekbank','','');
                //$data['gbrposter'] = $this->m_website_unlaacid->getAll('poster','','');
                $data['gbrposter'] = $this->m_website_unlaacid->getfPage('v_poster',$config['page_poster'], $id);
                $data['datasponsor'] = $this->m_website_unlaacid->getAllkerjasama('kerjasama','','');
                //$data['dataharga'] = $this->m_website_unlaacid->getAll('v_det_hewan','','');
                $data['datakegiatan']=  $this->m_website_unlaacid->getAll('kegiatan','','');
                $data['deskripsislider']=  $this->m_website_unlaacid->getAll('galeri','','');
				$data['datatestimoni'] = $this->m_website_unlaacid->getfPage('v_testimoni',$config['per_page_testi'], $id);
                $data['berita_foot'] = $this->m_website_unlaacid->getfPage('v_berita',$config['berita_foot'], $id);

                //$data['bnp']=  $this->m_website_unlaacid->getAll('v_beritanpengumuman','tglpublish','DESC');
                $data['agenda'] =$this->m_website_unlaacid->getAll('v_agenda','','');
				
				$data['tautan']=$this->m_website_unlaacid->getByid('idstpublish','1','tautan');
				$data['menuatas']=$this->m_website_unlaacid->getByid('menuatas','1','menuweb');
				$data['menukanan']=$this->m_website_unlaacid->getByid('menukanan','1','menuweb');
				$data['kerjasama']=$this->m_website_unlaacid->getByid('idmenuweb','33','menuweb');
				
				$temp = base_url();
				$slash='';
				for ($i=0; $i < strlen($temp); $i++)  { 
					if ($temp[$i]=='/') {
						$slash .= $temp[$i]; // fungsi menghilangkan url website ini dengan menambahkan karakter slash
					}
				}
				
				$data['addslash'] = $slash;
				$this->load->view('web/index-web',$data);
		
	}
        
        function detail($id=NULL,$idstposting){
            $id=$this->uri->segment(4);
            $data['data']=$this->m_website_unlaacid->getByid('idhalaman',$id,'v_halaman');
            $look=0;
			$message = 'Maaf, Anda Bukan Member';
			
            if($data['data']!=''){
                $look = $this->jumlah($id,'dilihat', 'halaman');
                if($look == null){
                    $look = 1;
                }else{
                    $look = $look +1;
                }
                    
                $q = "update halaman set dilihat=".$look." where idhalaman = ".$id;
                $this->db->query($q);
            }


			
			if((!$this->my_usessionpmb->logged_in)&&($message!='')&&($idstposting != 1)){
				//echo '<script> alert("'.str_replace(array("\r","\n"), '', $message).'"); </script>';
				redirect("index.php/web/content/memberonline");
			}
			
			
		
		
		
		if(($this->my_usessionpmb->logged_in) || ($idstposting == 1) || ($idstposting == 2))	
		{

            $jml = $this->db->get('v_beritanpengumuman');
            $config['base_url'] = base_url().'index.php/web/content/index';
            $config['total_rows'] = $jml->num_rows();
            $config['per_page'] = '5';
            $config['per_pagehead'] = '1';
            $config['per_page_pop'] = '3';
            $config['per_page_footerpost'] = '5';
			$config['per_page_testi'] = '1';
            $config['footerpost'] = '5';
			$config['num_links'] = '5';
			$config['uri_segment'] = 4;
			$config['page_poster'] = '1';
            $config['berita_foot'] = '2';
            $config['service_page'] = '20';

            $this->pagination->initialize($config);

            $data['beritalainnya']=  $this->m_website_unlaacid->getWithoutid('idhalaman',$id,'v_beritanpengumuman');
            $data['postpopular'] = $this->m_website_unlaacid->getfPage('v_popularpost',$config['per_page_pop'], NULL);
            $data['postrecent'] = $this->m_website_unlaacid->getfPage('v_berita',$config['per_page_pop'], NULL);
            $data['queryhead'] = $this->m_website_unlaacid->getfPage('v_beritanpengumuman',$config['per_pagehead'], NULL);
            $data['agendalainnya']=  $this->m_website_unlaacid->getWithoutid('idhalaman',$id,'v_agenda');
			$data['menuatas']=$this->m_website_unlaacid->getByid('menuatas','1','menuweb');
			$data['databank'] = $this->m_website_unlaacid->getAll('rekbank','','');
			$data['gbrposter'] = $this->m_website_unlaacid->getfPage('v_poster',$config['page_poster'], NULL);
			$data['footerpost'] = $this->m_website_unlaacid->getfPage('v_popularpost',$config['per_page_footerpost'], NULL);
            $data['datasponsor'] = $this->m_website_unlaacid->getAllkerjasama('kerjasama','','');
            //$data['dataharga'] = $this->m_website_unlaacid->getAll('v_det_hewan','','');
            $data['datakegiatan']=  $this->m_website_unlaacid->getAll('kegiatan','','');
            $data['datatestimoni'] = $this->m_website_unlaacid->getfPage('v_testimoni',$config['per_page_testi'], NULL);
            $data['berita_foot'] = $this->m_website_unlaacid->getfPage('v_berita',$config['berita_foot'], NULL);
            $data['menu_tea'] = $this->m_website_unlaacid->getfPage('v_service',$config['service_page'], NULL);
			
            $this->load->view('web/detail-halaman',$data);
		}
    }


    function detail_blog($id=NULL,$idstposting){
            $id=$this->uri->segment(4);
            $data['data']=$this->m_website_unlaacid->getByid('idhalaman',$id,'v_halaman');
            $look=0;
            $message = 'Maaf, Anda Bukan Member';
            
            if($data['data']!=''){
                $look = $this->jumlah($id,'dilihat', 'halaman');
                if($look == null){
                    $look = 1;
                }else{
                    $look = $look +1;
                }
                    
                $q = "update halaman set dilihat=".$look." where idhalaman = ".$id;
                $this->db->query($q);
            }


            
            if((!$this->my_usessionpmb->logged_in)&&($message!='')&&($idstposting != 1)){
                //echo '<script> alert("'.str_replace(array("\r","\n"), '', $message).'"); </script>';
                redirect("index.php/web/content/memberonline");
            }
            
            
        
        
        
        if(($this->my_usessionpmb->logged_in) || ($idstposting == 1) || ($idstposting == 2))    
        {

            $jml = $this->db->get('v_beritanpengumuman');
            $config['base_url'] = base_url().'index.php/web/content/index';
            $config['total_rows'] = $jml->num_rows();
            $config['per_page'] = '5';
            $config['per_pagehead'] = '1';
            $config['per_page_pop'] = '3';
            $config['per_page_footerpost'] = '5';
            $config['per_page_testi'] = '1';
            $config['footerpost'] = '5';
            $config['num_links'] = '5';
            $config['uri_segment'] = 4;
            $config['page_poster'] = '1';
            $config['berita_foot'] = '2';
            $config['service_page'] = '20';

            $this->pagination->initialize($config);

            $data['beritalainnya']=  $this->m_website_unlaacid->getWithoutid('idhalaman',$id,'v_beritanpengumuman');
            $data['postpopular'] = $this->m_website_unlaacid->getfPage('v_popularpost',$config['per_page_pop'], NULL);
            $data['postrecent'] = $this->m_website_unlaacid->getfPage('v_berita',$config['per_page_pop'], NULL);
            $data['queryhead'] = $this->m_website_unlaacid->getfPage('v_beritanpengumuman',$config['per_pagehead'], NULL);
            $data['agendalainnya']=  $this->m_website_unlaacid->getWithoutid('idhalaman',$id,'v_agenda');
            $data['menuatas']=$this->m_website_unlaacid->getByid('menuatas','1','menuweb');
            $data['databank'] = $this->m_website_unlaacid->getAll('rekbank','','');
            $data['gbrposter'] = $this->m_website_unlaacid->getfPage('v_poster',$config['page_poster'], NULL);
            $data['footerpost'] = $this->m_website_unlaacid->getfPage('v_popularpost',$config['per_page_footerpost'], NULL);
            $data['datasponsor'] = $this->m_website_unlaacid->getAllkerjasama('kerjasama','','');
            //$data['dataharga'] = $this->m_website_unlaacid->getAll('v_det_hewan','','');
            $data['datakegiatan']=  $this->m_website_unlaacid->getAll('kegiatan','','');
            $data['datatestimoni'] = $this->m_website_unlaacid->getfPage('v_testimoni',$config['per_page_testi'], NULL);
            $data['berita_foot'] = $this->m_website_unlaacid->getfPage('v_berita',$config['berita_foot'], NULL);
            $data['menu_tea'] = $this->m_website_unlaacid->getfPage('v_service',$config['service_page'], NULL);
            $data['menu_maincourse'] = $this->m_website_unlaacid->getfPage('v_project',$config['service_page'], NULL);
            $data['berita'] = $this->m_website_unlaacid->getfPage('v_berita',$config['footerpost'], NULL);
            
            $this->load->view('web/detail-halaman-blog',$data);
        }
    }

		
		//PROFIL
		
		function profil($id=NULL)
		{       
  
            $id=$this->uri->segment(4);
            $data['data']=$this->m_website_unlaacid->getByid('idhalaman',$id,'v_halaman');
            $look=0;
            if($data['data']!=''){
                $look = $this->jumlah($id,'dilihat', 'halaman');
                if($look == null){
                    $look = 1;
                }else{
                    $look = $look +1;
                }
                    
                $q = "update halaman set dilihat=".$look." where idhalaman = ".$id;
                $this->db->query($q);
            }

            	 //ARTIKEL FOOTER
           		 $jml = $this->db->get('v_beritanpengumuman');
           		 $config['base_url'] = base_url().'index.php/web/content/index';
                 $config['total_rows'] = $jml->num_rows();
                 $config['per_page'] = '5';
                 $config['per_page_pop'] = '5';
                 $config['per_pagehead'] = '1';
                 $config['per_page_footerpost'] = '5';
				 $config['per_page_testi'] = '1';
				 $config['num_links'] = '5';
				 $config['uri_segment'] = 4;
				 $config['page_poster'] = '1';

				 //inisialisasi config
                 $this->pagination->initialize($config);

                //buat pagination
                 $data['halaman'] = $this->pagination->create_links();
                
				//tamplikan data
                $data['postpopular'] = $this->m_website_unlaacid->getfPage('v_popularpost',$config['per_page_pop'], NULL);
            	$data['postrecent'] = $this->m_website_unlaacid->getfPage('v_recentpost',$config['per_page_pop'], NULL);
                $data['queryhead'] = $this->m_website_unlaacid->getfPage('v_beritanpengumuman',$config['per_pagehead'], $id);
                $data['footerpost'] = $this->m_website_unlaacid->getfPage('v_popularpost',$config['per_page_footerpost'], $id);
                $data['datasponsor'] = $this->m_website_unlaacid->getAllkerjasama('kerjasama','','');
                //$data['dataharga'] = $this->m_website_unlaacid->getAll('v_det_hewan','','');
                $data['datakegiatan']=  $this->m_website_unlaacid->getAll('kegiatan','','');
				$data['datatestimoni'] = $this->m_website_unlaacid->getfPage('v_testimoni',$config['per_page_testi'], NULL);
                 //END ARTIKEL FOOTER

                 $data['databank'] = $this->m_website_unlaacid->getAll('rekbank','','');
                 $data['gbrposter'] = $this->m_website_unlaacid->getfPage('v_poster',$config['page_poster'], NULL);
				
			
			$data['menuatas']=$this->m_website_unlaacid->getByid('menuatas','1','menuweb');	

            $this->load->view('web/profil',$data);
				
		}
	
	//PROFIL
		
		function list_anggota($id=NULL)
		{       
  
            $id=$this->uri->segment(4);
            $data['data']=$this->m_website_unlaacid->getByid('idhalaman',$id,'v_halaman');
            $look=0;
            if($data['data']!=''){
                $look = $this->jumlah($id,'dilihat', 'halaman');
                if($look == null){
                    $look = 1;
                }else{
                    $look = $look +1;
                }
                    
                $q = "update halaman set dilihat=".$look." where idhalaman = ".$id;
                $this->db->query($q);
            }

            	 //ARTIKEL FOOTER
           		 $jml = $this->db->get('v_beritanpengumuman');
           		 $config['base_url'] = base_url().'index.php/web/content/index';
                 $config['total_rows'] = $jml->num_rows();
                 $config['per_page'] = '5';
                 $config['per_page_pop'] = '5';
                 $config['per_pagehead'] = '1';
                 $config['per_page_footerpost'] = '5';
				 $config['per_page_testi'] = '1';
				 $config['num_links'] = '5';
				 $config['uri_segment'] = 4;
				 $config['page_poster'] = '1';

				 //inisialisasi config
                 $this->pagination->initialize($config);

                //buat pagination
                 $data['halaman'] = $this->pagination->create_links();
                
				//tamplikan data
                $data['postpopular'] = $this->m_website_unlaacid->getfPage('v_popularpost',$config['per_page_pop'], NULL);
            	$data['postrecent'] = $this->m_website_unlaacid->getfPage('v_recentpost',$config['per_page_pop'], NULL);
                $data['queryhead'] = $this->m_website_unlaacid->getfPage('v_beritanpengumuman',$config['per_pagehead'], $id);
                $data['footerpost'] = $this->m_website_unlaacid->getfPage('v_popularpost',$config['per_page_footerpost'], $id);
                $data['datasponsor'] = $this->m_website_unlaacid->getAllkerjasama('kerjasama','','');
                //$data['dataharga'] = $this->m_website_unlaacid->getAll('v_det_hewan','','');
                $data['datakegiatan']=  $this->m_website_unlaacid->getAll('kegiatan','','');
				$data['datatestimoni'] = $this->m_website_unlaacid->getfPage('v_testimoni',$config['per_page_testi'], $id);
                 //END ARTIKEL FOOTER

                 $data['databank'] = $this->m_website_unlaacid->getAll('rekbank','','');
                 $data['gbrposter'] = $this->m_website_unlaacid->getfPage('v_poster',$config['page_poster'], NULL);
				
			
			$data['menuatas']=$this->m_website_unlaacid->getByid('menuatas','1','menuweb');	

            $this->load->view('web/daftar-anggota',$data);
				
		}


	function indeasdasdx($id=NULL)
	{       
                 $jml = $this->db->get('v_beritanpengumuman');

                 //pengaturan pagination
                 $config['base_url'] = base_url().'index.php/web/content/index';
                 $config['total_rows'] = $jml->num_rows();
                 $config['per_page'] = '5';
				 $config['num_links'] = '5';
				 $config['uri_segment'] = 4;
                /*  $config['first_page'] = 'Awal';
                 $config['last_page'] = 'Akhir';
                 $config['next_page'] = '&laquo;';
                 $config['prev_page'] = '&raquo;'; */
	
                //inisialisasi config
                 $this->pagination->initialize($config);

                //buat pagination
                $data['halaman'] = $this->pagination->create_links();

				//slider
				 $data['slider']=$this->m_website_unlaacid->getSelectByid('file','slideratas',1,'galeri');
                
				//tamplikan data
                 $data['query'] = $this->m_website_unlaacid->getfPage('v_beritanpengumuman',$config['per_page'], $id);

                //$data['bnp']=  $this->m_website_unlaacid->getAll('v_beritanpengumuman','tglpublish','DESC');
                $data['agenda'] =$this->m_website_unlaacid->getAll('v_agenda','','');
				
				$data['tautan']=$this->m_website_unlaacid->getByid('idstpublish','1','tautan');
				$data['menuatas']=$this->m_website_unlaacid->getByid('menuatas','1','menuweb');
				$data['menukanan']=$this->m_website_unlaacid->getByid('menukanan','1','menuweb');
				$data['kerjasama']=$this->m_website_unlaacid->getByid('idmenuweb','33','menuweb');
				
				$temp = base_url();
				$slash='';
				for ($i=0; $i < strlen($temp); $i++)  { 
					if ($temp[$i]=='/') {
						$slash .= $temp[$i]; // fungsi menghilangkan url website ini dengan menambahkan karakter slash
					}
				}
				
				$data['addslash'] = $slash;
				$this->load->view('web/index-web',$data);
		
	}



		
		function galeri($id=NULL){  

				$jml = $this->db->get('v_galeri');

                 //pengaturan pagination
                 $config['base_url'] = base_url().'index.php/web/content/galeri';
                 $config['total_rows'] = $jml->num_rows();
                 $config['per_page'] = '20';
                 $config['per_pagehead'] = '1';
				 $config['per_page_testi'] = '1';
				 $config['num_links'] = '5';
				 $config['uri_segment'] = 4;
               	 
	
                //inisialisasi config
                 $this->pagination->initialize($config);

                //buat pagination
                $data['halaman'] = $this->pagination->create_links();
				$data['queryhead'] = $this->m_website_unlaacid->getfPage('v_beritanpengumuman',$config['per_pagehead'], $id);
				$data['datax']=  $this->m_website_unlaacid->getfPage('v_galeri',$config['per_page'], $id);
				$data['menuatas']=$this->m_website_unlaacid->getByid('menuatas','1','menuweb');


			     //ARTIKEL FOOTER
           		 $jml = $this->db->get('v_beritanpengumuman');
           		 $config['base_url'] = base_url().'index.php/web/content/index';
                 $config['total_rows'] = $jml->num_rows();
                 $config['per_page'] = '5';
                 $config['per_page_footerpost'] = '5';
				 $config['per_page_pop'] = '5';
				 $config['num_links'] = '5';
				 $config['uri_segment'] = 4;
				 $config['page_poster'] = '1';

				 //inisialisasi config
                 $this->pagination->initialize($config);

                //buat pagination
                 $data['halaman'] = $this->pagination->create_links();
                
				//tamplikan data
				$data['postpopular'] = $this->m_website_unlaacid->getfPage('v_popularpost',$config['per_page_pop'], NULL);
                $data['footerpost'] = $this->m_website_unlaacid->getfPage('v_popularpost',$config['per_page_footerpost'], $id);
                $data['databank'] = $this->m_website_unlaacid->getfPage('rekbank',$config['per_page'], $id);
                $data['gbrposter'] = $this->m_website_unlaacid->getfPage('v_poster',$config['page_poster'], $id);
                $data['datasponsor'] = $this->m_website_unlaacid->getAllkerjasama('kerjasama','','');
                //$data['dataharga'] = $this->m_website_unlaacid->getAll('v_det_hewan','','');
                $data['datakegiatan']=  $this->m_website_unlaacid->getAll('kegiatan','','');
				$data['datatestimoni'] = $this->m_website_unlaacid->getfPage('v_testimoni',$config['per_page_testi'], $id);
				//END ARTIKEL FOOTER
			
            $this->load->view('web/galeri/halaman-galeri', $data);
        }
		
		function detail_galeri(){
			$id=$this->uri->segment(4);

			$data['gbr']=  $this->m_website_unlaacid->getByid('idklpgaleri', $id,'v_galeri_list');
			$data['nmgaleri']=  $this->m_website_unlaacid->getByid('idklpgaleri',$id,'klpgaleri');      			
			$data['menuatas']=$this->m_website_unlaacid->getByid('menuatas','1','menuweb');

			//ARTIKEL FOOTER
           		 $jml = $this->db->get('v_beritanpengumuman');
           		 $config['base_url'] = base_url().'index.php/web/content/index';
                 $config['total_rows'] = $jml->num_rows();
                 $config['per_page'] = '5';
                 $config['per_pagehead'] = '1';
                 $config['per_page_footerpost'] = '5';
				 $config['per_page_pop'] = '5';
				 $config['per_page_testi'] = '1';
				 $config['num_links'] = '5';
				 $config['uri_segment'] = 4;
				 $config['page_poster'] = '1';

				//inisialisasi config
                $this->pagination->initialize($config);

                //buat pagination
                 $data['halaman'] = $this->pagination->create_links();
                
				//tamplikan data
                $data['footerpost'] = $this->m_website_unlaacid->getfPage('v_popularpost',$config['per_page_footerpost'], $id);
                $data['queryhead'] = $this->m_website_unlaacid->getfPage('v_beritanpengumuman',$config['per_pagehead'], $id);
				$data['postpopular'] = $this->m_website_unlaacid->getfPage('v_popularpost',$config['per_page_pop'], NULL);
                $data['datax']=  $this->m_website_unlaacid->getfPage('v_galeri',$config['per_page'], NULL);
                $data['datasponsor'] = $this->m_website_unlaacid->getAllkerjasama('kerjasama','','');
                //$data['dataharga'] = $this->m_website_unlaacid->getAll('v_det_hewan','','');
                $data['datakegiatan']=  $this->m_website_unlaacid->getAll('kegiatan','','');
				$data['datatestimoni'] = $this->m_website_unlaacid->getfPage('v_testimoni',$config['per_page_testi'], $id);
				//END ARTIKEL FOOTER

                 $data['databank'] = $this->m_website_unlaacid->getAll('rekbank','','');
                 $data['gbrposter'] = $this->m_website_unlaacid->getfPage('v_poster',$config['page_poster'], $id);

            $this->load->view('web/galeri/detail-halaman-galeri', $data);
        }
		
		function hubungi_kami($cekcap=NULL,$id=NULL){    
				$data['datax']=$this->m_website_unlaacid->getAll('hubkamiinfo','','');
				$data['menuatas']=$this->m_website_unlaacid->getByid('menuatas','1','menuweb');

				$config['per_page_footerpost'] = '5';
				$config['per_page_pop'] = '5';
				$config['per_page_testi'] = '1';
				$config['per_pagehead'] = '1';
				$config['page_poster'] = '1';
                $config['berita_foot'] = '2';

				$data['queryhead'] = $this->m_website_unlaacid->getfPage('v_beritanpengumuman',$config['per_pagehead'], $id);
				$data['postpopular'] = $this->m_website_unlaacid->getfPage('v_popularpost',$config['per_page_pop'], NULL);
            	$data['postrecent'] = $this->m_website_unlaacid->getfPage('v_recentpost',$config['per_page_pop'], NULL);
				$data['footerpost'] = $this->m_website_unlaacid->getfPage('v_popularpost',$config['per_page_footerpost'], $id);
                $data['datasponsor'] = $this->m_website_unlaacid->getAllkerjasama('kerjasama','','');
                //$data['dataharga'] = $this->m_website_unlaacid->getAll('v_det_hewan','','');
                $data['datakegiatan']=  $this->m_website_unlaacid->getAll('kegiatan','','');
				$data['datatestimoni'] = $this->m_website_unlaacid->getfPage('v_testimoni',$config['per_page_testi'], $id);
                $data['databank'] = $this->m_website_unlaacid->getAll('rekbank','','');
                $data['gbrposter'] = $this->m_website_unlaacid->getfPage('v_poster',$config['page_poster'], $id);
				$data['cap_img']=$this->Mediatutorialcaptcha->make_captcha();
				$data['cekcaptcha']=$cekcap;
                $data['berita_foot'] = $this->m_website_unlaacid->getfPage('v_berita',$config['berita_foot'], $id);

				$this->load->view('web/hubungi-kami/detail-halaman-hubungi-kami',$data);
        }

        function aboutus($cekcap=NULL,$id=NULL){    
                $data['datax']=$this->m_website_unlaacid->getAll('hubkamiinfo','','');
                $data['menuatas']=$this->m_website_unlaacid->getByid('menuatas','1','menuweb');

                $config['per_page_footerpost'] = '5';
                $config['per_page_pop'] = '5';
                $config['per_page_testi'] = '5';
                $config['per_pagehead'] = '5';
                $config['page_poster'] = '5';
                $config['berita_foot'] = '2';

                $data['queryhead'] = $this->m_website_unlaacid->getfPage('v_beritanpengumuman',$config['per_pagehead'], $id);
                $data['profil'] = $this->m_website_unlaacid->getfPage('v_post_profil',$config['per_pagehead'], $id);
                $data['postpopular'] = $this->m_website_unlaacid->getfPage('v_popularpost',$config['per_page_pop'], NULL);
                $data['postrecent'] = $this->m_website_unlaacid->getfPage('v_recentpost',$config['per_page_pop'], NULL);
                $data['footerpost'] = $this->m_website_unlaacid->getfPage('v_popularpost',$config['per_page_footerpost'], $id);
                $data['datasponsor'] = $this->m_website_unlaacid->getAllkerjasama('kerjasama','','');
                //$data['dataharga'] = $this->m_website_unlaacid->getAll('v_det_hewan','','');
                $data['datakegiatan']=  $this->m_website_unlaacid->getAll('kegiatan','','');
                $data['datatestimoni'] = $this->m_website_unlaacid->getfPage('v_testimoni',$config['per_page_testi'], $id);
                $data['databank'] = $this->m_website_unlaacid->getAll('rekbank','','');
                $data['gbrposter'] = $this->m_website_unlaacid->getfPage('v_poster',$config['page_poster'], $id);
                $data['cap_img']=$this->Mediatutorialcaptcha->make_captcha();
                $data['cekcaptcha']=$cekcap;
                $data['berita_foot'] = $this->m_website_unlaacid->getfPage('v_berita',$config['berita_foot'], $id);

                $this->load->view('web/profil',$data);
        }

        function reservation($cekcap=NULL,$id=NULL){    
                $data['datax']=$this->m_website_unlaacid->getAll('hubkamiinfo','','');
                $data['menuatas']=$this->m_website_unlaacid->getByid('menuatas','1','menuweb');

                $config['per_page_footerpost'] = '5';
                $config['per_page'] = '9';
                $config['per_page_pop'] = '5';
                $config['per_page_testi'] = '1';
                $config['per_pagehead'] = '1';
                $config['page_poster'] = '1';

                $data['query'] = $this->m_website_unlaacid->getfPage('v_kamar',$config['per_page'], $id);
                $data['meeting'] = $this->m_website_unlaacid->getfPage('v_meeting',$config['per_page'], $id);
                $data['package'] = $this->m_website_unlaacid->getfPage('v_package',$config['per_page'], $id);
                $data['queryhead'] = $this->m_website_unlaacid->getfPage('v_beritanpengumuman',$config['per_pagehead'], $id);
                $data['postpopular'] = $this->m_website_unlaacid->getfPage('v_popularpost',$config['per_page_pop'], NULL);
                $data['postrecent'] = $this->m_website_unlaacid->getfPage('v_recentpost',$config['per_page_pop'], NULL);
                $data['footerpost'] = $this->m_website_unlaacid->getfPage('v_popularpost',$config['per_page_footerpost'], $id);
                $data['datasponsor'] = $this->m_website_unlaacid->getAllkerjasama('kerjasama','','');
                //$data['dataharga'] = $this->m_website_unlaacid->getAll('v_det_hewan','','');
                $data['datakegiatan']=  $this->m_website_unlaacid->getAll('kegiatan','','');
                $data['datatestimoni'] = $this->m_website_unlaacid->getfPage('v_testimoni',$config['per_page_testi'], $id);
                $data['databank'] = $this->m_website_unlaacid->getAll('rekbank','','');
                $data['gbrposter'] = $this->m_website_unlaacid->getfPage('v_poster',$config['page_poster'], $id);
                $data['cap_img']=$this->Mediatutorialcaptcha->make_captcha();
                $data['cekcaptcha']=$cekcap;
                $this->load->view('web/reservasi',$data);
        }

        function blog($cekcap=NULL,$id=NULL){    
                $data['datax']=$this->m_website_unlaacid->getAll('hubkamiinfo','','');
                $data['menuatas']=$this->m_website_unlaacid->getByid('menuatas','1','menuweb');

                $config['per_page_footerpost'] = '5';
                $config['per_page'] = '9';
                $config['per_page_pop'] = '5';
                $config['per_page_testi'] = '1';
                $config['per_pagehead'] = '50';
                $config['page_poster'] = '1';
                $config['berita_foot'] = '2';

                $this->pagination->initialize($config);

                //buat pagination
                $data['halaman'] = $this->pagination->create_links();

                //$data['query'] = $this->m_website_unlaacid->getfPage('v_kamar',$config['per_page'], $id);
                //$data['meeting'] = $this->m_website_unlaacid->getfPage('v_meeting',$config['per_page'], $id);
                //$data['package'] = $this->m_website_unlaacid->getfPage('v_package',$config['per_page'], $id);
                $data['berita'] = $this->m_website_unlaacid->getfPage('v_berita',$config['per_pagehead'], $id);
                $data['queryhead'] = $this->m_website_unlaacid->getfPage('v_beritanpengumuman',$config['per_pagehead'], $id);
                $data['postpopular'] = $this->m_website_unlaacid->getfPage('v_popularpost',$config['per_page_pop'], NULL);
                $data['postrecent'] = $this->m_website_unlaacid->getfPage('v_recentpost',$config['per_page_pop'], NULL);
                $data['footerpost'] = $this->m_website_unlaacid->getfPage('v_popularpost',$config['per_page_footerpost'], $id);
                $data['datasponsor'] = $this->m_website_unlaacid->getAllkerjasama('kerjasama','','');
                //$data['dataharga'] = $this->m_website_unlaacid->getAll('v_det_hewan','','');
                $data['datakegiatan']=  $this->m_website_unlaacid->getAll('kegiatan','','');
                $data['datatestimoni'] = $this->m_website_unlaacid->getfPage('v_testimoni',$config['per_page_testi'], $id);
                $data['databank'] = $this->m_website_unlaacid->getAll('rekbank','','');
                $data['gbrposter'] = $this->m_website_unlaacid->getfPage('v_poster',$config['page_poster'], $id);
                $data['cap_img']=$this->Mediatutorialcaptcha->make_captcha();
                $data['cekcaptcha']=$cekcap;
                $data['berita_foot'] = $this->m_website_unlaacid->getfPage('v_berita',$config['berita_foot'], $id);

                $this->load->view('web/blog',$data);
        }

        function service($cekcap=NULL,$id=NULL){    
                $data['datax']=$this->m_website_unlaacid->getAll('hubkamiinfo','','');
                $data['menuatas']=$this->m_website_unlaacid->getByid('menuatas','1','menuweb');

                $config['per_page_footerpost'] = '5';
                $config['per_page'] = '50';
                $config['per_page_pop'] = '5';
                $config['per_page_testi'] = '1';
                $config['per_pagehead'] = '3';
                $config['page_poster'] = '1';
                $config['berita_foot'] = '2';

                //$data['query'] = $this->m_website_unlaacid->getfPage('v_kamar',$config['per_page'], $id);
                //$data['meeting'] = $this->m_website_unlaacid->getfPage('v_meeting',$config['per_page'], $id);
                //$data['package'] = $this->m_website_unlaacid->getfPage('v_package',$config['per_page'], $id);
                $data['service'] = $this->m_website_unlaacid->getfPage('v_service',$config['per_page'], $id);
                $data['menu_maincourse'] = $this->m_website_unlaacid->getfPage('v_project',$config['per_page'], $id);
                $data['menu_meals'] = $this->m_website_unlaacid->getfPage('v_loker',$config['per_page'], $id);
                $data['menu_cookies'] = $this->m_website_unlaacid->getfPage('v_menu_cookies',$config['per_page'], $id);
                $data['menu_desserts'] = $this->m_website_unlaacid->getfPage('v_menu_desserts',$config['per_page'], $id);
                $data['berita'] = $this->m_website_unlaacid->getfPage('v_berita',$config['per_pagehead'], $id);
                $data['queryhead'] = $this->m_website_unlaacid->getfPage('v_beritanpengumuman',$config['per_pagehead'], $id);
                $data['postpopular'] = $this->m_website_unlaacid->getfPage('v_popularpost',$config['per_page_pop'], NULL);
                $data['postrecent'] = $this->m_website_unlaacid->getfPage('v_recentpost',$config['per_page_pop'], NULL);
                $data['footerpost'] = $this->m_website_unlaacid->getfPage('v_popularpost',$config['per_page_footerpost'], $id);
                $data['datasponsor'] = $this->m_website_unlaacid->getAllkerjasama('kerjasama','','');
                //$data['dataharga'] = $this->m_website_unlaacid->getAll('v_det_hewan','','');
                $data['datakegiatan']=  $this->m_website_unlaacid->getAll('kegiatan','','');
                $data['datatestimoni'] = $this->m_website_unlaacid->getfPage('v_testimoni',$config['per_page_testi'], $id);
                $data['databank'] = $this->m_website_unlaacid->getAll('rekbank','','');
                $data['gbrposter'] = $this->m_website_unlaacid->getfPage('v_poster',$config['page_poster'], $id);
                $data['cap_img']=$this->Mediatutorialcaptcha->make_captcha();
                $data['cekcaptcha']=$cekcap;
                $data['berita_foot'] = $this->m_website_unlaacid->getfPage('v_berita',$config['berita_foot'], $id);

                $this->load->view('web/service',$data);
        }

        function product($cekcap=NULL,$id=NULL){    
                $data['datax']=$this->m_website_unlaacid->getAll('hubkamiinfo','','');
                $data['menuatas']=$this->m_website_unlaacid->getByid('menuatas','1','menuweb');

                $config['per_page_footerpost'] = '5';
                $config['per_page'] = '50';
                $config['per_page_pop'] = '5';
                $config['per_page_testi'] = '1';
                $config['per_pagehead'] = '3';
                $config['page_poster'] = '1';
                $config['berita_foot'] = '2';

                //$data['query'] = $this->m_website_unlaacid->getfPage('v_kamar',$config['per_page'], $id);
                //$data['meeting'] = $this->m_website_unlaacid->getfPage('v_meeting',$config['per_page'], $id);
                //$data['package'] = $this->m_website_unlaacid->getfPage('v_package',$config['per_page'], $id);
                $data['product'] = $this->m_website_unlaacid->getfPage('v_product',$config['per_page'], $id);
                $data['menu_maincourse'] = $this->m_website_unlaacid->getfPage('v_project',$config['per_page'], $id);
                $data['menu_meals'] = $this->m_website_unlaacid->getfPage('v_loker',$config['per_page'], $id);
                $data['menu_cookies'] = $this->m_website_unlaacid->getfPage('v_menu_cookies',$config['per_page'], $id);
                $data['menu_desserts'] = $this->m_website_unlaacid->getfPage('v_menu_desserts',$config['per_page'], $id);
                $data['berita'] = $this->m_website_unlaacid->getfPage('v_berita',$config['per_pagehead'], $id);
                $data['queryhead'] = $this->m_website_unlaacid->getfPage('v_beritanpengumuman',$config['per_pagehead'], $id);
                $data['postpopular'] = $this->m_website_unlaacid->getfPage('v_popularpost',$config['per_page_pop'], NULL);
                $data['postrecent'] = $this->m_website_unlaacid->getfPage('v_recentpost',$config['per_page_pop'], NULL);
                $data['footerpost'] = $this->m_website_unlaacid->getfPage('v_popularpost',$config['per_page_footerpost'], $id);
                $data['datasponsor'] = $this->m_website_unlaacid->getAllkerjasama('kerjasama','','');
                //$data['dataharga'] = $this->m_website_unlaacid->getAll('v_det_hewan','','');
                $data['datakegiatan']=  $this->m_website_unlaacid->getAll('kegiatan','','');
                $data['datatestimoni'] = $this->m_website_unlaacid->getfPage('v_testimoni',$config['per_page_testi'], $id);
                $data['databank'] = $this->m_website_unlaacid->getAll('rekbank','','');
                $data['gbrposter'] = $this->m_website_unlaacid->getfPage('v_poster',$config['page_poster'], $id);
                $data['cap_img']=$this->Mediatutorialcaptcha->make_captcha();
                $data['cekcaptcha']=$cekcap;
                $data['berita_foot'] = $this->m_website_unlaacid->getfPage('v_berita',$config['berita_foot'], $id);

                $this->load->view('web/product',$data);
        }

        function promo($cekcap=NULL,$id=NULL){    
                $data['datax']=$this->m_website_unlaacid->getAll('hubkamiinfo','','');
                $data['menuatas']=$this->m_website_unlaacid->getByid('menuatas','1','menuweb');

                $config['per_page_footerpost'] = '5';
                $config['per_page'] = '50';
                $config['per_page_pop'] = '5';
                $config['per_page_testi'] = '1';
                $config['per_pagehead'] = '3';
                $config['page_poster'] = '1';
                $config['berita_foot'] = '2';

                //$data['query'] = $this->m_website_unlaacid->getfPage('v_kamar',$config['per_page'], $id);
                //$data['meeting'] = $this->m_website_unlaacid->getfPage('v_meeting',$config['per_page'], $id);
                //$data['package'] = $this->m_website_unlaacid->getfPage('v_package',$config['per_page'], $id);
                $data['product'] = $this->m_website_unlaacid->getfPage('v_product',$config['per_page'], $id);
                $data['promo'] = $this->m_website_unlaacid->getfPage('v_promo',$config['per_page'], $id);
                $data['menu_meals'] = $this->m_website_unlaacid->getfPage('v_loker',$config['per_page'], $id);
                $data['menu_cookies'] = $this->m_website_unlaacid->getfPage('v_menu_cookies',$config['per_page'], $id);
                $data['menu_desserts'] = $this->m_website_unlaacid->getfPage('v_menu_desserts',$config['per_page'], $id);
                $data['berita'] = $this->m_website_unlaacid->getfPage('v_berita',$config['per_pagehead'], $id);
                $data['queryhead'] = $this->m_website_unlaacid->getfPage('v_beritanpengumuman',$config['per_pagehead'], $id);
                $data['postpopular'] = $this->m_website_unlaacid->getfPage('v_popularpost',$config['per_page_pop'], NULL);
                $data['postrecent'] = $this->m_website_unlaacid->getfPage('v_recentpost',$config['per_page_pop'], NULL);
                $data['footerpost'] = $this->m_website_unlaacid->getfPage('v_popularpost',$config['per_page_footerpost'], $id);
                $data['datasponsor'] = $this->m_website_unlaacid->getAllkerjasama('kerjasama','','');
                //$data['dataharga'] = $this->m_website_unlaacid->getAll('v_det_hewan','','');
                $data['datakegiatan']=  $this->m_website_unlaacid->getAll('kegiatan','','');
                $data['datatestimoni'] = $this->m_website_unlaacid->getfPage('v_testimoni',$config['per_page_testi'], $id);
                $data['databank'] = $this->m_website_unlaacid->getAll('rekbank','','');
                $data['gbrposter'] = $this->m_website_unlaacid->getfPage('v_poster',$config['page_poster'], $id);
                $data['cap_img']=$this->Mediatutorialcaptcha->make_captcha();
                $data['cekcaptcha']=$cekcap;
                $data['berita_foot'] = $this->m_website_unlaacid->getfPage('v_berita',$config['berita_foot'], $id);

                $this->load->view('web/promo',$data);
        }

        function project($cekcap=NULL,$id=NULL){    
                $data['datax']=$this->m_website_unlaacid->getAll('hubkamiinfo','','');
                $data['menuatas']=$this->m_website_unlaacid->getByid('menuatas','1','menuweb');

                $config['per_page_footerpost'] = '5';
                $config['per_page'] = '10';
                $config['per_page_pop'] = '5';
                $config['per_page_testi'] = '1';
                $config['per_pagehead'] = '3';
                $config['page_poster'] = '1';
                $config['berita_foot'] = '2';

                $this->pagination->initialize($config);

                //buat pagination
                $data['halaman'] = $this->pagination->create_links();

                //$data['query'] = $this->m_website_unlaacid->getfPage('v_kamar',$config['per_page'], $id);
                //$data['meeting'] = $this->m_website_unlaacid->getfPage('v_meeting',$config['per_page'], $id);
                //$data['package'] = $this->m_website_unlaacid->getfPage('v_package',$config['per_page'], $id);
                $data['menu_tea'] = $this->m_website_unlaacid->getfPage('v_service',$config['per_page'], $id);
                $data['menu_maincourse'] = $this->m_website_unlaacid->getfPage('v_project',$config['per_page'], $id);
                $data['menu_meals'] = $this->m_website_unlaacid->getfPage('v_loker',$config['per_page'], $id);
                $data['menu_cookies'] = $this->m_website_unlaacid->getfPage('v_menu_cookies',$config['per_page'], $id);
                $data['menu_desserts'] = $this->m_website_unlaacid->getfPage('v_menu_desserts',$config['per_page'], $id);
                $data['berita'] = $this->m_website_unlaacid->getfPage('v_berita',$config['per_pagehead'], $id);
                $data['queryhead'] = $this->m_website_unlaacid->getfPage('v_beritanpengumuman',$config['per_pagehead'], $id);
                $data['postpopular'] = $this->m_website_unlaacid->getfPage('v_popularpost',$config['per_page_pop'], NULL);
                $data['postrecent'] = $this->m_website_unlaacid->getfPage('v_recentpost',$config['per_page_pop'], NULL);
                $data['footerpost'] = $this->m_website_unlaacid->getfPage('v_popularpost',$config['per_page_footerpost'], $id);
                $data['datasponsor'] = $this->m_website_unlaacid->getAllkerjasama('kerjasama','','');
                //$data['dataharga'] = $this->m_website_unlaacid->getAll('v_det_hewan','','');
                $data['datakegiatan']=  $this->m_website_unlaacid->getAll('kegiatan','','');
                $data['datatestimoni'] = $this->m_website_unlaacid->getfPage('v_testimoni',$config['per_page_testi'], $id);
                $data['databank'] = $this->m_website_unlaacid->getAll('rekbank','','');
                $data['gbrposter'] = $this->m_website_unlaacid->getfPage('v_poster',$config['page_poster'], $id);
                $data['cap_img']=$this->Mediatutorialcaptcha->make_captcha();
                $data['cekcaptcha']=$cekcap;
                $data['berita_foot'] = $this->m_website_unlaacid->getfPage('v_berita',$config['berita_foot'], NULL);

                $this->load->view('web/project',$data);
        }

        function career($cekcap=NULL,$id=NULL){    
                $data['datax']=$this->m_website_unlaacid->getAll('hubkamiinfo','','');
                $data['menuatas']=$this->m_website_unlaacid->getByid('menuatas','1','menuweb');

                $config['per_page_footerpost'] = '5';
                $config['per_page'] = '10';
                $config['per_page_pop'] = '5';
                $config['per_page_testi'] = '1';
                $config['per_pagehead'] = '3';
                $config['page_poster'] = '1';
                $config['berita_foot'] = '2';

                $this->pagination->initialize($config);

                //buat pagination
                $data['halaman'] = $this->pagination->create_links();

                //$data['query'] = $this->m_website_unlaacid->getfPage('v_kamar',$config['per_page'], $id);
                //$data['meeting'] = $this->m_website_unlaacid->getfPage('v_meeting',$config['per_page'], $id);
                //$data['package'] = $this->m_website_unlaacid->getfPage('v_package',$config['per_page'], $id);
                $data['menu_tea'] = $this->m_website_unlaacid->getfPage('v_service',$config['per_page'], $id);
                $data['menu_maincourse'] = $this->m_website_unlaacid->getfPage('v_project',$config['per_page'], $id);
                $data['menu_loker'] = $this->m_website_unlaacid->getfPage('v_loker',$config['per_page'], $id);
                $data['menu_cookies'] = $this->m_website_unlaacid->getfPage('v_menu_cookies',$config['per_page'], $id);
                $data['menu_desserts'] = $this->m_website_unlaacid->getfPage('v_menu_desserts',$config['per_page'], $id);
                $data['berita'] = $this->m_website_unlaacid->getfPage('v_berita',$config['per_pagehead'], $id);
                $data['queryhead'] = $this->m_website_unlaacid->getfPage('v_beritanpengumuman',$config['per_pagehead'], $id);
                $data['postpopular'] = $this->m_website_unlaacid->getfPage('v_popularpost',$config['per_page_pop'], NULL);
                $data['postrecent'] = $this->m_website_unlaacid->getfPage('v_recentpost',$config['per_page_pop'], NULL);
                $data['footerpost'] = $this->m_website_unlaacid->getfPage('v_popularpost',$config['per_page_footerpost'], $id);
                $data['datasponsor'] = $this->m_website_unlaacid->getAllkerjasama('kerjasama','','');
                //$data['dataharga'] = $this->m_website_unlaacid->getAll('v_det_hewan','','');
                $data['datakegiatan']=  $this->m_website_unlaacid->getAll('kegiatan','','');
                $data['datatestimoni'] = $this->m_website_unlaacid->getfPage('v_testimoni',$config['per_page_testi'], $id);
                $data['databank'] = $this->m_website_unlaacid->getAll('rekbank','','');
                $data['gbrposter'] = $this->m_website_unlaacid->getfPage('v_poster',$config['page_poster'], $id);
                $data['cap_img']=$this->Mediatutorialcaptcha->make_captcha();
                $data['cekcaptcha']=$cekcap;
                $data['berita_foot'] = $this->m_website_unlaacid->getfPage('v_berita',$config['berita_foot'], NULL);

                $this->load->view('web/career',$data);
        }

        function reservation_detail($cekcap=NULL,$id=NULL){

            $id=$this->uri->segment(4);
            $data['data']=$this->m_website_unlaacid->getByid('idhalaman',$id,'v_halaman');
            $look=0;
            if($data['data']!=''){
                $look = $this->jumlah($id,'dilihat', 'halaman');
                if($look == null){
                    $look = 1;
                }else{
                    $look = $look +1;
                }
                    
                $q = "update halaman set dilihat=".$look." where idhalaman = ".$id;
                $this->db->query($q);
            }


            $jml = $this->db->get('v_beritanpengumuman');


                $data['datax']=$this->m_website_unlaacid->getAll('hubkamiinfo','','');
                $data['menuatas']=$this->m_website_unlaacid->getByid('menuatas','1','menuweb');

                $config['per_page_footerpost'] = '5';
                $config['per_page'] = '9';
                $config['per_page_pop'] = '5';
                $config['per_page_testi'] = '1';
                $config['per_pagehead'] = '1';
                $config['page_poster'] = '1';

                //$data['data']=$this->m_website_unlaacid->getByid('idhalaman',$id,'v_halaman');
                $data['query'] = $this->m_website_unlaacid->getfPage('v_kamar',$config['per_page'], $id);
                $data['meeting'] = $this->m_website_unlaacid->getfPage('v_meeting',$config['per_page'], $id);
                $data['package'] = $this->m_website_unlaacid->getfPage('v_package',$config['per_page'], $id);
                $data['queryhead'] = $this->m_website_unlaacid->getfPage('v_beritanpengumuman',$config['per_pagehead'], $id);
                $data['postpopular'] = $this->m_website_unlaacid->getfPage('v_popularpost',$config['per_page_pop'], NULL);
                $data['postrecent'] = $this->m_website_unlaacid->getfPage('v_recentpost',$config['per_page_pop'], NULL);
                $data['footerpost'] = $this->m_website_unlaacid->getfPage('v_popularpost',$config['per_page_footerpost'], $id);
                $data['datasponsor'] = $this->m_website_unlaacid->getAllkerjasama('kerjasama','','');
                //$data['dataharga'] = $this->m_website_unlaacid->getAll('v_det_hewan','','');
                $data['datakegiatan']=  $this->m_website_unlaacid->getAll('kegiatan','','');
                $data['datatestimoni'] = $this->m_website_unlaacid->getfPage('v_testimoni',$config['per_page_testi'], $id);
                $data['databank'] = $this->m_website_unlaacid->getAll('rekbank','','');
                $data['gbrposter'] = $this->m_website_unlaacid->getfPage('v_poster',$config['page_poster'], $id);
                $data['cap_img']=$this->Mediatutorialcaptcha->make_captcha();
                $data['cekcaptcha']=$cekcap;
                $this->load->view('web/reservasi-detail',$data);
        }

		
		function testimoni($cekcap=NULL,$id=NULL){   
				$id=$this->uri->segment(4);
				
		        $jml = $this->db->get('v_testimoni');
           		$config['base_url'] = base_url().'index.php/web/content/testimoni';
                //$config['base_url'] = base_url().'index.php/web/content/index';
                $config['total_rows'] = $jml->num_rows();
                $config['per_page'] = '5';
                $config['per_page_pop'] = '5';
                $config['per_pagehead'] = '1';
                $config['per_page_footerpost'] = '5';
                $config['per_page_testi'] = '10';
                $config['per_page_testi5'] = '5';
				$config['num_links'] = '5';
				$config['uri_segment'] = 4; 
				$config['page_poster'] = '1';

				$this->pagination->initialize($config);

				$data['datax']=  $this->m_website_unlaacid->getAll('hubkamiinfo','','');
				$data['postpopular'] = $this->m_website_unlaacid->getfPage('v_popularpost',$config['per_page_pop'], NULL);
            	$data['postrecent'] = $this->m_website_unlaacid->getfPage('v_recentpost',$config['per_page_pop'], NULL);
				$data['menuatas']=$this->m_website_unlaacid->getByid('menuatas','1','menuweb');
				$data['halaman'] = $this->pagination->create_links();
				$data['queryhead'] = $this->m_website_unlaacid->getfPage('v_beritanpengumuman',$config['per_pagehead'], $id);
				$data['footerpost'] = $this->m_website_unlaacid->getfPage('v_popularpost',$config['per_page_footerpost'], $id);
                $data['datasponsor'] = $this->m_website_unlaacid->getAllkerjasama('kerjasama','','');
                //$data['dataharga'] = $this->m_website_unlaacid->getAll('v_det_hewan','','');
                $data['datakegiatan']=  $this->m_website_unlaacid->getAll('kegiatan','','');
                $data['databank'] = $this->m_website_unlaacid->getAll('rekbank','','');
                $data['gbrposter'] = $this->m_website_unlaacid->getfPage('v_poster',$config['page_poster'], $id);
                $data['datatestimoni'] = $this->m_website_unlaacid->getfPage('v_testimoni',$config['per_page_testi'], $id);
                $data['datatestimoni5'] = $this->m_website_unlaacid->getfPage('v_testimoni',$config['per_page_testi5'], $id);

                $data['cap_img']=$this->Mediatutorialcaptcha->make_captcha();
				$data['cekcaptcha']=$cekcap;
                

                $this->load->view('web/testimoni',$data);
        }
		
		function memberlist($cekcap=NULL,$id=NULL){   
				$id=$this->uri->segment(4);
				
		        $jml = $this->db->get('v_testimoni');
           		$config['base_url'] = base_url().'index.php/web/content/memberlist';
                //$config['base_url'] = base_url().'index.php/web/content/index';
                $config['total_rows'] = $jml->num_rows();
                $config['per_page'] = '5';
                $config['per_page_pop'] = '5';
                $config['per_pagehead'] = '1';
                $config['per_page_footerpost'] = '5';
				$config['per_page_member'] = '50';
                $config['per_page_testi'] = '1';
				$config['num_links'] = '5';
				$config['uri_segment'] = 4; 
				$config['page_poster'] = '1';

				$this->pagination->initialize($config);
				
				$data['memberlist'] = $this->m_website_unlaacid->getfPage('v_memberlist',$config['per_page_member'], $id);
				$data['datax']=  $this->m_website_unlaacid->getAll('hubkamiinfo','','');
				$data['postpopular'] = $this->m_website_unlaacid->getfPage('v_popularpost',$config['per_page_pop'], NULL);
            	$data['postrecent'] = $this->m_website_unlaacid->getfPage('v_recentpost',$config['per_page_pop'], NULL);
				$data['menuatas']=$this->m_website_unlaacid->getByid('menuatas','1','menuweb');
				$data['halaman'] = $this->pagination->create_links();
				$data['queryhead'] = $this->m_website_unlaacid->getfPage('v_beritanpengumuman',$config['per_pagehead'], $id);
				$data['footerpost'] = $this->m_website_unlaacid->getfPage('v_popularpost',$config['per_page_footerpost'], $id);
                $data['datasponsor'] = $this->m_website_unlaacid->getAllkerjasama('kerjasama','','');
                $data['datakegiatan']=  $this->m_website_unlaacid->getAll('kegiatan','','');
                $data['databank'] = $this->m_website_unlaacid->getAll('rekbank','','');
                $data['gbrposter'] = $this->m_website_unlaacid->getfPage('v_poster',$config['page_poster'], NULL);
                $data['datatestimoni'] = $this->m_website_unlaacid->getfPage('v_testimoni',$config['per_page_testi'], $id);

                $data['cap_img']=$this->Mediatutorialcaptcha->make_captcha();
				$data['cekcaptcha']=$cekcap;
                

                $this->load->view('web/daftar-anggota',$data);
        }
		
        function daftar_anggota($cekcap=NULL,$id=NULL){   
				$id=$this->uri->segment(4);
				
		        $jml = $this->db->get('v_beritanpengumuman');
           		$config['base_url'] = base_url().'index.php/web/content/testimoni';
                //$config['base_url'] = base_url().'index.php/web/content/index';
                $config['total_rows'] = $jml->num_rows();
                $config['per_page'] = '5';
                $config['per_page_pop'] = '5';
                $config['per_page_poster'] = '1';
                $config['per_pagehead'] = '1';
                $config['per_page_footerpost'] = '5';
                $config['per_page_testi'] = '1';
				$config['num_links'] = '5';
				$config['uri_segment'] = 4; 
				$config['page_poster'] = '1';

				$this->pagination->initialize($config);

				$data['datax']=  $this->m_website_unlaacid->getAll('hubkamiinfo','','');
				$data['postpopular'] = $this->m_website_unlaacid->getfPage('v_popularpost',$config['per_page_pop'], NULL);
            	$data['postrecent'] = $this->m_website_unlaacid->getfPage('v_recentpost',$config['per_page_pop'], NULL);
				$data['menuatas']=$this->m_website_unlaacid->getByid('menuatas','1','menuweb');
				$data['halaman'] = $this->pagination->create_links();
				$data['queryhead'] = $this->m_website_unlaacid->getfPage('v_beritanpengumuman',$config['per_pagehead'], $id);
				$data['footerpost'] = $this->m_website_unlaacid->getfPage('v_popularpost',$config['per_page_footerpost'], $id);
                $data['datasponsor'] = $this->m_website_unlaacid->getAllkerjasama('kerjasama','','');
                //$data['dataharga'] = $this->m_website_unlaacid->getAll('v_det_hewan','','');
                $data['datakegiatan']=  $this->m_website_unlaacid->getAll('kegiatan','','');
                $data['databank'] = $this->m_website_unlaacid->getfPage('rekbank',$config['per_page_poster'], $id);
                $data['gbrposter'] = $this->m_website_unlaacid->getfPage('v_poster',$config['page_poster'], $id);
                $data['datatestimoni'] = $this->m_website_unlaacid->getfPage('v_testimoni',$config['per_page_testi'], $id);

                $data['cap_img']=$this->Mediatutorialcaptcha->make_captcha();
				$data['cekcaptcha']=$cekcap;
                

                $this->load->view('web/daftar-anggota',$data);
        }
		
        
        //======================================================================
        function jumlah($id,$column,$tbl){
        $q = "SELECT max(".$column.") as max FROM ".$tbl." where idhalaman=".$id ;
        $query  = $this->db->query($q);
        $max = ''; 
                    
        if ($query->num_rows() != 0)
        {
            $row = $query->row();
            $max=$row->max;
        }
        if ($max == null){
            $max=0;
        }
        return $max;
    }
	
	function jumlahjurnal($id,$column,$tbl){
        $q = "SELECT max(".$column.") as max FROM ".$tbl." where kdjurnal=".$id ;
        $query  = $this->db->query($q);
        $max = ''; 
                    
        if ($query->num_rows() != 0)
        {
            $row = $query->row();
            $max=$row->max;
        }
        if ($max == null){
            $max=0;
        }
        return $max;
    }
	
	public function logoutmember()
    {
        $this->my_usessionpmb->unset_userdata("nomember");
        $this->my_usessionpmb->unset_userdata('password');
		session_destroy();
		redirect("index.php/web/content/");
    }
	
	 function lowongan($id=NULL)
	{       
  
                $data['title']="Lowongan Pekerjaan";
				
				$jml = $this->db->get('v_lowongankerja');

                 //pengaturan pagination
                 $config['base_url'] = base_url().'index.php/web/content/lowongan';
                 $config['total_rows'] = $jml->num_rows();
                 $config['per_page'] = '9';
				 $config['num_links'] = '5';
				 $config['per_pagehead'] = '1';
				 $config['page_poster'] = '1';
                 $config['per_page_pop'] = '5';
				 $config['per_page_testi'] = '1';
				 $config['uri_segment'] = 4;
               
	
                //inisialisasi config
                 $this->pagination->initialize($config);

                //buat pagination
                $data['halaman'] = $this->pagination->create_links();
                $data['queryhead'] = $this->m_website_unlaacid->getfPage('v_beritanpengumuman',$config['per_pagehead'], $id);
                $data['gbrposter'] = $this->m_website_unlaacid->getfPage('v_poster',$config['page_poster'], $id);
                $data['postrecent'] = $this->m_website_unlaacid->getfPage('v_recentpost',$config['per_page_pop'], $id);
				$data['datax']=  $this->m_website_unlaacid->getfPage('v_lowongankerja',$config['per_page'], $id);
				$data['menuatas']=$this->m_website_unlaacid->getByid('menuatas','1','menuweb');
				$data['postpopular'] = $this->m_website_unlaacid->getfPage('v_popularpost',$config['per_page_pop'], $id);
		        $data['postrecent'] = $this->m_website_unlaacid->getfPage('v_recentpost',$config['per_page_pop'], $id);
		        $data['datatestimoni'] = $this->m_website_unlaacid->getfPage('v_testimoni',$config['per_page_testi'], $id);
			
           $this->load->view('web/lowongan',$data);
				
	}
	
	function ejurnal($id=NULL){  
		$jml = $this->db->get('v_jurnalpublish');

        //pengaturan pagination
        $config['base_url'] = base_url().'index.php/web/content/ejurnal';
        $config['total_rows'] = $jml->num_rows();
        $config['per_page'] = '5';
        $config['per_page_footerpost'] = '5';
		$config['num_links'] = '5';
		$config['per_pagehead'] = '1';
        $config['page_poster'] = '1';
        $config['per_page_pop'] = '5';
		$config['per_page_testi'] = '1';
		$config['uri_segment'] = 4;
        
        //inisialisasi config
        $this->pagination->initialize($config);

        //buat pagination
        $data['halaman'] = $this->pagination->create_links();
			
		//tamplikan data
		$data['query']=  $this->m_website_unlaacid->getfPage('v_jurnalpublish',$config['per_page'], $id);
		$data['menuatas']=$this->m_website_unlaacid->getByid('menuatas','1','menuweb');
        $data['datasponsor'] = $this->m_website_unlaacid->getAllkerjasama('kerjasama','','');
        //$data['dataharga'] = $this->m_website_unlaacid->getAll('v_det_hewan','','');
        $data['datakegiatan']=  $this->m_website_unlaacid->getAll('kegiatan','','');

                
		// ARTIKEL FOOTER
        $data['footerpost'] = $this->m_website_unlaacid->getfPage('v_popularpost',$config['per_page_footerpost'], $id);
		//END ARTIKEL FOOTER

        $data['queryhead'] = $this->m_website_unlaacid->getfPage('v_beritanpengumuman',$config['per_pagehead'], $id);
        $data['postpopular'] = $this->m_website_unlaacid->getfPage('v_popularpost',$config['per_page_pop'], $id);
        $data['postrecent'] = $this->m_website_unlaacid->getfPage('v_recentpost',$config['per_page_pop'], $id);
        $data['datatestimoni'] = $this->m_website_unlaacid->getfPage('v_testimoni',$config['per_page_testi'], $id);

        $this->load->view('web/e-jurnal/halaman-ejurnal', $data);
    }
		
	function ejurnalcari($id=NULL){
		$jml = $this->db->get('v_jurnalpublish');
		
        //pengaturan pagination
        $config['base_url'] = base_url().'index.php/web/content/ejurnal';
        $config['total_rows'] = $jml->num_rows();
        $config['per_page'] =  '5';
		$config['num_links'] = '5';
		$config['uri_segment'] = 4;
        
		//inisialisasi config
        $this->pagination->initialize($config);

        //buat pagination
        $data['halaman'] = $this->pagination->create_links();
		
		//tamplikan data
		$data['query']=$this->m_website_unlaacid->caridata();
		$data['menuatas']=$this->m_website_unlaacid->getByid('menuatas','1','menuweb');
		
		if($data['query']==null) {
			$this->load->view('web/e-jurnal/hasil-cari-jurnal', $data);
        }else {
            $this->load->view('web/e-jurnal/halaman-ejurnal', $data);  
		}
	}
		
	function detail_ejurnal(){
        $id=$this->uri->segment(4);
        $data['data']=$this->m_website_unlaacid->getByid('kdjurnal',$id,'v_jurnalpublish');
        $look=0;
        if($data['data']!=''){
            $look = $this->jumlahjurnal($id,'dilihat', 'jurnal');
            if($look == null){
                $look = 1;
            }else{
                $look = $look +1;
            }
                    
            $q = "update jurnal set dilihat=".$look." where kdjurnal = ".$id;
            $this->db->query($q);
        }
		$data['menuatas']=$this->m_website_unlaacid->getByid('menuatas','1','menuweb');

				 //ARTIKEL FOOTER
           		 $jml = $this->db->get('v_beritanpengumuman');
           		 $config['base_url'] = base_url().'index.php/web/content/index';
                 $config['total_rows'] = $jml->num_rows();
                 $config['per_page'] = '5';
                 $config['per_page_footerpost'] = '5';
				 $config['num_links'] = '5';
				 $config['per_page_pop'] = '5';
				 $config['per_page_testi'] = '1';
				 $config['per_pagehead'] = '1';
				 $config['uri_segment'] = 4;

				 //inisialisasi config
                 $this->pagination->initialize($config);

                //buat pagination
                 $data['halaman'] = $this->pagination->create_links();
                
				//tamplikan data
                 $data['footerpost'] = $this->m_website_unlaacid->getfPage('v_popularpost',$config['per_page_footerpost'], $id);
				//END ARTIKEL FOOTER

                 $data['databank'] = $this->m_website_unlaacid->getAll('rekbank','','');
                 $data['postpopular'] = $this->m_website_unlaacid->getfPage('v_popularpost',$config['per_page_pop'], NULL);
			     $data['postrecent'] = $this->m_website_unlaacid->getfPage('v_recentpost',$config['per_page_pop'], NULL);
			     $data['datatestimoni'] = $this->m_website_unlaacid->getfPage('v_testimoni',$config['per_page_testi'], NULL);
			     $data['queryhead'] = $this->m_website_unlaacid->getfPage('v_beritanpengumuman',$config['per_pagehead'], NULL);

        $this->load->view('web/e-jurnal/detail-halaman-ejurnal',$data);
    }
	
	function download_jurnal(){
		$kdjurnal=$this->uri->segment(4);
		$userid=$this->uri->segment(5);
		$file=$this->uri->segment(6);
		$this->load->helper('download'); //jika sudah diaktifkan di autoload, maka tidak perlu di tulis kembali
		$name = $file;
		$data = file_get_contents("resources/jurnal/".$file); // letak file pada aplikasi kita
		$data['data']=$this->m_website_unlaacid->update_download($kdjurnal,$userid);
		force_download($name,$data);
	}
	
	function kontakhubkami(){
		/* if(($this->input->post('captcha') <> ""){
		
		}else{
		
		} */
	
		if(($this->input->post('captcha') <> "") AND ($this->Mediatutorialcaptcha->check_captcha()==TRUE)) {
			$nama = $this->input->post("nama");
			$from = $this->input->post("email");
			$message = $this->input->post("pesan");
			
			$email;
			$pass;
			
			$query = $this->db->query('SELECT emailadmin,passadmin FROM hubkamiinfo');
			foreach ($query->result() as $row){
			$email = $row->emailadmin;
			$pass = base64_decode($row->passadmin);
			}
		
			$this->load->library('email');
			$config['protocol']    	= 'smtp';
			$config['smtp_host']    = 'ssl://smtp.gmail.com';
			$config['smtp_port']    = '465';
			$config['smtp_timeout'] = '7';
			$config['smtp_user']    = $email;
			$config['smtp_pass']    = $pass;
			$config['charset']    	= 'utf-8';
			$config['newline']    	= "\r\n";
			$config['mailtype']		= 'text'; // or html
			$config['validation']	= TRUE; // bool whether to validate email or not      

			$this->email->initialize($config);

			$this->email->from($from, $nama);
			$this->email->to($email); 
			//$this->email->cc($cc);

			$this->email->subject($nama);
			$this->email->message($message);  

			$this->email->send();
		
			$data['query']=$this->m_website_unlaacid->simpanpesan();
			//$data['datax']=  $this->m_website_unlaacid->getAll('hubkamiinfo','','');
			//$data['menuatas']=$this->m_website_unlaacid->getByid('menuatas','1','menuweb');
			//$this->load->view('web/hubungi-kami/detail-halaman-hubungi-kami',$data);
			$cekcap='';
			$this->hubungi_kami($cekcap);
			redirect('index.php/web/content/hubungi_kami');
		}else{
			$cekcap=' Captcha salah, coba lagi';
			$this->hubungi_kami($cekcap);
		}
	}

		function formtesti(){
		if(($this->input->post('captcha') <> "") AND ($this->Mediatutorialcaptcha->check_captcha()==TRUE)) {
			$dari = $this->input->post("dari");
			$deskripsi = $this->input->post("deskripsi");
			$data['query']=$this->m_website_unlaacid->simpantestimoni();
			$cekcap='';
			$this->testimoni($cekcap);
			redirect('index.php/web/content/formtesti');
		}else{
			//$cekcap=' Captcha salah, coba lagi';
            $cekcap=' ';
			$this->testimoni($cekcap);
		}
	}

	function bank(){    
	$data = NULL;
	$data['databank']=  $this->m_website_unlaacid->getAll('rekbank','','');
	$this->load->view('web/right-index-web',$data);
        }

	function sponsor(){    
	$data = NULL;
	$data['datasponsor']=  $this->m_website_unlaacid->getAllkerjasama('kerjasama','','');
	$this->load->view('web/foot_web',$data);
        }       
		
	function testimonikanan(){    
	$config['per_page_testi'] = '1';
	$data = NULL;
	
	$data['datatestimoni'] = $this->m_website_unlaacid->getfPage('v_testimoni',$config['per_page_testi'], $id);
	$this->load->view('web/right-index-web',$data);
        }  

    function kegiatan(){    
	$data = NULL;
	$data['datakegiatan']=  $this->m_website_unlaacid->getAll('kegiatan','','');
	$this->load->view('webfoot_web',$data);
        }

    function deslider(){    
	$data = NULL;
	$data['deskripsislider']=  $this->m_website_unlaacid->getAll('galeri','','');
	$this->load->view('head_web',$data);
        }
			


	function memberonline($pesan=null){
		$config['per_page_footerpost'] = '5';
		$config['per_pagehead'] = '1';
		$config['page_poster'] = '1';
		$config['per_page_pop'] = '5';
		$id=NULL;

		$data['postpopular'] = $this->m_website_unlaacid->getfPage('v_popularpost',$config['per_page_pop'], NULL);
        $data['postrecent'] = $this->m_website_unlaacid->getfPage('v_recentpost',$config['per_page_pop'], NULL);
		$data['gbrposter'] = $this->m_website_unlaacid->getfPage('v_poster',$config['page_poster'], NULL);
		$data['queryhead'] = $this->m_website_unlaacid->getfPage('v_beritanpengumuman',$config['per_pagehead'], $id);
		$data['footerpost'] = $this->m_website_unlaacid->getfPage('v_popularpost',$config['per_page_footerpost'], $id);
		$data['datasponsor'] = $this->m_website_unlaacid->getAllkerjasama('kerjasama','','');
        $data['datakegiatan']=  $this->m_website_unlaacid->getAll('kegiatan','','');
		$data['menuatas']=$this->m_website_unlaacid->getByid('menuatas','1','menuweb');
		
		$data['title']="Prosedur Pendaftaran";
		//$data['pesan']="Maaf Informasi Tidak Bisa Diakses, Anda Bukan Member..";
        $data['datax']=  $this->m_website_unlaacid->getAll('sopmember','','');
		
				
		$this->load->view('web/pendaftaran',$data);
    } 

	function myprofile2($isempty=null){
		$config['per_page_footerpost'] = '5';
		$config['per_pagehead'] = '1';
		$config['page_poster'] = '1';
		$config['per_page_pop'] = '5';
		$id=NULL;

		$data['postpopular'] = $this->m_website_unlaacid->getfPage('v_popularpost',$config['per_page_pop'], NULL);
        $data['postrecent'] = $this->m_website_unlaacid->getfPage('v_recentpost',$config['per_page_pop'], NULL);
		$data['gbrposter'] = $this->m_website_unlaacid->getfPage('v_poster',$config['page_poster'], NULL);
		$data['queryhead'] = $this->m_website_unlaacid->getfPage('v_beritanpengumuman',$config['per_pagehead'], $id);
		$data['footerpost'] = $this->m_website_unlaacid->getfPage('v_popularpost',$config['per_page_footerpost'], $id);
		$data['datasponsor'] = $this->m_website_unlaacid->getAllkerjasama('kerjasama','','');
        $data['datakegiatan']=  $this->m_website_unlaacid->getAll('kegiatan','','');
		$data['menuatas']=$this->m_website_unlaacid->getByid('menuatas','1','menuweb');
		
		
		if ($isempty) {
			redirect('web/content');
		}
		
		if($this->my_usessionpublic->userdata('idmember')){	
			$data['page']="success";
		} else {
			$data['page']="daftar";
		}		
		$this->load->view('web/myprofile',$data);
    }

	 function myprofile()
	{  
		if ($this->my_usessionpmb->logged_in) {
		
			$config['per_page_footerpost'] = '5';
			$config['per_pagehead'] = '1';
			$config['page_poster'] = '1';
			$config['per_page_pop'] = '5';
			$id=NULL;

			$data['postpopular'] = $this->m_website_unlaacid->getfPage('v_popularpost',$config['per_page_pop'], NULL);
			$data['postrecent'] = $this->m_website_unlaacid->getfPage('v_recentpost',$config['per_page_pop'], NULL);
			$data['gbrposter'] = $this->m_website_unlaacid->getfPage('v_poster',$config['page_poster'], NULL);
			$data['queryhead'] = $this->m_website_unlaacid->getfPage('v_beritanpengumuman',$config['per_pagehead'], $id);
			$data['footerpost'] = $this->m_website_unlaacid->getfPage('v_popularpost',$config['per_page_footerpost'], $id);
			$data['datasponsor'] = $this->m_website_unlaacid->getAllkerjasama('kerjasama','','');
			$data['datakegiatan']=  $this->m_website_unlaacid->getAll('kegiatan','','');
			$data['menuatas']=$this->m_website_unlaacid->getByid('menuatas','1','menuweb');
		
			//$data['title']="Biodata Pendaftaran";
			$data['datax']= $this->my_usessionpmb->userdata('nomember');
			
			$this->load->view('web/myprofile',$data);
		} else {
			redirect('web/content');
		}
	
	}	
	
		
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */