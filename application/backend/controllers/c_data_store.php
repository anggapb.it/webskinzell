<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of c_setting_biaya
 *
 * @author Yakuza Ayame
 */
class C_data_store extends Controller {

    public function __construct() {
        parent::Controller();
    }
    
    function getData($sql) {
        $query = $this->db->query($sql);
        $data = array();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        }
        $ttl = count($data);
        $build_array = array("success" => true, "results" => $ttl, "data" => array());

        if ($ttl > 0) {
            $build_array["data"] = $data;
        }
        echo json_encode($build_array);
    }
    
    function get_jnsbiaya() {
        $sql = "SELECT idjnsbiaya, kdjnsbiaya FROM jbiaya";
        $this->getData($sql);
    }
    
    function get_tahun() {
        $sql = "SELECT tahun FROM tahun";
        $this->getData($sql);
    }
    
    function get_prodi() {
        $sql = "SELECT kdprodi, CONCAT_WS(' ',nmprodi,nmkodtbkod) as nmprodi FROM prodi
                LEFT JOIN tbkod ON prodi.kdjenjangstudi = tbkod.kdkodtbkod
                WHERE tbkod.kdapltbkod = 4 ";
        $this->getData($sql);
    }
    
    function get_jnskelas() {
        $sql = "SELECT idjnskls, nmjnskls FROM jkls";
        $this->getData($sql);
    }

    function get_status() {
        $sql = "SELECT idstatus, nmstatus FROM `status`";
        $this->getData($sql);
    }
    
    function get_statussmt() {
        $sql = "SELECT kdstsemester, CONCAT_WS('-',nmthnakademik,nmjnssemester) AS statussmt 
                FROM stsemester
                LEFT JOIN thnakademik ON stsemester.idthnakademik = thnakademik.idthnakademik
                LEFT JOIN jsemester ON stsemester.idjnssemester = jsemester.idjnssemester
                WHERE idstatus = 1";
        $this->getData($sql);
    }
    
    function get_jnsdiskon() {
        $sql = "SELECT idjnsdiskon, nmdiskon FROM jdiskon";
        $this->getData($sql);
    }
    
}

/* End of file c_data_store.php */
/* Location: ./application/backend/controllers/keuangan/c_data_store.php */
