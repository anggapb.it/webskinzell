<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Data_Controller extends Controller {
	var $stat;

    public function __construct()
    {
        parent::Controller();
    }

    // START HELPER ===========================================================    
    function getData($q) {
		$query  = $this->db->query($q);
        $data = array();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        }        
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
        echo json_encode($build_array);
	}
	
	function get_gender(){
		$q = "SELECT kdkodtbkod as kdjnskelamin, nmkodtbkod as jnskelamin FROM tbkod 
				WHERE kdapltbkod='08'";
		$this->getData($q);	
	}
	
	function get_religion(){
		$q = "SELECT kdkodtbkod as kdagama, nmkodtbkod as agama ,
				CASE WHEN kdkodtbkod='I' THEN '1' 
					WHEN kdkodtbkod='K' THEN '2'
					WHEN kdkodtbkod='P' THEN '3'
					WHEN kdkodtbkod='H' THEN '4'
					WHEN kdkodtbkod='B' THEN '5'
					ELSE '9'
				END as nourut
				FROM tbkod WHERE kdapltbkod='51' ORDER BY nourut";
		$this->getData($q);	
	}
	
	function get_bloodtype(){
		$q = "SELECT idgoldarah, kdgoldarah, nmgoldarah FROM goldarah 
				WHERE idgoldarah<>''";
		$this->getData($q);	
	}
	
	function get_nationality(){
		$q = "SELECT kdkodtbkod as kdwn, nmkodtbkod as nmwn FROM tbkod 
			WHERE kdapltbkod='50'";
		$this->getData($q);	
	}
	
	function get_city(){ //KOTA
        $q="SELECT CONCAT(rtrim(kdprotbpro),'.',ltrim(kdkabtbpro)) as kdkota, 
			nmkabtbpro as nmkota, nmprotbpro as nmprov 
			from tbpro where kdkabtbpro <> ''";        
        $this->getData($q);		
    }
	
	function get_prov(){ //PROPINSI
        $q="SELECT DISTINCT kdprotbpro as kdprov, nmprotbpro as nmprov 
		from tbpro where kdprotbpro <> ''";        
        $this->getData($q);
    }
    
	function get_parentjob(){ //
        $q="SELECT idpekerjaanortu as idjob,kdpekerjaanortu as kdjob,
		nmpekerjaanortu as nmjob, deskripsi 
		from pekerjaanortu where idpekerjaanortu <> ''";        
        $this->getData($q);
    }
	
	function get_parentedu(){ //
        $q="SELECT idpendidikanortu as idedu, kdpendidikanortu as kdedu,
		nmpendidikanortu as nmedu, deskripsi 
		from pendidikanortu where idpendidikanortu <> ''";        
        $this->getData($q);
    }
	
	function get_univ(){ //P.T.
        $q="SELECT kdptitbpti as kdpt, nmptitbpti as nmpt, kotaatbpti as kota from tbpti where kdptitbpti <> ''";        
        $this->getData($q);
    }
	
	function get_prodi(){ //Program Studi indonesia
        $q="SELECT kdpsttbpst as kdprodi, nmpsttbpst as nmprodi, nomortbpst as nomor from tbpst where kdpsttbpst <> ''";        
        $this->getData($q);
    }
	
	function get_prodireg(){ //Program Studi lokal
        $q="SELECT kdprodi, nmprodi, CONCAT(RTRIM(kdprodi),'. ',nmprodi) as lnmprodi,
		kdjenjangstudi, tglawalberdiri, email, noskakreditasi, tglskakreditasi, notelpprodi
		FROM prodi where kdprodi <> ''";        
        $this->getData($q);
    }
	
	function get_dosen(){ //Dosen/Promotor
        $q="SELECT nidn as iddosen , nmdosdgngelar as nmdosen from dosen where nidn <> ''";        
        $this->getData($q);
    }
	
	function get_stsmt(){ //status smester
        $q="SELECT kdstsemester as kdsmt, idthnakademik, idjnssemester as idjnsmt, idstatus, deskripsi, stpmb 
		from stsemester where kdstsemester <> ''";        
        $this->getData($q);
    }
	
	function get_shift(){ 
        $q="SELECT kdkodtbkod as shiftmhs, nmkodtbkod as nmshift 
		FROM tbkod WHERE kdapltbkod='64'";        
        $this->getData($q);
    }
	
	function get_class(){ 
        $q="SELECT idklsmhs, kdklsmhs , nmklsmhs, deskripsi 
		FROM klsmhs WHERE idklsmhs<>''";        
        $this->getData($q);
    }
	
	function get_years(){
		$q = "SELECT tahun FROM tahun where tahun<>''";
		$this->getData($q);
	}
	
	function get_stfirst(){
		$q = "SELECT kdkodtbkod as kdstawal, nmkodtbkod as nmstawal 
		FROM tbkod WHERE kdapltbkod='06'";
		$this->getData($q);
	}
	
	function get_staktiv(){
		$q = "SELECT kdkodtbkod as kdstaktiv, nmkodtbkod as nmstaktiv 
		FROM tbkod WHERE kdapltbkod='05'";
		$this->getData($q);
	}
	
	function get_jslta(){
		$q = "SELECT idjnsslta, kdjnsslta, nmjnsslta, deskripsi 
		FROM jslta WHERE idjnsslta<>''";
		$this->getData($q);
	}
	
	function get_stakrslta(){
		$q = "SELECT idstakreditasislta as idstakr, kdstakreditasislta as kdstakr, 
		nmstakreditasislta as nmstakr, deskripsi 
		FROM stakreditasislta WHERE idstakreditasislta<>''";
		$this->getData($q);
	}
	
	function get_jpt(){
		$q = "SELECT idjnspt, kdjnspt, nmjnspt, deskripsi 
		FROM jpt WHERE idjnspt<>''";
		$this->getData($q);
	}
	
	function get_stakrpt(){
		$q = "SELECT idstakreditasi as idstakr, kdstakreditasi as kdstakr, 
		nmstakreditasi as nmstakr, deskripsi 
		FROM stakreditasi WHERE idstakreditasi<>''";
		$this->getData($q);
	}
	
	function get_kuakrpt(){
		$q = "SELECT DISTINCT idkuakreditasi as idkuakr, kdkuakreditasi as kdkuakr, 
		nmkuakreditasi as nmkuakr, deskripsi 
		FROM kuakreditasi WHERE idkuakreditasi<>''";
		$this->getData($q);
	}
    
	function get_mhsjob(){ //
        $q="SELECT idpekerjaanmhs as idjob,kdpekerjaanmhs as kdjob,
		nmpekerjaanmhs as nmjob, deskripsi 
		from pekerjaanmhs where idpekerjaanmhs <> ''";        
        $this->getData($q);
    }
	
	function get_biaya(){
		$q="SELECT kdkodtbkod as kdbiayastudi, nmkodtbkod as nmbiayastudi 
		FROM tbkod WHERE kdapltbkod='16'";        
        $this->getData($q);
	}
	
	function get_jkls() {
        $sql = "SELECT idjnskls,kdjnskls, nmjnskls,deskripsi
			FROM jkls";
        $this->getData($sql);
    }
	
	function get_jjurnal() {
        $sql = "SELECT * FROM jnsjurnal";
        $this->getData($sql);
    }
	// END HELPER =============================================================
  
}
