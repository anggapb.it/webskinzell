<?php
class Lap_qurbantoexcel extends Controller {
    function __construct(){
        parent::__construct();
		//$this->load->library('pdf');        
    }

	function exportexcel($tglawal,$tglakhir,$program,$daerah,$cbcek,$cek) {		
		$tablename='v_validasiqurban';
		$header = array(
			'No. Qurban',
			'Tgl. Daftar',
			'Nama Pemesan',
			'Alamat',
			'No. Telp',
			'Email',
			'Disalurkan Ke Propinsi',
			'Daerah',
			'Jenis Hewan',
			'Kelas Hewan',
			'Harga Hewan',
			'Sodaqoh',
			'Total',
			'Pequrban',
			'Publis',
			'Sumber Informasi',
			'Info',
			'Bank',
			'No. Rekening',
			'Atas Nama',
			'Atas Nama Pemesan',
			'Tgl. Konfirmasi',
			'Status'
		);

		$data['eksport'] = $this->getExport($tablename,$tglawal,$tglakhir,$program,$daerah,$cbcek,$cek);
		$data['table'] = $tablename;
		$data['fieldname'] = $header;//$this->db->list_fields($tablename);
		$data['numrows'] = $this->getNumRows($tablename);
		$this->load->view('exportexcellqurban', $data); 	
	}
	
	function getNumRows($table) {
        $query = $this->db->get($table);
        return $query->num_rows();
    }
	
	function getExport($table,$tglawal,$tglakhir,$program,$daerah,$cbcek,$cek) {
		/* var_dump($table,$tglawal,$tglakhir,$program,$daerah,$cbcek,$cek);
		exit; */
		
		if($daerah != "all" AND $cek = "all"){
		
			if($daerah == "all") $dae = "";
			else $dae = $daerah;
			
			/* if($cek = "all") $ccek = "";
			else $ccek = $cek; */
			
			$q = "SELECT 
				noqurban,
				tgldaftar,
				nmpemesan,
				alamat,
				notelp,
				email,
				provinsi,
				nmdaerah,
				nmjnshewan,
				nmklshewan,
				nominal,
				shodaqoh,
				total,
				nmmuqorib,
				nmstpublish,
				nmsbrinfo,
				rekomendasidari,	
				nmbank,		
				norek,
				atasnama,
				atasnamapemesan,
				tglkonfirmasi,
				nmstqurban			
			FROM ".$table." where date(tgldaftar) BETWEEN '".$tglawal."' AND '".$tglakhir."'
				AND idprogram ='".$program."'
				AND iddaerah ='".$dae."'";
				
			$query = $this->db->query($q);	
			
			$this->db->where($cbcek,$cek);
			
			if ($query->num_rows() > 0) {
				return $query->result();
			} else {
				return array();
			}
			
		}else if($daerah = "all" AND $cek != "all"){
		
			/* if($daerah == "all") $dae = "";
			else $dae = $daerah; */
			
			if($cek == "all") $ccek = "";
			else $ccek = $cek;
			
			$q = "SELECT 
				noqurban,
				tgldaftar,
				nmpemesan,
				alamat,
				notelp,
				email,
				provinsi,
				nmdaerah,
				nmjnshewan,
				nmklshewan,
				nominal,
				shodaqoh,
				total,
				nmmuqorib,
				nmstpublish,
				nmsbrinfo,
				rekomendasidari,	
				nmbank,		
				norek,
				atasnama,
				atasnamapemesan,
				tglkonfirmasi,
				nmstqurban		
			FROM ".$table." where date(tgldaftar) BETWEEN '".$tglawal."' AND '".$tglakhir."'
				AND idprogram ='".$program."'
				AND ".$cbcek." ='".$cek."'";
				
			$query = $this->db->query($q);	
			
			$dae = "";
			$this->db->where("iddaerah",$dae);
			
			if ($query->num_rows() > 0) {
				return $query->result();
			} else {
				return array();
			}
			
		}else if($daerah = "all" AND $cek = "all"){
			$q = "SELECT 
				noqurban,
				tgldaftar,
				nmpemesan,
				alamat,
				notelp,
				email,
				provinsi,
				nmdaerah,
				nmjnshewan,
				nmklshewan,
				nominal,
				shodaqoh,
				total,
				nmmuqorib,
				nmstpublish,
				nmsbrinfo,
				rekomendasidari,	
				nmbank,		
				norek,
				atasnama,
				atasnamapemesan,
				tglkonfirmasi,
				nmstqurban			
			FROM ".$table." where date(tgldaftar) BETWEEN '".$tglawal."' AND '".$tglakhir."' AND idprogram ='".$program."'";
			$query = $this->db->query($q);
			
			$dae = "";
			$this->db->where("iddaerah",$dae);
			
			$this->db->where($cbcek,$cek);
			
			if ($query->num_rows() > 0) {
				return $query->result();
			} else {
				return array();
			}
		}   
    }	
}