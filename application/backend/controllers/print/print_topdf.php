<?php
/**
 * Mencetak Semua Dokumen Baik itu report atau dokumen
 *
 * @author gambasvb
 */
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Print_topdf extends Controller {
    function __construct(){
        parent::__construct();
		$this->load->library('cezpdf');
    }
    
    function ceklog() {
        $user = $this->session->userdata("user_id1unla");
            if($user==""){
                redirect('user/login');
            }
	}
	
	function ceklogpmb() {
        $user = $this->session->userdata("nopmbunla");
            if($user==""){
                redirect('index.php/web/content/prosedurpmb');
            }
    }
	
	//=================================Header Khusus Transkrip=============================================
	function header_transkrip(){
   
            $x_image = 20;
            $y_image = 770;
            
            $x_text1 = 140;
            $y_text1 = 820;
            
            $x_text2 = 140;
            $y_text2 = 795;
            $alamat='                                 Jl. Karapitan No. 116 Bandung 40261, Telp. 022-4218084, Fax. 022-4237144';
			$this->cezpdf->line(20,765,570,765);
        
        $image = imagecreatefrompng(base_url().'resources/img/report.png');
        $this->cezpdf->addImage($image,$x_image,$y_image,90);
        
       // $this->cezpdf->addPngFromFile(base_url().'resources/common-web/gfx/theme1/icon.png',100,490,100,'left'); //245,510,100,'left');
        // $this->cezpdf->ezText($text,7,array('justification' => 'right'));

        //head info laporan
        $this->cezpdf->selectFont('./fonts/Helvetica-Bold');
        $this->cezpdf->addText($x_text1,$y_text1,16,"YAYASAN PENDIDIKAN TRI BHAKTI LANGLANGBUANA",0);
        $this->cezpdf->addText($x_text2,$y_text2,26,"UNIVERSITAS LANGLANGBUANA",0);
        $this->cezpdf->selectFont('./fonts/Helvetica');
        
        $head_unla[]=array(   'kol1'=>'','kol2'=>'');
        $head_unla[]=array(   'kol1'=>'','kol2'=>'');
        $head_unla[]=array(   'kol1'=>'','kol2'=>'');
        $head_unla[]=array(   'kol1'=>'','kol2'=>$alamat);

		
             
            $this->cezpdf->ezTable($head_unla, '', '', array( 'width'=>660,
                                                              'fontSize' => 11,
                                                              'showLines'=> 0,
                                                              'showHeadings'=>0,
                                                              'shaded'=>0,
                                                              'cols'=>array(
                                                                    'kol1'=>array('justification'=>'left','width'=>'35','fontSize' => 12),
                                                                    'kol2'=>array('justification'=>'left','width'=>'500'))));
        
    }
    
	//=================================Header Report=============================================

    function header($orientation){
        if($orientation=='landscape'){
            $x_image = 100;
            $y_image = 475;
            
            $x_text1 = 218;
            $y_text1 = 535;
            
            $x_text2 = 218;
            $y_text2 = 515;
            $alamat='          Jl. Karapitan No. 116 Bandung 40261, Telp. 022-4218084, Fax. 022-4237144';
			$this->cezpdf->line(90,465,745,465);
        }else{
            $x_image = 50;
            $y_image = 725;
            
            $x_text1 = 168;
            $y_text1 = 785;
            
            $x_text2 = 168;
            $y_text2 = 765;
            $alamat='                                 Jl. Karapitan No. 116 Bandung 40261, Telp. 022-4218084, Fax. 022-4237144';
			$this->cezpdf->line(55,720,550,720);
		}
        
        $image = imagecreatefrompng(base_url().'resources/img/report.png');
        $this->cezpdf->addImage($image,$x_image,$y_image,110);
        
       // $this->cezpdf->addPngFromFile(base_url().'resources/common-web/gfx/theme1/icon.png',100,490,100,'left'); //245,510,100,'left');
        // $this->cezpdf->ezText($text,7,array('justification' => 'right'));

        //head info laporan
        $this->cezpdf->selectFont('./fonts/Helvetica-Bold');
        $this->cezpdf->addText($x_text1,$y_text1,14,"YAYASAN PENDIDIKAN TRI BHAKTI LANGLANGBUANA",0);
        $this->cezpdf->addText($x_text2,$y_text2,23,"UNIVERSITAS LANGLANGBUANA",0);
        $this->cezpdf->selectFont('./fonts/Helvetica');
        
        $head_unla[]=array(   'kol1'=>'','kol2'=>'');
        $head_unla[]=array(   'kol1'=>'','kol2'=>'');
        $head_unla[]=array(   'kol1'=>'','kol2'=>'');
        $head_unla[]=array(   'kol1'=>'','kol2'=>$alamat);

		
             
            $this->cezpdf->ezTable($head_unla, '', '', array( 'width'=>660,
                                                              'fontSize' => 11,
                                                              'showLines'=> 0,
                                                              'showHeadings'=>0,
                                                              'shaded'=>0,
                                                              'cols'=>array(
                                                                    'kol1'=>array('justification'=>'left','width'=>'35','fontSize' => 12),
                                                                    'kol2'=>array('justification'=>'left','width'=>'500'))));
        
    }
    
	//=================================Konversi Tanggal Ke Indo=============================================

	  function TanggalIndo($date){
            $BulanIndo = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");

            $tahun = substr($date, 0, 4);
            $bulan = substr($date, 4, 2);
            $tgl   = substr($date, 6, 2);
             
            $result =$tgl . " " . $BulanIndo[(int)$bulan-1] . " ". $tahun;		
            return($result);
        }

	//=================================Format Bilangan untuk angka/nominal uang=============================================

    function terbilang($x)
    {
        $abil = array(
            "",
            " Satu",
            " Dua",
            " Tiga",
            " Empat",
            " Lima",
            " Enam",
            " Tujuh",
            " Delapan",
            " Sembilan",
            " Sepuluh",
            " Sebelas");
        if ($x < 12){
            return $abil[$x];}
        elseif ($x < 20){
            return $this->terbilang($x - 10) . " Belas";}
        elseif ($x < 100){
            return $this->terbilang($x / 10) . " Puluh" . $this->terbilang($x % 10);}
        elseif ($x < 200){
            return " Seratus" . $this->terbilang($x - 100);}
        elseif ($x < 1000){
            return $this->terbilang($x / 100) . " Ratus" . $this->terbilang($x % 100);}
        elseif ($x < 2000){
            return " Seribu" . $this->terbilang($x - 1000);}
        elseif ($x < 1000000){
            return $this->terbilang($x / 1000) . " Ribu" . $this->terbilang($x % 1000);}
        elseif ($x < 1000000000){
            return $this->terbilang($x / 1000000) . " Juta" . $this->terbilang($x % 1000000);}
    }


	//=================================Fungsi Mencari nama field=============================================

     function nm_field($column,$tbl,$whereb, $wherea){
        $q = "SELECT ".$column." as nm FROM ".$tbl." where ".$whereb." = '".$wherea."' " ;
        $query  = $this->db->query($q);
        $nm= ''; 
                    
        if ($query->num_rows() != 0)
        {
            $row = $query->row();
            $nm=$row->nm;
        }
        return $nm;
    }  
	
	function nm_field2($column,$tbl,$whereb, $wherea){
		$KEDUA= $this->load->database('second', TRUE);
		
        $q = "SELECT ".$column." as nm FROM ".$tbl." where ".$whereb." = '".$wherea."' " ;
        $query  = $KEDUA->query($q);
        $nm= ''; 
                    
        if ($query->num_rows() != 0)
        {
            $row = $query->row();
            $nm=$row->nm;
        }
        return $nm;
    }
	
	//=================================Fungsi mencari data dosen wali mahasiswa=============================================

	function get_wali($column,$tbl,$whereb, $wherea){
		
		$KEDUA= $this->load->database('second', TRUE);
		
		 $q = "SELECT ".$column." as nm FROM ".$tbl." where ".$whereb." = '".$wherea."' " ;
        $query  = $KEDUA->query($q);
        $nm= ''; 
                    
        if ($query->num_rows() != 0)
        {
            $row = $query->row();
            $nm=$row->nm;
        }
        return $nm;
    }
	
	//=================================Fungsi mencari data jadwal usm=============================================

	function get_jadwalusm($idstmskmhs){
        $q = "SELECT * FROM v_jadwalusmshow where idstmskmhs = '".$idstmskmhs."' " ;
        $query  = $this->db->query($q);
        $nm= ''; 
        $result = array();           
        if ($query->num_rows() != 0)
        {
            $result = $query->result();
        }
        return $result;
    }  

	//=================================Fungsi mencari data student body=============================================

     function get_vstudentbody($kdstsemester){
		$KEDUA= $this->load->database('second', TRUE);
        $query  = $KEDUA->query("CALL sp_getStudentbody(?)",array($kdstsemester));
        $result = array();
        if ($query->num_rows() > 0) {
            $result = $query->result();
        }
        return $result;
	}
	//=================================Fungsi mencari data pmb=============================================

    function getlpmb($wherex){
        $idjadwalpmbx            = $wherex['idjadwalpmb'];
        $kdprodix               = $wherex['kdprodi'];
        
            $this->db->select("*");
            $this->db->from("v_pendaftar"); 
       
        if($idjadwalpmbx !=''){
            $where['idjadwalpmb'] =$idjadwalpmbx;
        }
        if($kdprodix !=''){
            $where['kdprodi1'] =$kdprodix;
        }
        //======================================================================
        
         if($idjadwalpmbx !='' || $kdprodix !=''){
            $this->db->where($where);
        }
        
        $q  = $this->db->get(); 
        $result = array();
        if ($q->num_rows() > 0) {
            $result = $q->result();
        }
        return $result;
    }

	//=================================Fungsi mencari data pmb terverifikasi=============================================

     function getllpterverifikasi($wherex){
        
        $idjadwalpmbx            = $wherex['idjadwalpmb'];
        $kdprodix               = $wherex['kdprodi'];
        
            $this->db->select("*");
            $this->db->from("v_pmbterverifikasi1"); 
       
        if($idjadwalpmbx !=''){
            $where['idjadwalpmb'] =$idjadwalpmbx;
        }
        if($kdprodix !=''){
            $where['kdprodi1'] =$kdprodix;
        }
        //======================================================================
        
       if($idjadwalpmbx !='' || $kdprodix !=''){
            $this->db->where($where);
        }
              
        $q  = $this->db->get(); 
        $result = array();
        if ($q->num_rows() > 0) {
            $result = $q->result();
        }
        return $result;
    }
	
	//=================================Fungsi mencari data hasil usm=============================================
    
    function getlhasilusm($wherex){
        
        $idjadwalpmbx            = $wherex['idjadwalpmb'];
        $idstusmx               = $wherex['idstusm'];
        
            $this->db->select("*");
            $this->db->from("v_hasilusm"); 
       
        if($idjadwalpmbx !=''){
            $where['idjadwalpmb'] =$idjadwalpmbx;
        }
        if($idstusmx !=''){
            $where['idstusm'] =$idstusmx;
        }
         
        if($idjadwalpmbx !='' || $idstusmx !=''){
            $this->db->where($where);
        }
              
        $q  = $this->db->get(); 
        $result = array();
        if ($q->num_rows() > 0) {
            $result = $q->result();
        }
        return $result;
    }
    
	//=================================Fungsi mencari data head rekap pmb=============================================
 
    function getHeadlrpmb(){
        
        $qq ="SELECT kdfakultas, nmfakultas, 
            sum(if(isnull(jmlpendaftar),0,1)) as headjmlpendaftar, 
            sum(terverifikasi) as headterverifikasi,
            sum(diterima) as headditerima
            FROM v_rekappmb group by kdfakultas";
            
        $q  = $this->db->query($qq); 
        $result = array();
        if ($q->num_rows() > 0) {
            $result = $q->result();
        }
        return $result;
    }
    
	//=================================Fungsi mencari data rekap pmb=============================================

    function getlrpmb($wherex=''){
        
        $idjadwalpmbx            = $wherex['idjadwalpmb'];
        
            $this->db->select("*");
            $this->db->from("v_rekappmb"); 
       
        if($idjadwalpmbx !=''){
            $where['idjadwalpmb'] =$idjadwalpmbx;
        }
        if($idjadwalpmbx !=''){
            $this->db->where($where);
        }
              
            $this->db->order_by('kdfakultas');
            
        $q  = $this->db->get(); 
        $result = array();
        if ($q->num_rows() > 0) {
            $result = $q->result();
        }
        return $result;
    }
    
	//=================================Fungsi mencari data kuitansi pmb=============================================

    function getkwitansi($wherex=''){
        
            $this->db->select("*");
            $this->db->from("v_kuitansipmb"); 
       
        if($wherex !=''){
            $this->db->where('nokuitansipmb',$wherex);
        }
            
        $q  = $this->db->get(); 
        $result = array();
        if ($q->num_rows() > 0) {
            $result = $q->result();
        }
        return $result;
    }
    
	//=================================Fungsi mencari data mahasiswa=============================================
    
    function getmahasiswa($wherex=''){
         $KEDUA= $this->load->database('second', TRUE);  /*membuka koneksi database untuk d_mahasiswa*/
       
            $KEDUA->select("*");
            $KEDUA->from("vv_mahasiswa_profile2"); 
       
        if($wherex !=''){
            $KEDUA->where('nim',$wherex);
        }
            
        $q  = $KEDUA->get(); 
        $result = array();
        if ($q->num_rows() > 0) {
            $result = $q->result();
        }
        return $result;
    }
	
	//=================================Fungsi mencari data nilai mahasiswa per semester=============================================
 
    function getvnilai($wherex=''){
	$KEDUA= $this->load->database('second', TRUE);
            $KEDUA->select("*");
            $KEDUA->from("v_nilai"); 
       
        if($wherex !=''){
            $KEDUA->where('nim', $wherex['nim']);
            $KEDUA->where('kdstsemester', $wherex['kdstsemester']);
            $KEDUA->where('idsemester', $wherex['idsemester']);
        }
            
        $q  = $KEDUA->get(); 
        $result = array();
        if ($q->num_rows() > 0) {
            $result = $q->result();
        }
        return $result;
    }
 
	//=================================Fungsi mencari data semua nilai mahasiswa =============================================
 
	function getvnilaiall($nim,$kdprodi,$tahunmsk,$limit,$start){
	$KEDUA= $this->load->database('second', TRUE);  /*membuka koneksi database untuk d_mahasiswa*/
    $q = $KEDUA->query("SELECT `kurikulum`.`kdprodi` AS `kdprodi`
     , `kurikulum`.`thnmasuk` AS `thnmasuk`
     , `kurikulum`.`kdmk` AS `kdmk`
     , `matakuliah`.`nmmkind` AS `nmmkind`
     , `matakuliah`.`nmmkeng` AS `nmmkeng`
     , ifnull((SELECT `nilai`.`nilaihuruf` AS `nilaihuruf`
        FROM
          `nilai`
        WHERE
          ((`nilai`.`nim` = '".$nim."')
          AND (`nilai`.`kdprodi` = `kurikulum`.`kdprodi`)
          AND (`nilai`.`kdmk` = `kurikulum`.`kdmk`))),'-') AS `nilaihuruf`
     , ifnull((SELECT `nilai`.`bobotnilai` AS `bobotnilai`
        FROM
          `nilai`
        WHERE
          ((`nilai`.`nim` = '".$nim."')
          AND (`nilai`.`kdprodi` = `kurikulum`.`kdprodi`)
          AND (`nilai`.`kdmk` = `kurikulum`.`kdmk`))),0) AS `bobotnilai`
     , `kurikulum`.`jmlsks` AS `jmlsks`
     , ifnull(((SELECT `nilai`.`bobotnilai` AS `bobotnilai`
         FROM
           `nilai`
         WHERE
           ((`nilai`.`nim` = '".$nim."')
           AND (`nilai`.`kdprodi` = `kurikulum`.`kdprodi`)
           AND (`nilai`.`kdmk` = `kurikulum`.`kdmk`))) * `kurikulum`.`jmlsks`),0) AS `mutu`
FROM
  (`kurikulum`
JOIN `matakuliah`
ON (((`kurikulum`.`kdprodi` = `matakuliah`.`kdprodi`) AND (`kurikulum`.`kdmk` = `matakuliah`.`kdmk`))))
WHERE `kurikulum`.kdprodi ='".$kdprodi."' AND `kurikulum`.thnmasuk ='".$tahunmsk."'
ORDER BY
  `kurikulum`.`idsemester`
, `kurikulum`.`kdmk`
LIMIT
  ".$start.", ".$limit."");
            
        $result = array();
        if ($q->num_rows() > 0) {
            $result = $q->result();
        }
        return $result;
    }

	//=================================Fungsi cek jumlah record data semua nilai mahasiswa=============================================
	
	function cekjumlahvnilaiall($nim,$kdprodi,$tahunmsk){
	$KEDUA= $this->load->database('second', TRUE);  /*membuka koneksi database untuk d_mahasiswa*/
	$q = $KEDUA->query("SELECT `kurikulum`.`kdprodi` AS `kdprodi`
     , `kurikulum`.`thnmasuk` AS `thnmasuk`
     , `kurikulum`.`kdmk` AS `kdmk`
     , `matakuliah`.`nmmkind` AS `nmmkind`
     , `matakuliah`.`nmmkeng` AS `nmmkeng`
     , ifnull((SELECT `nilai`.`nilaihuruf` AS `nilaihuruf`
        FROM
          `nilai`
        WHERE
          ((`nilai`.`nim` = '".$nim."')
          AND (`nilai`.`kdprodi` = `kurikulum`.`kdprodi`)
          AND (`nilai`.`kdmk` = `kurikulum`.`kdmk`))),'-') AS `nilaihuruf`
     , ifnull((SELECT `nilai`.`bobotnilai` AS `bobotnilai`
        FROM
          `nilai`
        WHERE
          ((`nilai`.`nim` = '".$nim."')
          AND (`nilai`.`kdprodi` = `kurikulum`.`kdprodi`)
          AND (`nilai`.`kdmk` = `kurikulum`.`kdmk`))),0) AS `bobotnilai`
     , `kurikulum`.`jmlsks` AS `jmlsks`
     , ifnull(((SELECT `nilai`.`bobotnilai` AS `bobotnilai`
         FROM
           `nilai`
         WHERE
           ((`nilai`.`nim` = '".$nim."')
           AND (`nilai`.`kdprodi` = `kurikulum`.`kdprodi`)
           AND (`nilai`.`kdmk` = `kurikulum`.`kdmk`))) * `kurikulum`.`jmlsks`),0) AS `mutu`
FROM
  (`kurikulum`
JOIN `matakuliah`
ON (((`kurikulum`.`kdprodi` = `matakuliah`.`kdprodi`) AND (`kurikulum`.`kdmk` = `matakuliah`.`kdmk`))))
WHERE `kurikulum`.kdprodi ='".$kdprodi."' AND `kurikulum`.thnmasuk ='".$tahunmsk."'
ORDER BY
  `kurikulum`.`idsemester`
, `kurikulum`.`kdmk`");
 
        $result = 0;
        if ($q->num_rows() > 0) {
            $result = $q->num_rows();
        } else {
			$result = 0;
		}
        return $result;
    }
	
	//=================================Fungsi mencari data pmb=============================================

	function getpmb($wherex=''){
        
            $this->db->select("*");
            $this->db->from("pmb"); 
       
        if($wherex !=''){
            $this->db->where('nopmb',$wherex);
        }
            
        $q  = $this->db->get(); 
        $result = array();
        if ($q->num_rows() > 0) {
            $result = $q->result();
        }
        return $result;
    }
	
	//=================================Fungsi mencari data kas pmb=============================================

	function getkaspmb($cek1,$where1,$cek2,$where2,$where3,$cek4,$where4){
    
        $this->db->select("*");
        $this->db->from("v_kaspmb"); 
         
		
		if($cek1=='true'){
	   
			$this->db->where('left(nopmb,5) =', $where1);
	   
	   }
	   
	   if($cek2=='true'){

			$this->db->where("date(tglkuitansipmb) between '". $where2 ."' and '". $where3."'" );
	   
	   }
	   
	   if($cek4=='true'){

			$this->db->where('nominal', $where4);
	   
	   }
              
        $q  = $this->db->get(); 
        $result = array();
        if ($q->num_rows() > 0) {
            $result = $q->result();
        }
        return $result;
    }
// ===============================================================================================================
    
	// CETAK STUDENT BODY
    function pstudentbody($var)
     {
		$this->ceklog();
        $par                    = explode("istra", $var);
        $kdstsemester           = $par[0];
        $temp       			= $par[1];
		
		$thnakademikpar			= substr($temp,0,4);
		$semesterpar			= substr($temp,5,15);
        
      //  $this->cezpdf->ezText("<b>White Rabbit checking watch</b>",12,array("justification"=>"center"));
        
       //$text = "Software Developer Phone: 08987070737-Reza";
        
       // $this->cezpdf->stream();
        //====================================================
        $this->cezpdf->Cezpdf('A4','landscape'); //landscape
        $this->cezpdf->ezSetMargins(15,30,10,10); //kiri, atas, kanan, bawah
        
         
      //  $this->cezpdf->addText(150,100,10,"the quick <b>brown</b> fox jumps over the lazy dog!",0);
        
         $this->header('landscape'); 
        
         $this->cezpdf->ezSetDy(-50, 'makeSpace'); //separator space
//            
            $this->cezpdf->selectFont('./fonts/Helvetica-Bold');
            $this->cezpdf->ezText("<b>STUDENT BODY</b>", 12, array("justification" => "center")); //addText(350,450,14,"<b>STUDENT BODY</b> <i>dodol</i>",0);
            $this->cezpdf->selectFont('./fonts/Helvetica');
       
           // $head_1[]=array(   'kol1'=>'','kol2'=>'','kol3'=>'');
            $head_2[]=array(   'kol1'=>'Tahun Akademik - Semester: '.$thnakademikpar.'/'.$semesterpar,'kol2'=>'','kol3'=>'');
             
//            $this->cezpdf->ezTable($head_1, '', '', array( 'width'=>660,
//                                                              'fontSize' => 12,
//                                                              'showLines'=> 0,
//                                                              'showHeadings'=>0,
//                                                              'shaded'=>0,
//                                                              'cols'=>array(
//                                                                    'kol1'=>array('justification'=>'left'),
//                                                                    'kol2'=>array('justification'=>'center'),
//                                                                    'kol3'=>array('justification'=>'center'))));
//            
            $this->cezpdf->ezTable($head_2, '', '', array( 'width'=>660,
                                                              'fontSize' => 8, 
                                                              'showLines'=> 0,
                                                              'showHeadings'=>0,
                                                              'shaded'=>0,
                                                              'cols'=>array(
                                                                    'kol1'=>array('justification'=>'left'),
                                                                    'kol2'=>array('justification'=>'center'),
                                                                    'kol3'=>array('justification'=>'center'))));
            

    
       
// nourutprodi,nmprodi,nmjenjangstudi,jumlah,aktif,cuti,do,keluar,lulus,nonaktif,sdd
                    $col_names3[] = array(
                                    'nourutprodi2' =>'No.',
                                    'nmprodi' => 'Program Studi',
                                    'nmjenjangstudi' => 'Jenjang Studi',
                                    'jumlah' => 'Jumlah',
                                    'aktif' => 'Aktif',
                                    'cuti' => 'Cuti',
                                    'do' => 'DO',
                                    'keluar' => 'Keluar',
                                    'lulus' => 'Lulus',
                                    'nonaktif' => 'Non-Aktif',
                                    'sdd' => 'Sedang Double
 Degree',
                            );


                 $this->cezpdf->ezTable($col_names3, '', '', array('width'=>800,
                                                              'fontSize' => 8,
                                                              'showLines'=> 1,'showHeadings'=>0,'shaded'=>0,'justification'=>"right",
                                                              'cols'=>array(
                                                                  'nourutprodi2'=>array('width'=>40,'justification'=>"center"),
                                                                  'nmprodi'=>array('width'=>200,'justification'=>"center"),
                                                                  'nmjenjangstudi'=>array('width'=>60,'justification'=>"center"),
                                                                  'jumlah'=>array('width'=>40,'justification'=>"center"),
                                                                  'aktif'=>array('width'=>40,'justification'=>"center"),
                                                                  'cuti'=>array('width'=>40,'justification'=>"center"),
                                                                  'do'=>array('width'=>40,'justification'=>"center"),
                                                                  'keluar'=>array('width'=>40,'justification'=>"center"),
                                                                  'lulus'=>array('width'=>40,'justification'=>"center"),
                                                                  'nonaktif'=>array('width'=>40,'justification'=>"center"),
                                                                  'sdd'=>array('width'=>70,'justification'=>"center"),

                                                                )
                                                                ));

                        $all_jumlah =0;
                        $all_aktif =0;
                        $all_cuti =0;
                        $all_do =0;
                        $all_keluar =0;
                        $all_lulus =0;
                        $all_nonaktif =0;
                        $all_sdd =0;
                  // data table
                $so = $this->get_vstudentbody($kdstsemester);
                    $i = 0;
                    foreach($so as $item)
                    {
                        $i = ($i+1);
                        $db_data[] = array(//'no' => $i.'.',
                                    'nourutprodi' => $item->nourutprodi,
                                    'nmprodi' => $item->nmprodi,
                                    'nmjenjangstudi' => $item->nmjenjangstudi,//'Rp. '.number_format ($item->nmjenjangstudi),
                                    'jumlah' => $item->jumlah,
                                    'aktif' => $item->aktif,
                                    'cuti' => $item->cuti,
                                    'do' => $item->do,
                                    'keluar' => $item->keluar,
                                    'lulus' => $item->lulus,
                                    'nonaktif' => $item->nonaktif,
                                    'sdd' => $item->sdd,
                                      );
                        $all_jumlah +=  $item->jumlah;
                        $all_aktif += $item->aktif;
                        $all_cuti += $item->cuti;
                        $all_do += $item->do;
                        $all_keluar += $item->keluar;
                        $all_lulus += $item->lulus;
                        $all_nonaktif += $item->nonaktif;
                        $all_sdd += $item->sdd;
                    }
                 $this->cezpdf->ezTable($db_data, '', '', array('width'=>800,
                                                              'fontSize' => 8,
                                                              'showLines'=> 1,'showHeadings'=>0,'shaded'=>0,'justification'=>"right",
                                                              'cols'=>array(
                                                                  'nourutprodi'=>array('width'=>40,'justification'=>"center"),
                                                                  'nmprodi'=>array('width'=>200,'justification'=>"left"),
                                                                  'nmjenjangstudi'=>array('width'=>60,'justification'=>"center"),
                                                                  'jumlah'=>array('width'=>40,'justification'=>"right"),
                                                                  'aktif'=>array('width'=>40,'justification'=>"right"),
                                                                  'cuti'=>array('width'=>40,'justification'=>"right"),
                                                                  'do'=>array('width'=>40,'justification'=>"right"),
                                                                  'keluar'=>array('width'=>40,'justification'=>"right"),
                                                                  'lulus'=>array('width'=>40,'justification'=>"right"),
                                                                  'nonaktif'=>array('width'=>40,'justification'=>"right"),
                                                                  'sdd'=>array('width'=>70,'justification'=>"right"),

                                                                )
                                                                ));

           // data footer
             $col_namesfoot[] = array(  'nmjenjangstudi'=>'Total',
                                        'jumlah' => $all_jumlah,
                                        'aktif' => $all_aktif,
                                        'cuti' => $all_cuti,
                                        'do' => $all_do,
                                        'keluar' => $all_keluar,
                                        'lulus' => $all_lulus,
                                        'nonaktif' => $all_nonaktif,
                                        'sdd' => $all_sdd
                                    );


                 $this->cezpdf->ezTable($col_namesfoot, '', '', array('width'=>800,
                                                              'fontSize' => 8,
                                                              'showLines'=> 1,'showHeadings'=>0,'shaded'=>0,'justification'=>"right",
                                                              'cols'=>array(
                                                                  'nmjenjangstudi'=>array('width'=>300,'justification'=>"right"),
                                                                  'jumlah'=>array('width'=>40,'justification'=>"right"),
                                                                  'aktif'=>array('width'=>40,'justification'=>"right"),
                                                                  'cuti'=>array('width'=>40,'justification'=>"right"),
                                                                  'do'=>array('width'=>40,'justification'=>"right"),
                                                                  'keluar'=>array('width'=>40,'justification'=>"right"),
                                                                  'lulus'=>array('width'=>40,'justification'=>"right"),
                                                                  'nonaktif'=>array('width'=>40,'justification'=>"right"),
                                                                  'sdd'=>array('width'=>70,'justification'=>"right"),

                                                                )
                                                                ));

                 
            $this->cezpdf->ezText('',10,array('justification' => 'left'));
          //  $this->cezpdf->ezText('   Jumlah data: '.$i ,8,array('justification' => 'left'));
           

        $this->cezpdf->ezStream(array('Content-Disposition'=>'StudentBody'.$kdstsemester.'.pdf'));
     }
     
	 // CETAK LAPORAN PMB
     function plpmb($var)
     {
		$this->ceklog();
        $par                    = explode("istra", $var);
        $idjadwalpmb           = $par[0];
        $kdprodi               = $par[1];
        
        $this->cezpdf->Cezpdf('A4','landscape'); //landscape
        $this->cezpdf->ezSetMargins(15,30,10,10); //kiri, atas, kanan, bawah
      
          $this->header('landscape'); 
        
            $this->cezpdf->ezSetDy(-50, 'makeSpace'); //separator space
//            
            $this->cezpdf->selectFont('./fonts/Helvetica-Bold');
            $this->cezpdf->ezText("<b>LAPORAN PENERIMAAN MAHASISWA BARU</b>", 12, array("justification" => "center")); //addText(350,450,14,"<b>STUDENT BODY</b> <i>dodol</i>",0);
            $this->cezpdf->selectFont('./fonts/Helvetica');
       
            $jadwalpmbshow = $this->nm_field('nmjadwalpmb','jadwalpmb','idjadwalpmb',$idjadwalpmb);
            $prodishow = $this->nm_field('nmprodi','v_prodi','kdprodi',$kdprodi).' '. $this->nm_field('nmjenjangstudi','v_prodi','kdprodi',$kdprodi);
            
            $this->cezpdf->ezSetDy(-20, 'makeSpace'); //separator space
            $head_2[]=array(   'kol1'=>'Jadwal PMB                : '.$jadwalpmbshow,'kol2'=>'','kol3'=>'');
            $head_2[]=array(   'kol1'=>'Program Studi             : '.$prodishow,'kol2'=>'','kol3'=>'');
             
         
            $this->cezpdf->ezTable($head_2, '', '', array( 'width'=>660,
                                                              'fontSize' => 8, 
                                                              'showLines'=> 0,
                                                              'showHeadings'=>0,
                                                              'shaded'=>0,
                                                              'cols'=>array(
                                                                    'kol1'=>array('justification'=>'left'),
                                                                    'kol2'=>array('justification'=>'center'),
                                                                    'kol3'=>array('justification'=>'center'))));
            
            $col_names3[] = array(
                                    'nourut' =>'No.',
                                    'nopmb' => 'No. PMB',
                                    'peserta' => 'Peserta',
                                    'lp' => '(L/P)',
                                    'prodi' => 'Program Studi',
                                    'kelas' => 'Kelas',
                                    's_pendaftar' => 'Status Pendaftar'
                            );


                 $this->cezpdf->ezTable($col_names3, '', '', array('width'=>800,
                                                              'fontSize' => 8,
                                                              'showLines'=> 1,'showHeadings'=>0,'shaded'=>0,'justification'=>"right",
                                                              'cols'=>array(
                                                                  'nourut'=>array('width'=>40,'justification'=>"center"),
                                                                  'nopmb'=>array('width'=>100,'justification'=>"center"),
                                                                  'peserta'=>array('width'=>150,'justification'=>"center"),
                                                                  'lp'=>array('width'=>40,'justification'=>"center"),
                                                                  'prodi'=>array('width'=>140,'justification'=>"center"),
                                                                  'kelas'=>array('width'=>90,'justification'=>"center"),
                                                                  's_pendaftar'=>array('width'=>90,'justification'=>"center"),
                                                                  
                                                                )
                                                                ));

//                  // data table
                 $where['idjadwalpmb']=$idjadwalpmb; 
                 $where['kdprodi']=$kdprodi; 
        
                 $so = $this->getlpmb($where);
                    $i = 0;
                    foreach($so as $item)
                    {
                        $i = ($i+1);
                        $db_data[] = array('nourut' => $i.'.',
                                    'nopmb' => $item->nopmb,
                                    'peserta' => $item->nama,//'Rp. '.number_format ($item->nmjenjangstudi),
                                    'lp' => $item->kdjk,
                                    'prodi' => $this->nm_field('nourutprodi','v_prodi','kdprodi',$item->kdprodi1).'.'.$this->nm_field('nmprodi','v_prodi','kdprodi',$item->kdprodi1).' '. $this->nm_field('nmjenjangstudi','v_prodi','kdprodi',$item->kdprodi1),
                                    'kelas' => $item->nmklsmhs,
                                    's_pendaftar' => $item->nmstmskmhs,
                                    
                                      );
                      
                    }
                 $this->cezpdf->ezTable($db_data, '', '', array('width'=>800,
                                                              'fontSize' => 8,
                                                              'showLines'=> 1,'showHeadings'=>0,'shaded'=>0,'justification'=>"right",
                                                              'cols'=>array(
                                                                  'nourut'=>array('width'=>40,'justification'=>"center"),
                                                                  'nopmb'=>array('width'=>100,'justification'=>"center"),
                                                                  'peserta'=>array('width'=>150,'justification'=>"center"),
                                                                  'lp'=>array('width'=>40,'justification'=>"center"),
                                                                  'prodi'=>array('width'=>140,'justification'=>"center"),
                                                                  'kelas'=>array('width'=>90,'justification'=>"center"),
                                                                  's_pendaftar'=>array('width'=>90,'justification'=>"center"),
                                                               
                                                                )
                                                                ));

                 
            $this->cezpdf->ezText('',10,array('justification' => 'left'));
          //  $this->cezpdf->ezText('   Jumlah data: '.$i ,8,array('justification' => 'left'));
           

        $this->cezpdf->ezStream(array('Content-Disposition'=>'LaporanPMB.pdf'));
     }

	 // CETAK LAPORAN PENDAFTAR TERVERIFIKASI
      function plpendaftarterverifikasi($var)
     {
		$this->ceklog();
        $par                    = explode("istra", $var);
        $idjadwalpmb           = $par[0];
        $kdprodi               = $par[1];
        
        $this->cezpdf->Cezpdf('A4','landscape'); //landscape
        $this->cezpdf->ezSetMargins(15,30,10,10); //kiri, atas, kanan, bawah
      
          $this->header('landscape'); 
        
            $this->cezpdf->ezSetDy(-50, 'makeSpace'); //separator space
//            
            $this->cezpdf->selectFont('./fonts/Helvetica-Bold');
            $this->cezpdf->ezText("<b>LAPORAN PENDAFTAR TERVERIFIKASI</b>", 12, array("justification" => "center")); //addText(350,450,14,"<b>STUDENT BODY</b> <i>dodol</i>",0);
            $this->cezpdf->selectFont('./fonts/Helvetica');
       
            $jadwalpmbshow = $this->nm_field('nmjadwalpmb','jadwalpmb','idjadwalpmb',$idjadwalpmb);
            $prodishow = $this->nm_field('nmprodi','v_prodi','kdprodi',$kdprodi).' '. $this->nm_field('nmjenjangstudi','v_prodi','kdprodi',$kdprodi);
            
            $this->cezpdf->ezSetDy(-20, 'makeSpace'); //separator space
            $head_2[]=array(   'kol1'=>'Jadwal PMB                : '.$jadwalpmbshow,'kol2'=>'','kol3'=>'');
            $head_2[]=array(   'kol1'=>'Program Studi             : '.$prodishow,'kol2'=>'','kol3'=>'');
             
         
            $this->cezpdf->ezTable($head_2, '', '', array( 'width'=>660,
                                                              'fontSize' => 8, 
                                                              'showLines'=> 0,
                                                              'showHeadings'=>0,
                                                              'shaded'=>0,
                                                              'cols'=>array(
                                                                    'kol1'=>array('justification'=>'left'),
                                                                    'kol2'=>array('justification'=>'center'),
                                                                    'kol3'=>array('justification'=>'center'))));
            
            $col_names3[] = array(
                                    'nourut' =>'No.',
                                    'nopmb' => 'No. PMB',
                                    'nousm' => 'No. USM',
                                    'peserta' => 'Peserta',
                                    'lp' => '(L/P)',
                                    'prodi' => 'Program Studi',
                                    'kelas' => 'Kelas',
                                    's_pendaftar' => 'Status Pendaftar'
                            );


                 $this->cezpdf->ezTable($col_names3, '', '', array('width'=>800,
                                                              'fontSize' => 8,
                                                              'showLines'=> 1,'showHeadings'=>0,'shaded'=>0,'justification'=>"right",
                                                              'cols'=>array(
                                                                  'nourut'=>array('width'=>40,'justification'=>"center"),
                                                                  'nopmb'=>array('width'=>80,'justification'=>"center"),
                                                                  'nousm'=>array('width'=>80,'justification'=>"center"),
                                                                  'peserta'=>array('width'=>150,'justification'=>"center"),
                                                                  'lp'=>array('width'=>30,'justification'=>"center"),
                                                                  'prodi'=>array('width'=>140,'justification'=>"center"),
                                                                  'kelas'=>array('width'=>70,'justification'=>"center"),
                                                                  's_pendaftar'=>array('width'=>70,'justification'=>"center"),
                                                                     
                                                                )
                                                                ));

//                  // data table
                 $where['idjadwalpmb']=$idjadwalpmb; 
                 $where['kdprodi']=$kdprodi; 
        
                 $so = $this->getllpterverifikasi($where);
                    $i = 0;
                    foreach($so as $item)
                    {
                        $i = ($i+1);
                        $db_data[] = array('nourut' => $i.'.',
                                    'nopmb' => $item->nopmb,
                                    'nousm' => $item->nousm,
                                    'peserta' => $item->nama,//'Rp. '.number_format ($item->nmjenjangstudi),
                                    'lp' => $item->kdjk,
                                    'prodi' => $this->nm_field('nourutprodi','v_prodi','kdprodi',$item->kdprodi1).'.'.$this->nm_field('nmprodi','v_prodi','kdprodi',$item->kdprodi1).' '. $this->nm_field('nmjenjangstudi','v_prodi','kdprodi',$item->kdprodi1),
                                    'kelas' => $item->nmklsmhs,
                                    's_pendaftar' => $item->nmstmskmhs,
                                    
                                      );
                      
                    }
                 $this->cezpdf->ezTable($db_data, '', '', array('width'=>800,
                                                              'fontSize' => 8,
                                                              'showLines'=> 1,'showHeadings'=>0,'shaded'=>0,'justification'=>"right",
                                                              'cols'=>array(
                                                                  'nourut'=>array('width'=>40,'justification'=>"center"),
                                                                  'nopmb'=>array('width'=>80,'justification'=>"center"),
                                                                  'nousm'=>array('width'=>80,'justification'=>"center"),
                                                                  'peserta'=>array('width'=>150,'justification'=>"center"),
                                                                  'lp'=>array('width'=>30,'justification'=>"center"),
                                                                  'prodi'=>array('width'=>140,'justification'=>"center"),
                                                                  'kelas'=>array('width'=>70,'justification'=>"center"),
                                                                  's_pendaftar'=>array('width'=>70,'justification'=>"center"),
                                                                 
                                                                )
                                                                ));

                 
            $this->cezpdf->ezText('',10,array('justification' => 'left'));
          //  $this->cezpdf->ezText('   Jumlah data: '.$i ,8,array('justification' => 'left'));
           

        $this->cezpdf->ezStream(array('Content-Disposition'=>'LaporanPendaftarTerverifikasi.pdf'));
     }
     
	 // CETAK KWITANSI PMB
     function pcetakskwitansi($var)
      
     {
		$this->ceklog();
        $par             = explode("istra", $var);
        $nokw           = $par[0];
        $nokuitansiy="";
		$nopmby= "";
        $namay="";
		$nominaly="";
		$pembayarany= "";
		$tglkuitansiy="";
		$untukbayary= "";
        $nominaly= "";
		$terbilangy="";
		$statusmasuk= "";
		$passwordy="";
        $so = $this->getkwitansi($nokw);
                    foreach($so as $item)
                    {
					if ($item->nokuitansipmb) {
                       $nokuitansiy= $item->nokuitansipmb;
					   $nopmby= $item->nopmb;
                       $namay= $item->nama;
					   $tglkuitansiy= $this->TanggalIndo(date("Ymd",strtotime($item->tglkuitansipmb)));
                       $pembayarany= $item->nmjadwalpmb;
                       $nominaly= 'Rp. '.number_format($item->jmlbayar);
					   $terbilangy= trim($this->terbilang($item->jmlbayar).' Rupiah');
					   $statusmasuk= $item->nmstmskmhs;
					   $passwordy= $item->password;
                       }
                    }
        
     
        //====================================================
        $this->cezpdf->Cezpdf('A4','landscape'); //landscape
        $this->cezpdf->ezSetMargins(15,30,10,10); //kiri, atas, kanan, bawah
        
         
      //  $this->cezpdf->addText(150,100,10,"the quick <b>brown</b> fox jumps over the lazy dog!",0);
        $tambah = 0;
        $kurang = 70;
        
        $image = imagecreatefrompng(base_url().'resources/img/report.png');
        $this->cezpdf->addImage($image,100+$tambah-$kurang,475,110);
        
       

        //head info laporan
        $this->cezpdf->addText(518+$tambah-$kurang+150,535,12,"Nomor Kuitansi   : ".$nokuitansiy,0);
		$this->cezpdf->addText(518+$tambah-$kurang+150,515,12,"Nomor PMB        : ".$nopmby,0);
        
        $this->cezpdf->selectFont('./fonts/Helvetica-Bold');
        $this->cezpdf->addText(218+$tambah-$kurang,535,14,"PANITIA PENERIMAAN MAHASISWA BARU",0);
        $this->cezpdf->addText(218+$tambah-$kurang,515,18,"UNIVERSITAS LANGLANGBUANA",0);
        
        $this->cezpdf->selectFont('./fonts/Helvetica');
        $this->cezpdf->addText(218+$tambah-$kurang,500,12,"Jl. Karapitan No. 116 Bandung 40261, Telp. 022-4218084, Fax. 022-4237144",0);
        
        
            $this->cezpdf->ezSetDy(-120, 'makeSpace'); //separator space
//            
            $this->cezpdf->selectFont('./fonts/Helvetica-Bold');
            $this->cezpdf->ezText("<b>KUITANSI</b>", 16, array("justification" => "center")); 
            $this->cezpdf->selectFont('./fonts/Helvetica');
           
            $this->cezpdf->ezSetDy(-20, 'makeSpace'); //separator space
            
            $head_2[]=array('kol1'=>'Sudah diterima dari','kol2'=>':'.$namay,'kol3'=>'');
            $head_2[]=array('kol1'=>'Untuk Pembayaran','kol2'=>':'.$pembayarany,'kol3'=>'');
			$head_2[]=array('kol1'=>'Uang Sebesar','kol2'=>':'.$nominaly,'kol3'=>'');
			$head_2[]=array('kol1'=>'Terbilang','kol2'=>':'.$terbilangy,'kol3'=>'');       
            
            $this->cezpdf->ezTable($head_2, '', '', array( 'width'=>500,
                                                              'fontSize' => 12, 
                                                              'showLines'=> 0,
                                                              'showHeadings'=>0,
                                                              'shaded'=>0,
                                                              'cols'=>array(
                                                                    'kol1'=>array('justification'=>'left','width' => 200),
                                                                    'kol2'=>array('justification'=>'left','width' => 300),
                                                                    'kol3'=>array('justification'=>'left')
                                                                    )));
       
            $this->cezpdf->ezSetDy(-20, 'makeSpace'); //separator space
            
            $head_3[]=array('kol0'=>'','kol1'=>'','kol2'=>'','kol3'=>'Bandung, '.$tglkuitansiy);
            $head_3[]=array('kol0'=>'','kol1'=>'','kol2'=>'','kol3'=>'Petugas Pendaftaran');
            $head_3[]=array('kol0'=>'','kol1'=>'','kol2'=>'','kol3'=>'');
			$head_3[]=array('kol0'=>'','kol1'=>'','kol2'=>'','kol3'=>'');
			
			
            $head_3[]=array('kol0'=>'','kol1'=>'','kol2'=>'','kol3'=>'');
            $head_3[]=array('kol0'=>'','kol1'=>'','kol2'=>'','kol3'=>'');
            $head_3[]=array('kol0'=>'','kol1'=>'','kol2'=>'','kol3'=>'');
		
        
            $head_3[]=array('kol0'=>'','kol1'=>'','kol2'=>'','kol3'=>$this->my_usessionpublic->userdata('username1unla'));
            $head_3[]=array('kol0'=>'','kol1'=>'','kol2'=>'','kol3'=>'');
            $head_3[]=array('kol0'=>'','kol1'=>'Lembar Ke-1 (Putih) untuk    : Peserta','kol2'=>'','kol3'=>'');
            $head_3[]=array('kol0'=>'','kol1'=>'Lembar Ke-2 (Merah) untuk  : PPMB','kol2'=>'','kol3'=>'');
           // $head_3[]=array('kol0'=>'','kol1'=>'Lembar Ke-3 (Kuning) untuk : UNLA','kol2'=>'','kol3'=>'');
			$head_3[]=array('kol0'=>'','kol1'=>'','kol2'=>'','kol3'=>'');
			$head_3[]=array('kol0'=>'','kol1'=>'Password PMB Online :'.$passwordy,'kol2'=>'','kol3'=>'');
            
            
            $this->cezpdf->ezTable($head_3, '', '', array( 'width'=>660,
                                                              'fontSize' => 12, 
                                                              'showLines'=> 0,
                                                              'showHeadings'=>0,
                                                              'shaded'=>0,
                                                              'cols'=>array(
                                                                    'kol0'=>array('justification'=>'justify','width' => 85),
                                                                    'kol1'=>array('justification'=>'justify','width' => 290),
                                                                    'kol2'=>array('justification'=>'right','width' => 135),
                                                                    'kol3'=>array('justification'=>'center','width' => 250))));

                 
            $this->cezpdf->ezText('',10,array('justification' => 'left'));
          //  $this->cezpdf->ezText('   Jumlah data: '.$i ,8,array('justification' => 'left'));
           

        $this->cezpdf->ezStream(array('Content-Disposition'=>$nokuitansiy.'.pdf'));
     }

	 // CETAK HASIL USM
    function plhasilusm($var)
     {
		$this->ceklog();
        $par                    = explode("istra", $var);
        $idjadwalpmb           = $par[0];
        $idstusm               = $par[1];
        
        $this->cezpdf->Cezpdf('A4','landscape'); //landscape
        $this->cezpdf->ezSetMargins(15,30,10,10); //kiri, atas, kanan, bawah
      
        $this->header('landscape'); 
        
            $this->cezpdf->ezSetDy(-50, 'makeSpace'); //separator space
//            
            $this->cezpdf->selectFont('./fonts/Helvetica-Bold');
            $this->cezpdf->ezText("<b>LAPORAN HASIL USM</b>", 12, array("justification" => "center")); //addText(350,450,14,"<b>STUDENT BODY</b> <i>dodol</i>",0);
            $this->cezpdf->selectFont('./fonts/Helvetica');
       
            $jadwalpmbshow = $this->nm_field('nmjadwalpmb','jadwalpmb','idjadwalpmb',$idjadwalpmb);
            $statususmshow = $this->nm_field('nmstusm','stusm','idstusm',$idstusm);
            
            $this->cezpdf->ezSetDy(-20, 'makeSpace'); //separator space
            $head_2[]=array(   'kol1'=>'Jadwal PMB                : '.$jadwalpmbshow,'kol2'=>'','kol3'=>'');
            $head_2[]=array(   'kol1'=>'Status USM                : '.$statususmshow,'kol2'=>'','kol3'=>'');
             
         
            $this->cezpdf->ezTable($head_2, '', '', array( 'width'=>660,
                                                              'fontSize' => 8, 
                                                              'showLines'=> 0,
                                                              'showHeadings'=>0,
                                                              'shaded'=>0,
                                                              'cols'=>array(
                                                                    'kol1'=>array('justification'=>'left'),
                                                                    'kol2'=>array('justification'=>'center'),
                                                                    'kol3'=>array('justification'=>'center'))));
            
            $col_names3[] = array(
                                    'nourut' =>'No.',
                                    'nopmb' => 'No. PMB',
                                    'nousm' => 'No. USM',
                                    'peserta' => 'Peserta',
                                    'lp' => '(L/P)',
                                    'prodi' => 'Program Studi',
                                    'kelas' => 'Kelas',
                                    's_pendaftar' => 'Status Pendaftar'
                            );


                 $this->cezpdf->ezTable($col_names3, '', '', array('width'=>800,
                                                              'fontSize' => 8,
                                                              'showLines'=> 1,'showHeadings'=>0,'shaded'=>0,'justification'=>"right",
                                                              'cols'=>array(
                                                                  'nourut'=>array('width'=>40,'justification'=>"center"),
                                                                  'nopmb'=>array('width'=>80,'justification'=>"center"),
                                                                  'nousm'=>array('width'=>80,'justification'=>"center"),
                                                                  'peserta'=>array('width'=>150,'justification'=>"center"),
                                                                  'lp'=>array('width'=>30,'justification'=>"center"),
                                                                  'prodi'=>array('width'=>140,'justification'=>"center"),
                                                                  'kelas'=>array('width'=>70,'justification'=>"center"),
                                                                  's_pendaftar'=>array('width'=>70,'justification'=>"center"),
                                                                 
                                                                )
                                                                ));

//                  // data table
                 $where['idjadwalpmb']=$idjadwalpmb; 
                 $where['idstusm']=$idstusm; 
        
                 $so = $this->getlhasilusm($where);
                    $i = 0;
                    foreach($so as $item)
                    {
                        $i = ($i+1);
                        $db_data[] = array('nourut' => $i.'.',
                                    'nopmb' => $item->nopmb,
                                    'nousm' => $item->nousm,
                                    'peserta' => $item->nama,//'Rp. '.number_format ($item->nmjenjangstudi),
                                    'lp' => $item->kdjk,
                                    'prodi' => $this->nm_field('nourutprodi','v_prodi','kdprodi',$item->kdprodi).'.'.$this->nm_field('nmprodi','v_prodi','kdprodi',$item->kdprodi).' '. $this->nm_field('nmjenjangstudi','v_prodi','kdprodi',$item->kdprodi),
                                    'kelas' => $item->nmklsmhs,
                                    's_pendaftar' => $item->nmstmskmhs,
                                    
                                      );
                      
                    }
                 $this->cezpdf->ezTable($db_data, '', '', array('width'=>800,
                                                              'fontSize' => 8,
                                                              'showLines'=> 1,'showHeadings'=>0,'shaded'=>0,'justification'=>"right",
                                                              'cols'=>array(
                                                                  'nourut'=>array('width'=>40,'justification'=>"center"),
                                                                  'nopmb'=>array('width'=>80,'justification'=>"center"),
                                                                  'nousm'=>array('width'=>80,'justification'=>"center"),
                                                                  'peserta'=>array('width'=>150,'justification'=>"center"),
                                                                  'lp'=>array('width'=>30,'justification'=>"center"),
                                                                  'prodi'=>array('width'=>140,'justification'=>"center"),
                                                                  'kelas'=>array('width'=>70,'justification'=>"center"),
                                                                  's_pendaftar'=>array('width'=>70,'justification'=>"center"),
                                                               
                                                                )
                                                                ));

                 
            $this->cezpdf->ezText('',10,array('justification' => 'left'));
            $this->cezpdf->ezStream(array('Content-Disposition'=>'LaporanHasilUSM-'.$idjadwalpmb.'-'.$statususmshow.'.pdf'));
     } 
     
	 // CETAK REKAP PMB
    function plrpmb($var="")
     {
		$this->ceklog();
		$par                    = explode("istra", $var);
        $idjadwalpmb           = $par[0];
       
        $this->cezpdf->Cezpdf('A4','landscape'); //landscape
        $this->cezpdf->ezSetMargins(15,30,10,10); //kiri, atas, kanan, bawah
      
         $this->header('landscape'); 
         
            $this->cezpdf->ezSetDy(-50, 'makeSpace'); //separator space
//            
            $this->cezpdf->selectFont('./fonts/Helvetica-Bold');
            $this->cezpdf->ezText("<b>REKAPITULASI PENERIMAAN MAHASISWA BARU</b>", 12, array("justification" => "center")); //addText(350,450,14,"<b>STUDENT BODY</b> <i>dodol</i>",0);
            $this->cezpdf->selectFont('./fonts/Helvetica');
       
            $jadwalpmbshow = $this->nm_field('nmjadwalpmb','jadwalpmb','idjadwalpmb',$idjadwalpmb);
            
            $this->cezpdf->ezSetDy(-20, 'makeSpace'); //separator space
            $head_2[]=array('kol1'=>'Jadwal PMB  : '.$jadwalpmbshow,'kol2'=>'','kol3'=>'');
             
         
            $this->cezpdf->ezTable($head_2, '', '', array( 'width'=>780,
                                                              'fontSize' => 8, 
                                                              'showLines'=> 0,
                                                              'showHeadings'=>0,
                                                              'shaded'=>0,
                                                              'cols'=>array(
                                                                    'kol1'=>array('justification'=>'left'),
                                                                    'kol2'=>array('justification'=>'center'),
                                                                    'kol3'=>array('justification'=>'center'))));
            
            $col_names3[] = array(
                                   // 'nourut' =>'No.',
                                    'nmprodi' => 'Fakultas / Program Studi',
                                    'jmlpendaftar' => 'Jumlah Pendaftar',
									'jmlpendaftarhariini' => 'Jumlah Pendaftar Hari Ini',
									'jmlpendaftarsampaihariini' => 'Jumlah Pendaftar Sampai Hari Ini',
                                    'pendaftar' => 'Pendaftar Terverifikasi',
                                    'diterima' => 'Diterima',
									'regular' => 'Regular',
									'nonregular' => 'Non Regular',
                                   );


                 $this->cezpdf->ezTable($col_names3, '', '', array('width'=>800,
                                                              'fontSize' => 8,
                                                              'showLines'=> 1,'showHeadings'=>0,'shaded'=>0,'justification'=>"right",
                                                              'cols'=>array(
                                                                  'nmprodi'=>array('width'=>200,'justification'=>"center"),
                                                                  'jmlpendaftar'=>array('width'=>90,'justification'=>"center"),
																  'jmlpendaftarhariini'=>array('width'=>90,'justification'=>"center"),
																  'jmlpendaftarsampaihariini'=>array('width'=>90,'justification'=>"center"),
                                                                  'pendaftar'=>array('width'=>90,'justification'=>"center"),
                                                                  'diterima'=>array('width'=>70,'justification'=>"center"),
																  'regular'=>array('width'=>70,'justification'=>"center"),
																  'nonregular'=>array('width'=>70,'justification'=>"center"),
                                                                )
                                                                ));

//                  // data table
					$where['idjadwalpmb']=$idjadwalpmb;
					$kdfakultas='';
					$num=1;
					
             
 
                             $so22 = $this->getlrpmb($where);   
                                foreach($so22 as $item22)
                                {
                                   
                                        if($item22->kdfakultas == $kdfakultas){
										
										$db_data2[] = array(
                                            'nmprodi2' => ' - '.$item22->nmprodi,
                                            'jmlpendaftar2' => $item22->jmlpendaftar,
											'jmlpendaftarhariini2' => $item22->jmlpendaftarhariini,
											'jmlpendaftarsampaihariini2' => $item22->jmlpendaftarsampaihariini,
                                            'pendaftar2' => $item22->terverifikasi,
                                            'diterima2' => $item22->diterima,
											'regular2' => $item22->regular,
											'nonregular2' => $item22->nonregular,
                                              );
											  
										} else {
											
											$kdfakultas = $item22->kdfakultas;
											
											$db_data2[] = array(
                                            'nmprodi2' => $num++.'. FAKULTAS '.$item22->nmfakultas,
                                            'jmlpendaftar2' => '',
											'jmlpendaftarhariini2' => '',
											'jmlpendaftarsampaihariini2' => '',
                                            'pendaftar2' => '',
                                            'diterima2' => '',
											'regular2' => '',
											'nonregular2' => '',
                                              );
											  
											$db_data2[] = array(
                                            'nmprodi2' => ' - '.$item22->nmprodi,
                                            'jmlpendaftar2' => $item22->jmlpendaftar,
											'jmlpendaftarhariini2' => $item22->jmlpendaftarhariini,
											'jmlpendaftarsampaihariini2' => $item22->jmlpendaftarsampaihariini,
                                            'pendaftar2' => $item22->terverifikasi,
                                            'diterima2' => $item22->diterima,
											'regular2' => $item22->regular,
											'nonregular2' => $item22->nonregular,
                                              );
										}
                                        
                                       
                                    }
                                    
                           
                                 $this->cezpdf->ezTable($db_data2, '', '', array('width'=>800,
                                                              'fontSize' => 8,
                                                              'showLines'=> 1,'showHeadings'=>0,'shaded'=>0,'justification'=>"right",
                                                              'cols'=>array(
                                                                  'nmprodi2'=>array('width'=>200,'justification'=>"left"),
                                                                  'jmlpendaftar2'=>array('width'=>90,'justification'=>"center"),
																  'jmlpendaftarhariini2'=>array('width'=>90,'justification'=>"center"),
																  'jmlpendaftarsampaihariini2'=>array('width'=>90,'justification'=>"center"),
                                                                  'pendaftar2'=>array('width'=>90,'justification'=>"center"),
                                                                  'diterima2'=>array('width'=>70,'justification'=>"center"),
																  'regular2'=>array('width'=>70,'justification'=>"center"),
																  'nonregular2'=>array('width'=>70,'justification'=>"center"),
                                                                 )
                                                                ));
                            
                    //}
                     $this->cezpdf->ezTable($db_data, '', '', array('width'=>800,
                                                              'fontSize' => 8,
                                                              'showLines'=> 1,'showHeadings'=>0,'shaded'=>0,'justification'=>"right",
                                                              'cols'=>array(
                                                                  'nmprodi'=>array('width'=>200,'justification'=>"left"),
                                                                  'jmlpendaftar'=>array('width'=>90,'justification'=>"center"),
																  'jmlpendaftarhariini'=>array('width'=>90,'justification'=>"center"),
																  'jmlpendaftarsampaihariini'=>array('width'=>90,'justification'=>"center"),
                                                                  'pendaftar'=>array('width'=>90,'justification'=>"center"),
                                                                  'diterima'=>array('width'=>70,'justification'=>"center"),
																  'regular'=>array('width'=>70,'justification'=>"center"),
																  'nonregular'=>array('width'=>70,'justification'=>"center"),
                                                                 )
                                                                ));
                 
            $this->cezpdf->ezText('',10,array('justification' => 'left'));
            $this->cezpdf->ezStream(array('Content-Disposition'=>'LaporanRekapPMB-'.$idjadwalpmb.'.pdf'));
     } 
     
	 // CETAK KARTU HASIL STUDI
     function pkartuhasilstudi($var)
     {
		$this->ceklog();
        $par                   = explode("istra", $var);
        $nim          = $par[0];
		$ipkup        = $par[1];
		$kdstsemester = $par[2];
		$idsemester   = $par[3];

        
        $this->cezpdf->Cezpdf('A4','potrait'); //landscape
        $this->cezpdf->ezSetMargins(4,3,3,3); //kiri, atas, kanan, bawah
      
         $this->header('potrait'); 
         
            $this->cezpdf->ezSetDy(-30, 'makeSpace'); //separator space
//            
            $this->cezpdf->selectFont('./fonts/Helvetica-Bold');
            $this->cezpdf->ezText("<b>KARTU HASIL STUDI</b>", 12, array("justification" => "center")); //addText(350,450,14,"<b>STUDENT BODY</b> <i>dodol</i>",0);
            $this->cezpdf->selectFont('./fonts/Helvetica');
       
            
            $this->cezpdf->ezSetDy(-20, 'makeSpace'); //separator space
            
            $where=$nim;

			$pimpin2='';			
            $so = $this->getmahasiswa($where);
        
            foreach($so as $item):
                    $head_2[]=array(   'kol1'=>'NAMA','kol2'=>': '.$item->nmmhs,'kol3'=>'');
                    $head_2[]=array(   'kol1'=>'NPM' ,'kol2'=>': '.$item->nim,'kol3'=>'');
                    $head_2[]=array(   'kol1'=>'FAKULTAS','kol2'=>': '.$item->nmfakultas,'kol3'=>'');
                    $head_2[]=array(   'kol1'=>'PROGRAM STUDI','kol2'=>': '.$item->nmprodi,'kol3'=>'');
                    $head_2[]=array(   'kol1'=>'JENJANG STUDI','kol2'=>': '.$item->nmjenjangstudi,'kol3'=>'');
                    $head_2[]=array(   'kol1'=>'TAHUN AKADEMIK','kol2'=>': '.$item->kdthnakademik,'kol3'=>'');
                    $head_2[]=array(   'kol1'=>'SEMESTER','kol2'=>': '.$item->nmjnssemester,'kol3'=>'');
                    $head_2[]=array(   'kol1'=>'DOSEN WALI','kol2'=>': '.$this->get_wali('nmdostpgelar', 'dosen', 'nidu',$this->get_wali('nidu', 'dosenwalidet', 'nim',$item->nim)),'kol3'=>'');
					
					$pimpin2=$this->nm_field2('nmpimpinan', 'pimpinan', 'idpimpinan',$item->idpimpinan2);
                    
            endforeach;
                    
            $this->cezpdf->ezTable($head_2, '', '', array( 'width'=>500,
                                                              'fontSize' => 8, 
                                                              'showLines'=> 0,
                                                              'showHeadings'=>0,
                                                              'shaded'=>0,
                                                              'cols'=>array(
                                                                    'kol1'=>array('width'=>100,'justification'=>'left'),
                                                                    'kol2'=>array('width'=>150,'justification'=>'left'),
                                                                    'kol3'=>array('width'=>230,'justification'=>'center'))));
             $this->cezpdf->ezSetDy(-30, 'makeSpace'); //separator space
            $col_names3[] = array(
                                    'nourut' =>'No.',
                                    'kode' => 'Kode',
                                    'matakuliah' => 'Mata Kuliah',
                                    'hm' => 'HM',
                                    'am' => 'AM',
                                    'sks' => 'sks',
                                    'm' => 'M'
                            );


                 $this->cezpdf->ezTable($col_names3, '', '', array('width'=>500,
                                                              'fontSize' => 8,
                                                              'showLines'=> 1,'showHeadings'=>0,'shaded'=>0,'justification'=>"right",
                                                              'cols'=>array(
                                                                  'nourut'=>array('width'=>35,'justification'=>"center"),
                                                                  'kode'=>array('width'=>55,'justification'=>"center"),
                                                                  'matakuliah'=>array('width'=>225,'justification'=>"center"),
                                                                  'hm'=>array('width'=>40,'justification'=>"center"),
                                                                  'am'=>array('width'=>40,'justification'=>"center"),
                                                                  'sks'=>array('width'=>40,'justification'=>"center"),
                                                                  'm'=>array('width'=>40,'justification'=>"center"),
                                                                          
                                                                )
                                                                ));

                  // data table
                    $where2['nim']=$nim;
                    $where2['kdstsemester']=$kdstsemester;
                    $where2['idsemester']=$idsemester;
                    
                    $so2 = $this->getvnilai($where2);
                    $i = 0;
                    $jmlsksxmutu=0;
                    $jmlsks=0;
					$jmlmutu=0;
                    foreach($so2 as $item2)
                    {
                        $i = ($i+1);
                        $db_data[] = array('nourut' => $i.'.',
                                    'kode' => $item2->kdmk,
                                    'matakuliah' => $item2->nmmkind,
                                    'hm' => $item2->nilaihuruf,//'Rp. '.number_format ($item->nmjenjangstudi),
                                    'am' => $item2->bobotnilai,
                                    'sks' => $item2->jmlsks,
                                    'm' => $item2->mutu,
                                    
                                      );
                        $jmlsksxmutu=$jmlsksxmutu+$item2->jmlsks * $item2->mutu;
                        $jmlsks=$jmlsks+$item2->jmlsks;
						$jmlmutu=$jmlmutu+$item2->mutu;
                       
                    }
                 $this->cezpdf->ezTable($db_data, '', '', array('width'=>500,
                                                              'fontSize' => 8,
                                                              'showLines'=> 1,'showHeadings'=>0,'shaded'=>0,'justification'=>"right",
                                                              'cols'=>array(
                                                                  'nourut'=>array('width'=>35,'justification'=>"center"),
                                                                  'kode'=>array('width'=>55,'justification'=>"center"),
                                                                  'matakuliah'=>array('width'=>225,'justification'=>"left"),
                                                                  'hm'=>array('width'=>40,'justification'=>"center"),
                                                                  'am'=>array('width'=>40,'justification'=>"center"),
                                                                  'sks'=>array('width'=>40,'justification'=>"center"),
                                                                  'm'=>array('width'=>40,'justification'=>"center"),
                                                                          
                                                                )
                                                                ));

             $this->cezpdf->ezSetDy(-5, 'makeSpace'); //separator space
             $this->cezpdf->ezText('                             Keterangan: HM=Huruf Mutu, AM=Angka Mutu, sks = satuan kredit semester, M=HM x AM.',8,array('justification' => 'left'));
             //$this->cezpdf->line(60,310,520,310);
             
             $this->cezpdf->ezSetDy(-10, 'makeSpace'); //separator space
             
             $footer[] = array('nourut' =>'Jumlah SKS x Mutu    ','kode' => ' : '.$jmlsksxmutu,'matakuliah' => '');
             $footer[] = array('nourut' =>'Jumlah SKS          ','kode' => ' : '.$jmlsks,'matakuliah' => '');
             $footer[] = array('nourut' =>'Indeks Prestasi (IP)','kode' => ' : '.substr($jmlmutu/$jmlsks,0,4),'matakuliah' => '');
             $footer[] = array('nourut' =>'Indeks Prestasi Akumulasi','kode' => ' : '.$ipkup,'matakuliah' => '');
             $footer[] = array('nourut' =>'','kode' => '','matakuliah' => 'Bandung, '.$this->TanggalIndo(date("Ymd")));
             $footer[] = array('nourut' =>'','kode' => '','matakuliah' => 'Dekan,');
             $footer[] = array('nourut' =>'','kode' => '','matakuliah' => '');
             $footer[] = array('nourut' =>'','kode' => '','matakuliah' => '');
             $footer[] = array('nourut' =>'','kode' => '','matakuliah' => '');
             $footer[] = array('nourut' =>'','kode' => '','matakuliah' => '');
             $footer[] = array('nourut' =>'','kode' => '','matakuliah' => $pimpin2);


                 $this->cezpdf->ezTable($footer, '', '', array('width'=>500,
                                                              'fontSize' => 8,
                                                              'showLines'=> 0,'showHeadings'=>0,'shaded'=>0,'justification'=>"right",
                                                              'cols'=>array(
                                                                  'nourut'=>array('width'=>120,'justification'=>"left"),
                                                                  'kode'=>array('width'=>130,'justification'=>"left"),
                                                                  'matakuliah'=>array('width'=>230,'justification'=>"center"),
                                                                  
                                                                )
                                                                ));
             
        $this->cezpdf->ezStream(array('Content-Disposition'=>'KHS-'.$nim.'.pdf'));
     }
	 
	 // CETAK KARTU USM
	 function pcetakkartuusm()
      
     {
		$this->ceklogpmb();
        $nopmb           = $this->my_usessionpmb->userdata('nopmbunla');
        $nokuitansiy="";
		$nopmby= "";
        $namay="";
		$idstmskmhs="";
		$nmstmskmhs="";
		
		
        $so = $this->getpmb($nopmb);
                    foreach($so as $item)
                    {
					if ($item->nokuitansipmb) {
                       $nokuitansiy= $item->nokuitansipmb;
					   $nopmby= $item->nopmb;
                       $namay= $item->nama;
					   $idstmskmhs=$item->idstmskmhs;
					   $nmstmskmhs=$this->nm_field('nmstmskmhs','stmskmhs','idstmskmhs',$item->idstmskmhs);
                       }
                    } 
        
     
        //====================================================
        $this->cezpdf->Cezpdf('A5','landscape'); //landscape
        $this->cezpdf->ezSetMargins(15,30,10,10); //kiri, atas, kanan, bawah
        
         
      //  $this->cezpdf->addText(150,100,10,"the quick <b>brown</b> fox jumps over the lazy dog!",0);
        $tambah = 0;
        $kurang = 70;
        
        $image = imagecreatefrompng(base_url().'resources/img/report.png');
        $this->cezpdf->addImage($image,100+$tambah-$kurang,300,110);
        
       

        //head info laporan
        
        $this->cezpdf->selectFont('./fonts/Helvetica-Bold');
        $this->cezpdf->addText(218+$tambah-$kurang,360,14,"PANITIA PENERIMAAN MAHASISWA BARU",0);
        $this->cezpdf->addText(218+$tambah-$kurang,340,18,"UNIVERSITAS LANGLANGBUANA",0);
        
        $this->cezpdf->selectFont('./fonts/Helvetica');
        $this->cezpdf->addText(218+$tambah-$kurang,325,12,"Jl. Karapitan No. 116 Bandung 40261, Telp. 022-4218084, Fax. 022-4237144",0);
        
        
            $this->cezpdf->ezSetDy(-90, 'makeSpace'); //separator space
//            
            $this->cezpdf->selectFont('./fonts/Helvetica-Bold');
            $this->cezpdf->ezText("<b>KARTU USM</b>", 16, array("justification" => "center")); 
            $this->cezpdf->selectFont('./fonts/Helvetica');
           
            $this->cezpdf->ezSetDy(-20, 'makeSpace'); //separator space
            
            $head_2[]=array(	'kol1'=>'No PMB                                      : ','kol2'=>$nopmby,'kol3'=>'');
            $head_2[]=array(	'kol1'=>'Nama                                          : ','kol2'=>$namay,'kol3'=>'');
			$head_2[]=array(	'kol1'=>'Status Masuk                              : ','kol2'=>$nmstmskmhs,'kol3'=>'');      
			
			$usm = $this->get_jadwalusm($idstmskmhs);
                    foreach($usm as $item2)
                    {
            $head_2[]=array('kol1'=>'Jenis USM             	                      : ','kol2'=>$item2->nmjnsusm,'kol3'=>'');
            $head_2[]=array('kol1'=>'Tanggal USM                              : ','kol2'=>$this->TanggalIndo(date("Ymd",strtotime($item2->tglusm))),'kol3'=>'');
            $head_2[]=array('kol1'=>'Jam USM                                    : ','kol2'=>date ('H:i',strtotime($item2->jamusmdari)).' s/d '.date ('H:i',strtotime($item2->jamusmsampai)),'kol3'=>'');
			
			}
			
			$head_2[]=array('kol0'=>'','kol1'=>'','kol2'=>'','kol3'=>'');
			$head_2[]=array('kol0'=>'','kol1'=>'','kol2'=>'','kol3'=>'Photo');
            
            $this->cezpdf->ezTable($head_2, '', '', array( 'width'=>660,
                                                              'fontSize' => 12, 
                                                              'showLines'=> 0,
                                                              'showHeadings'=>0,
                                                              'shaded'=>0,
                                                              'cols'=>array(
                                                                    'kol1'=>array('justification'=>'right'),
                                                                    'kol2'=>array('justification'=>'left'),
                                                                    'kol3'=>array('justification'=>'left')
                                                                    )));
																	
			
       
            $this->cezpdf->ezSetDy(-20, 'makeSpace'); //separator space
          
                 
            $this->cezpdf->ezText('',10,array('justification' => 'left'));
          //  $this->cezpdf->ezText('   Jumlah data: '.$i ,8,array('justification' => 'left'));
           

        $this->cezpdf->ezStream(array('Content-Disposition'=>'USM'.$nopmby.'.pdf'));
     }

	 
	 // CETAK TRANSKRIP AKADEMIK
	  function ptranskripakademik($var)
     {
		$this->ceklog();
        $par                   = explode("istra", $var);
        $nimnya          = $par[0];
		$kdprodinya          = $par[1];
		$thnmasuknya          = $par[2];

        
        $this->cezpdf->Cezpdf('F4','potrait'); //landscape
        $this->cezpdf->ezSetMargins(30,20,20,20); //kiri, atas, kanan, bawah
      
         $this->header_transkrip();
         
            $this->cezpdf->ezSetDy(-30, 'makeSpace'); //separator space
           
			$where=$nimnya;
			
			$pimpin1='';
			$pimpin2='';
			$skripsi='';
			
			$batas1=0;
			$mulai1=0;
			
			$batas2=0;
			$mulai2=0;
			
            $so = $this->getmahasiswa($where);
			
            foreach($so as $item):
			
            $this->cezpdf->selectFont('./fonts/Helvetica-Bold');
            $this->cezpdf->ezText("<b>TRANSKRIP AKADEMIK</b>", 14, array("justification" => "center")); //addText(350,450,14,"<b>STUDENT BODY</b> <i>dodol</i>",0);
            
			$this->cezpdf->selectFont('./fonts/Helvetica');
			$this->cezpdf->ezText("Nomor: ".$item->notranskrip, 10, array("justification" => "center"));
			$this->cezpdf->ezText("", 10, array("justification" => "center"));
			$this->cezpdf->ezText("Nama: ".$item->nmmhs, 10, array("justification" => "center"));
			//$this->cezpdf->selectFont('./fonts/Helvetica');
       
            
           $this->cezpdf->ezSetDy(-5, 'makeSpace'); //separator space
            
            
                    $head_2[]=array(   'kol0'=>'Nomor Pokok Mahasiswa','kol01'=>':','kol1'=>$item->nim,'kol2'=>'Fakultas','kol02'=>':','kol3'=>$item->nmfakultas);
                    $head_2[]=array(   'kol0'=>'Tempat, Tanggal Lahir','kol01'=>':','kol1'=>$item->tptlahir.', '.$this->TanggalIndo(date("Ymd",strtotime($item->tgllahir))),'kol2'=>'Program Studi','kol02'=>':','kol3'=>$item->nmprodi);
                    $head_2[]=array(   'kol0'=>'Tahun Masuk','kol01'=>':','kol1'=>$item->thnmasuk,'kol2'=>'Status Program Studi','kol02'=>':','kol3'=>$item->stprodi);
                    $head_2[]=array(   'kol0'=>'Status Awal Mahasiswa','kol01'=>':','kol1'=>$item->nmstawalmhs,'kol2'=>'','kol02'=>':','kol3'=>'');
                    $head_2[]=array(   'kol0'=>'Jenjang Pendidikan','kol01'=>':','kol1'=>$item->nmjenjangstudi,'kol2'=>'','kol02'=>':','kol3'=>'');
            
                    $kdthnakademik=$item->kdthnakademik;
                    $nmjnssemester=$item->nmjnssemester;
            
			$pimpin1=$this->nm_field2('nmpimpinan', 'pimpinan', 'idpimpinan',$item->idpimpinan1);
			$pimpin2=$this->nm_field2('nmpimpinan', 'pimpinan', 'idpimpinan',$item->idpimpinan2);
			$skripsi=$item->jdlskripsiind;			
            endforeach;
                    
            $this->cezpdf->ezTable($head_2, '', '', array( 'width'=>500,
                                                              'fontSize' => 8, 
                                                              'showLines'=> 0,
                                                              'showHeadings'=>0,
                                                              'shaded'=>0,
                                                              'cols'=>array(
                                                                    'kol0'=>array('width'=>127,'justification'=>'left'),
																	'kol01'=>array('width'=>13,'justification'=>'right'),
                                                                    'kol1'=>array('width'=>145,'justification'=>'left'),
                                                                    'kol2'=>array('width'=>127,'justification'=>'left'),
																	'kol02'=>array('width'=>13,'justification'=>'right'),
																	'kol3'=>array('width'=>145,'justification'=>'left'))));
             $this->cezpdf->ezSetDy(-30, 'makeSpace'); //separator space
			 
			 //==========================================================
			 $numrows = $this->cekjumlahvnilaiall($nimnya, $kdprodinya, $thnmasuknya); // jumlah record
			 $mod = $numrows % 2;
			 
			  if ($mod == 1){
				$batas1= ($numrows -1) / 2;
				$mulai1=0;
				
				$batas2= (($numrows -1) / 2) + 1;
				$mulai2= ($numrows -1) / 2;
			  
			  } else {
				$batas1=$numrows  / 2;
				$mulai1=0;
				
				$batas2=$numrows / 2;
				$mulai2=$numrows / 2;
			  
			  }
			 $this->cezpdf->ezColumnsStart(array("num"=>2, "gap"=>10)); // bikin dua kolom
			 $widthcol = 150; // lebar kolom untuk masing2 tabel

			 //========================================================== KOLOM 1
            $col_names3[] = array(
                                    'nourut' =>'No.',
                                    'matakuliah' => 'Mata Kuliah',
                                    'hm' => 'HM',
                                    'am' => 'AM',
                                    'sks' => 'sks',
                                    'm' => 'M',
                            );


                 $this->cezpdf->ezTable($col_names3, '', '', array('width'=>500,
                                                              'fontSize' => 8,
                                                              'showLines'=> 1,'showHeadings'=>0,'shaded'=>0,'justification'=>"left",
                                                              'cols'=>array(
                                                                  'nourut'=>array('width'=>25,'justification'=>"center"),
                                                                  'matakuliah'=>array('width'=>$widthcol,'justification'=>"center"),
                                                                  'hm'=>array('width'=>25,'justification'=>"center"),
                                                                  'am'=>array('width'=>25,'justification'=>"center"),
                                                                  'sks'=>array('width'=>25,'justification'=>"center"),
                                                                  'm'=>array('width'=>25,'justification'=>"center")          
                                                                )
                                                                ));

                  // data table
                    
                    $so1 = $this->getvnilaiall($nimnya,$kdprodinya,$thnmasuknya,$batas1,$mulai1);
                    $i = 0;
                    $jmlsksxmutu1=0;
                    $jmlsks1=0;
					$jmlsksselect1=0;
					$jmlmutu1=0;
                    foreach($so1 as $item1)
                    {
                        $i = ($i+1);
                        $db_data1[] = array('nourut' => $i.'.',
                                    'matakuliah' => $item1->nmmkind,
                                    'hm' => $item1->nilaihuruf,//'Rp. '.number_format ($item->nmjenjangstudi),
                                    'am' => substr($item1->bobotnilai,0,1),
                                    'sks' => $item1->jmlsks,
                                    'm' => substr($item1->mutu,0,1),
                                      );
                        $jmlsksxmutu1=$jmlsksxmutu1+$item1->jmlsks * $item1->mutu;
                        $jmlsks1=$jmlsks1+$item1->jmlsks;
						if ($item1->nilaihuruf != '-') {$jmlsksselect1=$jmlsksselect1+$item1->jmlsks;}
						$jmlmutu1=$jmlmutu1+$item1->mutu;
                       
                    }

                 $this->cezpdf->ezTable($db_data1, '', '', array('width'=>500,
                                                              'fontSize' => 8,
                                                              'showLines'=> 1,'showHeadings'=>0,'shaded'=>0,'justification'=>"left",
                                                              'cols'=>array(
                                                                  'nourut'=>array('width'=>25,'justification'=>"center"),
                                                                  'matakuliah'=>array('width'=>$widthcol,'justification'=>"left"),
                                                                  'hm'=>array('width'=>25,'justification'=>"center"),
                                                                  'am'=>array('width'=>25,'justification'=>"center"),
                                                                  'sks'=>array('width'=>25,'justification'=>"center"),
                                                                  'm'=>array('width'=>25,'justification'=>"center"),
                                                                )
                                                                ));
				
				
			$this->cezpdf->ezNewPage(); // masuk ke kolom kedua bikin tabel sisa
			
			//========================================================== KOLOM 2
			$col_names2[] = array(
                                    'nourut2' =>'No.',
                                    'matakuliah2' => 'Mata Kuliah',
                                    'hm2' => 'HM',
                                    'am2' => 'AM',
                                    'sks2' => 'sks',
                                    'm2' => 'M',
                            );


                 $this->cezpdf->ezTable($col_names2, '', '', array('width'=>500,
                                                              'fontSize' => 8,
                                                              'showLines'=> 1,'showHeadings'=>0,'shaded'=>0,'justification'=>"left",
                                                              'cols'=>array(
                                                                  'nourut2'=>array('width'=>25,'justification'=>"center"),
                                                                  'matakuliah2'=>array('width'=>150,'justification'=>"center"),
                                                                  'hm2'=>array('width'=>25,'justification'=>"center"),
                                                                  'am2'=>array('width'=>25,'justification'=>"center"),
                                                                  'sks2'=>array('width'=>25,'justification'=>"center"),
                                                                  'm2'=>array('width'=>25,'justification'=>"center")          
                                                                )
                                                                ));
																
					$so2 = $this->getvnilaiall($nimnya,$kdprodinya,$thnmasuknya,$batas2,$mulai2);
                    $jmlsksxmutu2=0;
                    $jmlsks2=0;
					$jmlsksselect2=0;
					$jmlmutu2=0;
                    foreach($so2 as $item2)
                    {
                        $i = ($i+1);
                        $db_data2[] = array('nourut2' => $i.'.',
                                    'matakuliah2' => $item2->nmmkind,
                                    'hm2' => $item2->nilaihuruf,//'Rp. '.number_format ($item->nmjenjangstudi),
                                    'am2' => substr($item2->bobotnilai,0,1),
                                    'sks2' => $item2->jmlsks,
                                    'm2' => substr($item2->mutu,0,1),
                                      );
                        $jmlsksxmutu2=$jmlsksxmutu2+$item2->jmlsks * $item2->mutu;
                        $jmlsks2=$jmlsks2+$item2->jmlsks;
						if ($item2->nilaihuruf != '-') {$jmlsksselect2=$jmlsksselect2+$item2->jmlsks;}
						$jmlmutu2=$jmlmutu2+$item2->mutu;
                       
                    }

                 $this->cezpdf->ezTable($db_data2, '', '', array('width'=>500,
                                                              'fontSize' => 8,
                                                              'showLines'=> 1,'showHeadings'=>0,'shaded'=>0,'justification'=>"left",
                                                              'cols'=>array(
                                                                  'nourut2'=>array('width'=>25,'justification'=>"center"),
                                                                  'matakuliah2'=>array('width'=>150,'justification'=>"left"),
                                                                  'hm2'=>array('width'=>25,'justification'=>"center"),
                                                                  'am2'=>array('width'=>25,'justification'=>"center"),
                                                                  'sks2'=>array('width'=>25,'justification'=>"center"),
                                                                  'm2'=>array('width'=>25,'justification'=>"center"),
                                                                )
                                                                ));			
																
			
			$this->cezpdf->ezColumnsStop();		// akhir dua kolom	
			$this->cezpdf->ezColumnsStart(array("num"=>1, "gap"=>10)); // bikin satu kolom			

			$this->cezpdf->ezSetDy(-5, 'makeSpace'); //separator space

             $this->cezpdf->ezText('<b>Keterangan: <b>HM</b>=Huruf Mutu, <b>AM</b>=Angka Mutu, sks = satuan kredit semester, M=HM x AM.',8,array('justification' => 'center'));
             //$this->cezpdf->line(60,310,520,310);
             
             $this->cezpdf->ezSetDy(-10, 'makeSpace'); //separator space
			 
			 $ipk1 =0;
			 $ipk2 =0;
			 
			 if ($jmlmutu1 !=0 && $jmlsksselect1 !=0) {
				$ipk1 = $jmlmutu1/$jmlsksselect1;
			 }
			 if ($jmlmutu2 !=0 && $jmlsksselect2 !=0) {
				$ipk2 = $jmlmutu2/$jmlsksselect2;
			 }
			
             $footer[] = array('nourut' =>'Jumlah SKS    : '.($jmlsks1 + $jmlsks2),'kode' => 'Judul Tugas Akhir   :','matakuliah' => '');
             //$footer[] = array('nourut' =>'IPK                  : '.substr(($jmlmutu1/$jmlsks1) + ($jmlmutu2/$jmlsks2),0,4),'kode' => $skripsi,'matakuliah' => '');
			 $footer[] = array('nourut' =>'IPK                  : '.substr($ipk1 + $ipk2,0,4),'kode' => $skripsi,'matakuliah' => '');
    
			 $footer[] = array('nourut' =>'Yudisium         : ','kode' => '','matakuliah' => '');
             $footer[] = array('nourut' =>'','kode' => '','matakuliah' => '');
             $footer[] = array('nourut' =>'','kode' => '','matakuliah' => 'Bandung, '.$this->TanggalIndo(date("Ymd")));
             $footer[] = array('nourut' =>'Dekan','kode' => '','matakuliah' => 'Rektor,');
             $footer[] = array('nourut' =>'','kode' => '','matakuliah' => '');
             $footer[] = array('nourut' =>'','kode' => '','matakuliah' => '');
             $footer[] = array('nourut' =>'','kode' => '','matakuliah' => '');
             $footer[] = array('nourut' =>'','kode' => '','matakuliah' => '');
             $footer[] = array('nourut' =>$pimpin2,'kode' => '','matakuliah' =>$pimpin1);


                 $this->cezpdf->ezTable($footer, '', '', array('width'=>500,
                                                              'fontSize' => 8,
                                                              'showLines'=> 0,'showHeadings'=>0,'shaded'=>0,'justification'=>"left",
                                                              'cols'=>array(
                                                                  'nourut'=>array('width'=>220,'justification'=>"left"),
                                                                  'kode'=>array('width'=>170,'justification'=>"left"),
                                                                  'matakuliah'=>array('width'=>170,'justification'=>"left"),
                                                                  
                                                                )
                                                                ));
             
        $this->cezpdf->ezStream(array('Content-Disposition'=>'TRANSKRIP-'.$nimnya.'.pdf'));
     }

	 // CETAK KAS PMB
	 function plkaspmb($var)
     {
		$this->ceklog();
		$par                     = explode("istra", $var);
        $cbakademik           	 = $par[0];
        $cbperiode               = $par[1];
		$cbbiaya               	 = $par[2];
		
		$tahunakademik            = $par[3];
		
		$tglawal    		     = $par[4];
		$tglakhir     			 = $par[5];
		
		$total     			 	 = $par[6];
		
		$jmlbiaya           	 = $par[7];
		
		// parameter tampil print tahun akademik
		$tahunakademikshow = "-";
		if ($cbakademik == 'true') {
			if ($tahunakademik != '') {
				$tahunakademikshow = $this->nm_field('nmthnakademik','v_tahunakademik','kdstsemester',$tahunakademik);
			} else {
				$tahunakademikshow = "-";
			}
		}
		
		// parameter tampil print periode
		$tglshow = "-";
		if ($cbperiode == 'true') {
			if ($tglawal == $tglakhir) {
				$tglshow = $this->TanggalIndo(date("Ymd",strtotime($tglawal)));
			} else {
				$tglshow = $this->TanggalIndo(date("Ymd",strtotime($tglawal))).' - '.$this->TanggalIndo(date("Ymd",strtotime($tglakhir)));
			}
		}
		
		// parameter tampil jml biaya
		$jmlbiayashow = "-";
		if ($cbbiaya == 'true') {
			if ($jmlbiaya !='') {
				$jmlbiayashow = number_format($jmlbiaya);
			} else {
				$jmlbiayashow = "-";
			}
		}
        
        $this->cezpdf->Cezpdf('A4','landscape'); //landscape
        $this->cezpdf->ezSetMargins(15,30,10,10); //kiri, atas, kanan, bawah
      
          $this->header('landscape'); 
        
            $this->cezpdf->ezSetDy(-50, 'makeSpace'); //separator space
//            
            $this->cezpdf->selectFont('./fonts/Helvetica-Bold');
            $this->cezpdf->ezText("<b>LAPORAN PENERIMAAN KAS PMB</b>", 12, array("justification" => "center")); //addText(350,450,14,"<b>STUDENT BODY</b> <i>dodol</i>",0);
            $this->cezpdf->selectFont('./fonts/Helvetica');
       
            $this->cezpdf->ezSetDy(-20, 'makeSpace'); //separator space
            $head_2[]=array(   'kol1'=>'Tahun Akademik - Semester : '.$tahunakademikshow,'kol2'=>'','kol3'=>'');
            $head_2[]=array(   'kol1'=>'Periode                                  : '.$tglshow,'kol2'=>'','kol3'=>'');
			$head_2[]=array(   'kol1'=>'Jumlah Biaya                         : '.$jmlbiayashow,'kol2'=>'','kol3'=>'');
             
         
            $this->cezpdf->ezTable($head_2, '', '', array( 'width'=>660,
                                                              'fontSize' => 8, 
                                                              'showLines'=> 0,
                                                              'showHeadings'=>0,
                                                              'shaded'=>0,
                                                              'cols'=>array(
                                                                    'kol1'=>array('justification'=>'left'),
                                                                    'kol2'=>array('justification'=>'center'),
                                                                    'kol3'=>array('justification'=>'center'))));
            
            $col_names3[] = array(
                                    'nourut' =>'No.',
                                    //'nopmb' => 'No. PMB',
									'nokuitansipmb' => 'No. Kuitansi PMB',
                                    'tglkuitansipmb' => 'Tanggal Daftar',
									'nama' => 'Nama Pendaftar',
                                    'kdjk' => '(L/P)',
                                    'nmstmskmhs' => 'Status Pendaftar',
                                    'nmfakultas' => 'Fakultas',
                                    'nmprodi' => 'Program Studi',
									'nominal' => 'Nominal',
                                    'nmcarabyr' => 'Cara Bayar',
									'namapembayar' => 'Nama Pembayar'
                            );


                 $this->cezpdf->ezTable($col_names3, '', '', array('width'=>800,
                                                              'fontSize' => 8,
                                                              'showLines'=> 1,'showHeadings'=>0,'shaded'=>0,'justification'=>"right",
                                                              'cols'=>array(
                                                                  'nourut'=>array('width'=>30,'justification'=>"center"),
                                                                  //'nopmb'=>array('width'=>70,'justification'=>"center"),
                                                                  'nokuitansipmb'=>array('width'=>90,'justification'=>"center"),
                                                                  'tglkuitansipmb'=>array('width'=>70,'justification'=>"center"),
                                                                  'nama'=>array('width'=>90,'justification'=>"center"),
																  'kdjk'=>array('width'=>30,'justification'=>"center"),
                                                                  'nmstmskmhs'=>array('width'=>70,'justification'=>"center"),
                                                                  'nmfakultas'=>array('width'=>70,'justification'=>"center"),
																  'nmprodi'=>array('width'=>70,'justification'=>"center"),
																  'nominal'=>array('width'=>70,'justification'=>"center"),
																  'nmcarabyr'=>array('width'=>70,'justification'=>"center"),
																  'namapembayar'=>array('width'=>130,'justification'=>"left"),
                                                                  
                                                                )
                                                                ));

//                  // data table
        
                 $so = $this->getkaspmb($cbakademik,$tahunakademik,$cbperiode,$tglawal,$tglakhir,$cbbiaya,$jmlbiaya);
                    $i = 0;
                    foreach($so as $item)
                    {
                        $i = ($i+1);
                        $db_data[] = array('nourut' => $i.'.',
                                    'nokuitansipmb' => $item->nokuitansipmb,
                                    'tglkuitansipmb' => $item->tglkuitansipmb,
									'nama' => $item->nama,
                                    'kdjk' => $item->kdjk,
                                    'nmstmskmhs' => $item->nmstmskmhs,
                                    'nmfakultas' => $item->nmfakultas,
                                    'nmprodi' => $item->nmprodi,
									'nmcarabyr' => $item->nmcarabyr,
									'nominal' => number_format($item->nominal),
									'namapembayar' => $item->namapembayar,
                                      );
                      
                    }
                 $this->cezpdf->ezTable($db_data, '', '', array('width'=>800,
                                                              'fontSize' => 8,
                                                              'showLines'=> 1,'showHeadings'=>0,'shaded'=>0,'justification'=>"right",
                                                              'cols'=>array(
                                                                  'nourut'=>array('width'=>30,'justification'=>"center"),
                                                                  //'nopmb'=>array('width'=>70,'justification'=>"center"),
                                                                  'nokuitansipmb'=>array('width'=>90,'justification'=>"center"),
                                                                  'tglkuitansipmb'=>array('width'=>70,'justification'=>"center"),
                                                                  'nama'=>array('width'=>90,'justification'=>"center"),
																  'kdjk'=>array('width'=>30,'justification'=>"center"),
                                                                  'nmstmskmhs'=>array('width'=>70,'justification'=>"center"),
                                                                  'nmfakultas'=>array('width'=>70,'justification'=>"center"),
																  'nmprodi'=>array('width'=>70,'justification'=>"center"),
																  'nominal'=>array('width'=>70,'justification'=>"center"),
																  'nmcarabyr'=>array('width'=>70,'justification'=>"center"),
																  'namapembayar'=>array('width'=>130,'justification'=>"left"),    
                                                                )
                                                                ));

                 
            $this->cezpdf->ezText('',10,array('justification' => 'left'));
			$foot_3[]=array(   'kol1'=>'','kol2'=>'','kol3'=>'Total Kas PMB :','kol4'=>'Rp. '.number_format($total));
             
         
            $this->cezpdf->ezTable($foot_3, '', '', array( 'width'=>645,
                                                              'fontSize' => 8, 
                                                              'showLines'=> 0,
                                                              'showHeadings'=>0,
                                                              'shaded'=>0,
                                                              'cols'=>array(
                                                                    'kol1'=>array('justification'=>'left'),
                                                                    'kol2'=>array('width'=>70,'justification'=>'center'),
																	'kol3'=>array('width'=>70,'justification'=>'center'),
                                                                    'kol4'=>array('width'=>90,'justification'=>'center'))));
            

        $this->cezpdf->ezStream(array('Content-Disposition'=>'LaporanKasPMB.pdf'));
     }
	 
	 //==========================================================================================
    
}
