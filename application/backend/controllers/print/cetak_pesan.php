<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once APPPATH.'third_party/html2pdf/html2pdf.class'.EXT;

class Cetak_Pesan extends Controller {

    function __construct(){
        parent::__construct();
    }

	function pesan($noqurban=NULL) {
		$pdf_cetak = new HTML2PDF('P','A5','fr');
		
		$query = $this->db->query("SELECT `qurban`.`idqurban` AS `idqurban`
     , `program`.`nmprogram` AS `nmprogram`
     , `program`.`idstatus` AS `idstatus`
     , `qurban`.`tgldaftar` AS `tgldaftar`
     , `qurban`.`noqurban` AS `noqurban`
     , `qurban`.`nmpemesan` AS `nmpemesan`
	 , `qurban`.`alamat` AS `alamat`
     , `qurban`.`notelp` AS `notelp`
     , `qurban`.`email` AS `email`
     , `qurban`.`idjnshewan` AS `idjnshewan`
     , `jhewan`.`nmjnshewan` AS `nmjnshewan`
     , `qurban`.`idklshewan` AS `idklshewan`
     , `klshewan`.`nmklshewan` AS `nmklshewan`
     , `qurban`.`nominal` AS `nominal`
     , `qurban`.`shodaqoh` AS `shodaqoh`
     , `qurban`.`idrekbank` AS `idrekbank`
     , `rekbank`.`nmbank` AS `nmbank`
     , `rekbank`.`norek` AS `norek`
     , `rekbank`.`atasnama` AS `atasnama`
     , `rekbank`.`jabatan` AS `jabatan`
     , `qurban`.`nmmuqorib` AS `nmmuqorib`
     , `qurban`.`iddaerah` AS `iddaerah`
     , `daerah`.`nmdaerah` AS `nmdaerah`
     , `daerah`.`kelurahan` AS `kelurahan`
     , `daerah`.`kecamatan` AS `kecamatan`
     , `daerah`.`kotkab` AS `kotkab`
     , `daerah`.`provinsi` AS `provinsi`
     , `qurban`.`idstdokumentasi` AS `idstdokumentasi`
     , `stdokumentasi`.`nmstdokumentasi` AS `nmstdokumentasi`
     , `qurban`.`idstpublish` AS `idstpublish`
     , `stpublish`.`kdstpublish` AS `kdstpublish`
     , `stpublish`.`nmstpublish` AS `nmstpublish`
     , `qurban`.`idstqurban` AS `idstqurban`
     , `stqurban`.`kdstqurban` AS `kdstqurban`
     , `stqurban`.`nmstqurban` AS `nmstqurban`
     , `qurban`.`rekomendasidari` AS `rekomendasidari`
     , `qurban`.`foto1` AS `foto1`
     , `qurban`.`foto2` AS `foto2`
     , `qurban`.`foto3` AS `foto3`
     , `qurban`.`foto4` AS `foto4`
     , `qurban`.`tglkonfirmasi` AS `tglkonfirmasi`
     , `qurban`.`atasnama` AS `atasnamapemesan`
	 , concat(hrghewan.idprogram, '&', hrghewan.idjnshewan, '&', hrghewan.idklshewan, '&', hrghewan.harganominal) AS iddethewan
	 , `qurban`.`idsbrinfo` AS `idsbrinfo`
	 , `sbrinfo`.`nmsbrinfo` AS `nmsbrinfo`
	 , (`qurban`.`nominal` + `qurban`.`shodaqoh`) AS `total`
			FROM
			  `qurban`
			LEFT JOIN `program`
			ON `qurban`.`idprogram` = `program`.`idprogram`
			LEFT JOIN `jhewan`
			ON `qurban`.`idjnshewan` = `jhewan`.`idjnshewan`
			LEFT JOIN `klshewan`
			ON `qurban`.`idklshewan` = `klshewan`.`idklshewan`
			LEFT JOIN `rekbank`
			ON `qurban`.`idrekbank` = `rekbank`.`idrekbank`
			LEFT JOIN `daerah`
			ON `qurban`.`iddaerah` = `daerah`.`iddaerah`
			LEFT JOIN `stdokumentasi`
			ON `qurban`.`idstdokumentasi` = `stdokumentasi`.`idstdokumentasi`
			LEFT JOIN `stpublish`
			ON `qurban`.`idstpublish` = `stpublish`.`idstpublish`
			LEFT JOIN `stqurban`
			ON `qurban`.`idstqurban` = `stqurban`.`idstqurban`
			LEFT JOIN `hrghewan`
			ON `hrghewan`.`idprogram` = `program`.`idprogram`
			LEFT JOIN `sbrinfo`
			ON `qurban`.`idsbrinfo` = `sbrinfo`.`idsbrinfo`
		WHERE `qurban`.`noqurban`='".$noqurban."'");
		
		if ($query->num_rows() == 0) {
			redirect('web/content/qurbanonline');
		}
		
		$nmpemesan = isset($query->row()->nmpemesan) ? $query->row()->nmpemesan:"";
		$nmbank = isset($query->row()->nmbank) ? $query->row()->nmbank:"";
		$norek = isset($query->row()->norek) ? $query->row()->norek:"";
		$atasnama = isset($query->row()->atasnama) ? $query->row()->atasnama:"";
		
		$nominal = isset($query->row()->nominal) ? $query->row()->nominal:0;
		$shodaqoh = isset($query->row()->shodaqoh) ? $query->row()->shodaqoh:0;
		$total = $nominal + $shodaqoh;
		
		$psn = 
		'<page backtop="15mm" backbottom="15mm" backleft="15mm" backright="15mm"> 
		<div align="center">
		
		<b><span style="font-size: 20pt">Jazakumullah Khairan Katsiran</span><br><br>

		Kepada <span style="color: red">Yth. '.$nmpemesan.'</span><br>
		Yang telah mempercayakan Qurbannya<br>
		Di Salur Qurban Amanah<br><br></b>
		Semoga Allah S.W.T membalas dengan pahala yang berlipat<br>
		dan menjadi Amal Soleh<br><br>
		Insya Allah titipan hewan Qurbannya akan kami salurkan<br>
		kepada Mustahiq yang ada di Desa<br><br>
		
		<b>
		No. Qurban : <span style="color: red">'.$noqurban.'</span><br><br>
		Silahkan lakukan pembayaran ke Rekening :<br>
		<span style="color: red">'.$nmbank.'<br>
		No. Rekening : '.$norek.'<br>
		Atas Nama : '.$atasnama.'<br><br>
		Total Pembayaran Rp. '.number_format($total).',-<br><br>
		</span>
		Setelah melakukan pembayaran silahkan lakukan<br>
		Konfirmasi Pembayaran dengan cara
		</b>

		</div>
		<br><br>
		<b>1) SMS</b>
		<br>
		Format : Ketik No. Qurban Anda Kirim ke 085846554111<br>
		Contoh : Ketik SMS <span style="color: red">'.$noqurban.'</span> Kirim ke 085846554111
		<br><br>
		
		<b>2) Konfirmasi Pembayaran</b><br>
		- Login Profil Pemesanan Anda<br>
		- Pilih Menu Konfirmasi Pemesanan<br>
		- Klik tombol Konfirmasi
		<br><br>
		</page>';
       
		$pdf_cetak->writeHTML($psn);
		$pdf_cetak->Output($noqurban.'.pdf', 'I');
	}
	 
		
}

     