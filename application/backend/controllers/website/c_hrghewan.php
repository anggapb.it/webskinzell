<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class c_Hrghewan  extends Controller {
var $stat;

    public function __construct()
    {
        parent::Controller();
        $this->load->library('session');
       }
       
      
    function autoNumber($column,$tbl){
        $q = "SELECT max(".$column.")+1 as max FROM ".$tbl."" ;
        $query  = $this->db->query($q);
        $max = ''; 
                    
        if ($query->num_rows() != 0)
        {
            $row = $query->row();
            $max=$row->max;
        }
        if ($max == null){
            $max=0;
        }
        return $max;
    }  
    
    function id_field($column,$tbl,$whereb, $wherea){
        $q = "SELECT ".$column." as id FROM ".$tbl." where ".$whereb." = '".$wherea."' " ;
        $query  = $this->db->query($q);
        $id = ''; 
                    
        if ($query->num_rows() != 0)
        {
            $row = $query->row();
            $id=$row->id;
        }
        return $id;
    }  
    
    function nm_field($column,$tbl,$whereb, $wherea){
        $q = "SELECT ".$column." as nm FROM ".$tbl." where ".$whereb." = '".$wherea."' " ;
        $query  = $this->db->query($q);
        $nm= ''; 
                    
        if ($query->num_rows() != 0)
        {
            $row = $query->row();
            $nm=$row->nm;
        }
        return $nm;
    }  
       
       
    // START PENGGUNA
     
     function grid(){ //ISTRA
        
        //======================================================================
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                 = $this->input->post("fields");
        $query                  = $this->input->post("query");
        $id_module              = $this->input->post("id_module");
        
            $this->db->select("*");
            $this->db->from("hrghewan"); 
        if($id_module!=''){
            $where['idhrghewan']=$id_module;
            $this->db->where($where);
        }
        if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
    //        $a[explode(',', $r)];
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }

           // $this->db->bracket('open','like');
             $this->db->or_like($d, $query);
           // $this->db->bracket('close','like');
        }
        
        //$this->db->order_by("JDASHBOARD");
                
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(50,0);
        }
        
            $q = $this->db->get(); 
        
        
      //  $q = $this->db->get(); 
       
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
        $datax = $this->db->count_all('hrghewan');
        $ttl = $datax;
        
        //======================================================================
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

      foreach($data as $row) {
            array_push($build_array["data"],array(
            	
            	'idhrghewan'=>$row->idhrghewan,
                'idprogram'=>$row->idprogram,
                'nmprogram'=>$this->nm_field('nmprogram','program','idprogram',$row->idprogram),
				'harga'=>$row->harga,
				'harganominal'=>$row->harganominal,
                'keterangan'=>$row->keterangan,
                'idjnshewan'=>$row->idjnshewan,
                'nmjnshewan'=>$this->nm_field('nmjnshewan','jhewan','idjnshewan',$row->idjnshewan),
                'idklshewan'=>$row->idklshewan,
                'nmklshewan'=>$this->nm_field('nmklshewan','klshewan','idklshewan',$row->idklshewan),
                
                
                            ));
        }
        echo json_encode($build_array);
    }
    

     function count(){      // ISTRA
      	$h_program  = $this->input->post("program");
        $h_jhewan   = $this->input->post("jhewan");
        $h_klshewan = $this->input->post("klshewan");

        $concat = $h_program.$h_jhewan.$h_klshewan;
        $asa;
   		$query = $this->db->query('SELECT concat(idprogram,idjnshewan,idklshewan) AS sama, count(idprogram) AS jml FROM hrghewan WHERE idprogram="'.$h_program.'" AND idjnshewan="'.$h_jhewan.'" AND idklshewan="'.$h_klshewan.'"');
		
		foreach ($query->result() as $row){
		$asa = $row->sama;
		}

		//$queryy = $this->db->query($query);
		if ($query->num_rows() == 1){ 
			$jml = $query->row()->jml;		
			echo $jml;
		}
    }
       
   function save(){      // ISTRA
      	

	             $data = array(
	             'idhrghewan'=> $this->autoNumber('idhrghewan','hrghewan'),
	             'idprogram'=> $_POST['h_program'],
	             'harga'=>  $_POST['harga'],
	             'harganominal'=>  $_POST['harganominal'],
	             'keterangan'=>  $_POST['keterangan'],   
	             'idjnshewan'=> $_POST['h_jhewan'],
	             'idklshewan'=> $_POST['h_klshewan'],

	              );

	        $this->db->insert('hrghewan', $data);
	        if($this->db->affected_rows()){
	            $ret["success"]=true;
	            $ret["message"]='Simpan Data Berhasil';
	        }else{
	            $ret["success"]=false;
	            $ret["message"]='Simpan Data  Gagal';
	        }
	        return $ret;
    	
    }
   
    
    function update(){      // ISTRA

        
             $data = array(
             'idprogram'=> $_POST['h_program'],
             'harga'=>  $_POST['harga'],
             'harganominal'=>  $_POST['harganominal'],
             'keterangan'=>  $_POST['keterangan'],  
             'idjnshewan'=> $_POST['h_jhewan'],
             'idklshewan'=> $_POST['h_klshewan'], 

             );
 
        $this->db->trans_begin();
        
        $where['idhrghewan']=$this->input->post('idhrghewan');
        $this->db->where($where);
        $this->db->update("hrghewan", $data);

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $return["success"]=false;
            $return["message"]="Ubah Data gagal";
        }
        else
        {
            $this->db->trans_commit();
            $return["success"]=true;
            $return["message"]="Ubah Data Berhasil";
        }
        return $return;
     }
   
    
    function delete(){       //ISTRA
        $where['idhrghewan']=$this->input->post('hapus_id');

        $this->db->trans_begin();
       // $this->db->where($where);
        $this->db->delete("hrghewan",$where);
       
         if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $return["success"]=false;
            $return["message"]="Hapus Data gagal";
        }
        else
        {
            $this->db->trans_commit();
            $return["success"]=true;
            $return["message"]="Hapus Data Berhasil";
        }
        return $return;
    }

         
}
