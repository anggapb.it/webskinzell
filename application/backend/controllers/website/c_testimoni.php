<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_Testimoni extends Controller {
var $stat;

    public function __construct()
    {
        parent::Controller();
        $this->load->library('session');
       }
       
      
    function autoNumber($column,$tbl){
        $q = "SELECT max(".$column.")+1 as max FROM ".$tbl."" ;
        $query  = $this->db->query($q);
        $max = ''; 
                    
        if ($query->num_rows() != 0)
        {
            $row = $query->row();
            $max=$row->max;
        }
        if ($max == null){
            $max=0;
        }
        return $max;
    }  
    
    function id_field($column,$tbl,$whereb, $wherea){
        $q = "SELECT ".$column." as id FROM ".$tbl." where ".$whereb." = '".$wherea."' " ;
        $query  = $this->db->query($q);
        $id = ''; 
                    
        if ($query->num_rows() != 0)
        {
            $row = $query->row();
            $id=$row->id;
        }
        return $id;
    }  
    
    function nm_field($column,$tbl,$whereb, $wherea){
        $q = "SELECT ".$column." as nm FROM ".$tbl." where ".$whereb." = '".$wherea."' " ;
        $query  = $this->db->query($q);
        $nm= ''; 
                    
        if ($query->num_rows() != 0)
        {
            $row = $query->row();
            $nm=$row->nm;
        }
        return $nm;
    }  
       
       
    // START PENGGUNA
     
     function grid(){ //ISTRA
        
        //======================================================================
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                 = $this->input->post("fields");
        $query                  = $this->input->post("query");
        $id_module              = $this->input->post("id_module");
        
            $this->db->select("*");
            $this->db->from("testimoni"); 
        if($id_module!=''){
            $where['idtestimoni']=$id_module;
            $this->db->where($where);
        }
        if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
    //        $a[explode(',', $r)];
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }

           // $this->db->bracket('open','like');
             $this->db->or_like($d, $query);
           // $this->db->bracket('close','like');
        }
        
        //$this->db->order_by("JDASHBOARD");
                
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(50,0);
        }
        
            $q = $this->db->get(); 
        
        
      //  $q = $this->db->get(); 
       
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
        $datax = $this->db->count_all('testimoni');
        $ttl = $datax;
        
        //======================================================================
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

      foreach($data as $row) {
            array_push($build_array["data"],array(
                'idtestimoni'=>$row->idtestimoni,
				'dari'=>$row->dari,
                'deskripsi'=>$row->deskripsi,
                //'tgljaminput'=>$row->idstpublish,
                'idstpublish'=>$row->idstpublish,
				'nmstpublish'=>$this->nm_field('nmstpublish','stpublish','idstpublish',$row->idstpublish),
                'tglpublish'=> date("Y-m-d",strtotime($row->tglpublish)), //date("H:i:s")
                'jampublish'=> $row->jampublish, //date("H:i:s")
                
                            ));
        }
        echo json_encode($build_array);
    }
    
       
   function save(){      // ISTRA
      $arr_tgl1    = explode('/',$this->input->post("tglpublish"));
      $tglpublish  = $arr_tgl1[2]."-".$arr_tgl1[0]."-".$arr_tgl1[1];
      //$arr_tgl2    = explode('/',$this->input->post("tglagenda"));
      //$tglagenda  = $arr_tgl2[2]."-".$arr_tgl2[0]."-".$arr_tgl2[1];
      
             $data = array(
             'idtestimoni'=> $this->autoNumber('idtestimoni','testimoni'),
             'dari'=>  $_POST['dari'],
             'deskripsi'=>  $_POST['deskripsi'],	 
             'idstpublish'=> ($_POST['h_status']=="Pilih...") ? null:$_POST['h_status'],
             'tglpublish'=> $tglpublish,
             'jampublish'=> $_POST['jampublish'],
              );

        $this->db->insert('testimoni', $data);
        if($this->db->affected_rows()){
            $ret["success"]=true;
            $ret["message"]='Simpan Data Berhasil';
        }else{
            $ret["success"]=false;
            $ret["message"]='Simpan Data  Gagal';
        }
        return $ret;
    }
   
    
    function update(){      // ISTRA
      $arr_tgl1    = explode('/',$this->input->post("tglpublish"));
      $tglpublish  = $arr_tgl1[2]."-".$arr_tgl1[0]."-".$arr_tgl1[1];
      //$arr_tgl2    = explode('/',$this->input->post("tglagenda"));
      //$tglagenda  = $arr_tgl2[2]."-".$arr_tgl2[0]."-".$arr_tgl2[1];
      
        
             $data = array(
             
             'dari'=>  $_POST['dari'],
             'deskripsi'=> $_POST['deskripsi'],    
             'idstpublish'=> ($_POST['h_status']=="Pilih...") ? null:$_POST['h_status'],
             'tglpublish'=> $tglpublish,
             'jampublish'=> $_POST['jampublish'],
             );
 
        $this->db->trans_begin();
        
        $where['idtestimoni']=$this->input->post('idtestimoni');
        $this->db->where($where);
        $this->db->update("testimoni", $data);

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $return["success"]=false;
            $return["message"]="Ubah Data gagal";
        }
        else
        {
            $this->db->trans_commit();
            $return["success"]=true;
            $return["message"]="Ubah Data Berhasil";
        }
        return $return;
     }
   
    
    function delete(){       //ISTRA
        $where['idtestimoni']=$this->input->post('hapus_id');

        $this->db->trans_begin();
       // $this->db->where($where);
        $this->db->delete("testimoni",$where);
       
         if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $return["success"]=false;
            $return["message"]="Hapus Data gagal";
        }
        else
        {
            $this->db->trans_commit();
            $return["success"]=true;
            $return["message"]="Hapus Data Berhasil";
        }
        return $return;
    }

         
}
