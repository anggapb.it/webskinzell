<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class c_Kerjasama  extends Controller {
var $stat;

    public function __construct()
    {
        parent::Controller();
        $this->load->library('session');
       }
       
      
    function autoNumber($column,$tbl){
        $q = "SELECT max(".$column.")+1 as max FROM ".$tbl."" ;
        $query  = $this->db->query($q);
        $max = ''; 
                    
        if ($query->num_rows() != 0)
        {
            $row = $query->row();
            $max=$row->max;
        }
        if ($max == null){
            $max=0;
        }
        return $max;
    }  
    
    function id_field($column,$tbl,$whereb, $wherea){
        $q = "SELECT ".$column." as id FROM ".$tbl." where ".$whereb." = '".$wherea."' " ;
        $query  = $this->db->query($q);
        $id = ''; 
                    
        if ($query->num_rows() != 0)
        {
            $row = $query->row();
            $id=$row->id;
        }
        return $id;
    }  
    
    function nm_field($column,$tbl,$whereb, $wherea){
        $q = "SELECT ".$column." as nm FROM ".$tbl." where ".$whereb." = '".$wherea."' " ;
        $query  = $this->db->query($q);
        $nm= ''; 
                    
        if ($query->num_rows() != 0)
        {
            $row = $query->row();
            $nm=$row->nm;
        }
        return $nm;
    }  
       
       
    // START PENGGUNA
     
     function grid(){ //ISTRA
        
        //======================================================================
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                 = $this->input->post("fields");
        $query                  = $this->input->post("query");
        $id_module              = $this->input->post("id_module");
        
            $this->db->select("*");
            $this->db->from("kerjasama"); 
        if($id_module!=''){
            $where['idkerjasama']=$id_module;
            $this->db->where($where);
        }
        if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
    //        $a[explode(',', $r)];
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }

           // $this->db->bracket('open','like');
             $this->db->or_like($d, $query);
           // $this->db->bracket('close','like');
        }
        
        //$this->db->order_by("JDASHBOARD");
                
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(50,0);
        }
        
            $q = $this->db->get(); 
        
        
      //  $q = $this->db->get(); 
       
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
        $datax = $this->db->count_all('kerjasama');
        $ttl = $datax;
        
        //======================================================================
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

      foreach($data as $row) {
            array_push($build_array["data"],array(
                'idkerjasama'=>$row->idkerjasama,
				'kdkerjasama'=>$row->kdkerjasama,
                'nmkerjasama'=>$row->nmkerjasama,
                'idstatus'=>$row->idstatus,
                'nmstatus'=>$this->nm_field('nmstatus','status','idstatus',$row->idstatus),
                'idjnskerjasama'=>$row->idjnskerjasama,
                'nmjnskerjasama'=>$this->nm_field('nmjnskerjasama','jkerjasama','idjnskerjasama',$row->idjnskerjasama),
                'gambar'=>$row->gambar,
                
                            ));
        }
        echo json_encode($build_array);
    }
    
       
   function save(){      // ISTRA
      
             $data = array(
             'idkerjasama'=> $this->autoNumber('idkerjasama','kerjasama'),
             'kdkerjasama'=>  $_POST['kdkerjasama'],
             'nmkerjasama'=>  $_POST['nmkerjasama'],   
             'idstatus'=> $_POST['h_status'],
             'idjnskerjasama'=> $_POST['h_jkerja'],
             'gambar'=> $_POST['gambar'],
              );

        $this->db->insert('kerjasama', $data);
        if($this->db->affected_rows()){
            $ret["success"]=true;
            $ret["message"]='Simpan Data Berhasil';
        }else{
            $ret["success"]=false;
            $ret["message"]='Simpan Data  Gagal';
        }
        return $ret;
    }
   
    
    function update(){      // ISTRA

        
             $data = array(
             'kdkerjasama'=>  $_POST['kdkerjasama'],
             'nmkerjasama'=>  $_POST['nmkerjasama'],  
             'idstatus'=> $_POST['h_status'],
             'idjnskerjasama'=> $_POST['h_jkerja'], 
             'gambar'=> $_POST['gambar'],
             );
 
        $this->db->trans_begin();
        
        $where['idkerjasama']=$this->input->post('idkerjasama');
        $this->db->where($where);
        $this->db->update("kerjasama", $data);

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $return["success"]=false;
            $return["message"]="Ubah Data gagal";
        }
        else
        {
            $this->db->trans_commit();
            $return["success"]=true;
            $return["message"]="Ubah Data Berhasil";
        }
        return $return;
     }
   
    
    function delete(){       //ISTRA
        $where['idkerjasama']=$this->input->post('hapus_id');

        $this->db->trans_begin();
       // $this->db->where($where);
        $this->db->delete("kerjasama",$where);
       
         if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $return["success"]=false;
            $return["message"]="Hapus Data gagal";
        }
        else
        {
            $this->db->trans_commit();
            $return["success"]=true;
            $return["message"]="Hapus Data Berhasil";
        }
        return $return;
    }

         
}
