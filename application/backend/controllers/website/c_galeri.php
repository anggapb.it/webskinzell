<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Description of gallery
 *
 * @author eiostudio
 */
class C_galeri extends Controller {
var $stat;

    public function __construct()
    {
        parent::Controller();
        $this->load->library('session');
       }
    
function autoNumber($column,$tbl){
        $q = "SELECT max(".$column.")+1 as max FROM ".$tbl."" ;
        $query  = $this->db->query($q);
        $max = ''; 
                    
        if ($query->num_rows() != 0)
        {
            $row = $query->row();
            $max=$row->max;
        }
        if ($max == null){
            $max=0;
        }
        return $max;
    } 

	function cek_rows($wherex=''){
         
            $this->db->select("*");
            $this->db->from("galeri"); 
			
        if($wherex !=''){
            $this->db->where('kdgaleri', $wherex);
        }
            
        $q  = $this->db->get(); 
        $result = 0;
        if ($q->num_rows() > 0) {
            $result = $q->num_rows();
        } else {
			$result = 0;
		}
        return $result;
    }	
    
    function id_field($column,$tbl,$whereb, $wherea){
        $q = "SELECT ".$column." as id FROM ".$tbl." where ".$whereb." = '".$wherea."' " ;
        $query  = $this->db->query($q);
        $id = ''; 
                    
        if ($query->num_rows() != 0)
        {
            $row = $query->row();
            $id=$row->id;
        }
        return $id;
    }  
        
    function nm_field($column,$tbl,$whereb, $wherea){
        $q = "SELECT ".$column." as nm FROM ".$tbl." where ".$whereb." = '".$wherea."' " ;
        $query  = $this->db->query($q);
        $nm= ''; 
                    
        if ($query->num_rows() != 0)
        {
            $row = $query->row();
            $nm=$row->nm;
        }
        return $nm;
    }  
    
    function grid(){ //ISTRA
        
        //======================================================================
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                 = $this->input->post("fields");
        $query                  = $this->input->post("query");
        $id_module              = $this->input->post("id_module");
        $slideratas                 = $this->input->post("slideratas");
        $sliderbawah                = $this->input->post("sliderbawah");
        
            $this->db->select("*");
            $this->db->from("galeri"); 
        if($id_module!=''){
            $where['idgaleri']=$id_module;
            $this->db->where($where);
        }
        if($slideratas=='true'){
            $where['slideratas']=1;
            $this->db->where($where);
        }
        if($sliderbawah=='true'){
            $where['sliderbawah']=1;
            $this->db->where($where);
        }
        if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
    //        $a[explode(',', $r)];
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }

           // $this->db->bracket('open','like');
             $this->db->or_like($d, $query);
           // $this->db->bracket('close','like');
        }
        
        //$this->db->order_by("JDASHBOARD");
                
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(50,0);
        }
        
            $q = $this->db->get(); 
       
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
        $datax = $this->db->count_all('galeri');
        $ttl = $datax;
        $x='';
        //======================================================================
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());
        foreach($data as $row) {
           // $build_array["slideratas"] =$row->slideratas;
            array_push($build_array["data"],array(
				'idgaleri'=>$row->idgaleri,
                'kdgaleri'=>$row->kdgaleri,
                'nmgaleriind'=>$row->nmgaleriind,
				'idklpgaleri'=>$row->idklpgaleri,
                'nmklpgaleri'=>$this->nm_field('nmklpgaleriind', 'klpgaleri', 'idklpgaleri', $row->idklpgaleri),
                'idjnsgaleri'=>$row->idjnsgaleri,
				'nmjnsgaleri'=>$this->nm_field('nmjnsgaleriind', 'jgaleri', 'idjnsgaleri', $row->idjnsgaleri),
                'idstpublish'=>$row->idstpublish,
				'nmstpublish'=>$this->nm_field('nmstpublish','stpublish','idstpublish',$row->idstpublish),
                'tglpublish'=>$row->tglpublish,
				'userid'=>$row->userid,
                'pengguna'=>$this->nm_field('nmlengkap','pengguna','userid',$row->userid),
                'dilihat'=>$row->dilihat,

                'deskripsiind'=>strip_tags($row->deskripsiind),
                'deskripsieng'=>$row->deskripsieng,
                'nmgalerieng'=>$row->nmgalerieng,
                'file'=>$row->file,
                'url'=>$row->url,
                'slideratas'=>$row->slideratas,
                'sliderbawah'=>$row->sliderbawah,
                'tglpublish'=>date('Y-m-d',  strtotime($row->tglpublish)),
                            ));
           
        }
        
        echo json_encode($build_array);
    }
    
       function upload(){
			$ori_dir = 'resources/img/ori/';
			$thumb_dir = 'resources/img/thumbs/';
		
            $config['upload_path'] = './resources/img/ori/';
            $config['allowed_types'] = 'jpg|gif|png|jpeg|JPG|GIF|PNG|JPEG';
            $this->load->library('upload', $config);
			
			$kode = $this->input->post("idjnsgaleri");
			$numrows = $this->cek_rows($kode);

		if ($_FILES['photo-path']['size'] <= 800000) {	
	
				if ($numrows == 0) {
				
					if ($this->upload->do_upload('photo-path'))
					{

						$data=$this->upload->data();
						// create thumbnail
						$newname= str_replace(" ", "_", $_FILES['photo-path']['name']);
						$this->createThumb($newname, $ori_dir, $thumb_dir, $_FILES['photo-path']['type']);

						$return["success"]='true';
                
						if($this->simpantogaleri($data)){
							$return["data"] = ' berhasil di upload, data telah tersimpan ke database';
						}
						$return["nama"]=$data["file_name"];
						echo json_encode($return);
					
					}
					
				} else {
				
				$return["success"]='false';
				$return["data"] = ' gagal di upload, data gagal tersimpan ke database';
				$return["nama"]=$_FILES['photo-path']['name'];
				echo json_encode($return);
				
				}

			} else {
					
				$return["success"]='false';
                $return["nama"]=$_FILES['photo-path']['name'];
				$return["data"]=' gagal di upload, ukuran file maksimal 800kb';
                echo json_encode($return);
				
			}	

    } 
	
	function upload_update(){
		$ori_dir = 'resources/img/ori/';
		$thumb_dir = 'resources/img/thumbs/';
		
        $config['upload_path'] = './resources/img/ori/';
        $config['allowed_types'] = 'jpg|gif|png|jpeg|JPG|GIF|PNG|JPEG';
        $this->load->library('upload', $config);
		
		$oldphoto = $_POST['temp_foto'];
		$newphoto = $this->input->post("get_foto");
			
		if ($newphoto == $oldphoto) {
		
			$this->update($_POST['temp_foto'],'');
			
			$return["success"]='true';
			$return["nama"]='';
			$return["data"]='';		
			echo json_encode($return);
			
		} else {
					
			if ($_FILES['photo-path']['size'] <= 800000) {	

					if ($this->upload->do_upload('photo-path'))
					{

						$data=$this->upload->data();
						// create thumbnail
						$newname= str_replace(" ", "_", $_FILES['photo-path']['name']);
						$this->createThumb($newname, $ori_dir, $thumb_dir, $_FILES['photo-path']['type']);

						$this->update($data["file_name"],$data["full_path"]);
						
						//unlink("resources/img/ori/$oldphoto");
						//unlink("resources/img/thumbs/thumb_$oldphoto");
						
						$return["success"]='true';
						$return["nama"]=', File '.$data["file_name"];
						$return["data"]=' berhasil di upload';
						echo json_encode($return);
					}

			} else {
					
				$return["success"]='false';
                $return["nama"]=$_FILES['photo-path']['name'];
				$return["data"]=', Ukuran file maksimal 800kb';
                echo json_encode($return);
				
			}	
		}

    }
	
	
	 function createThumb($img_file, $ori_path, $thumb_path, $img_type)
        {

            // get the image source
            $path = $ori_path;
            $img = $path . $img_file;
            switch ($img_type) {
                case "image/jpeg":
                    $img_src = @imagecreatefromjpeg($img);
                    break;
                case "image/pjpeg":
                    $img_src = @imagecreatefromjpeg($img);
                    break;
                case "image/png":
                    $img_src = @imagecreatefrompng($img);
                    break;
                case "image/x-png":
                    $img_src = @imagecreatefrompng($img);
                    break;
                case "image/gif":
                    $img_src = @imagecreatefromgif($img);
                    break;
            }
            $img_width = imagesx($img_src);
            $img_height = imagesy($img_src);

            $square_size = 350;

            // check width, height, or square
            if ($img_width == $img_height) {
                // square
                $tmp_width = $square_size;
                $tmp_height = $square_size;
            } else
                if ($img_height < $img_width) {
                    // wide
                    $tmp_height = $square_size;
                    $tmp_width = intval(($img_width / $img_height) * $square_size);
                    if ($tmp_width % 2 != 0) {
                        $tmp_width++;
                    }
                } else
                    if ($img_height > $img_width) {
                        $tmp_width = $square_size;
                        $tmp_height = intval(($img_height / $img_width) * $square_size);
                        if ($tmp_height % 2 != 0) {
                            $tmp_height++;
                        }
                    }

            $img_new = imagecreatetruecolor($tmp_width, $tmp_height);
            imagecopyresampled($img_new, $img_src, 0, 0, 0, 0, $tmp_width, $tmp_height, $img_width,
                $img_height);

            // create temporary thumbnail and locate on the server
            $thumb = $thumb_path . "thumb_" . $img_file;
            switch ($img_type) {
                case "image/jpeg":
                    imagejpeg($img_new, $thumb);
                    break;
                case "image/pjpeg":
                    imagejpeg($img_new, $thumb);
                    break;
                case "image/png":
                    imagepng($img_new, $thumb);
                    break;
                case "image/x-png":
                    imagepng($img_new, $thumb);
                    break;
                case "image/gif":
                    imagegif($img_new, $thumb);
                    break;
            }

            // get tmp_image
            switch ($img_type) {
                case "image/jpeg":
                    $img_thumb_square = imagecreatefromjpeg($thumb);
                    break;
                case "image/pjpeg":
                    $img_thumb_square = imagecreatefromjpeg($thumb);
                    break;
                case "image/png":
                    $img_thumb_square = imagecreatefrompng($thumb);
                    break;
                case "image/x-png":
                    $img_thumb_square = imagecreatefrompng($thumb);
                    break;
                case "image/gif":
                    $img_thumb_square = imagecreatefromgif($thumb);
                    break;
            }

            $thumb_width = imagesx($img_thumb_square);
            $thumb_height = imagesy($img_thumb_square);

            if ($thumb_height < $thumb_width) {
                // wide
                $x_src = ($thumb_width - $square_size) / 2;
                $y_src = 0;
                $img_final = imagecreatetruecolor($square_size, $square_size);
                imagecopy($img_final, $img_thumb_square, 0, 0, $x_src, $y_src, $square_size, $square_size);
            } else
                if ($thumb_height > $thumb_width) {
                    // landscape
                    $x_src = 0;
                    $y_src = ($thumb_height - $square_size) / 2;
                    $img_final = imagecreatetruecolor($square_size, $square_size);
                    imagecopy($img_final, $img_thumb_square, 0, 0, $x_src, $y_src, $square_size, $square_size);
                } else {
                    $img_final = imagecreatetruecolor($square_size, $square_size);
                    imagecopy($img_final, $img_thumb_square, 0, 0, 0, 0, $square_size, $square_size);
                }

                switch ($img_type) {
                    case "image/jpeg":
                        @imagejpeg($img_final, $thumb);
                        break;
                    case "image/pjpeg":
                        @imagejpeg($img_final, $thumb);
                        break;
                    case "image/png":
                        @imagepng($img_final, $thumb);
                        break;
                    case "image/x-png":
                        @imagepng($img_final, $thumb);
                        break;
                    case "image/gif":
                        @imagegif($img_final, $thumb);
                        break;
                }
        }
    
    function simpantogaleri($dt){
      $arr_tgl1    = explode('/',$this->input->post("tglpublish"));
        $tglpublish  = $arr_tgl1[2]."-".$arr_tgl1[0]."-".$arr_tgl1[1];

        $datax = array(
             'idgaleri'=> $this->autoNumber('idgaleri','galeri'),
            // 'kdgaleri'=>  $_POST['kdgaleri'],
             'idklpgaleri'=>($_POST['h_kdklpgaleri']=="Pilih...") ? null:$_POST['h_kdklpgaleri'],
             
             'nmgaleriind'=> $_POST['nmgaleriind'],    
             'idjnsgaleri'=> ($_POST['h_kdjnsgaleri']=="Pilih...") ? null:$_POST['h_kdjnsgaleri'],
             'deskripsiind'=> $_POST['deskripsiind'],
             'nmgalerieng'=> $_POST['nmgalerieng'],
             'file'=> $dt["file_name"],//$_POST['file'],
             'deskripsieng'=> $_POST['deskripsieng'],
             'url'=> $dt['full_path'],//$_POST['url'],
             'slideratas'=> ($_POST['slideratasx']=='false') ? 0:1,
             'sliderbawah'=> ($_POST['sliderbawahx']=='false') ? 0:1,
             'idstpublish'=>($_POST['h_status']=="Pilih...") ? null:$_POST['h_status'],
             'tglpublish'=> $tglpublish,
             'tglinput'=>date('Y-m-d'),
             'userid'=>$_POST['penggunax'],
			 'idpublishgal'=> ($_POST['tbhgaleri']=='false') ? 0:1,
             );

        $this->db->insert('galeri', $datax);
        if($this->db->affected_rows()){
            $ret=' berhasil di upload dan data telah tersimpan ke database!?';
        }else{
            $ret=' berhasil di upload, tetapi data gagal tersimpan ke database!?';
        }
        return $ret;

    }
    
    function delete(){
        $where['idgaleri']=$this->input->post('id');
        $data = $this->input->post('filex');
        
		unlink("resources/img/ori/$data");
		unlink("resources/img/thumbs/thumb_$data");
         
        $this->db->trans_begin();

        $this->db->delete("galeri",$where);
       
         if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $return["success"]=false;
            $return["message"]="Hapus Data gagal";
        }
        else
        {
            $this->db->trans_commit();
            $return["success"]=true;
            $return["message"]="Hapus Data Berhasil";
        }
        return $return;
        
        }

   
    function update($dt,$dt2){     // ISTRA
        $arr_tgl1    = explode('/',$this->input->post("tglpublish"));
        $tglpublish  = $arr_tgl1[2]."-".$arr_tgl1[0]."-".$arr_tgl1[1];

		if ($dt2 != '') {
        
             $data = array(
             'idklpgaleri'=>($_POST['h_kdklpgaleri']=="Pilih...") ? null:$_POST['h_kdklpgaleri'],
             
             'nmgaleriind'=> $_POST['nmgaleriind'],    
             'idjnsgaleri'=> ($_POST['h_kdjnsgaleri']=="Pilih...") ? null:$_POST['h_kdjnsgaleri'],
             'deskripsiind'=> $_POST['deskripsiind'],
             'nmgalerieng'=> $_POST['nmgalerieng'],
			 'file'=> $dt,//$_POST['file'],
             'deskripsieng'=> $_POST['deskripsieng'],
			 'url'=> $dt2,//$_POST['file'],
             'slideratas'=> ($_POST['slideratasx']=='false') ? 0:1,
             'sliderbawah'=> ($_POST['sliderbawahx']=='false') ? 0:1,
             'idstpublish'=>($_POST['h_status']=="Pilih...") ? null:$_POST['h_status'],
             'tglpublish'=> $tglpublish,
             'tglinput'=>date('Y-m-d'),
			 'idpublishgal'=> ($_POST['tbhgaleri']=='false') ? 0:1,
          //   'userid'=>$this->id_field('userid', 'pengguna', 'nmlengkap',$_POST['pengguna']),
             );
			 
		} else {
			 
			  $data = array(
             'idklpgaleri'=>($_POST['h_kdklpgaleri']=="Pilih...") ? null:$_POST['h_kdklpgaleri'],
             
             'nmgaleriind'=> $_POST['nmgaleriind'],    
             'idjnsgaleri'=> ($_POST['h_kdjnsgaleri']=="Pilih...") ? null:$_POST['h_kdjnsgaleri'],
             'deskripsiind'=> $_POST['deskripsiind'],
             'nmgalerieng'=> $_POST['nmgalerieng'],
			 'file'=> $dt,//$_POST['file'],
             'deskripsieng'=> $_POST['deskripsieng'],
             'slideratas'=> ($_POST['slideratasx']=='false') ? 0:1,
             'sliderbawah'=> ($_POST['sliderbawahx']=='false') ? 0:1,
             'idstpublish'=>($_POST['h_status']=="Pilih...") ? null:$_POST['h_status'],
             'tglpublish'=> $tglpublish,
             'tglinput'=>date('Y-m-d')//,
          //   'userid'=>$this->id_field('userid', 'pengguna', 'nmlengkap',$_POST['pengguna']),
             );	 
		}
 
        $this->db->trans_begin();
        
        $where['idgaleri']=$this->input->post('id');
        $this->db->where($where);
        $this->db->update("galeri", $data);

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $return["success"]=false;
            $return["message"]="Ubah Data gagal";
        }
        else
        {
            $this->db->trans_commit();
            $return["success"]=true;
            $return["message"]="Ubah Data Berhasil";
        }
         return $return;
     }
}
