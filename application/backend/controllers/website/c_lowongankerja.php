<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_lowongankerja extends Controller {
var $stat;

    public function __construct()
    {
        parent::Controller();
        $this->load->library('session');
       }
       
      
    function autoNumber($column,$tbl){
        $q = "SELECT max(".$column.")+1 as max FROM ".$tbl."" ;
        $query  = $this->db->query($q);
        $max = ''; 
                    
        if ($query->num_rows() != 0)
        {
            $row = $query->row();
            $max=$row->max;
        }
        if ($max == null){
            $max=0;
        }
        return $max;
    }  
    
    function id_field($column,$tbl,$whereb, $wherea){
        $q = "SELECT ".$column." as id FROM ".$tbl." where ".$whereb." = '".$wherea."' " ;
        $query  = $this->db->query($q);
        $id = ''; 
                    
        if ($query->num_rows() != 0)
        {
            $row = $query->row();
            $id=$row->id;
        }
        return $id;
    }  
    
    function nm_field($column,$tbl,$whereb, $wherea){
        $q = "SELECT ".$column." as nm FROM ".$tbl." where ".$whereb." = '".$wherea."' " ;
        $query  = $this->db->query($q);
        $nm= ''; 
                    
        if ($query->num_rows() != 0)
        {
            $row = $query->row();
            $nm=$row->nm;
        }
        return $nm;
    }  
       
       
    // START PENGGUNA
     
     function grid(){ //ISTRA
        
        //======================================================================
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                 = $this->input->post("fields");
        $query                  = $this->input->post("query");
        $id_module              = $this->input->post("id_module");
        
            $this->db->select("*");
            $this->db->from("lowongankerja"); 
        if($id_module!=''){
            $where['idlowongan']=$id_module;
            $this->db->where($where);
        }
        if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
    //        $a[explode(',', $r)];
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }

           // $this->db->bracket('open','like');
             $this->db->or_like($d, $query);
           // $this->db->bracket('close','like');
        }
        
        //$this->db->order_by("JDASHBOARD");
                
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(50,0);
        }
        
            $q = $this->db->get(); 
       
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
        $datax = $this->db->count_all('lowongankerja');
        $ttl = $datax;
        
        //======================================================================
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

      foreach($data as $row) {
            array_push($build_array["data"],array(
				'idlowongan'=>$row->idlowongan,
                'kdlowongan'=>$row->kdlowongan,
                'idperusahaan'=>$row->idperusahaan,
				'nmperusahaan'=>$this->nm_field('nmperusahaan', 'perusahaan', 'idperusahaan', $row->idperusahaan),
                'idposisi'=>$row->idposisi,
				'nmposisi'=>$this->nm_field('nmposisi','posisi','idposisi',$row->idposisi),
                'kualifikasi'=>$row->kualifikasi,
                'bataslamaran'=>date("Y-m-d",strtotime($row->bataslamaran)),
				'deskripsi'=>$row->deskripsi,				
                            ));
        }
        echo json_encode($build_array);
    }
    
       
   function save(){      // ISTRA
      $arr_tgl1    = explode('/',$this->input->post("bataslamaran"));
      $bataslamaran  = $arr_tgl1[2]."-".$arr_tgl1[0]."-".$arr_tgl1[1];
      
         $data = array(
             'idlowongan'=> $this->autoNumber('idlowongan','lowongankerja'),
             'kdlowongan'=>  $_POST['kdlowongan'],
             'idperusahaan'=> ($_POST['h_kdperusahaan']=="Pilih...") ? null:$_POST['h_kdperusahaan'],
             'idposisi'=> ($_POST['h_kdposisi']=="Pilih...") ? null:$_POST['h_kdposisi'],
             'kualifikasi'=> $_POST['kualifikasi'],    
             'bataslamaran'=> $bataslamaran,
             'deskripsi'=> $_POST['deskripsi'],
              );

        $this->db->insert('lowongankerja', $data);
        if($this->db->affected_rows()){
            $ret["success"]=true;
            $ret["message"]='Simpan Data Berhasil';
        }else{
            $ret["success"]=false;
            $ret["message"]='Simpan Data  Gagal';
        }
        return $ret;
    }
   
    
    function update(){      // ISTRA
      $arr_tgl1    = explode('/',$this->input->post("bataslamaran"));
      $bataslamaran  = $arr_tgl1[2]."-".$arr_tgl1[0]."-".$arr_tgl1[1];
      
         $data = array(
             'kdlowongan'=>  $_POST['kdlowongan'],
             'idperusahaan'=> ($_POST['h_kdperusahaan']=="Pilih...") ? null:$_POST['h_kdperusahaan'],
             'idposisi'=> ($_POST['h_kdposisi']=="Pilih...") ? null:$_POST['h_kdposisi'],
             'kualifikasi'=> $_POST['kualifikasi'],    
             'bataslamaran'=> $bataslamaran,
             'deskripsi'=> $_POST['deskripsi'],
              );
 
        $this->db->trans_begin();
        
        $where['idlowongan']=$this->input->post('idlowongan');
        $this->db->where($where);
        $this->db->update("lowongankerja", $data);

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $return["success"]=false;
            $return["message"]="Ubah Data gagal";
        }
        else
        {
            $this->db->trans_commit();
            $return["success"]=true;
            $return["message"]="Ubah Data Berhasil";
        }
        return $return;
     }
   
    
    function delete(){       //ISTRA
        $where['idlowongan']=$this->input->post('hapus_id');

        $this->db->trans_begin();
        $this->db->delete("lowongankerja",$where);
       
         if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $return["success"]=false;
            $return["message"]="Hapus Data gagal";
        }
        else
        {
            $this->db->trans_commit();
            $return["success"]=true;
            $return["message"]="Hapus Data Berhasil";
        }
        return $return;
    }
     
         
}
