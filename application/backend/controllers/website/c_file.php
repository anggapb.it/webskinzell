<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_file extends Controller {
var $stat;

    public function __construct()
    {
        parent::Controller();
        $this->load->library('session');
       }
       
      
    function autoNumber($column,$tbl){
        $q = "SELECT max(".$column.")+1 as max FROM ".$tbl."" ;
        $query  = $this->db->query($q);
        $max = ''; 
                    
        if ($query->num_rows() != 0)
        {
            $row = $query->row();
            $max=$row->max;
        }
        if ($max == null){
            $max=0;
        }
        return $max;
    }  
    
    function id_field($column,$tbl,$whereb, $wherea){
        $q = "SELECT ".$column." as id FROM ".$tbl." where ".$whereb." = '".$wherea."' " ;
        $query  = $this->db->query($q);
        $id = ''; 
                    
        if ($query->num_rows() != 0)
        {
            $row = $query->row();
            $id=$row->id;
        }
        return $id;
    }  
    
    function nm_field($column,$tbl,$whereb, $wherea){
        $q = "SELECT ".$column." as nm FROM ".$tbl." where ".$whereb." = '".$wherea."' " ;
        $query  = $this->db->query($q);
        $nm= ''; 
                    
        if ($query->num_rows() != 0)
        {
            $row = $query->row();
            $nm=$row->nm;
        }
        return $nm;
    }  
       
       
    // START PENGGUNA
     
     function grid(){ //ISTRA
        
        //======================================================================
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                 = $this->input->post("fields");
        $query                  = $this->input->post("query");
        $id_module              = $this->input->post("id_module");
        
            $this->db->select("*");
            $this->db->from("galeri"); 
        if($id_module!=''){
            $where['kdgaleri']=$id_module;
            $this->db->where($where);
        }
        if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
    //        $a[explode(',', $r)];
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }

           // $this->db->bracket('open','like');
             $this->db->or_like($d, $query);
           // $this->db->bracket('close','like');
        }
        
        //$this->db->order_by("JDASHBOARD");
                
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(50,0);
        }
        
            $q = $this->db->get(); 
       
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
        $datax = $this->db->count_all('galeri');
        $ttl = $datax;
        
        //======================================================================
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

      foreach($data as $row) {
            array_push($build_array["data"],array(
                'kdgaleri'=>$row->kdgaleri,
                'nmgaleriind'=>$row->nmgaleriind,
                'idklpgaleri'=>$this->nm_field('nmklpgaleriind', 'klpgaleri', 'idklpgaleri', $row->idklpgaleri),
                'idjnsgaleri'=>$this->nm_field('nmjnsgaleriind', 'jgaleri', 'idjnsgaleri', $row->idjnsgaleri),
                'idstpublish'=>$this->nm_field('nmstpublish','stpublish','idstpublish',$row->idstpublish),
                'tglpublish'=>$row->tglpublish,
                'pengguna'=>$row->userid,
                'dilihat'=>$row->dilihat,

                'deskripsiind'=>$row->deskripsiind,
                'deskripsieng'=>$row->deskripsieng,
                'nmgalerieng'=>$row->nmgalerieng,
                'file'=>$row->file,
                'url'=>$row->url,
                'slideratas'=>$row->slideratas,
                'tglpublish'=>$row->tglpublish,
                            ));
        }
        echo json_encode($build_array);
    }
    
       
   function save(){      // ISTRA
      $arr_tgl1    = explode('/',$this->input->post("tglpublish"));
      $tglpublish  = $arr_tgl1[2]."-".$arr_tgl1[0]."-".$arr_tgl1[1];
      
      $mslider = $_POST['mslider'];
        if($mslider=='true'){
            $msliderx =1;
        }else{
            $msliderx =0;
        }
             $data = array(
             'idgaleri'=> $this->autoNumber('idgaleri','galeri'),
             'kdgaleri'=>  $_POST['kdgaleri'],
             'idklpgaleri'=> $this->id_field('idklpgaleri', 'klpgaleri', 'nmklpgaleriind', $_POST['idklpgaleri']),
             
             'nmgaleriind'=> $_POST['nmgaleriind'],    
             'idjnsgaleri'=>$this->id_field('idjnsgaleri', 'jgaleri', 'nmjnsgaleriind',$_POST['idjnsgaleri']),
             'deskripsiind'=> $_POST['deskripsiind'],
             'nmgalerieng'=> $_POST['nmgalerieng'],
             'file'=> $_POST['file'],
             'deskripsieng'=> $_POST['deskripsieng'],
             'url'=> $_POST['url'],
             'slideratas'=> $msliderx,
             'idstpublish'=> $this->id_field('idstpublish', 'stpublish', 'nmstpublish',$_POST['idstpublish']),
             'tglpublish'=> $tglpublish,
             'userid'=>$this->id_field('userid', 'pengguna', 'nmlengkap',$_POST['pengguna']),
             );

        $this->db->insert('galeri', $data);
        if($this->db->affected_rows()){
            $ret["success"]=true;
            $ret["message"]='Simpan Data Berhasil';
        }else{
            $ret["success"]=false;
            $ret["message"]='Simpan Data  Gagal';
        }
        return $ret;
    }
   
    
    function update(){      // ISTRA
      $arr_tgl1    = explode('/',$this->input->post("tglpublish"));
      $tglpublish  = $arr_tgl1[2]."-".$arr_tgl1[0]."-".$arr_tgl1[1];
      
        $mslider = $_POST['mslider'];
        if($mslider=='true'){
            $msliderx =1;
        }else{
            $msliderx =0;
        }
             $data = array(
             'idklpgaleri'=> $this->id_field('idklpgaleri', 'klpgaleri', 'nmklpgaleriind', $_POST['idklpgaleri']),
             
             'nmgaleriind'=> $_POST['nmgaleriind'],    
             'idjnsgaleri'=>$this->id_field('idjnsgaleri', 'jgaleri', 'nmjnsgaleriind',$_POST['idjnsgaleri']),
             'deskripsiind'=> $_POST['deskripsiind'],
             'nmgalerieng'=> $_POST['nmgalerieng'],
             'file'=> $_POST['file'],
             'deskripsieng'=> $_POST['deskripsieng'],
             'url'=> $_POST['url'],
             'slideratas'=> $msliderx,
             'idstpublish'=> $this->id_field('idstpublish', 'stpublish', 'nmstpublish',$_POST['idstpublish']),
             'tglpublish'=> $tglpublish,
             'userid'=>$this->id_field('userid', 'pengguna', 'nmlengkap',$_POST['pengguna']),
             );
 
        $this->db->trans_begin();
        
        $where['kdgaleri']=$this->input->post('kdgaleri');
        $this->db->where($where);
        $this->db->update("galeri", $data);

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $return["success"]=false;
            $return["message"]="Ubah Data gagal";
        }
        else
        {
            $this->db->trans_commit();
            $return["success"]=true;
            $return["message"]="Ubah Data Berhasil";
        }
        return $return;
     }
   
    
    function delete(){       //ISTRA
        $where['kdgaleri']=$this->input->post('hapus_id');

        $this->db->trans_begin();
       // $this->db->where($where);
        $this->db->delete("galeri",$where);
       
         if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $return["success"]=false;
            $return["message"]="Hapus Data gagal";
        }
        else
        {
            $this->db->trans_commit();
            $return["success"]=true;
            $return["message"]="Hapus Data Berhasil";
        }
        return $return;
    }
     
     //END JENIS KELOMPOK PENGGUNA
         
}
