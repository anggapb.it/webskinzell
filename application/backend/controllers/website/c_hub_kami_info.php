<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class C_hub_kami_info extends Controller {
var $stat;

    public function __construct()
    {
        parent::Controller();
        $this->load->library('session');
       }
       
    // START PENGGUNA
     
     function grid(){ //ISTRA
        $id_module              = $this->input->post("id_module");
        $this->db->select("*");
        $this->db->from("hubkamiinfo"); 
        
        if($id_module!=''){
            $where['idhubinfo']=$id_module;
            $this->db->where($where);
        }
   
        $q = $this->db->get(); 
       
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
        $datax = $this->db->count_all('hubkamiinfo');
        $ttl = $datax;
        
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

      foreach($data as $row) {
            array_push($build_array["data"],array(
                'idhubinfo'=>$row->idhubinfo,
                'deskripsi'=>$row->deskripsi,
				'judul'=>$row->judul,
				'emailadmin'=>$row->emailadmin,
				'passadmin'=>$row->passadmin,
            ));
        }
        echo json_encode($build_array);
    }
	
	function settingadmin(){ //ISTRA
        
        //======================================================================

            $this->db->select("*");
            $this->db->from("hubkamiinfo"); 
            $q = $this->db->get(); 
       
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
        $datax = $this->db->count_all('hubkamiinfo');
        $ttl = $datax;
        
        //======================================================================
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

      foreach($data as $row) {
            array_push($build_array["data"],array(
				'emailadmin'=>$row->emailadmin,
				'passadmin'=>base64_decode($row->passadmin),
            ));
        }
        echo json_encode($build_array);
    }
    
    function update(){      // ISTRA
      
             $data = array(
			 'judul'=> $_POST['judul'],    
             'deskripsi'=> $_POST['deskripsi'],    
             );
 
        $this->db->trans_begin();
        
        $where['idhubinfo']=$this->input->post('idhubinfo');
        $this->db->where($where);
        $this->db->update("hubkamiinfo", $data);

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $return["success"]=false;
            $return["message"]="Ubah Data gagal";
        }
        else
        {
            $this->db->trans_commit();
            $return["success"]=true;
            $return["message"]="Ubah Data Berhasil";
        }
        return $return;
     }
	 
	function update_settingadmin(){      // ISTRA
      
             $data = array(
			 'emailadmin'=> $_POST['emailadmin'],    
             'passadmin'=> base64_encode($_POST['passadmin']),    
             );
 
        $this->db->trans_begin();
        
        $where['idhubinfo']=0;
        $this->db->where($where);
        $this->db->update("hubkamiinfo", $data);

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $return["success"]=false;
            $return["message"]="Ubah Data gagal";
        }
        else
        {
            $this->db->trans_commit();
            $return["success"]=true;
            $return["message"]="Ubah Data Berhasil";
        }
        return $return;
     }
   
    
}
