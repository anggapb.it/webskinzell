<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_halaman extends Controller {
var $stat;

    public function __construct()
    {
        parent::Controller();
        $this->load->library('session');
       }
       
      
    function autoNumber($column,$tbl){
        $q = "SELECT max(".$column.")+1 as max FROM ".$tbl."" ;
        $query  = $this->db->query($q);
        $max = ''; 
                    
        if ($query->num_rows() != 0)
        {
            $row = $query->row();
            $max=$row->max;
        }
        if ($max == null){
            $max=0;
        }
        return $max;
    }  
    
    function id_field($column,$tbl,$whereb, $wherea){
        $q = "SELECT ".$column." as id FROM ".$tbl." where ".$whereb." = '".$wherea."' " ;
        $query  = $this->db->query($q);
        $id = ''; 
                    
        if ($query->num_rows() != 0)
        {
            $row = $query->row();
            $id=$row->id;
        }
        return $id;
    }  
    
    function nm_field($column,$tbl,$whereb, $wherea){
        $q = "SELECT ".$column." as nm FROM ".$tbl." where ".$whereb." = '".$wherea."' " ;
        $query  = $this->db->query($q);
        $nm= ''; 
                    
        if ($query->num_rows() != 0)
        {
            $row = $query->row();
            $nm=$row->nm;
        }
        return $nm;
    }  
       
       
    // START PENGGUNA
     
     function grid(){ //ISTRA
        
        //======================================================================
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                 = $this->input->post("fields");
        $query                  = $this->input->post("query");
        $id_module              = $this->input->post("id_module");
        
            $this->db->select("*");
            $this->db->from("halaman"); 
        if($id_module!=''){
            $where['idhalaman']=$id_module;
            $this->db->where($where);
        }
        if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
    //        $a[explode(',', $r)];
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }

           // $this->db->bracket('open','like');
             $this->db->or_like($d, $query);
           // $this->db->bracket('close','like');
        }
        
        //$this->db->order_by("JDASHBOARD");
                
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(50,0);
        }
        
            $q = $this->db->get(); 
        
        
      //  $q = $this->db->get(); 
       
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
        $datax = $this->db->count_all('halaman');
        $ttl = $datax;
        
        //======================================================================
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

      foreach($data as $row) {
            array_push($build_array["data"],array(
                'idhalaman'=>$row->idhalaman,
				'kdhalaman'=>$row->kdhalaman,
                'idktghalaman'=>$row->idktghalaman,
				'nmktghalaman'=>$this->nm_field('nmktghalamanind', 'ktghalaman', 'idktghalaman', $row->idktghalaman),
                'judulind'=>$row->judulind,
                'sinopsisind'=>$row->sinopsisind,
                'deskripsiind'=>$row->deskripsiind,
                'juduleng'=>$row->juduleng,
                'sinopsiseng'=>$row->sinopsiseng,
                'deskripsieng'=>$row->deskripsieng,
                'gambar'=>$row->gambar,
                'idstpublish'=>$row->idstpublish,
				'nmstpublish'=>$this->nm_field('nmstpublish','stpublish','idstpublish',$row->idstpublish),
                'tglpublish'=> date("Y-m-d",strtotime($row->tglpublish)), //date("H:i:s")
                'userid'=>$row->userid,
				'pengguna'=>$this->nm_field('nmlengkap','pengguna','userid',$row->userid),
                'idstposting'=>$row->idstposting,
                'nmstposting'=>$this->nm_field('nmstposting','stposting','idstposting',$row->idstposting),
                'dilihat'=>$row->dilihat,
                'jampublish'=> $row->jampublish, //date("H:i:s")
                'tglagenda'=> date("Y-m-d",strtotime($row->tglagenda)), //date("H:i:s")
                'jamagenda'=> $row->jamagenda, //date("H:i:s")
                
                            ));
        }
        echo json_encode($build_array);
    }
    
       
   function save(){      // ISTRA
      $arr_tgl1    = explode('/',$this->input->post("tglpublish"));
      $tglpublish  = $arr_tgl1[2]."-".$arr_tgl1[0]."-".$arr_tgl1[1];
      $arr_tgl2    = explode('/',$this->input->post("tglagenda"));
      $tglagenda  = $arr_tgl2[2]."-".$arr_tgl2[0]."-".$arr_tgl2[1];
      
             $data = array(
             'idhalaman'=> $this->autoNumber('idhalaman','halaman'),
             'kdhalaman'=>  ($_POST['kdhalaman']) ? $_POST['kdhalaman']:null,
			 'idktghalaman'=> ($_POST['h_ktghalaman']=="Pilih...") ? null:$_POST['h_ktghalaman'],
             'judulind'=> $_POST['judulind'],    
             'sinopsisind'=> $_POST['sinopsisind'],
             'deskripsiind'=> $_POST['deskripsiind'],
             'juduleng'=> $_POST['juduleng'],
             'sinopsiseng'=> $_POST['sinopsiseng'],
             'deskripsieng'=> $_POST['deskripsieng'],
             'gambar'=> $_POST['gambar'],
             'idstpublish'=> ($_POST['h_status']=="Pilih...") ? null:$_POST['h_status'],
             'idstposting'=> 1,//($_POST['h_stposting']=="Pilih...") ? null:$_POST['h_stposting'],
             'tglpublish'=> $tglpublish,
             'userid'=>$_POST['userid'],
             'jampublish'=> $_POST['jampublish'],
             'tglagenda'=> $tglagenda,
             'jamagenda'=> $_POST['jamagenda']
              );

        $this->db->insert('halaman', $data);
        if($this->db->affected_rows()){
            $ret["success"]=true;
            $ret["message"]='Simpan Data Berhasil';
        }else{
            $ret["success"]=false;
            $ret["message"]='Simpan Data  Gagal';
        }
        return $ret;
    }
   
    
    function update(){      // ISTRA
      $arr_tgl1    = explode('/',$this->input->post("tglpublish"));
      $tglpublish  = $arr_tgl1[2]."-".$arr_tgl1[0]."-".$arr_tgl1[1];
      $arr_tgl2    = explode('/',$this->input->post("tglagenda"));
      $tglagenda  = $arr_tgl2[2]."-".$arr_tgl2[0]."-".$arr_tgl2[1];
      
        
             $data = array(
             
             //'kdhalaman'=>  $_POST['kdhalaman'],
			 'kdhalaman'=>  ($_POST['kdhalaman']) ? $_POST['kdhalaman']:null,
			 'idktghalaman'=> ($_POST['h_ktghalaman']=="Pilih...") ? null:$_POST['h_ktghalaman'],
             'judulind'=> $_POST['judulind'],    
             'sinopsisind'=> $_POST['sinopsisind'],
             'deskripsiind'=> $_POST['deskripsiind'],
             'juduleng'=> $_POST['juduleng'],
             'sinopsiseng'=> $_POST['sinopsiseng'],
             'deskripsieng'=> $_POST['deskripsieng'],
             'gambar'=> $_POST['gambar'],
             'idstpublish'=> ($_POST['h_status']=="Pilih...") ? null:$_POST['h_status'],
             'idstposting'=> 1,//($_POST['h_stposting']=="Pilih...") ? null:$_POST['h_stposting'],
             'tglpublish'=> $tglpublish,
             'userid'=>$_POST['userid'],
             'jampublish'=> $_POST['jampublish'],
             'tglagenda'=> $tglagenda,
             'jamagenda'=> $_POST['jamagenda']
             );
 
        $this->db->trans_begin();
        
        $where['idhalaman']=$this->input->post('idhalaman');
        $this->db->where($where);
        $this->db->update("halaman", $data);

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $return["success"]=false;
            $return["message"]="Ubah Data gagal";
        }
        else
        {
            $this->db->trans_commit();
            $return["success"]=true;
            $return["message"]="Ubah Data Berhasil";
        }
        return $return;
     }
   
    
    function delete(){       //ISTRA
        $where['idhalaman']=$this->input->post('hapus_id');

        $this->db->trans_begin();
       // $this->db->where($where);
        $this->db->delete("halaman",$where);
       
         if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $return["success"]=false;
            $return["message"]="Hapus Data gagal";
        }
        else
        {
            $this->db->trans_commit();
            $return["success"]=true;
            $return["message"]="Hapus Data Berhasil";
        }
        return $return;
    }

         
}
