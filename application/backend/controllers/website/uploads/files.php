<?php
/**
 * Description of Files
 *
 * @author eiostudio
 */
if (!defined('BASEPATH'))exit('No direct script access allowed');
class Files extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model("cbs_files","cf");
    }

    function index() {
        $path = array(
            'uploads/file_list.js',
            'uploads/file_form.js',
            'uploads/file_upload.js'
        );
        $data["modules"] = $path;
        $this->load->view('admin/cbs_admin_module', $data);
    }
    function save(){
        $ret;
        $id=$this->input->post("id");
        $fields = array('album','information','nama','url_link','created_by');
        foreach($fields as $field){
            $insert[$field]=$this->input->post($field);
        }
        if($this->input->post("active")){$insert["active"]=1;}
        else{$insert["active"]=0;}
        
        $by["id"]=$id;
        $insert["updated_at"]=date("Y-m-d h:i:s");
        $insert["updated_by"]=$this->session->userdata("username");
        $ret = $this->cf->setUpdate($by, $insert);
        
        echo json_encode($ret);
    }
    function delete(){
        $id=$this->input->post("id");
        $where["id"]=$id;
        $data = $this->cf->getRow($where);
        unlink($data->full_path);
        $res=$this->cf->setDelete($where);
        echo json_encode($res);
    }
    function grid(){
        $limit=$this->input->post("limit");
        $start=$this->input->post("start");
        $status=$this->input->post("status");
        
        $by=null;
        
        $ttl=$this->cf->getCount($by);
        $arr = array ("success"=>TRUE,"total"=>$ttl,"data"=>array());
        if($ttl>0){
            $data=$this->cf->getGrid($start, $limit, $by);
            $arr["success"]=TRUE;
            foreach($data as $r){
            array_push($arr["data"],array(
                "id"=>$r->id,
                "name"=>$r->nama,
                "album"=>$r->album,
                "url"=>$r->url,
                "file_type"=>$r->file_type,
                "created_by"=>$r->created_by,
                "created_at"=>$r->created_at,
                "active"=>$r->active
                )
            );
            }
        }
        
        echo json_encode($arr);
    }
    function getRow(){
        $by["id"]=$this->input->post("id");
        $ttl = $this->cf->getCount($by);
        $arr = array ("success"=>TRUE,"results"=>$ttl,"data"=>array());
        if($ttl>0){
            $r = $this->cf->getRow($by);
            $arr["success"]=TRUE;
            array_push($arr["data"],array(
                "id"=>$r->id,
                "nama"=>$r->nama,
                "information"=>$r->information,
                "album"=>$r->album,
                "url"=>$r->url,
                "url_link"=>$r->url_link,
                "active"=>$r->active
                )
            );
        }
        echo json_encode($arr);
    }
    function getAll(){
        $by["active"]=1;
        $ttl = $this->cf->getCount($by);
        $arr = array ("success"=>TRUE,"results"=>$ttl,"data"=>array());
        if($ttl>0){
            $data = $this->cf->getData($by);
            $arr["success"]=TRUE;
            foreach($data as $r){
            array_push($arr["data"],array(
                "id"=>$r->id,
                "name"=>$r->name
                )
            );
            }
        }
        echo json_encode($arr);
    }
    function get_album(){
        $by=null;
        $group="album";
        $data = $this->cf->getRowGroup($by,$group);
        $ttl = count($data);
        $arr = array ("success"=>true,"total"=>$ttl,"data"=>array());
        if($ttl>0){
            foreach($data as $row){
                array_push($arr["data"]
                ,array(
                    "album" => $row->album
                ));
            }
        }
        echo json_encode($arr);
    }
    function upload(){
        $name=$this->input->post("name");
        $album=$this->input->post("album");
        $url_link=$this->input->post("url_link");
        $information=$this->input->post("information");
        $username=$this->session->userdata("username");
        if($this->input->post("active")){$active=1;}else{$active=0;}
        	
            $config['upload_path'] = './uploads/';
            $config['allowed_types'] = '*';

            $this->load->library('upload', $config);

            if ( ! $this->upload->do_upload('file-path'))
            {
                $error = $this->upload->display_errors();
                $res = explode("-",url_title(json_encode($error)));
                $res = implode(" ",$res);
                echo "{success:true, message:'$res'}";
            }
            else
            {
                $data=$this->upload->data();
                
                //{"file_name":"DSC_1403.JPG",
                //"file_type":"image\/jpeg","file_path":"E:\/phpProject\/kencana\/images\/upload\/",
                //"full_path":"E:\/phpProject\/kencana\/images\/upload\/DSC_1403.JPG",
                //"raw_name":"DSC_1403",
                //"orig_name":"DSC_1403.JPG",
                //"client_name":"DSC_1403.JPG",
                //"file_ext":".JPG",
                //"file_size":549.75,
                //"is_image":true,
                //"image_width":2000,
                //"image_height":3008,
                //"image_type":"jpeg",
                //"image_size_str":"width=\"2000\" height=\"3008\""}

                $result["success"]=true;
                $result["nama"]=$name;
                $result["url"]=$data["file_name"];
                $result["album"]=$album;
                
                $insert["nama"]=$name;
                $insert["information"]=$information;
                $insert["full_path"]=$data["full_path"];
                $insert["url"]=$data["file_name"];
                $insert["album"]=$album;
                $insert["height"]=$data["image_height"];
                $insert["width"]=$data["image_width"];
                $insert["file_type"]=$data["file_ext"];
                $insert["url_link"]=strtolower($url_link);
                $insert["url_link"]=url_title($insert["url_link"]);
                $insert["active"]=$active;
                $insert["album_seo"]= strtolower($album);
                $insert["album_seo"]=  url_title($insert["album_seo"]);
                $insert["created_by"]=  $username;
                $this->cf->setInsert($insert);
                
                $return["success"]=true;
                $return["message"]="Upload file berhasil..";
                $return["data"]=$data["file_name"];
                echo json_encode($return);
            }
    }
}

/* End of file files.php */
/* Location: ./application/controllers/files.php */