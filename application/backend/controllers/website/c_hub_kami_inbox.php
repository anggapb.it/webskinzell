<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_hub_kami_inbox extends Controller {
var $stat;

    public function __construct()
    {
        parent::Controller();
        $this->load->library('session');
       }
       
      
    function autoNumber($column,$tbl){
        $q = "SELECT max(".$column.")+1 as max FROM ".$tbl."" ;
        $query  = $this->db->query($q);
        $max = ''; 
                    
        if ($query->num_rows() != 0)
        {
            $row = $query->row();
            $max=$row->max;
        }
        if ($max == null){
            $max=0;
        }
        return $max;
    }  
    
    function id_field($column,$tbl,$whereb, $wherea){
        $q = "SELECT ".$column." as id FROM ".$tbl." where ".$whereb." = '".$wherea."' " ;
        $query  = $this->db->query($q);
        $id = ''; 
                    
        if ($query->num_rows() != 0)
        {
            $row = $query->row();
            $id=$row->id;
        }
        return $id;
    }  
    
    function nm_field($column,$tbl,$whereb, $wherea){
        $q = "SELECT ".$column." as nm FROM ".$tbl." where ".$whereb." = '".$wherea."' " ;
        $query  = $this->db->query($q);
        $nm= ''; 
                    
        if ($query->num_rows() != 0)
        {
            $row = $query->row();
            $nm=$row->nm;
        }
        return $nm;
    }  
       
       
    // START PENGGUNA
     
     function grid(){ //ISTRA
        
        //======================================================================
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                 = $this->input->post("fields");
        $query                  = $this->input->post("query");
        $id_module              = $this->input->post("id_module");
        
            $this->db->select("*");
            $this->db->from("hubkamiinbox"); 
			//$this->db->where("pesanmsk <> ''"); 
			$this->db->order_by("tglpsnmsk DESC"); 
        if($id_module!=''){
            $where['idpesan']=$id_module;
            $this->db->where($where);
        }
        if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
    //        $a[explode(',', $r)];
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }

           // $this->db->bracket('open','like');
             $this->db->or_like($d, $query);
           // $this->db->bracket('close','like');
        }
        
        //$this->db->order_by("JDASHBOARD");
                
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(50,0);
        }
        
            $q = $this->db->get(); 
        
        
      //  $q = $this->db->get(); 
       
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
        $datax = $this->db->count_all('hubkamiinbox');
        $ttl = $datax;
        
        //======================================================================
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

      foreach($data as $row) {
            array_push($build_array["data"],array(
                'idpesan'=>$row->idpesan,
				'nama'=>$row->nama,
                'email'=>$row->email, 
				'tlp'=>$row->tlp, 
				'pesanmsk'=>$row->pesanmsk, 
				'tglpsnmsk'=>$row->tglpsnmsk, 
				'pesanmsk'=>$row->pesanmsk, 
				'tglpsnklr'=>$row->tglpsnklr, 
				'pesanklr'=>$row->pesanklr, 
				'subject'=>$row->subject, 
				'status'=>$row->status, 
				'cc'=>$row->cc, 
            ));
        }
        echo json_encode($build_array);
    }
    
       
   function save(){      // ISTRA
		/* $this->load->library('email');
		$config['protocol']    	= 'smtp';
        $config['smtp_host']    = 'ssl://smtp.gmail.com';
        $config['smtp_port']    = '465';
        $config['smtp_timeout'] = '7';
        $config['smtp_user']    = 'deden.pradeka@gmail.com';
        $config['smtp_pass']    = 'muslimin';
        $config['charset']    	= 'utf-8';
        $config['newline']    	= "\r\n";
        $config['mailtype']		= 'text'; // or html
        $config['validation']	= TRUE; // bool whether to validate email or not      

        $this->email->initialize($config);

        $this->email->from('mygmail@gmail.com', 'myname');
        $this->email->to('deden.pradeka@yahoo.com'); 
		$this->email->cc('xcho85@gmail.com');

        $this->email->subject('Email Test');
        $this->email->message('Testing the email class.');  

        $this->email->send();

        echo $this->email->print_debugger(); */
		
		//simpan
		$date = date("Y-m-d H:i:s");
		$ip = $_SERVER['REMOTE_ADDR'];
		$login = $this->session->userdata("user_id1unla");
            $data = array(
                'email'=>$_POST['email'],
				'tglpsnklr'=>$date,
				'pesanklr'=>$_POST['pesanklr'], 
				'subject'=>$_POST['subject'],
				'status'=>'Terkirim',
				'cc'=>$_POST['subject'],
				'ipaddressadmin'=>$ip,
				'nmkomadmin'=>$_POST['nmcom'],
				'lastlogin'=>$login,
            );

        $this->db->insert('hubkamiinbox', $data);
        if($this->db->affected_rows()){
            $ret["success"]=true;
            $ret["message"]='Simpan Data Berhasil';
        }else{
            $ret["success"]=false;
            $ret["message"]='Simpan Data  Gagal';
        }
        return $ret;
    }
   
   function sendmail(){
		$to = $this->input->post("email");
		$cc = $this->input->post("cc");
		$subject = $this->input->post("subject");
		$message = $this->input->post("pesanklr");
		
		$email;
		$pass;
		
		$query = $this->db->query('SELECT emailadmin,passadmin FROM hubkamiinfo');
		foreach ($query->result() as $row){
		$email = $row->emailadmin;
		$pass = base64_decode($row->passadmin);
		}
		
		/* $email_pengunjung = $to;//$email_pengunjung = $POST["txtemail"];
		$subyek = $subject;
		$isiberita = $message;
		mail($email_pengunjung,$subyek,$isiberita); */
				
		$this->load->library('email');
		$config['protocol']    	= 'smtp';
        $config['smtp_host']    = 'ssl://smtp.gmail.com';
        $config['smtp_port']    = '465';
        $config['smtp_timeout'] = '7';
        $config['smtp_user']    = $email;
        $config['smtp_pass']    = $pass;
        $config['charset']    	= 'utf-8';
        $config['newline']    	= "\r\n";
        $config['mailtype']		= 'text'; // or html
        $config['validation']	= TRUE; // bool whether to validate email or not      

        $this->email->initialize($config);

        //$this->email->from('infohamzar@gmail.com', 'STIKES Hamzar');
        $this->email->from('isdakamitrarss@gmail.com', 'Gmail');
        $this->email->to($to); 
		$this->email->cc($cc);

        $this->email->subject($subject);
        $this->email->message($message);  

        $this->email->send();
		
		$this->update();
	}
    
    function update(){      // ISTRA
			$date = date("Y-m-d H:i:s");
			$ip = $_SERVER['REMOTE_ADDR'];
			$login = $this->session->userdata("user_id1unla");
            $data = array(
				'tglpsnklr'=>$date,
				'pesanklr'=>$_POST['pesanklr'], 
				'subject'=>($_POST['subject']=="") ? null:$_POST['subject'],
				'status'=>'Terkirim',
				'cc'=>($_POST['cc']=="") ? null:$_POST['cc'],
				'ipaddressadmin'=>$ip,
				//'nmkomadmin'=>$_POST['nmcom'],
				'lastlogin'=>$login,
            );
 
        $this->db->trans_begin();
        
        $where['idpesan']=$this->input->post('idpesan');
        $this->db->where($where);
        $this->db->update("hubkamiinbox", $data);

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $return["success"]=false;
            $return["message"]="Kirim Email Gagal";
        }
        else
        {
            $this->db->trans_commit();
            $return["success"]=true;
            $return["message"]="Kirim Email Berhasil";
        }
        return $return;
     }
   
    
    function delete(){       //ISTRA
        $where['idpesan']=$this->input->post('hapus_id');

        $this->db->trans_begin();
       // $this->db->where($where);
        $this->db->delete("hubkamiinbox",$where);
       
         if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $return["success"]=false;
            $return["message"]="Hapus Data Gagal";
        }
        else
        {
            $this->db->trans_commit();
            $return["success"]=true;
            $return["message"]="Hapus Data Berhasil";
        }
        return $return;
    }
	
     //END JENIS KELOMPOK PENGGUNA
         
}
