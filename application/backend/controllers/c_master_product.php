<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_master_product extends Controller {
var $stat;

    public function __construct()
    {
        parent::Controller();
        $this->load->library('cart');
        $this->load->library('session');
      //  $this->load->model("m_master_product","mmp");
    }

       
    
    //START GRID MASTER
    function gridMaster_product(){ //ISTRA
        $q="SELECT * from pegawai order by NIP";
        
        $query  = $this->db->query($q);
        $data = array();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        }
        
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
        echo json_encode($build_array);
    }
    
    
   //END GRID MASTER

   
   function saveAllGrid(){      // ISTRA
       
       $arr_tgl1    = explode('/',$this->input->post("tanggal_lahir"));
      $tgllahir  = $arr_tgl1[2]."-".$arr_tgl1[1]."-".$arr_tgl1[0];
      
      $arr_tgl2    = explode('/',$this->input->post("tanggal_masuk"));
      $tglmasuk  = $arr_tgl2[2]."-".$arr_tgl2[1]."-".$arr_tgl2[0];

      $arr_tgl3    = explode('/',$this->input->post("tanggal_keluar"));
      $tglkeluar  = $arr_tgl3[2]."-".$arr_tgl3[1]."-".$arr_tgl3[0];

      
             $data = array(
             'NIP'=> $_POST['nip'],
             'NMLENGKAP'=> $_POST['nama_lengkap'],
             'NMPANGGILAN'=> $_POST['nama_panggilan'],
             'KDJNSKELAMIN'=> $_POST['h_jk'], 
             'TPTLAHIR'=> $_POST['tempat_lahir'],
             'TGLLAHIR'=> $tgllahir,
             'ALAMAT'=> $_POST['alamat'], 
             'NOTELP'=> $_POST['telp_rumah'], 
             'NOHP'=> $_POST['handphone'], 
             'TGLMASUK'=> $tglmasuk,
             'TGLKELUAR'=> $tglkeluar,
                 'KDSTPEGAWAI'=> $_POST['status'],
                 'CATATAN'=> $_POST['catatan'],
                 'FOTO'=> $_POST['file_gambar'],
             );

        $this->db->insert('pegawai', $data);
        if($this->db->affected_rows()){
            $ret["success"]=true;
            $ret["message"]='Simpan Data Berhasil';
        }else{
            $ret["success"]=false;
            $ret["message"]='Simpan Data  Gagal';
        }
        return $ret;
    }
    
    function deleteAllGrid(){       //ISTRA
        $where['NIP']=$this->input->post('nip');

        $this->db->trans_begin();
       // $this->db->where($where);
        $this->db->delete("pegawai",$where);
       
         if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $return["success"]=false;
            $return["message"]="Hapus Data gagal";
        }
        else
        {
            $this->db->trans_commit();
            $return["success"]=true;
            $return["message"]="Hapus Data Berhasil";
        }
        return $return;
    }
   
    function update(){      // ISTRA
        $data = array(
             'NMLENGKAP'=> $_POST['nama_lengkap'],
             'NMPANGGILAN'=> $_POST['nama_panggilan'],
             'KDJNSKELAMIN'=> $_POST['h_jk'], 
             'TPTLAHIR'=> $_POST['tempat_lahir'],
             'TGLLAHIR'=> $_POST['tanggal_lahir'],
             'ALAMAT'=> $_POST['alamat'], 
             'NOTELP'=> $_POST['telp_rumah'], 
             'NOHP'=> $_POST['handphone'], 
             'TGLMASUK'=> $_POST['tanggal_masuk'],
             'TGLKELUAR'=> $_POST['tanggal_keluar'],
                 'KDSTPEGAWAI'=> $_POST['status'],
                 'CATATAN'=> $_POST['catatan'],
                 'FOTO'=> $_POST['file_gambar'],
             
             );
        
        $this->db->trans_begin();
        
        $where['NIP']=$this->input->post('nip');
        $this->db->where($where);
        $this->db->update("pegawai", $data);

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $return["success"]=false;
            $return["message"]="Ubah Data gagal";
        }
        else
        {
            $this->db->trans_commit();
            $return["success"]=true;
            $return["message"]="Ubah Data Berhasil";
        }
        return $return;
     }
     
     function gridMaster_kelamin(){ //ISTRA
        $q="SELECT * from jkelamin";
        
        $query  = $this->db->query($q);
        $data = array();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        }
        
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
        echo json_encode($build_array);
    }
    
    function gridMaster_status(){ //ISTRA
        $q="SELECT * from stpegawai";
        
        $query  = $this->db->query($q);
        $data = array();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        }
        
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
        echo json_encode($build_array);
    }
    
    function get_photox(){
       
        $dir =  "img/ori/";
        $dir_thumbs = "img/thumbs/";


        $images = array();
        $d = dir($dir);
        while($name = $d->read()){
            if(!preg_match('/\.(jpg|gif|png)$/', $name)) continue;
            $size = filesize($dir.$name);
            $lastmod = filemtime($dir.$name)*1000;
            $thumb = "thumb_".$name;
            $images[] = array('name' => $name, 'size' => $size,
                                'lastmod' => $lastmod, 'url' => $dir.$name,
                    'thumb_url' => $dir_thumbs.$thumb);
        }
        $d->close();
        $o = array('images'=>$images);
        echo json_encode($o);


    }
    
}
