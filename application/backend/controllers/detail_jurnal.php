<?php
/**
 * Description of kontak
 *
 * @author eiostudio
 */
if (!defined('BASEPATH'))exit('No direct script access allowed');

class Detail_jurnal extends Public_Controller {
 
    function index() {
        $data = $this->flow();
        $var_content_area['nav_link_side']=NULL;
        
        $pages = $this->uri->segment(2,0);
        if($pages == 0){
            $pages = 'jurnal';
        }
        $by["seo_url"]="jurnal";
        $by["status"]=4;
        $content=$this->am->getRow($by);
        
        $data["content"]=$content;

        $data['var_content_area']=$var_content_area;
        $data['content_slider']='main_blank';
		
	
        $data['data']=$this->getByid('kdjurnal',$id,'v_jurnalpublish');
        
        $data['content_area']='jurnal/halaman-ejurnal';
        $this->load->view('main',$data);
    }

	function detail_ejurnal(){
		$data = $this->flow();
        $var_content_area['nav_link_side']=NULL;
		
		$content=$this->am->getRow($by);
        
        $data["content"]=$content;
	
        $id=$this->uri->segment(4);
        $data['data']=$this->getByid('kdjurnal',$id,'v_jurnalpublish');
        $look=0;
        if($data['data']!=''){
            $look = $this->jumlahjurnal($id,'dilihat', 'jurnal');
            if($look == null){
                $look = 1;
            }else{
                $look = $look +1;
            }
                    
            $q = "update jurnal set dilihat=".$look." where kdjurnal = ".$id;
            $this->db->query($q);
        }
		 $this->load->view('jurnal/detail-halaman-ejurnal',$data);
    }
	
		function jumlahjurnal($id,$column,$tbl){
        $q = "SELECT max(".$column.") as max FROM ".$tbl." where kdjurnal=".$id ;
        $query  = $this->db->query($q);
        $max = ''; 
                    
        if ($query->num_rows() != 0)
        {
            $row = $query->row();
            $max=$row->max;
        }
        if ($max == null){
            $max=0;
        }
        return $max;
    }
	
	 function getfPage($tabel, $num, $offset)
    {
         $data = $this->db->get($tabel, $num, $offset);
         return $data->result();
    }
	
	function getByid($where,$value,$table){
        $this->db->where($where,$value);
        $query=$this->db->get($table);
        return $query->result();
    }
    

}
