<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    
    <link rel="shortcut icon" href="<?php echo base_url(); ?>resources/web-images/favicon-new.png" />
 
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>resources/js/ext/resources/css/ext-all.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>resources/js/ext/resources/css/xtheme-gray.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>resources/js/ext_plugins/statusbar/css/statusbar.css"/>
    
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>resources/js/ext/ux/css/Spinner.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>resources/js/ext/ux/data-view.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>resources/js/ext/ux/fileuploadfield.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>resources/css/examples.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>resources/js/ext/ux/RowEditor.css"/>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>resources/css/tabs.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>resources/img/icons/silk.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>resources/js/ext/ux/treegrid/treegrid.css" rel="stylesheet" />
    	
    <script type="text/javascript" src="<?php echo base_url(); ?>resources/js/ext/adapter/ext/ext-base.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>resources/js/ext/ext-all.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>application/framework/RH.js"></script>

	<script>
	    // PLUGIN TAMBAHAN HTML EDITOR

	</script>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>resources/css/htmleditorplugins.css" />
	<script src="<?php echo base_url(); ?>resources/js/plugins_htmleditor/Ext.ux.form.HtmlEditor.MidasCommand.js"></script>
    <script src="<?php echo base_url(); ?>resources/js/plugins_htmleditor/Ext.ux.form.HtmlEditor.Divider.js"></script>
    <script src="<?php echo base_url(); ?>resources/js/plugins_htmleditor/Ext.ux.form.HtmlEditor.HR.js"></script>
    <script src="<?php echo base_url(); ?>resources/js/plugins_htmleditor/Ext.ux.form.HtmlEditor.Image.js"></script>
    <script src="<?php echo base_url(); ?>resources/js/plugins_htmleditor/Ext.ux.form.HtmlEditor.RemoveFormat.js"></script>
    <script src="<?php echo base_url(); ?>resources/js/plugins_htmleditor/Ext.ux.form.HtmlEditor.IndentOutdent.js"></script>
    <script src="<?php echo base_url(); ?>resources/js/plugins_htmleditor/Ext.ux.form.HtmlEditor.SubSuperScript.js"></script>
    <script src="<?php echo base_url(); ?>resources/js/plugins_htmleditor/Ext.ux.form.HtmlEditor.RemoveFormat.js"></script>
    <script src="<?php echo base_url(); ?>resources/js/plugins_htmleditor/Ext.ux.form.HtmlEditor.FindAndReplace.js"></script>
    <script src="<?php echo base_url(); ?>resources/js/plugins_htmleditor/Ext.ux.form.HtmlEditor.Table.js"></script>
    <script src="<?php echo base_url(); ?>resources/js/plugins_htmleditor/Ext.ux.form.HtmlEditor.Word.js"></script>
    <script src="<?php echo base_url(); ?>resources/js/plugins_htmleditor/Ext.ux.form.HtmlEditor.Link.js"></script>
    <script src="<?php echo base_url(); ?>resources/js/plugins_htmleditor/Ext.ux.form.HtmlEditor.SpecialCharacters.js"></script>
    <script src="<?php echo base_url(); ?>resources/js/plugins_htmleditor/Ext.ux.form.HtmlEditor.UndoRedo.js"></script>
    <script src="<?php echo base_url(); ?>resources/js/plugins_htmleditor/Ext.ux.form.HtmlEditor.Heading.js"></script>
    <script src="<?php echo base_url(); ?>resources/js/plugins_htmleditor/Ext.ux.form.HtmlEditor.Plugins.js"></script>
	<script>
	    // this is how to override language strings in Midas Buttons
		if(Ext.ux.form.HtmlEditor.UndoRedo){
		   Ext.ux.form.HtmlEditor.UndoRedo.prototype.midasBtns[1].tooltip.title = "Oh crap - go back to the way it was!";
		}
	</script>
	<script>
	    // END PLUGIN TAMBAHAN HTML EDITOR

	</script>

    <script type="text/javascript">
        var BASE_URL = '<?php echo base_url(); ?>' + 'index.php/';
        var BASE_PATH = '<?php echo base_url(); ?>';
    </script>
    
    
    <style>
    body {
        background:#7F99BE;
    }
    #header {
        border-bottom:1px solid #666;
        background:#1E4176;
        padding:5px;
        color:#fff;
        font-size:20px;
        font-weight:bold;
        font-family:'Lucida Grande', Arial, Sans;
    }
    #detx {
        border-bottom:1px solid #666;
        background:#1E4176;
        padding:0px;
        color:#fff;
        font-size:5px;
        font-weight:bold;
        font-family:'Lucida Grande', Arial, Sans;
    }
    </style>
    <title>Skinzell</title>
</head>
<body>