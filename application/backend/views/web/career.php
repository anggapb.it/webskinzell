<?php $this->load->view('web/head_web'); ?>

                    <section>
                <div class="container">
                    <div class="row">
                        <div class="news-list-warp">
                            
                            <?php foreach ($menu_loker as $det) { ?>
                            <div class="col-md-6">
                                <div class="item-new-list grid-new"> <!-- add class no-position --> 

                                    <div class="feature-new-warp">
                                        <a href="#">
                                            <?php echo '<img src='.base_url().'resources/img/ori/'.$det->gambar.' style="width:100%; height:580px;"/>'; ?>
                                        </a>
                                    </div>
                                    <div class="box-new-info">
                                        <div class="new-info">
                                            <h4>
                                                <a><?php echo $det->judulind;?></a>
                                            </h4>
                                            <p><i class="fa fa-calendar" aria-hidden="true"></i><?php echo TanggalIndo(date("Ymd", strtotime($det->tglpublish))) ?></p>
                                            <p><i class="fa fa-user" aria-hidden="true"></i>By <?php echo $det->nmlengkap;?></p>
                                        </div>
                                        <div class="tapo">
                                            <p><?php echo $det->deskripsiind;?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                             <?php } ?>



                        </div>

                    </div>
                </div>
            </section>

<div class="clearfix"></div>

<?php $this->load->view('web/foot_web'); ?>
