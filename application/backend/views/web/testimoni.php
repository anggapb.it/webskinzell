
<!-- js check validasi kosong -->
<script type="text/javascript">
function formCheckTest(){
var dari = document.testimoni.inputPerson.value;
var deskripsi = document.testimoni.idpesan.value;
var captcha = document.testimoni.idcaptcha.value;

	if (dari == ''){
		Ext.MessageBox.alert("Informasi", "nama harus diisi.");
		document.getElementById('inputPerson').focus();
		document.getElementById('inputPerson').value='';
		return false;
	} else if (deskripsi == ''){
		Ext.MessageBox.alert("Informasi", "testimoni harus diisi.");
		document.getElementById('idpesan').focus();
		document.getElementById('idpesan').value='';
		return false;
	} else if (captcha == ''){
		Ext.MessageBox.alert("Informasi", "captcha harus diisi.");
		document.getElementById('idcaptcha').focus();
		document.getElementById('idcaptcha').value='';
		return false;
	} 
}
</script>										


    <?php $this->load->view('web/head_web'); ?>

            <!-- is search -->
        <div class="content-wrapper">
            <div class="gdlr-content">

                <!-- Above Sidebar Section-->

                <!-- Sidebar With Content Section-->
                <div class="with-sidebar-wrapper">
                    <div class="with-sidebar-container container">
                        <div class="with-sidebar-left twelve columns">
                            <div class="with-sidebar-content eight columns">
                                <section id="content-section-1">
                                    <div class="section-container container">

                                    	<form action="<?php print site_url();?>index.php/web/content/formtesti/" method="POST" name="testimonial" onsubmit="return formCheckTest(this)">
		                                <ul>
		                                <div>
											<br><?php echo $cekcaptcha;?><br>

											<label for="name">Name</label><br>
		                                	<input name="dari" size="100%" value="<?php echo set_value('dari'); ?>" type="text" 
		                                		onfocus="if(this.value=='') {this.value='';}" onblur="if(this.value=='') {this.value='Dari';}" />
		                                	<br><br>

		                                    <label for="name">Testimonial</label><br>
		                                    <textarea rows="3" cols="100%" name="deskripsi" class="txtarea" id="idpesan"><?php echo set_value('deskripsi'); ?></textarea>
											
		                                        <!-- captcha ci --> 
												<br><br>
												<label for="name">Chaptcha</label>
												<br>
												<?php echo $cap_img;?></li>
												<br>
												<input type="text" id="idcaptcha" name="captcha" value="" maxlength="20" 
													onfocus="if(this.value=='Chaptcha') {this.value='';}" onblur="if(this.value=='') {this.value='Chaptcha';}"/>
												<!-- end captcha ci --> 

											<br>&nbsp;<br>
											<button type="submit" id="submit3" class="submit-button"  name="get" onClick="return checkmail(this.form.email)">&nbsp; Send &nbsp;</button>

												<!-- <input class="btn1" type="submit" id="submit3" value="Simpan" onClick="return checkmail(this.form.email)" name="get"> --> 
											<br><br><br>
										</div>
		                                </ul>
									</form>

                                        <div class="clear"></div>
                                    </div>
                                </section>
                            </div>

                            <div class="gdlr-sidebar gdlr-left-sidebar four columns">
                                <div class="gdlr-item-start-content sidebar-left-item">
                                    <div id="text-6" class="widget widget_text gdlr-item gdlr-widget">
                                        <h3 class="gdlr-widget-title">Testimonial</h3>
                                        <div class="clear"></div>
                                        <div class="textwidget">Input your testimonial here.</div>
                                    </div>
                                    <div id="text-7" class="widget widget_text gdlr-item gdlr-widget">
                                        <h3 class="gdlr-widget-title">Contact Information</h3>
                                        <div class="clear"></div>
                                        <div class="textwidget">
                                            <p><i class="gdlr-icon fa fa-map-marker" style="color: #444444; font-size: 16px; "></i>Jalan Ir. H. Djuanda No. 4, 40116</p>
                                            <p><i class="gdlr-icon fa fa-phone" style="color: #444444; font-size: 16px; "></i> (022) 84288830</p>
                                            <p><i class="gdlr-icon fa fa-envelope" style="color: #444444; font-size: 16px; "></i> contact@utchotel.com</p>
                                            <p><i class="gdlr-icon fa fa-clock-o" style="color: #444444; font-size: 16px; "></i> Everyday 9:00-17:00</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="clear"></div>
                    </div>
                </div>


                    <section id="content-section-2">
                        <div class="gdlr-color-wrapper  gdlr-show-all no-skin" style="background-color: #ededed; padding-top: 80px; padding-bottom: 35px; ">
                            <div class="container">
                                <div class="gdlr-testimonial-item-wrapper">
                                    <div class="gdlr-item-title-wrapper gdlr-item pos-left gdlr-nav-container ">
                                        <div class="gdlr-item-title-head">
                                            <h3 class="gdlr-item-title gdlr-skin-title gdlr-skin-border">Testimonial</h3>
                                            <div class="gdlr-item-title-carousel"><i class="icon-angle-left gdlr-flex-prev"></i><i class="icon-angle-right gdlr-flex-next"></i></div>
                                            <div class="clear"></div>
                                        </div>
                                    </div>
                                    <div class="gdlr-item gdlr-testimonial-item carousel box-style">
                                        <div class="gdlr-ux gdlr-testimonial-ux">
                                            <div class="flexslider" data-type="carousel" data-nav-container="gdlr-testimonial-item" data-columns="1">
                                                
                                                <ul class="slides">

                                                	<?php foreach ($datatestimoni as $det) { ?>
                                                    <li class="testimonial-item">
                                                        <div class="testimonial-item-inner gdlr-skin-box">
                                                            <div class="testimonial-content gdlr-skin-content">
                                                                <p><?php echo $det->deskripsi; ?></p>
                                                            </div>
                                                            <div class="testimonial-info"><span class="testimonial-author gdlr-skin-link-color"><?php echo $det->dari; ?></span><span class="testimonial-position gdlr-skin-info"><span>
                                                            </div>
                                                            <div class="testimonial-author-image gdlr-skin-border"><img src="<?php echo base_url(); ?>resources/web-upload/user.png" alt="" width="150" height="150"></div>
                                                        </div>
                                                    </li>
                                                     <?php } ?>

                                                </ul>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clear"></div>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <div class="clear"></div>
                    </section>

                

            </div>
            <!-- gdlr-content -->
            <div class="clear"></div>
        </div>
        <!-- content wrapper -->

<?php $this->load->view('web/foot_web'); ?>
