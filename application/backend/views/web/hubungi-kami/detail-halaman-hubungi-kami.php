<?php $this->load->view('web/head_web'); ?>

                        <div class="contact-page clearfix">
    <div class="container">

        <!--contact form section-->
        <div class="blog-page-single clearfix">
            <article class="page type-page hentry  clearfix">
                <div class="full-width-contents">
                    <div class="entry-content">
                        <h2>Get in Touch</h2>
                        <p>Please contact us for any inquiry, use this form below.</p>
                    </div>
                </div>
            </article>
        </div>

        <div class="clearfix"></div>
        <!--contact form-->
        <div class="row">
            <div class="col-lg-5 col-md-5 col-sm-6 ">
                <form action="<?php print site_url();?>index.php/web/content/kontakhubkami/" method="POST" name="comment" onsubmit="return formCheck(this)">
                                    <?php echo $cekcaptcha;?>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <input type="text" class="form-control" name="nama" id="inputPerson" placeholder="First Name:" onfocus="this.value=''" value="<?php echo set_value('nama'); ?>"required>
                                        </div>
                                        <div class="col-md-6">
                                            <input type="email" class="form-control" name="email" id="inputMail" placeholder="Email:" onfocus="this.value=''" value="<?php echo set_value('email'); ?>" required>
                                        </div>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" name="tlp" id="inputPhone" placeholder="Phone:" onfocus="this.value=''" value="<?php echo set_value('tlp'); ?>" onchange="return validasiTlp()" required>
                                        </div>
                                        <div class="col-md-12">
                                            <textarea class="form-control" name="pesan" id="idpesan" rows="2" placeholder="Message" required><?php echo set_value('pesan'); ?></textarea>
                                        </div>

                                        <div class="col-md-3">
                                        <label for="name">Chaptcha</label>
                                                <br>
                                                <div style="width:300px;height:30px"><?php echo $cap_img;?></div>
                                                <br>
                                                <input class="form-control" type="text" id="idcaptcha" name="captcha" value="" maxlength="20" />
                                        </div>

                                        <div class="col-md-12">
                                            <input type="submit" id="submit3" value="Send Message" name="get" class="btn btn--secondary btn--block mt-10" onClick="return checkmail(this.form.email)">
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-12">
                                                <!--Alert Message-->
                                                <div class="contact-result"></div>
                                            </div>
                                    </div>
                                </form>
            </div>

            <!--contact detail-->
            <div class="col-lg-6 col-md-6 col-sm-6  col-lg-offset-1 col-md-offset-1">
                <div class="contact-sidebar clearfix">
                    <article class="address-area clearfix">

                        <div class="row">
                            <div class="col-md-6">
                                Please contact us for any inquiry.
                                </br></br>

                                <b>Clinic Location :</b>
                                </br>
                                Jalan Parahyangan Kavling 6-A No. 3
                                </br>
                                Kota Baru Parahyangan, Kel. Cipeundeuy, Kec. Padalarang, Kab. Bandung Barat, Prop. Jawa Barat
                                </br>
                               
                                <b>Contact :</b>
                                </br>
                                Phone : 0821-2010-0036
                                </br>
                               
                                <b>Open Hour :</b>
                                </br>
                                 09.00 - 19.00 
                                </br>
                             
                             
                            </div>
                        </div>
                    </article>
                    <!--social icons-->
                    <article class="social-icon clearfix">
                        <h5><span>Social :</span></h5>
                        <ul class="clearfix">
                            <li class="twitter-icon"><a target="_blank" href="https://twitter.com"><i class="fa fa-twitter"></i></a></li>
                            <li class="facebook-icon"><a target="_blank" href="https://id-id.facebook.com/"><i class="fa fa-facebook"></i></a></li>
                            <li class="instagram-icon"><a target="_blank"  href="http://www.instagram.com"><i class="fa fa-instagram"></i></a></li>
                            <li class="youtube-icon"><a target="_blank" href="https://www.youtube.com"><i class="fa fa-youtube"></i></a></li>
                        </ul>
                    </article>
                </div>
            </div>
        </div>

    </div>

    <!--google map -->
    </br></br></br>
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3403.73929977719!2d107.59480943447285!3d-6.888672347949051!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e68e6603f5b8f93%3A0x65b023128ec2b79a!2sParis%20Van%20Java%2C%20Cipedes%2C%20Kec.%20Sukajadi%2C%20Kota%20Bandung%2C%20Jawa%20Barat!5e0!3m2!1sen!2sid!4v1576607436091!5m2!1sen!2sid" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>

</div>


<?php $this->load->view('web/foot_web'); ?>

<!-- js check email -->
<script type="text/javascript">
var emailfilter=/^\w+[\+\.\w-]*@([\w-]+\.)*\w+[\w-]*\.([a-z]{2,4}|\d+)$/i;
function checkmail(e){
var returnval=emailfilter.test(e.value)
	if (document.getElementById('inputMail').value==''){
		return
	} else {
		if (returnval==false){
			Ext.MessageBox.alert("Informasi", "Ketik email dengan benar (example@mail.com).");
			e.select()
		} 
	}
	return returnval
}
</script>

<!-- js check validasi kosong -->
<script type="text/javascript">
function formCheck(){
var nama = document.comment.inputPerson.value;
var email = document.comment.inputMail.value;
var tlp = document.comment.inputPhone.value;
var pesan = document.comment.idpesan.value;
var captcha = document.comment.idcaptcha.value;

	if (nama == ''){
		Ext.MessageBox.alert("Informasi", "name harus diisi.");
		document.getElementById('inputPerson').focus();
		doc ument.getElementById('inputPerson').value='';
		return false;
	} else if (email == ''){
		Ext.MessageBox.alert("Informasi", "email harus diisi.");
		document.getElementById('inputMail').focus();
		document.getElementById('inputMail').value='';
		return false;
	} else if (tlp == ''){
		Ext.MessageBox.alert("Informasi", "phone harus diisi.");
		document.getElementById('inputPhone').focus();
		document.getElementById('inputPhone').value='';
		return false;
	} else if (pesan == ''){
		Ext.MessageBox.alert("Informasi", "text harus diisi.");
		document.getElementById('idpesan').focus();
		document.getElementById('idpesan').value='';
		return false;
	} else if (captcha == ''){
		Ext.MessageBox.alert("Informasi", "captcha harus diisi.");
		document.getElementById('idcaptcha').focus();
		document.getElementById('idcaptcha').value='';
		return false;
	} 
}
</script>

<!--js no.telpon angka only -->
<script type="text/javascript">
function validasiTlp(){
	var tlp = document.forms['comment']['tlp'].value;
	if(/\D/g.test(tlp)){
		Ext.MessageBox.alert("Informasi", "Input Phone harus digit 0-9!");
		document.forms['comment']['tlp'].value='';
		//membatalkan pengiriman form
		return false;
	}
}
</script>
