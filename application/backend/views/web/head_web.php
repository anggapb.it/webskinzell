<!DOCTYPE html>
<html lang="en-US"><!--<![endif]-->
<head>
    <!-- META TAGS -->
    <meta charset="UTF-8">

    <!-- Title -->
    <title>Skinzell</title>

    <!-- Define a viewport to mobile devices to use - telling the browser to assume that the page is as wide as the device (width=device-width) and setting the initial page zoom level to be 1 (initial-scale=1.0) -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="format-detection" content="telephone=no">
    <meta name="SKYPE_TOOLBAR" content ="SKYPE_TOOLBAR_PARSER_COMPATIBLE"/>
    <!-- favicon -->
    <link rel="shortcut icon" href="<?php echo base_url(); ?>resources/web-images/favicon-new.png">

    <!-- Google Web Font -->
    <link href="http://fonts.googleapis.com/css?family=Raleway:400,100,500,600,700,800,900,300,200" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic" rel="stylesheet" type="text/css">
    <!-- Include the bootstrap stylesheet -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>resources/web-css/bootstrap.css" type="text/css" media="all">
    <!-- Flexslider stylesheet -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>resources/web-css/flexslider.css" type="text/css" media="all">
    <!-- Animations stylesheet -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>resources/web-css/animations.css" type="text/css" media="all">
    <!-- Awesome Font stylesheet -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>resources/web-css/font-awesome.css" type="text/css" media="all">
    <!-- Datepciker stylesheet -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>resources/web-css/datepicker.css" type="text/css" media="all">
    <!-- Swipebox stylesheet -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>resources/web-css/swipebox.css" type="text/css" media="all">
    <!-- meanmenu stylesheet -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>resources/web-css/meanmenu.css" type="text/css" media="all">
    <!-- Include the site main stylesheet -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>resources/web-css/main.css" type="text/css" media="all">
    <!-- Include the site ressponsive  stylesheet -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>resources/web-css/custom-responsive.css" type="text/css" media="all">


    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
</head>
<body>

<!--[if lt IE 7]>
<p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="<php echo 'http://www.google.com/chromeframe/?redirect=true'; ?>">activate Google Chrome Frame</a> to improve your experience.</p>
<![endif]-->

<div class="header-top clearfix">
    <div class="container">
        <div class="row">
            <div class="col-md-5 ">
                <p>Welcome to Skinzell - Aesthetic Clinic</p>
            </div>
            <!--opening hours-->
            <div class="col-md-7 text-right">
                <p>
                    Opening Hours : Weekday (08.00-16.00) Weekend (10.00-18.00)
                </p>
            </div>

        </div>
    </div>
</div>


<header id="header">
    <div class="container">
        <div class="row">
            <!-- Website Logo -->
            <div class="logo clearfix">
                <a href="#">
                    <img src="<?php echo base_url(); ?>resources/web-images/logo-skinzell.png" alt="Skinzell">
                </a>
            </div>

            <!-- Main Navigation -->
            <nav class="main-menu">
                <ul class="header-nav clearfix" id="menu-main-menu">
                <?php 
                           foreach ($menuatas as $mnatas) { 
                           if (strlen($mnatas->kdmenuweb) == 2 && $mnatas->idstatus== 1) { 
                            if (!$mnatas->idhalaman) {
                                echo '<li>'; echo anchor("index.php/web/content/".$mnatas->url,$mnatas->nmmenuind);
                            } else {
                                echo '<li>'; echo anchor("index.php/web/content/profil/".$mnatas->idhalaman,$mnatas->nmmenuind);
                            }
                                echo '<ul>';
                                    foreach ($menuatas as $mnatas2) {
                                        if (strlen($mnatas2->kdmenuweb) == 4 && substr($mnatas2->kdmenuweb,0,-2) == $mnatas->kdmenuweb && $mnatas2->idstatus== 1) {
                                            if (!$mnatas2->idhalaman) {
                                                echo '<li>'; echo anchor("index.php/web/content/".$mnatas2->url,$mnatas2->nmmenuind);
                                            } else {
                                                echo '<li>'; echo anchor("index.php/web/content/profil/".$mnatas2->idhalaman,$mnatas2->nmmenuind);
                                            }
                                        echo '<ul>';
                                            foreach ($menuatas as $mnatas3) {
                                                if (strlen($mnatas3->kdmenuweb) == 6 && substr($mnatas3->kdmenuweb,0,-2) == $mnatas2->kdmenuweb && $mnatas3->idstatus== 1) {
                                                    if (!$mnatas3->idhalaman) {
                                                        echo '<li>'; echo anchor("index.php/web/content/".$mnatas3->url,$mnatas3->nmmenuind);
                                                    } else {
                                                        echo '<li>'; echo anchor("index.php/web/content/profil/".$mnatas3->idhalaman,$mnatas3->nmmenuind);
                                                    }
                                            }
                                        }   
                                        echo '</ul>';   
                                     echo '</li>';      
                                    }
                                }   
                                echo '</ul>';
                             echo '</li>';    
                            }
                           }
                        ?>
                
            </ul>
            </nav>
            <div id="responsive-menu-container"></div>
        </div>
    </div>
</header>



<div class="banner clearfix"></div>
<div class="page-top clearfix">
    <div class="container">
        <div class="row">
            <div class="col-lg-9 col-md-8 col-sm-7">
                <nav class="bread-crumb">
                    <ul class="breadcrumb clearfix">
                        <li><li class="active">Skinzell - Aesthetic Clinic</li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</div>






<?php
        function TanggalIndo($date){
            $BulanIndo = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");

            $tahun = substr($date, 0, 4);
            $bulan = substr($date, 4, 2);
            $tgl   = substr($date, 6, 2);
            
            $hari=date('w',  strtotime($tahun."-".$bulan."-".$tgl));
    
                switch($hari){     
                    case 0 : {
                                $hari='Sunday';
                            }break;
                    case 1 : {
                                $hari='Monday';
                            }break;
                    case 2 : {
                                $hari='Tuesday';
                            }break;
                    case 3 : {
                                $hari='Wednesday';
                            }break;
                    case 4 : {
                                $hari='Thursday';
                            }break;
                    case 5 : {
                                $hari="Friday";
                            }break;
                    case 6 : {
                                $hari='Saturday';
                            }break;
                    default: {
                                $hari='Unknown';
                            }break;
                }
            $result =$hari.", ".$tgl . " " . $BulanIndo[(int)$bulan-1] . " ". $tahun;       
            return($result);
        }
        
        
?>