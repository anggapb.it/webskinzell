<?php $this->load->view('web/head_web'); ?>
  
  <div class="gallery-page clearfix">
    <!--filter-->

    <div class="container isotope-wrapper text-center">
        <div class="row">
            <div id="isotope-container" class="clearfix">
                
                <!--column start-->

                <?php foreach($datax as $isi){ ?>
                <div class="col-md-3 col-sm-6 ">
                    <article class="common clearfix hentry four-col-gallery">
                        <figure class="overlay-effect">
                            <a href="#" title="Medical Record Keeping">
                                <img style="height:200px;width:100%;" src="<?php echo base_url().'resources/img/ori/'.$isi->file;?>" alt="gallery-1" />
                            </a>
                            <a class="overlay" href="<?php echo base_url().'resources/img/ori/'.$isi->file;?>"><i class="top"></i> <i class="bottom"></i></a>
                        </figure>
                        <div class="content clearfix">
                            <h5><?php echo $isi->nmgaleriind; ?></h5>

                        </div>
                    </article>
                </div>
                <?php } ?>

                <!--column end-->

            </div>
        </div>
    </div>
</div>

<?php $this->load->view('web/foot_web'); ?>

        