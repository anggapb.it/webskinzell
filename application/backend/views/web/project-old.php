<?php $this->load->view('web/head_web'); ?>

            <section>
                <div class="container">
                    <div class="row">

                        <div class="title-block text-center">
                                    <span class="top-title"></span>
                                    <h2>Projects</h2>
                                    <p class="sub-title">PT. Mandiri Maslahat Masagi</p>
                                    <span class="bottom-title"></span>
                                </div>
                                </br></br></br>

                        <div class="news-list-warp">
                            
                            <?php foreach ($menu_maincourse as $det) { ?>
                            <div class="col-md-6">
                                <div class="item-new-list grid-new"> <!-- add class no-position --> 

                                    <div class="feature-new-warp">
                                        <a href="#">
                                            <?php echo '<img src='.base_url().'resources/img/ori/'.$det->gambar.' style="width:100%; height:350px;"/>'; ?>
                                        </a>
                                    </div>
                                    <div class="box-new-info">
                                        <div class="new-info">
                                            <h4>
                                                <a href="single_new.html"><?php echo anchor("index.php/web/content/detail_blog/" .
                                                            $det->idhalaman."/".$det->idstposting, $det->judulind); ?></a>
                                            </h4>
                                            <p><i class="fa fa-calendar" aria-hidden="true"></i><?php echo TanggalIndo(date("Ymd", strtotime($det->tglpublish))) ?></p>
                                            <p><i class="fa fa-user" aria-hidden="true"></i>By <?php echo $det->nmlengkap;?></p>
                                        </div>
                                        <div class="tapo">
                                            <p><?php 
                                                if (strlen($det->sinopsisind) > 20) {
                                                echo substr(strip_tags($det->sinopsisind),0,60).' ...'; 
                                                }
                                                ?></p>
                                        </div>
                                        <div class="ot-btn btn-sub-color"><?php echo anchor("index.php/web/content/detail_blog/" .$det->idhalaman."/".$det->idstposting, "Learn More"); ?></div>
                                    </div>
                                </div>
                            </div>

                            <?php } ?>

                        </div>

                        <div class="col-md-12">
                            <ul class="pagination">
                                <li><a href="#">Page <?php echo $halaman; ?></a></li>
                            </ul>
                        </div>

                    </div>
                </div>
            </section>

<div class="clearfix"></div>

<?php $this->load->view('web/foot_web'); ?>
