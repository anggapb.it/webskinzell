<?php $this->load->view('web/head_web'); ?>

                <!-- is search -->
        <div class="content-wrapper">
            <div class="gdlr-content">

                <!-- Sidebar With Content Section-->
                <div class="with-sidebar-wrapper">
                    <section id="content-section-1">
                        <div class="section-container container">
                            <div class="blog-item-wrapper">
                                <div class="blog-item-holder">
                                    <div class="gdlr-isotope" data-type="blog" data-layout="fitRows">
                                        <div class="clear">

                                        </div><h3><center>HOTEL ROOMS</center></h3><?php foreach ($query as $det) { ?>
                                        <div class="three columns">
                                            <div class="gdlr-item gdlr-blog-grid">
                                                <div class="gdlr-ux gdlr-blog-grid-ux">

                                                    

                                                    <article id="post-859" class="post-859 post type-post status-publish format-standard has-post-thumbnail hentry category-blog category-fit-row">
                                                        <div class="gdlr-standard-style">
                                                            <div class="gdlr-blog-thumbnail">
                                                                <a href="#"> <img src="<?php echo base_url(); ?>resources/img/ori/<?php echo $det->gambar; ?>" alt="" style="width:400px; height:180px;"></a>
                                                            </div>

                                                            <h3 class="gdlr-heading-shortcode with-background " style="color: #ffffff;background-color: #804021;font-size: 14px;font-weight: bold;"><?php echo $det->sinopsisind; ?></h3>

                                                            <header class="post-header">
                                                                <h3 class="gdlr-blog-title"><a href="#"><?php echo anchor("index.php/web/content/reservation_detail/" .$det->idhalaman."/".$det->idstposting, $det->judulind); ?></a></h3>
                                                                <div class="clear"></div>
                                                            </header>
                                                            <!-- entry-header -->

                                                            <div class="gdlr-blog-content"><?php echo $det->deskripsiind; ?>
                                                                <div class="clear"></div></div>
                                                        </div>
                                                    </article>

                                                   

                                                    <!-- #post -->
                                                </div> 
                                            </div>
                                        </div><?php } ?>

                                        <div class="clear"></div>
                                        

                                        <h3><center>MEETING ROOMS</center></h3>
                                        

                                        <?php foreach ($meeting as $det) { ?>

                                        <div class="three columns">
                                            <div class="gdlr-item gdlr-blog-grid">
                                                <div class="gdlr-ux gdlr-blog-grid-ux">
                                                    <article id="post-43" class="post-43 post type-post status-publish format-standard has-post-thumbnail hentry category-blog category-fit-row category-life-style tag-blog tag-life-style tag-news">
                                                        <div class="gdlr-standard-style">
                                                            <div class="gdlr-blog-thumbnail">
                                                                <a href="#"> <img src="<?php echo base_url(); ?>resources/img/ori/<?php echo $det->gambar; ?>" alt="" style="width:400px; height:180px;"></a>
                                                            </div>

                                                            <h3 class="gdlr-heading-shortcode with-background " style="color: #ffffff;background-color: #c11d34;font-size: 14px;font-weight: bold;"><?php echo $det->sinopsisind; ?></h3>

                                                            <header class="post-header">
                                                                <h3 class="gdlr-blog-title"><a href="#"><?php echo anchor("index.php/web/content/reservation_detail/" .$det->idhalaman."/".$det->idstposting, $det->judulind); ?></a></h3>
                                                                <div class="clear"></div>
                                                            </header>
                                                            <!-- entry-header -->

                                                            <div class="gdlr-blog-content"><?php echo $det->deskripsiind; ?>
                                                                <div class="clear"></div></div>
                                                        </div>
                                                    </article>
                                                    <!-- #post -->
                                                </div>
                                            </div>
                                        </div>

                                        <?php } ?>

                                        <div class="clear"></div>
                                        

                                        <h3><center>MEETING PACKAGE</center></h3>

                                        <!--------------------- Meeting Package --------------------->  
                                        <div class="main-content-container container gdlr-item-start-content">
                                            <div class="gdlr-item gdlr-main-content">
                                            
                                            
                                            
                                                <div class="gdlr-space" style="margin-top: 20px;"></div>
                                                    <div class="gdlr-shortcode-wrapper gdlr-row-shortcode">

                                                        <?php foreach ($package as $det) { ?>
                                                        
                                                        <div class="three columns">
                                                            <div class="gdlr-item gdlr-column-shortcode">
                                                                <div class="gdlr-shortcode-wrapper">
                                                                    <div class="gdlr-box-with-icon-ux gdlr-ux">
                                                                        <div class="gdlr-item gdlr-box-with-icon-item pos-top type-circle">
                                                                            <div class="box-with-circle-icon" style="background-color: #91d549"><i class="fa fa-coffee" style="color:#ffffff;"></i>
                                                                                <br>
                                                                            </div>
                                                                            <!--<h4 class="box-with-icon-title"><?php echo anchor("index.php/web/content/detail/" .$det->idhalaman."/".$det->idstposting, $det->judulind); ?></h4>-->
                                                                            <h4 class="box-with-icon-title"><?php echo anchor("index.php/web/content/reservation_detail/" .$det->idhalaman."/".$det->idstposting, $det->judulind); ?></h4>
                                                                            <div class="clear"></div>
                                                                            <h3 class="gdlr-heading-shortcode with-background " style="color: #ffffff;background-color: #446ee4;font-size: 14px;font-weight: bold;"><?php echo $det->sinopsisind; ?></h3>
                                                                            <div class="box-with-icon-caption">
                                                                                <?php echo $det->deskripsiind; ?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                        <?php } ?>

                                                        <div class="clear"></div>
                                                    </div>
                                                    <p><b>*</b> Get Special Prices - call our marketing: 089 7291 9293 WA/0822 6227 2223 (Rony)</p>
                                                    <div class="clear"></div>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </section>
                </div>

                <!-- Below Sidebar Section-->

               

            </div>
            <!-- gdlr-content -->
            <div class="clear"></div>
        </div>
        <!-- content wrapper -->

<?php $this->load->view('web/foot_web'); ?>
