<?php $this->load->view('web/head_web_index'); ?>


    <!--general services-->
<div class="home-features clearfix">
    <div class="container">
        <div class="row">

            <div class="col-md-4">
                <div class="features-intro clearfix">
                    <h2><span>Skinzell</span> </br>Product and Service</h2>
                    <p>Selamat datang di website Skinzell - Aesthetic Clinic, Skinzell merupakan klinik kecantikan dan perawatan kulit</p>
                    <a class="read-more" href="<?php echo base_url(); ?>index.php/web/content/service">View Our Services</a>
                </div>
            </div>

            <div class="col-md-8">
                <div class="row">

                    <?php foreach ($service as $det) { ?>
                    <div class="col-sm-6 single-feature">
                        <div class="row">
                            <div class="col-sm-3 icon-wrapper">
                               <?php echo '<img src='.base_url().'resources/img/ori/'.$det->gambar.' style="width:80px; height:65px;"/>'; ?>
                            </div>
                            <div class="col-sm-9">
                                <h3><?php echo anchor("index.php/web/content/detail/" .$det->idhalaman."/".$det->idstposting, $det->judulind); ?></h3>
                                <p><?php echo $det->sinopsisind; ?></p>
                            </div>
                        </div>
                    </div>
                    <?php } ?>

                </div>
            </div>
        </div>
    </div>
</div>
<!--general services end-->

<!--doctors section-->
<div class="home-doctors  clearfix">
    <div class="container">
        <div class="slogan-section animated fadeInUp clearfix">
            <h2>Get Our <span>Newest Product</span></h2>
            <p>Dapatkan produk terbaik dari Skinzell</p>
        </div>

        <div class="row">
            
            <?php foreach ($product as $det) { ?>

            <div class="col-md-3 col-sm-6  text-center">
                <div class="common-doctor animated fadeInUp clearfix">
                    <figure>
                        <a href="">
                            <?php echo '<img src='.base_url().'resources/img/ori/'.$det->gambar.' style="width:100%; height:300px;"/>'; ?>
                        </a>
                    </figure>
                    <div class="text-content">
                        <h5><a href=""><?php echo anchor("index.php/web/content/detail/" .$det->idhalaman."/".$det->idstposting, $det->judulind); ?></a></h5>
                    </div>
                </div>
            </div>

            <?php } ?>

            <div class="visible-sm clearfix margin-gap"></div>
        </div>
    </div>
</div>
<!--doctors section end-->

<!--doctors section-->
<div class="home-doctors  clearfix">
    <div class="container">
        <div class="slogan-section animated fadeInUp clearfix">
            <h2>Get Our <span>Special Offer</span></h2>
            <p>Dapatkan promo terbaik dari Skinzell</p>
        </div>

        <div class="row">
            
            <?php foreach ($promo as $det) { ?>

            <div class="col-md-3 col-sm-6  text-center">
                <div class="common-doctor animated fadeInUp clearfix">
                    <figure>
                        <a href="">
                            <?php echo '<img src='.base_url().'resources/img/ori/'.$det->gambar.' style="width:100%; height:300px;"/>'; ?>
                        </a>
                    </figure>
                    <div class="text-content">
                        <h5><a href=""><?php echo anchor("index.php/web/content/detail/" .$det->idhalaman."/".$det->idstposting, $det->judulind); ?></a></h5>
                    </div>
                </div>
            </div>

            <?php } ?>

            <div class="visible-sm clearfix margin-gap"></div>
        </div>
    </div>
</div>
<!--doctors section end-->

<!--blog posts section-->
<div class="home-blog text-center clearfix">
    <div class="container">

        <a target="_blank" href="https://api.whatsapp.com/send?phone=6282120100036" class="float">
            <img src="<?php echo base_url(); ?>resources/web-images/wa2.png">
        </a>

        <div class="slogan-section animated fadeInUp clearfix">
            <h2>Latest News from <span>Skinzell</span></h2>
            <p>Berita dan informasi terkini seputar Skinzell dan Kecantikan</p>
        </div>
        <div class="row">

            <?php foreach ($berita as $det) { ?>

            <div class="col-md-4">
                <article class="common-blog-post animated fadeInRight clearfix">
                    <figure>
                        <a href="#">
                            <img src="<?php echo base_url().'resources/img/ori/'.$det->gambar;?>" />
                        </a>
                    </figure>
                    <div class="text-content clearfix">
                        <h5><a href="#"><?php echo anchor("index.php/web/content/detail/" .$det->idhalaman."/".$det->idstposting, $det->judulind); ?></a></h5>
                        <div class="entry-meta">
                            <span><?php echo TanggalIndo(date("Ymd", strtotime($det->tglpublish))) ?></span>, by <a href="#" ><?php echo $det->nmlengkap;?></a>
                        </div>
                        <div class="for-border"></div>
                        <p><?php echo $det->sinopsisind2;?></p>
                    </div>
                </article>
            </div>

            <?php } ?>

        </div>

        </br></br></br>
        <div class="slogan-section animated fadeInUp clearfix">
            <h2>Watch Our<span>Latest Video</span></h2>
            <p>Watch our latest video below</p>

            <iframe width="560" height="315" src="https://www.youtube.com/embed/mG71vVzNktI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

        </div>

    </div>
</div>
<!--blog posts section end-->

<!--testimonials section-->
<div class="home-testimonial  clearfix">
    <div class="container">
        <div class="text-center">
            <div class="slogan-section animated fadeInUp clearfix">
                <h2>What patients say <span>About Skinzell</span></h2>
                <p>Testimonial customer</p>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-1"></div>
            <div class="col-sm-10 text-center">
                <div class="flexslider-three animated fadeInUp">
                    <ul class="slides">

                        <?php foreach ($datatestimoni as $det) { ?>

                        <li>
                            <img class="img-circle" src="<?php echo base_url(); ?>resources/web-images/temp-images/ava.png" alt="author-22" />
                            <blockquote>
                                <p><?php echo $det->deskripsi; ?></p>
                            </blockquote>

                            <div class="testimonial-footer clearfix">
                                <h3><?php echo $det->dari; ?></h3>

                            </div>
                        </li>

                        <?php } ?> 

                    </ul>
                </div>
            </div>
            <div class="col-sm-1"></div>
        </div>
    </div>
</div>
<!--testimonials section end-->
  
  
<?php $this->load->view('web/foot_web'); ?>
