<?php $this->load->view('web/head_web'); ?>
        

                <!-- is search -->
        <div class="content-wrapper">
            <div class="gdlr-content">

                <!-- Above Sidebar Section-->

                <!-- Sidebar With Content Section-->

                <?php foreach($data as $isi):?>

                <div class="with-sidebar-wrapper">
                    <section id="content-section-1">
                        <div class="section-container container">
                            <div class="blog-item-wrapper">
                                <div class="blog-item-holder">
                                    <div class="gdlr-item gdlr-blog-medium">
                                        <div class="gdlr-ux gdlr-blog-medium-ux">
                                            <article id="post-859" class="post-859 post type-post status-publish format-standard has-post-thumbnail hentry category-blog category-fit-row">
                                                <div class="gdlr-standard-style">
                                                    <div class="gdlr-blog-thumbnail">
                                                        <a> <img src="<?php echo base_url(); ?>resources/img/ori/<?php echo $isi->gambar; ?>" style="width:400px; height:300px;"></a>
                                                    </div>

                                                    <div class="gdlr-blog-content-wrapper">
                                                        <header class="post-header">
                                                            <h3 class="gdlr-blog-title"><a><?php echo $isi->judulind?></a></h3>
                                                            <h3 class="gdlr-heading-shortcode with-background " style="color: #ffffff;background-color: #446ee4;font-size: 14px;font-weight: bold;"><?php echo $isi->sinopsisind; ?></h3>

                                                            <div class="clear"></div>
                                                        </header>
                                                        <!-- entry-header -->

                                                        <div class="gdlr-blog-content"><?php echo $isi->deskripsiind?>
                                                        </div>
                                                    </div>
                                                    <div class="clear"></div>
                                                </div>
                                            </article>
                                            <!-- #post -->
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </section>
                </div>

                <?php endforeach ?>

                <!-- Below Sidebar Section-->

            </div>
            <!-- gdlr-content -->
            <div class="clear"></div>
        </div>
        <!-- content wrapper -->


        
        <div class="content-wrapper">
            <div class="gdlr-content">

                <div class="with-sidebar-wrapper">
                    <div class="with-sidebar-container container gdlr-class-no-sidebar">
                        <div class="with-sidebar-left twelve columns">
                            <div class="with-sidebar-content twelve columns">
                                <div class="gdlr-item gdlr-item-start-content" id="gdlr-single-booking-content" data-ajax="https://demo.goodlayers.com/hotelmaster/wp-admin/admin-ajax.php">

                                    <form class="gdlr-reservation-bar" data-action="gdlr_hotel_booking">
                                        <div class="gdlr-reservation-bar-title">Your Reservation</div>
                                        <div class="gdlr-reservation-bar-summary-form" ></div>
                                        <div class="gdlr-reservation-bar-room-form" ></div>
                                        <div class="gdlr-reservation-bar-date-form">
                                            <div class="gdlr-reservation-field gdlr-resv-datepicker"><span class="gdlr-reservation-field-title">Check In</span>
                                                <div class="gdlr-datepicker-wrapper">
                                                    <input type="text" id="gdlr-check-in" class="gdlr-datepicker" autocomplete="off" data-dfm="d M yy" data-block="[&quot;2018-02-14&quot;,&quot;2018-02-15&quot;]" value="2018-05-03">
                                                    <input type="hidden" class="gdlr-datepicker-alt" name="gdlr-check-in" autocomplete="off" value="2018-05-03">
                                                </div>
                                            </div>
                                            <div class="gdlr-reservation-field gdlr-resv-combobox "><span class="gdlr-reservation-field-title">Night</span>
                                                <div class="gdlr-combobox-wrapper">
                                                    <select name="gdlr-night" id="gdlr-night">
                                                        <option value="1" selected="">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5">5</option>
                                                        <option value="6">6</option>
                                                        <option value="7">7</option>
                                                        <option value="8">8</option>
                                                        <option value="9">9</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="clear"></div>
                                            <div class="gdlr-reservation-field gdlr-resv-datepicker"><span class="gdlr-reservation-field-title">Check Out</span>
                                                <div class="gdlr-datepicker-wrapper">
                                                    <input type="text" id="gdlr-check-out" class="gdlr-datepicker" autocomplete="off" data-min-night="1" data-dfm="d M yy" data-block="[&quot;2018-02-14&quot;,&quot;2018-02-15&quot;]" value="2018-05-04">
                                                    <input type="hidden" class="gdlr-datepicker-alt" name="gdlr-check-out" autocomplete="off" value="2018-05-04">
                                                </div>
                                            </div>
                                            <div class="clear"></div>
                                            <div class="gdlr-reservation-field gdlr-resv-combobox gdlr-reservation-bar-room-number"><span class="gdlr-reservation-field-title">Rooms</span>
                                                <div class="gdlr-combobox-wrapper">
                                                    <select name="gdlr-room-number" id="gdlr-room-number">
                                                        <option value="1" selected="">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5">5</option>
                                                        <option value="6">6</option>
                                                        <option value="7">7</option>
                                                        <option value="8">8</option>
                                                        <option value="9">9</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="clear"></div>
                                            <div class="gdlr-reservation-people-amount-wrapper" id="gdlr-reservation-people-amount-wrapper">
                                                <div class="gdlr-reservation-people-amount">
                                                    <div class="gdlr-reservation-people-title">Room <span>1</span></div>
                                                    <div class="gdlr-reservation-field gdlr-resv-combobox "><span class="gdlr-reservation-field-title">Adults</span>
                                                        <div class="gdlr-combobox-wrapper">
                                                            <select name="gdlr-adult-number[]">
                                                                <option value="1">1</option>
                                                                <option value="2" selected="">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                                <option value="7">7</option>
                                                                <option value="8">8</option>
                                                                <option value="9">9</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="gdlr-reservation-field gdlr-resv-combobox "><span class="gdlr-reservation-field-title">Children</span>
                                                        <div class="gdlr-combobox-wrapper">
                                                            <select name="gdlr-children-number[]">
                                                                <option value="0">0</option>
                                                                <option value="1">1</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                                <option value="7">7</option>
                                                                <option value="8">8</option>
                                                                <option value="9">9</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="clear"></div>
                                                </div>
                                            </div>
                                            <div class="clear"></div><a id="gdlr-reservation-bar-button" class="gdlr-reservation-bar-button gdlr-button with-border" href="#">Check Availability</a>
                                            <div class="clear"></div>
                                        </div>
                                        <div class="gdlr-reservation-bar-service-form" id="gdlr-reservation-bar-service-form"></div>
                                    </form>
                                    <div class="gdlr-booking-content">
                                        <div class="gdlr-booking-process-bar" id="gdlr-booking-process-bar" data-state="1">
                                            <div data-process="1" class="gdlr-booking-process gdlr-active">1. Choose Date</div>
                                            <div data-process="2" class="gdlr-booking-process ">2. Choose Room</div>
                                            <div data-process="3" class="gdlr-booking-process ">3. Make a Reservation</div>
                                            <div data-process="4" class="gdlr-booking-process ">4. Confirmation</div>
                                        </div>
                                        <div class="gdlr-booking-content-wrapper">
                                            <div class="gdlr-booking-content-inner" id="gdlr-booking-content-inner">
                                                <div class="gdlr-datepicker-range-wrapper">
                                                    <div class="gdlr-datepicker-range" id="gdlr-datepicker-range" data-dfm="d M yy" data-block="[&quot;2018-02-14&quot;,&quot;2018-02-15&quot;]"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>

                            <div class="clear"></div>
                        </div>

                        <div class="clear"></div>
                    </div>
                </div>

            </div>
            <!-- gdlr-content -->
            <div class="clear"></div>
        </div>
        <!-- content wrapper -->

<?php $this->load->view('web/foot_web'); ?>
