<?php $this->load->view('web/head_web'); ?>

<div class="blog-page clearfix">
<div class="container">
<div class="row">
<div class="col-lg-9 col-md-8">
<div class="blog-post-single clearfix">

<div class="row">
<div class="col-sm-2">



</div>
<div class="col-sm-10">
    <article class="post format-gallery hentry clearfix">

        <?php foreach($profil as $isi):?>
        <div class="right-contents">
            <header class="entry-header">

                <figure>
                    
                        <img src="<?php echo base_url().'resources/img/ori/'.$isi->gambar;?>" class="attachment-blog-page wp-post-image" alt="news-2" />
                   
                </figure>

                <h3 class="entry-title"><?php echo $isi->judulind?></h3>
                   <span class="entry-author">
                         Posted by : <?php echo $isi->nmlengkap;?></a> | <?php echo TanggalIndo(date("Ymd", strtotime($isi->tglpublish))) ?>
                   </span>
            </header>

            <div class="entry-content">
                <p><?php echo $isi->deskripsiind?></p>
            </div>
        </div>
        <?php endforeach ?>

    </article>

</div>
</div>

</div>

</div>

    <?php $this->load->view('web/right-index-web'); ?>

</div>
</div>
</div>

<?php $this->load->view('web/foot_web'); ?>
