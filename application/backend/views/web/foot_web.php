

<!--page footer-->
<footer id="main-footer" class="site-footer clearfix">
    <div class="container">
        <div class="row">


            <!--about widget-->
            <div class="col-md-3 col-sm-6">
                <section  class="widget animated fadeInLeft">
                    <h3 class="title">About Skinzell</h3>
                    <div class="textwidget">
                        <img src="<?php echo base_url(); ?>resources/web-images/logo-skinzell.png">
                        </br>&nbsp;</br>
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod
                            tincidunt ut laoreet dolore magna aliquam erat.</p>
                    </div>
                </section>
            </div>


            <!--general services-->
            <div class="col-md-3 col-sm-6  ">
                <section class="widget animated fadeInLeft ae-animation-fadeInLeft">
                    <h3 class="title">Web Menu</h3>
                    <?php 
                                           foreach ($menuatas as $mnatas) { 
                                           if (strlen($mnatas->kdmenuweb) == 2 && $mnatas->idstatus== 1) { 
                                            if (!$mnatas->idhalaman) {
                                                echo '<li>'; echo anchor("index.php/web/content/".$mnatas->url,$mnatas->nmmenuind);
                                            } else {
                                                echo '<li>'; echo anchor("index.php/web/content/profil/".$mnatas->idhalaman,$mnatas->nmmenuind);
                                            }
                                                echo '<ul>';
                                                      
                                                echo '</ul>';
                                             echo '</li>';    
                                            }
                                           }
                                        ?>
                </section>
            </div>


            <!--about widget-->
            <div class="col-md-3 col-sm-6">
                <section  class="widget animated fadeInLeft">
                    <h3 class="title">Contact Us</h3>
                    <div class="textwidget">
                        <p>Hubungi kami via Whatsapp, klik tombol dibawah</p>
                        <a target="_blank" href="https://api.whatsapp.com/send?phone=6282120100036">
                            <img src="<?php echo base_url(); ?>resources/web-images/wa3.png" alt="Skinzell">
                        </a>
                    </div>
                </section>
            </div>


            <div class="clearfix visible-sm"></div>

        </div>
        <div class="footer-bottom animated fadeInDown clearfix">
            <div class="row">
                <div class="col-sm-7">
                    <p>&copy; Copyright 2019. All Rights Reserved by Skinzell</p>
                </div>
                <!--footer social icons-->
                <div class="col-sm-5 clearfix">
                    <ul class="footer-social-nav">
                        <li><a target="_blank" href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a target="_blank" href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a target="_blank" href="#"><i class="fa fa-instagram"></i></a></li>
                        <li><a target="_blank" href="#"><i class="fa fa-youtube"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>

<a href="#top" id="scroll-top"></a>

<script type='text/javascript' id='quick-js'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>resources/web-js/jquery-2.2.3.min.js'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>resources/web-js/bootstrap.min.js'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>resources/web-js/jquery.flexslider-min.js'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>resources/web-js/jquery.swipebox.min.js'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>resources/web-js/jquery.isotope.min.js'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>resources/web-js/jquery.appear.js'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>resources/web-js/jquery.ui.core.min.js'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>resources/web-js/jquery.ui.datepicker.min.js'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>resources/web-js/jquery.validate.min.js'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>resources/web-js/jquery.form.js'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>resources/web-js/jquery.autosize.min.js'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>resources/web-js/jquery.meanmenu.min.js'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>resources/web-js/jquery.velocity.min.js'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>resources/web-js/jquery-twitterFetcher.js'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>resources/web-js/respond.min.js'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>resources/web-js/jquery-migrate-1.2.1.min.js'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>resources/web-js/custom.js'></script>

</body>
</html>
