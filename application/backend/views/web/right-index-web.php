<div class="col-lg-3 col-md-4">
    <aside class="sidebar clearfix">
        <section  class="widget tabs-widget">
            <div class="tabs clearfix">
                <div class = "tab-head active">
                    <h6>Popular</h6>
                </div>
                <div class = "tab-head ">
                    <h6>Latest</h6>
                </div>


                <div class="tabs-content clearfix">
                
                <?php foreach ($postpopular as $det) { ?>
                    <div class="tab-post-listing clearfix">
                        <figure>
                            <a href="#">
                                <img src="<?php echo base_url(); ?>resources/img/ori/<?php echo $det->gambar; ?>" class="tabs-thumb wp-post-image" alt="news-2" />
                            </a>
                        </figure>
                        <div class="post-content">
                            <h6><a href="#"><?php echo anchor("index.php/web/content/detail/" .
                                                            $det->idhalaman."/".$det->idstposting, $det->judulind); //detail-halaman.html ?></a></h6>
                            <span><?php echo TanggalIndo(date("Ymd", strtotime($det->tglpublish))) ?></span>
                        </div>
                    </div>
                <?php } ?>

                </div>


                <div class="tabs-content clearfix">

                <?php foreach ($postrecent as $det) { ?>
                    
                <div class="tab-post-listing clearfix">
                    <figure>
                        <a href="#">
                            <img src="<?php echo base_url(); ?>resources/img/ori/<?php echo $det->gambar; ?>" class="tabs-thumb wp-post-image" alt="news-2" />
                        </a>
                    </figure>
                    <div class="post-content">
                        <h6><a href="#"><?php echo anchor("index.php/web/content/detail/" .
                                                            $det->idhalaman."/".$det->idstposting, $det->judulind); //detail-halaman.html ?></a></h6>
                        <span><?php echo TanggalIndo(date("Ymd", strtotime($det->tglpublish))) ?></span>
                    </div>
                </div>

                <?php } ?>

            </div>

            </div>
        </section>
    </aside>
</div>