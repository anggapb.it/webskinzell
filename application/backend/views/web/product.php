<?php $this->load->view('web/head_web'); ?>

<div class="services-page clearfix">
    <div class="container">
        <div class="row ">

            <?php foreach ($product as $det) { ?>
            <!--column start-->
            <div class="col-md-4 col-sm-6">
                <article class="service type-service hentry three-col-service">
                    <figure>
                    &nbsp;</br>
                         <center><?php echo '<img src='.base_url().'resources/img/ori/'.$det->gambar.' style="width:100%; height:300px;"/>'; ?></center>
                    </figure>
                    <div class="contents clearfix">
                        <center><h4><a href="#"><?php echo anchor("index.php/web/content/detail/" .
                                                            $det->idhalaman."/".$det->idstposting, $det->judulind); ?></a></h4></center>
                        <div class="entry-content">
                             <center><p><?php echo $det->sinopsisind; ?></p></center>
                        </div>
                    </div>
                </article>
            </div>
            <!--column end-->
            <?php } ?>

        </div>
    </div>
</div>


<?php $this->load->view('web/foot_web'); ?>
