<?php $this->load->view('web/head_web'); ?>

            <section>
                <div class="container">
                    <div class="row">

                        <?php foreach($data as $isi):?>
                        <div class="col-md-9">
                            <div class="single-team-warp">
                                <div class="header-st">
                                    <div class="header-st-inner">
                                        <center><?php echo '<img src='.base_url().'resources/img/ori/'.$isi->gambar.' style="width:100%; height:300px;"/>'; ?></center>
                                    </div>
                                </div>
                                <div class="body-st">
                                    <h4><?php echo $isi->judulind?></h4>
                                    <p><i class="fa fa-calendar" aria-hidden="true"></i> <?php echo TanggalIndo(date("Ymd", strtotime($isi->tglpublish))) ?></p>
                                        <p><i class="fa fa-user" aria-hidden="true"></i> By <?php echo $isi->nmlengkap;?></p>
                                    <br>
                                    <p>
                                        <?php echo $isi->deskripsiind?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <?php endforeach ?>

                        <div class="col-md-3">
                            <div class="sideabar">

                                <div class="widget widget-sidebar widget-list-link">
                                    <h4 class="title-widget-sidebar">
                                        Latest Project 
                                    </h4>
                                    <ul class="wd-list-link">

                                        <?php foreach ($menu_maincourse as $det) { ?>

                                        <li><?php echo anchor("index.php/web/content/detail_blog/" .
                                                            $det->idhalaman."/".$det->idstposting, $det->judulind); //detail-halaman.html ?></li>

                                        <?php } ?>

                                    </ul>
                                </div>

                                <div class="widget widget-sidebar widget-list-link">
                                    <h4 class="title-widget-sidebar">
                                        Latest News 
                                    </h4>
                                    <ul class="wd-list-link">

                                        <?php foreach ($berita as $det) { ?>

                                        <li><?php echo anchor("index.php/web/content/detail_blog/" .
                                                            $det->idhalaman."/".$det->idstposting, $det->judulind); //detail-halaman.html ?></li>

                                        <?php } ?>

                                    </ul>
                                </div>

                                <!--<div class="widget-sidebar widget widget-html">
                                    <div class="wd-html-block">
                                        <a href="#">
                                            <img src="http://placehold.it/269x180/ccc.jpg" class="img-responsive" alt="Image">
                                        </a>
                                        <div class="content-wd-html-inner">
                                            <span>HIRING</span>
                                            <p>
                                                COME TO JOIN OUR TEAM !
                                            </p>
                                        </div>
                                        <a href="#" class="ot-btn btn-sub-color" >
                                            Join Now
                                        </a>
                                    </div>
                                </div>-->


                            </div>
                        </div>
                    </div>
                </div>
            </section>

<?php $this->load->view('web/foot_web'); ?>