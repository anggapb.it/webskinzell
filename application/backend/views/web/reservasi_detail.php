<?php $this->load->view('web/head_web'); ?>
        

                <!-- is search -->
        <div class="content-wrapper">
            <div class="gdlr-content">

                <!-- Above Sidebar Section-->

                <!-- Sidebar With Content Section-->
                <div class="with-sidebar-wrapper">
                    <section id="content-section-1">
                        <div class="section-container container">
                            <div class="blog-item-wrapper">
                                <div class="blog-item-holder">
                                    <div class="gdlr-item gdlr-blog-medium">
                                        <div class="gdlr-ux gdlr-blog-medium-ux">
                                            <article id="post-859" class="post-859 post type-post status-publish format-standard has-post-thumbnail hentry category-blog category-fit-row">
                                                <div class="gdlr-standard-style">
                                                    <div class="gdlr-blog-thumbnail">
                                                        <a href="#"> <img src="upload\photodune-1576481-relaxing-m-400x300.jpg" alt="" width="400" height="300"></a>
                                                        <div class="gdlr-sticky-banner"><i class="fa fa-bullhorn"></i>Sticky Post</div>
                                                    </div>
                                                    <div class="blog-date-wrapper gdlr-title-font">
                                                        <span class="blog-date-day">3</span>
                                                        <span class="blog-date-month">Dec</span>
                                                    </div>

                                                    <div class="gdlr-blog-content-wrapper">
                                                        <header class="post-header">
                                                            <div class="gdlr-blog-info gdlr-info">
                                                                <div class="blog-info blog-author"><i class="fa fa-pencil"></i><a href="#" title="Posts by John Doe" rel="author">John Doe</a></div>
                                                                <div class="blog-info blog-comment"><i class="fa fa-comment-o"></i><a href="#">0</a></div>
                                                                <div class="blog-info blog-category"><i class="fa fa-folder-open-o"></i><a href="#" rel="tag">Blog</a><span class="sep">,</span> <a href="#" rel="tag">Fit Row</a></div>
                                                                <div class="clear"></div>
                                                            </div>
                                                            <h3 class="gdlr-blog-title"><a href="#">Sedial eiusmod tempor</a></h3>

                                                            <div class="clear"></div>
                                                        </header>
                                                        <!-- entry-header -->

                                                        <div class="gdlr-blog-content">Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Idque Caesaris facere voluntate liceret: sese habere. Magna pars studiorum, prodita quaerimus. Magna pars studiorum, prodita quaerimus. Fabio vel iudice vincam, sunt in culpa qui officia. Vivamus sagittis lacus vel augue laoreet rutrum faucibus. Nihilne te nocturnum praesidium Palati, nihil urbis vigiliae. Non equidem invideo, miror magis posuere velit aliquet. Qui ipsorum lingua Celtae, nostra Galli appellantur. Prima luce, cum quibus mons aliud consensu ab eo. Petierunt uti sibi concilium totius Galliae in diem certam indicere. Lorem ipsum dolor sit amet, consectetur...
                                                            <div class="clear"></div><a href="#" class="excerpt-read-more">Continue Reading<i class="fa fa-long-arrow-right icon-long-arrow-right"></i></a></div>
                                                    </div>
                                                    <div class="clear"></div>
                                                </div>
                                            </article>
                                            <!-- #post -->
                                        </div>
                                    </div>
                                    <div class="gdlr-item gdlr-blog-medium">
                                        <div class="gdlr-ux gdlr-blog-medium-ux">
                                            <article id="post-852" class="post-852 post type-post status-publish format-standard has-post-thumbnail hentry category-fit-row tag-blog tag-life-style">
                                                <div class="gdlr-standard-style">
                                                    <div class="gdlr-blog-thumbnail">
                                                        <a href="#"> <img src="upload\photodune-3290449-concierges-holding-the-cart-and-posing-m-400x300.jpg" alt="" width="400" height="300"></a>
                                                    </div>
                                                    <div class="blog-date-wrapper gdlr-title-font">
                                                        <span class="blog-date-day">3</span>
                                                        <span class="blog-date-month">Dec</span>
                                                    </div>

                                                    <div class="gdlr-blog-content-wrapper">
                                                        <header class="post-header">
                                                            <div class="gdlr-blog-info gdlr-info">
                                                                <div class="blog-info blog-author"><i class="fa fa-pencil"></i><a href="#" title="Posts by John Doe" rel="author">John Doe</a></div>
                                                                <div class="blog-info blog-comment"><i class="fa fa-comment-o"></i><a href="#">2</a></div>
                                                                <div class="blog-info blog-category"><i class="fa fa-folder-open-o"></i><a href="#" rel="tag">Fit Row</a></div>
                                                                <div class="clear"></div>
                                                            </div>
                                                            <h3 class="gdlr-blog-title"><a href="#">Donec luctus imperdiet</a></h3>

                                                            <div class="clear"></div>
                                                        </header>
                                                        <!-- entry-header -->

                                                        <div class="gdlr-blog-content">Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Idque Caesaris facere voluntate liceret: sese habere. Magna pars studiorum, prodita quaerimus. Magna pars studiorum, prodita quaerimus. Fabio vel iudice vincam, sunt in culpa qui officia. Vivamus sagittis lacus vel augue laoreet rutrum faucibus. Nihilne te nocturnum praesidium Palati, nihil urbis vigiliae. Non equidem invideo, miror magis posuere velit aliquet. Qui ipsorum lingua Celtae, nostra Galli appellantur. Prima luce, cum quibus mons aliud consensu ab eo. Petierunt uti sibi concilium totius Galliae in diem certam indicere. Lorem ipsum dolor sit amet, consectetur...
                                                            <div class="clear"></div><a href="#" class="excerpt-read-more">Continue Reading<i class="fa fa-long-arrow-right icon-long-arrow-right"></i></a></div>
                                                    </div>
                                                    <div class="clear"></div>
                                                </div>
                                            </article>
                                            <!-- #post -->
                                        </div>
                                    </div>
                                    <div class="gdlr-item gdlr-blog-medium">
                                        <div class="gdlr-ux gdlr-blog-medium-ux">
                                            <article id="post-862" class="post-862 post type-post status-publish format-standard has-post-thumbnail hentry category-blog category-fit-row tag-blog tag-link tag-news">
                                                <div class="gdlr-standard-style">
                                                    <div class="gdlr-blog-thumbnail">
                                                        <a href="#"> <img src="upload\photodune-2681114-penthouse-room-on-a-sunny-day-m-400x300.jpg" alt="" width="400" height="300"></a>
                                                    </div>
                                                    <div class="blog-date-wrapper gdlr-title-font">
                                                        <span class="blog-date-day">3</span>
                                                        <span class="blog-date-month">Dec</span>
                                                    </div>

                                                    <div class="gdlr-blog-content-wrapper">
                                                        <header class="post-header">
                                                            <div class="gdlr-blog-info gdlr-info">
                                                                <div class="blog-info blog-author"><i class="fa fa-pencil"></i><a href="#" title="Posts by John Doe" rel="author">John Doe</a></div>
                                                                <div class="blog-info blog-comment"><i class="fa fa-comment-o"></i><a href="#">2</a></div>
                                                                <div class="blog-info blog-category"><i class="fa fa-folder-open-o"></i><a href="#" rel="tag">Blog</a><span class="sep">,</span> <a href="#" rel="tag">Fit Row</a></div>
                                                                <div class="clear"></div>
                                                            </div>
                                                            <h3 class="gdlr-blog-title"><a href="#">Magna pars studiorum</a></h3>

                                                            <div class="clear"></div>
                                                        </header>
                                                        <!-- entry-header -->

                                                        <div class="gdlr-blog-content">Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Idque Caesaris facere voluntate liceret: sese habere. Magna pars studiorum, prodita quaerimus. Magna pars studiorum, prodita quaerimus. Fabio vel iudice vincam, sunt in culpa qui officia. Vivamus sagittis lacus vel augue laoreet rutrum faucibus. Nihilne te nocturnum praesidium Palati, nihil urbis vigiliae. Non equidem invideo, miror magis posuere velit aliquet. Qui ipsorum lingua Celtae, nostra Galli appellantur. Prima luce, cum quibus mons aliud consensu ab eo. Petierunt uti sibi concilium totius Galliae in diem certam indicere. Lorem ipsum dolor sit amet, consectetur...
                                                            <div class="clear"></div><a href="#" class="excerpt-read-more">Continue Reading<i class="fa fa-long-arrow-right icon-long-arrow-right"></i></a></div>
                                                    </div>
                                                    <div class="clear"></div>
                                                </div>
                                            </article>
                                            <!-- #post -->
                                        </div>
                                    </div>
                                    <div class="gdlr-item gdlr-blog-medium">
                                        <div class="gdlr-ux gdlr-blog-medium-ux">
                                            <article id="post-858" class="post-858 post type-post status-publish format-standard has-post-thumbnail hentry category-fit-row category-life-style tag-animal tag-safari">
                                                <div class="gdlr-standard-style">
                                                    <div class="gdlr-blog-thumbnail">
                                                        <a href="#"> <img src="upload\photodune-4864105-guest-filling-up-a-formular-at-hotel-counter-m-400x300.jpg" alt="" width="400" height="300"></a>
                                                    </div>
                                                    <div class="blog-date-wrapper gdlr-title-font">
                                                        <span class="blog-date-day">3</span>
                                                        <span class="blog-date-month">Dec</span>
                                                    </div>

                                                    <div class="gdlr-blog-content-wrapper">
                                                        <header class="post-header">
                                                            <div class="gdlr-blog-info gdlr-info">
                                                                <div class="blog-info blog-author"><i class="fa fa-pencil"></i><a href="#" title="Posts by John Doe" rel="author">John Doe</a></div>
                                                                <div class="blog-info blog-comment"><i class="fa fa-comment-o"></i><a href="#">3</a></div>
                                                                <div class="blog-info blog-category"><i class="fa fa-folder-open-o"></i><a href="#" rel="tag">Fit Row</a><span class="sep">,</span> <a href="#" rel="tag">Life Style</a></div>
                                                                <div class="clear"></div>
                                                            </div>
                                                            <h3 class="gdlr-blog-title"><a href="#">Eiusmod tempor incidunt</a></h3>

                                                            <div class="clear"></div>
                                                        </header>
                                                        <!-- entry-header -->

                                                        <div class="gdlr-blog-content">Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Idque Caesaris facere voluntate liceret: sese habere. Magna pars studiorum, prodita quaerimus. Magna pars studiorum, prodita quaerimus. Fabio vel iudice vincam, sunt in culpa qui officia. Vivamus sagittis lacus vel augue laoreet rutrum faucibus. Nihilne te nocturnum praesidium Palati, nihil urbis vigiliae. Non equidem invideo, miror magis posuere velit aliquet. Qui ipsorum lingua Celtae, nostra Galli appellantur. Prima luce, cum quibus mons aliud consensu ab eo. Petierunt uti sibi concilium totius Galliae in diem certam indicere. Lorem ipsum dolor sit amet, consectetur...
                                                            <div class="clear"></div><a href="#" class="excerpt-read-more">Continue Reading<i class="fa fa-long-arrow-right icon-long-arrow-right"></i></a></div>
                                                    </div>
                                                    <div class="clear"></div>
                                                </div>
                                            </article>
                                            <!-- #post -->
                                        </div>
                                    </div>
                                    <div class="gdlr-item gdlr-blog-medium">
                                        <div class="gdlr-ux gdlr-blog-medium-ux">
                                            <article id="post-43" class="post-43 post type-post status-publish format-standard has-post-thumbnail hentry category-blog category-fit-row category-life-style tag-blog tag-life-style tag-news">
                                                <div class="gdlr-standard-style">
                                                    <div class="gdlr-blog-thumbnail">
                                                        <a href="#"> <img src="upload\photodune-4076311-interior-of-modern-comfortable-hotel-room-m-400x300.jpg" alt="" width="400" height="300"></a>
                                                    </div>
                                                    <div class="blog-date-wrapper gdlr-title-font">
                                                        <span class="blog-date-day">5</span>
                                                        <span class="blog-date-month">Oct</span>
                                                    </div>

                                                    <div class="gdlr-blog-content-wrapper">
                                                        <header class="post-header">
                                                            <div class="gdlr-blog-info gdlr-info">
                                                                <div class="blog-info blog-author"><i class="fa fa-pencil"></i><a href="#" title="Posts by John Doe" rel="author">John Doe</a></div>
                                                                <div class="blog-info blog-comment"><i class="fa fa-comment-o"></i><a href="#">1</a></div>
                                                                <div class="blog-info blog-category"><i class="fa fa-folder-open-o"></i><a href="#" rel="tag">Blog</a><span class="sep">,</span> <a href="#" rel="tag">Fit Row</a><span class="sep">,</span> <a href="#" rel="tag">Life Style</a></div>
                                                                <div class="clear"></div>
                                                            </div>
                                                            <h3 class="gdlr-blog-title"><a href="#">Standard Post Format Title</a></h3>

                                                            <div class="clear"></div>
                                                        </header>
                                                        <!-- entry-header -->

                                                        <div class="gdlr-blog-content">Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Idque Caesaris facere voluntate liceret: sese habere. Magna pars studiorum, prodita quaerimus. Magna pars studiorum, prodita quaerimus. Fabio vel iudice vincam, sunt in culpa qui officia. Vivamus sagittis lacus vel augue laoreet rutrum faucibus. Nihilne te nocturnum praesidium Palati, nihil urbis vigiliae. Non equidem invideo, miror magis posuere velit aliquet. Qui ipsorum lingua Celtae, nostra Galli appellantur. Prima luce, cum quibus mons aliud consensu ab eo. Petierunt uti sibi concilium totius Galliae in diem certam indicere. Lorem ipsum dolor sit amet, consectetur...
                                                            <div class="clear"></div><a href="#" class="excerpt-read-more">Continue Reading<i class="fa fa-long-arrow-right icon-long-arrow-right"></i></a></div>
                                                    </div>
                                                    <div class="clear"></div>
                                                </div>
                                            </article>
                                            <!-- #post -->
                                        </div>
                                    </div>
                                    <div class="gdlr-item gdlr-blog-medium">
                                        <div class="gdlr-ux gdlr-blog-medium-ux">
                                            <article id="post-876" class="post-876 post type-post status-publish format-standard has-post-thumbnail hentry category-blog category-fit-row tag-blog tag-business tag-identity-2 tag-life-style">
                                                <div class="gdlr-standard-style">
                                                    <div class="gdlr-blog-thumbnail">
                                                        <a href="#"> <img src="upload\photodune-2673580-brown-sofas-the-lobby-m-400x300.jpg" alt="" width="400" height="300"></a>
                                                    </div>
                                                    <div class="blog-date-wrapper gdlr-title-font">
                                                        <span class="blog-date-day">3</span>
                                                        <span class="blog-date-month">Oct</span>
                                                    </div>

                                                    <div class="gdlr-blog-content-wrapper">
                                                        <header class="post-header">
                                                            <div class="gdlr-blog-info gdlr-info">
                                                                <div class="blog-info blog-author"><i class="fa fa-pencil"></i><a href="#" title="Posts by John Doe" rel="author">John Doe</a></div>
                                                                <div class="blog-info blog-comment"><i class="fa fa-comment-o"></i><a href="#">0</a></div>
                                                                <div class="blog-info blog-category"><i class="fa fa-folder-open-o"></i><a href="#" rel="tag">Blog</a><span class="sep">,</span> <a href="#" rel="tag">Fit Row</a></div>
                                                                <div class="clear"></div>
                                                            </div>
                                                            <h3 class="gdlr-blog-title"><a href="#">Diem certam indicere</a></h3>

                                                            <div class="clear"></div>
                                                        </header>
                                                        <!-- entry-header -->

                                                        <div class="gdlr-blog-content">Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Idque Caesaris facere voluntate liceret: sese habere. Magna pars studiorum, prodita quaerimus. Magna pars studiorum, prodita quaerimus. Fabio vel iudice vincam, sunt in culpa qui officia. Vivamus sagittis lacus vel augue laoreet rutrum faucibus. Nihilne te nocturnum praesidium Palati, nihil urbis vigiliae. Non equidem invideo, miror magis posuere velit aliquet. Qui ipsorum lingua Celtae, nostra Galli appellantur. Prima luce, cum quibus mons aliud consensu ab eo. Petierunt uti sibi concilium totius Galliae in diem certam indicere. Lorem ipsum dolor sit amet, consectetur...
                                                            <div class="clear"></div><a href="#" class="excerpt-read-more">Continue Reading<i class="fa fa-long-arrow-right icon-long-arrow-right"></i></a></div>
                                                    </div>
                                                    <div class="clear"></div>
                                                </div>
                                            </article>
                                            <!-- #post -->
                                        </div>
                                    </div>
                                </div>
                                <div class="gdlr-pagination"><span aria-current='page' class='page-numbers current'>1</span>
                                    <a class='page-numbers' href='page\2\index.html'>2</a>
                                    <a class="next page-numbers" href="page\2\index.html">Next &rsaquo;</a></div>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </section>
                </div>

                <!-- Below Sidebar Section-->

            </div>
            <!-- gdlr-content -->
            <div class="clear"></div>
        </div>
        <!-- content wrapper -->


        
        <div class="content-wrapper">
            <div class="gdlr-content">

                <div class="with-sidebar-wrapper">
                    <div class="with-sidebar-container container gdlr-class-no-sidebar">
                        <div class="with-sidebar-left twelve columns">
                            <div class="with-sidebar-content twelve columns">
                                <div class="gdlr-item gdlr-item-start-content" id="gdlr-single-booking-content" data-ajax="https://demo.goodlayers.com/hotelmaster/wp-admin/admin-ajax.php">

                                    <form class="gdlr-reservation-bar" data-action="gdlr_hotel_booking">
                                        <div class="gdlr-reservation-bar-title">Your Reservation</div>
                                        <div class="gdlr-reservation-bar-summary-form" ></div>
                                        <div class="gdlr-reservation-bar-room-form" ></div>
                                        <div class="gdlr-reservation-bar-date-form">
                                            <div class="gdlr-reservation-field gdlr-resv-datepicker"><span class="gdlr-reservation-field-title">Check In</span>
                                                <div class="gdlr-datepicker-wrapper">
                                                    <input type="text" id="gdlr-check-in" class="gdlr-datepicker" autocomplete="off" data-dfm="d M yy" data-block="[&quot;2018-02-14&quot;,&quot;2018-02-15&quot;]" value="2018-05-03">
                                                    <input type="hidden" class="gdlr-datepicker-alt" name="gdlr-check-in" autocomplete="off" value="2018-05-03">
                                                </div>
                                            </div>
                                            <div class="gdlr-reservation-field gdlr-resv-combobox "><span class="gdlr-reservation-field-title">Night</span>
                                                <div class="gdlr-combobox-wrapper">
                                                    <select name="gdlr-night" id="gdlr-night">
                                                        <option value="1" selected="">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5">5</option>
                                                        <option value="6">6</option>
                                                        <option value="7">7</option>
                                                        <option value="8">8</option>
                                                        <option value="9">9</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="clear"></div>
                                            <div class="gdlr-reservation-field gdlr-resv-datepicker"><span class="gdlr-reservation-field-title">Check Out</span>
                                                <div class="gdlr-datepicker-wrapper">
                                                    <input type="text" id="gdlr-check-out" class="gdlr-datepicker" autocomplete="off" data-min-night="1" data-dfm="d M yy" data-block="[&quot;2018-02-14&quot;,&quot;2018-02-15&quot;]" value="2018-05-04">
                                                    <input type="hidden" class="gdlr-datepicker-alt" name="gdlr-check-out" autocomplete="off" value="2018-05-04">
                                                </div>
                                            </div>
                                            <div class="clear"></div>
                                            <div class="gdlr-reservation-field gdlr-resv-combobox gdlr-reservation-bar-room-number"><span class="gdlr-reservation-field-title">Rooms</span>
                                                <div class="gdlr-combobox-wrapper">
                                                    <select name="gdlr-room-number" id="gdlr-room-number">
                                                        <option value="1" selected="">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5">5</option>
                                                        <option value="6">6</option>
                                                        <option value="7">7</option>
                                                        <option value="8">8</option>
                                                        <option value="9">9</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="clear"></div>
                                            <div class="gdlr-reservation-people-amount-wrapper" id="gdlr-reservation-people-amount-wrapper">
                                                <div class="gdlr-reservation-people-amount">
                                                    <div class="gdlr-reservation-people-title">Room <span>1</span></div>
                                                    <div class="gdlr-reservation-field gdlr-resv-combobox "><span class="gdlr-reservation-field-title">Adults</span>
                                                        <div class="gdlr-combobox-wrapper">
                                                            <select name="gdlr-adult-number[]">
                                                                <option value="1">1</option>
                                                                <option value="2" selected="">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                                <option value="7">7</option>
                                                                <option value="8">8</option>
                                                                <option value="9">9</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="gdlr-reservation-field gdlr-resv-combobox "><span class="gdlr-reservation-field-title">Children</span>
                                                        <div class="gdlr-combobox-wrapper">
                                                            <select name="gdlr-children-number[]">
                                                                <option value="0">0</option>
                                                                <option value="1">1</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                                <option value="7">7</option>
                                                                <option value="8">8</option>
                                                                <option value="9">9</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="clear"></div>
                                                </div>
                                            </div>
                                            <div class="clear"></div><a id="gdlr-reservation-bar-button" class="gdlr-reservation-bar-button gdlr-button with-border" href="#">Check Availability</a>
                                            <div class="clear"></div>
                                        </div>
                                        <div class="gdlr-reservation-bar-service-form" id="gdlr-reservation-bar-service-form"></div>
                                    </form>
                                    <div class="gdlr-booking-content">
                                        <div class="gdlr-booking-process-bar" id="gdlr-booking-process-bar" data-state="1">
                                            <div data-process="1" class="gdlr-booking-process gdlr-active">1. Choose Date</div>
                                            <div data-process="2" class="gdlr-booking-process ">2. Choose Room</div>
                                            <div data-process="3" class="gdlr-booking-process ">3. Make a Reservation</div>
                                            <div data-process="4" class="gdlr-booking-process ">4. Confirmation</div>
                                        </div>
                                        <div class="gdlr-booking-content-wrapper">
                                            <div class="gdlr-booking-content-inner" id="gdlr-booking-content-inner">
                                                <div class="gdlr-datepicker-range-wrapper">
                                                    <div class="gdlr-datepicker-range" id="gdlr-datepicker-range" data-dfm="d M yy" data-block="[&quot;2018-02-14&quot;,&quot;2018-02-15&quot;]"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>

                            <div class="clear"></div>
                        </div>

                        <div class="clear"></div>
                    </div>
                </div>

            </div>
            <!-- gdlr-content -->
            <div class="clear"></div>
        </div>
        <!-- content wrapper -->

<?php $this->load->view('web/foot_web'); ?>
