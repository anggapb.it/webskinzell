<!-- Sidebar -->
					<div class="col-lg-3 col-md-3 col-sm-4 sidebar">
						
					<!-- Upcoming Events -->
						<div class="sidebar-box white animate-onscroll">
							<h3>Artikel Populer</h3>
							<ul class="upcoming-events">
							
								<?php foreach ($postpopular as $det) { ?>
								<!-- Event -->
								<li>
									<div class="date">
										<span>
											<img src="<?php echo base_url(); ?>resources/img/ori/<?php echo $det->gambar; ?>" alt="image" width="50px" height="40px">
										</span>
									</div>
									
									<div class="event-content">
										<h6><a href=""><?php echo anchor("index.php/web/content/detail/" .
                                                            $det->idhalaman."/".$det->idstposting, $det->judulind); //detail-halaman.html ?></a></h6>
										<ul class="event-meta">
											<li><i class="icons icon-clock"></i><?php echo TanggalIndo(date("Ymd", strtotime($det->tglpublish))) ?></li>
										</ul>
									</div>
								</li>
								<!-- /Event -->
							 <?php } ?>
							 
							</ul>
						</div>
						
						<!-- Flickr Photos -->
						<div class="sidebar-box white animate-onscroll">
							<h3>LOKASI</h3>
							
							<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3317.520062304237!2d116.55741710193173!3d-8.574981676102317!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x1264582c0a17e15f!2sSTIKES+HAMZAR+MAMBEN+LOMBOK+TIMUR!5e0!3m2!1sid!2sid!4v1516169709535" width="300" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>
							
						</div>
						<!-- /Flickr Photos -->	
						
						
						
					</div>
					<!-- /Sidebar -->