<?php $this->load->view('header'); ?>
<!-- PLUGIN dan kawan2  -->   

      <script type="text/javascript">
                                
        var USERNAME='';
        var L_MEMBER='';
		var NM_KLP='';
		
		        Ext.Ajax.request({
                url:BASE_URL + 'c_tools/get_user',
                method:'POST',
                success: function(response){
                    var r = Ext.decode(response.responseText);
                    USERID = r.user_id;
                    USERNAME = r.username;
                    L_MEMBER = r.level_member;
					NM_KLP = r.nm_klp;
					
					if (!USERID) {
						window.location = BASE_URL + 'user/login';
					} else {
						  Ext.MessageBox.show({
							msg: 'Proses mendapatkan data...',
							title: 'Loading..',
							width:300,
							wait:true,
							waitConfig: {interval:600}
                    	});
					}
                    
                }

            });
			
           
    </script>
    
    <script type="text/javascript" src="<?php echo base_url(); ?>resources/js/ext/ux/treegrid/TreeGridSorter.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>resources/js/ext/ux/treegrid/TreeGridColumnResizer.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>resources/js/ext/ux/treegrid/TreeGridNodeUI.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>resources/js/ext/ux/treegrid/TreeGridLoader.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>resources/js/ext/ux/treegrid/OverrideNodeUI.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>resources/js/ext/ux/treegrid/TreeGridColumns.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>resources/js/ext/ux/treegrid/TreeGrid.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>resources/js/ext/ux/CheckColumn.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>resources/js/ext/ux/Ext.ux.grid.Search.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>resources/js/ext/ux/RowEditor.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>resources/js/ext/ux/Spinner.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>resources/js/ext/ux/SpinnerField.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>resources/js/ext/ux/GroupSummary.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>resources/js/ext/ux/form/NumberField.js"></script>


    <script type="text/javascript" src="<?php echo base_url(); ?>resources/js/ext/ux/DataView-more.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>resources/js/ext/ux/FileUploadField.js"></script>
        
<!-- ================== -->
    <script type="text/javascript" src="<?php echo base_url(); ?>application/frontend/store/DataMaster.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>application/frontend/store/DataMasterPublic.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>application/frontend/m_product.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>application/frontend/m_image.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>application/frontend/find_m.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>application/frontend/upload_m.js"></script>

<!--main-->
    <script type="text/javascript" src="<?php echo base_url(); ?>application/frontend/main/control_page.js"></script>
    
<!--    new priv    -->
    <script type="text/javascript" src="<?php echo base_url(); ?>application/frontend/utility/u1_menu.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>application/frontend/utility/u2_jdashboard.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>application/frontend/utility/u3_usergroup.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>application/frontend/utility/u4_otority.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>application/frontend/utility/u5_user.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>application/frontend/utility/u6_userlog.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>application/frontend/utility/u7_setting.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>application/frontend/utility/u8_proguser.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>application/frontend/utility/u9_pps.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>application/frontend/utility/u10_password.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>application/frontend/utility/u11_klpsetting.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>application/frontend/utility/u12_setting.js"></script>
    
	<!-- <script type="text/javascript" src="<?php echo base_url(); ?>application/frontend/e_akademic/mahasiswa/tabMahasiswa.js"></script>  -->
        <script type="text/javascript" src="<?php echo base_url(); ?>application/frontend/e_akademic/mahasiswa/pMahasiswa.js"></script>
    
        <script type="text/javascript" src="<?php echo base_url(); ?>application/frontend/main/search_bar.js"></script>
    
    
    
<!--    website     -->
    <script type="text/javascript" src="<?php echo base_url(); ?>application/frontend/website/menuwebsite.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>application/frontend/website/menuwebsite_form.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>application/frontend/website/kategorihalaman.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>application/frontend/website/kategorihalaman_form.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>application/frontend/website/halaman.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>application/frontend/website/halaman_form.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>application/frontend/website/kelompokgaleri.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>application/frontend/website/kelompokgaleri_form.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>application/frontend/website/galeri.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>application/frontend/website/galeri_form.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>application/frontend/website/kelompoktautan.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>application/frontend/website/kelompoktautan_form.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>application/frontend/website/tautan.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>application/frontend/website/tautan_form.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>application/frontend/website/perusahaan.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>application/frontend/website/perusahaan_form.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>application/frontend/website/posisi.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>application/frontend/website/posisi_form.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>application/frontend/website/lowongankerja.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>application/frontend/website/lowongankerja_form.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>application/frontend/website/hub_kami_info.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>application/frontend/website/hub_kami_inbox.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>application/frontend/website/hub_kami_inbox_form.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>application/frontend/website/rek_bank.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>application/frontend/website/rek_bank_form.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>application/frontend/website/download.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>application/frontend/website/download_form.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>application/frontend/website/jkerjasama.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>application/frontend/website/jkerjasama_form.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>application/frontend/website/kerjasama.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>application/frontend/website/kerjasama_form.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>application/frontend/website/kegiatan.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>application/frontend/website/kegiatan_form.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>application/frontend/website/testimoni.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>application/frontend/website/testimoni_form.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>application/frontend/website/poster.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>application/frontend/website/poster_form.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>application/frontend/website/stposting.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>application/frontend/website/stposting_form.js"></script>

<!--    registrasi     -->
    <script type="text/javascript" src="<?php echo base_url(); ?>application/frontend/registrasi/provinsi.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>application/frontend/registrasi/provinsi_form.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>application/frontend/registrasi/regional.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>application/frontend/registrasi/regional_form.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>application/frontend/registrasi/stakreditasi.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>application/frontend/registrasi/stakreditasi_form.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>application/frontend/registrasi/stners.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>application/frontend/registrasi/stners_form.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>application/frontend/registrasi/jabatan.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>application/frontend/registrasi/jabatan_form.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>application/frontend/registrasi/negaratujuan.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>application/frontend/registrasi/negaratujuan_form.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>application/frontend/registrasi/periode.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>application/frontend/registrasi/periode_form.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>application/frontend/registrasi/daftarmember.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>application/frontend/registrasi/daftarmember_form.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>application/frontend/registrasi/prosedurmember.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>application/frontend/registrasi/bayar_member.js"></script>
	
<!--upload-->
    <script type="text/javascript" src="<?php echo base_url(); ?>application/frontend/website/file_list.js"></script>
    
 
<!-- e-library -->
    <script type="text/javascript" src="<?php echo base_url(); ?>application/frontend/e_jurnal/jurnal_list.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>application/frontend/e_jurnal/jurnal_form.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>application/frontend/e_jurnal/jnsjurnal.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>application/frontend/e_jurnal/jnsjurnal_form.js"></script>
     
    <script type="text/javascript" src="<?php echo base_url(); ?>application/frontend/main/MainMenu.js"></script>   

   

<?php $this->load->view('footer'); ?>