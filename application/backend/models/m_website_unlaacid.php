<?php

class M_website_unlaacid extends Model {

    function __construct() {
        parent::__construct();
    }

    function getByid($where,$value,$table){
        $this->db->where($where,$value);
        $query=$this->db->get($table);
        return $query->result();
    }
    
	 
    
	function getSelectByid($select,$where,$value,$table){
        $this->db->select($select);
        $this->db->where($where,$value);
        $query=$this->db->get($table);
        return $query->result();
    }
    function getWithoutid($where,$value,$table){
//        $this->db->where($where,$value);
//        $query=$this->db->get($table);
        $q="SELECT * from ".$table." where ".$where." != '".$value."'";
        
        $query  = $this->db->query($q);
        $data = array();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        }
        return $data;
    }
	
	
    
    function getAll($table,$order,$desasc) {
        
        if($order != ""){
            $this->db->order_by($order,$desasc);
        }
        if($table=="halaman"){
            $this->db->where('idstpublish !=', 0) ;
        }
        $query = $this->db->get($table);//'soppmb');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return array();
        }
    }

    function getAllkerjasama($table,$order,$desasc) {
        
        if($order != ""){
            $this->db->order_by($order,$desasc);
        }
        if($table=="kerjasama"){
            $this->db->where('idstatus !=', 0) ;
        }
        $query = $this->db->get($table);//'soppmb');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return array();
        }
    }


    
    function getfPageById($tabel, $num, $offset,$where,$value)
    {
        $this->db->where($where,$value);
         
         $data = $this->db->get($tabel, $num, $offset);
         return $data->result();
    }
	
	  function getfPage($tabel, $num, $offset)
    {
         $data = $this->db->get($tabel, $num, $offset);
         return $data->result();
    }
	
	function login_member($nomember,$pass){
			$q = "SELECT * FROM daftarmember WHERE nomember = '".$nomember."'
					AND password = '".$pass."'";
			$query = $this->db->query($q);
			$data = array();
			if($query->num_rows() > 0){
				$data = $query->result();
			}
			 return $data;
	} 
	
	function cekkuitansi($nomember){
			$q = "select count(nomember) as jml from kuitansi where nomember='".$nomember."'";
			$query = $this->db->query($q);
			$data = array();
			if($query->num_rows() > 0){
				$data = $query->result();
			}
			 return $data;
	} 
		
	function getjadwalpmb($nopmb){

        $q = "SELECT getjadwalpmb('".$nopmb."') as jdwl;";
		
        $query  = $this->db->query($q);
        $data = array(); 
                    
        if ($query->num_rows() > 0)
        {
            $data = $query->result();
        }
        return $data;
    }
		
	function periksausm(){
			$q = "SELECT * FROM v_periksausm WHERE nopmb = '".$this->my_usessionpmb->userdata('nopmbunla')."'";
			$query = $this->db->query($q);
			$data = array();
			if($query->num_rows() > 0){
				$data = $query->result();
			}
			 return $data;
	} 
	
	function caridata(){ // keperluan e-jurnal
		$c = $this->input->POST ('nama');
		$this->db->like('nmjnsjurnal', $c);
		$this->db->or_like('issn', $c);
		$this->db->or_like('judul', $c);
		$this->db->or_like('penulis', $c);
		$data = $this->db->get ('v_jurnalpublish');
		return $data->result(); 
	}
	
	function update_download($kdjurnal,$userid){ // keperluan e-jurnal
		$download;
		$query = $this->db->query('SELECT download FROM jurnal WHERE kdjurnal ="'.$kdjurnal.'"');
		foreach ($query->result() as $row){
			$download = $row->download;
		}
	
		$updownload = $download + 1;
		
        $data = array(
            'download'=>$updownload,
        );
        
        $where['kdjurnal']=$kdjurnal;
        $this->db->where($where);
        $this->db->update("jurnal", $data);
		
		$this->simpandownload($kdjurnal,$userid);
    }
	 
	function id_field($column,$tbl,$whereb, $wherea){ // keperluan e-jurnal
        $q = "SELECT ".$column." as id FROM ".$tbl." where ".$whereb." = '".$wherea."' " ;
        $query  = $this->db->query($q);
        $id = ''; 
                    
        if ($query->num_rows() != 0)
        {
            $row = $query->row();
            $id=$row->id;
        }
        return $id;
    }  
	
	function simpandownload($kdjurnal,$userid){ // keperluan e-jurnal
		$user=$this->id_field('userid', 'pengguna', 'nmlengkap',$userid);
        $this->db->query("CALL SP_simpan_jurnal_download (?,?)",
            array(
                $userid,
                $kdjurnal, 
            ));
    }
	
	function simpanpesan(){
		$date = date("Y-m-d H:i:s");
		$ip = $_SERVER['REMOTE_ADDR'];
		$nmcom = gethostbyaddr($_SERVER['REMOTE_ADDR']);
		$data = array(
			 'nama'=>$this->input->POST('nama'),
             'email'=>$this->input->POST('email'),
			 'tlp'=>$this->input->POST('tlp'),
			 'pesanmsk'=>$this->input->POST('pesan'),
			 'status'=>"Belum",
			 'tglpsnmsk'=>$date,
			 'ipaddresspengirim'=>$ip,
			 'nmkompengirim'=>$nmcom,
        );
        $this->db->insert('hubkamiinbox', $data);
	}
	
	function simpantestimoni(){
		$tgljaminput = date("Y-m-d H:i:s");
		$tglpublish = date("Y-m-d");
		$jampublish = date("H:i:s");
		/* $ip = $_SERVER['REMOTE_ADDR'];
		$nmcom = gethostbyaddr($_SERVER['REMOTE_ADDR']); */
		$data = array(
			 'dari'=>$this->input->POST('dari'),
             'deskripsi'=>$this->input->POST('deskripsi'),
			 'tgljaminput'=>$tgljaminput,
			 'idstpublish'=>0,
			 'tglpublish'=>$tglpublish,
			 'jampublish'=>$jampublish,
        );
        $this->db->insert('testimoni', $data);
	}
	
    function view_bank(){

        $q = "SELECT * FROM rekbank ORDER BY idrekbank";
        
        $query  = $this->db->query($q);
        $data = array(); 
                    
        if ($query->num_rows() > 0)
        {
            $data = $query->result();
        }
        return $data;
    }

	/* function kerjasama(){
		/* $this->db->like('nmjnsjurnal', $c);
		$this->db->or_like('issn', $c);
		$this->db->or_like('judul', $c);
		$this->db->or_like('penulis', $c);
		$data = $this->db->get ('v_jurnalpublish');
		return $data->result();
		
		$this->db->select("*");
        $this->db->from("halaman"); 
		$this->db->where("idhalaman = 27 AND kdhalaman = 27");
		$q = "SELECT * FROM menuweb WHERE idmenuweb = '33'";
		$data=$this->db->query($q);
		return $data->result();
	} */

}

?>
