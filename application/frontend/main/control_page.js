function control_page(id_module){
    function pengembangan(){
        Ext.MessageBox.alert("Informasi",'Menu Masih Tahap Pengembangan. ^_^ ');
    }
	
	function bantuan(){
        //alert('Help!!!!!!!!!!!');
		window.open('../resources/bantuan/Manual Book Unla Public.pdf','_blank');
    }
    
     switch(id_module){
        case "0101": u_menu();break;     //utility
        case "0102": u2();break;
        case "0103": u3();break;
        case "0104": u4();break;        //otoritas x
        case "0105": u5();break;
        case "0106": u6();break;        // log pengguna x
        case "0107": f_ganti_password();break;//u7();break;        // ganti password x
        case "0108": pengembangan();break;//u8();break;        // pengaturan umum x
        case "0109": f_ppstudi();break;//u9();break;        // pengguna program studi x
        case "0110": bantuan();break;
		case "0111": u11_klpsetting();break;
		case "0112": u12_setting();break;
        
        case "0401": f_menuwebsite();break;     //website
        case "0402": f_kategorihalaman();break;
        case "0403": f_halaman();break;
        case "0404": f_kelompokgaleri();break;
        case "0405": f_galeri();break;
        case "0406": f_file();break;//f_uploadfile();break;      //x
        case "0407": f_perusahaan();break;      //x
        case "0408": f_posisi();break;          //x
        case "0409": f_lowongankerja();break;   //x
        case "0410": f_kelompoktautan();break;
        case "0411": f_tautan();break; //
		case "0412": f_hub_kami_info();break; //
		case "0413": f_hub_kami_inbox();break; //
        case "0414": rek_bank();break; //
        case "0415": download();break; //
        case "0416": jkerjasama();break; //
        case "0417": kerjasama();break; //
        case "0418": poster();break; //
        case "0421": kegiatan();break; //
        case "0422": testimoni();break; //
        case "0423": stposting();break; //
		
		//Registrasi//		
		case "0501": provinsi();break; //
		case "0502": regional();break; //
		case "0503": stakreditasi();break; //
		case "0504": stners();break; //
		case "0505": jabatan();break; //
		case "0506": negaratujuan();break; //
		case "0507": periode();break; //
		case "0508": daftarmember();break; //
		case "0509": prosedurmember();break; //
		case "0510": bayar_member();break; //
        
		//e-library//
		case "0601": jnsjurnal();break;//();break;
		case "0602": jurnal_list();break;//();break;

      
        case 1000:
 
           Ext.Ajax.request({
                url: BASE_URL + 'user/ext_logout',
                method: 'POST',
                success: function(xhr) {
                    window.location = BASE_URL + 'user/login';
                }
            });
     
        break;
       
        //default : alert("Info Menu", "Menu Belum Didaftarkan");
    }
     
}
