// setInterval(function(){
//		var d=new Date();
//		var t=d.toLocaleTimeString();
//		Ext.getCmp('lb.time').setValue(t);
//	},1000)
        
function do_logout() {
        Ext.Ajax.request({
		url: BASE_URL + 'user/ext_logout',
		method: 'POST',
		success: function(xhr) {
			window.location = BASE_URL + 'user/login';
		}
	});
}

function get_content(objek) {
	var centerRegion = Ext.getCmp("centerRegion");
	centerRegion.removeAll();
	centerRegion.add(objek);
	centerRegion.doLayout();
}
//function do_home() {
//    Ext.Ajax.request({
//        url: BASE_URL + 'user/index',
//        method: 'POST',
//        success: function(xhr) {
//            window.location = BASE_URL + 'user/index';
//        }
//    });
//}
var task = new Ext.util.DelayedTask(function() {
	tab_center = new Ext.Panel({
		xtype: 'container',
		region: 'center',
		layout: 'fit',
		id: 'centerRegion',
		split: true,
		autoEl: {},
		items: []
	});
	menuTreeLoader = new Ext.tree.TreeLoader({
		dataUrl: BASE_URL + 'c_mutama/getTree',
		baseParams: {
			id: "1",
			OTOR: USERNAME
		},
		listeners: {
			'load': function() {
				Ext.MessageBox.hide();
			}
		}
	});
	tree = new Ext.tree.TreePanel({
		id: 'menu-tree',
		region: 'west',
		title: 'Menu',
		iconCls: 'silk-sitemap',
		split: true,
		width: 200,
		//height: 200,
		minSize: 175,
		maxSize: 400,
		collapsible: true,
		margins: '0 0 5 5',
		loader: menuTreeLoader,
		rootVisible: false,
		lines: false,
		autoScroll: true,
		root: new Ext.tree.AsyncTreeNode({
			expanded: false
		}),
		listeners: {
			'click': function(n) {
				//var sn=this.selModel.selNode||{};
				var kd_menu; //,url;
				if (n.leaf) {
					kd_menu = n.attributes.kode;
					//url=n.attributes.url;
					//alert(kd_menu);
					control_page(kd_menu);
				}
			},
			'afterlayout': function() {
				// Ext.MessageBox.hide();
			}
		}
	});
	layout_main = new Ext.Viewport({
		layout: 'border',
		renderTo: Ext.getBody(),
		defaults: {
			collapsible: true,
			split: true,
			frame: false,
			bodyStyle: 'padding: 0px 0px 0px 1px'
		},
		items: [{
			 collapsible: false,
                         region:'north',
                         height:62,
                         border:false,
                         split:false,
                         margins:'0 0 0 0',
                         //bodyStyle: 'background-image:-webkit-gradient(linear,50% 0,50% 100%,color-stop(0%,#4796ca),color-stop(100%,#29709e));background-image:-webkit-linear-gradient(top,#4796ca,#29709e);background-image:-moz-linear-gradient(top,#4796ca,#29709e);background-image:-o-linear-gradient(top,#4796ca,#29709e);background-image:linear-gradient(top,#4796ca,#29709e);border-bottom:1px solid #4796ca;',
                         items:[new Ext.Toolbar({
				height: 62,
				items: [
                       {
						xtype:'tbtext',
						//style: 'font-weight: bold;text-align: right;color: darkblue;',
						text: "<img src='"+ BASE_PATH +"/resources/web-images/logo-back.png' />",
                        },'->',
						{
					text: '<div style="color: white">Dashboard</div>',
					id: 'st_dsh',
					iconCls: 'silk-dash',
					handler: function() {
						//            dashboard(ROLE_ID);
						}
						}, '<div style="color: white">|</div>',
						{
					text: 'Status Logout',
					id: 'sts_logout_id',
					iconCls: 'silk-user'
                                }, '<div style="color: white">|</div>',
						{
                    text: 'Logout',
                    id: 'id_pengguna_',
                    handler: do_logout
                                        
					}],
					style: 'padding:0px;background:#eee;font-family:"Lucida Grande";height: 62px;-moz-box-shadow:0px 0px 3px 1px rgba(0,0,0,0.3);-webkit-box-shadow:0px 0px 3px 1px rgba(0,0,0,0.3);box-shadow:0px 0px 3px 1px rgba(0,0,0,0.3);background-image:-webkit-gradient(linear,50% 0,50% 100%,color-stop(0%,#dab262),color-stop(100%,#c7922a));background-image:-webkit-linear-gradient(top,#dab262,#c7922a);background-image:-moz-linear-gradient(top,#dab262,#c7922a);background-image:-o-linear-gradient(top,#dab262,#c7922a);background-image:linear-gradient(top,#dab262,#c7922a);border-bottom:1px solid #c7922a;',	
                    //background-image:-webkit-gradient(linear,50% 0,50% 100%,color-stop(0%,#3EBA02),color-stop(100%,#2F8605));background-image:-webkit-linear-gradient(top,#3EBA02,#2F8605);background-image:-moz-linear-gradient(top,#3EBA02,#2F8605);background-image:-o-linear-gradient(top,#3EBA02,#2F8605);background-image:linear-gradient(top,#3EBA02,#2F8605);border-bottom:1px solid #567422    
			})]
		},
//		{
//			collapsible: false,
//			xtype: 'panel',
//			height: 30,
//			border: false,
//			region: 'south',
//			items: []
//		},
		tree,{
                    collapsible: false,
                    region: 'center',
                    layout: 'card',
                    activeItem: 0,
                    items:[tab_center]}
//		{
//			collapsible: false,
//			id: 'content-panel',
//			bodyStyle: 'padding:0px',
//			region: 'center',
//			layout: 'card',
//			margins: '0 0 0 0',
//			activeItem: 0,
//			border: true,
//			//width: 1000, 
//			//       tbar: tbCenter, 
//			items: [tab_center]
//		}
            ]
	});
	//=====================================================================================
	layout_main.show();
	Ext.getCmp("sts_logout_id").setText("<font color='white'>Pengguna: </font>" + "<font color='white'>" + NM_KLP);
	Ext.getCmp("id_pengguna_").setText("<font color='white'>Logout (</font>"+"<font color='white'>"+USERNAME+"</font>"+ "<font color='white'>)</font>");
  
});
task.delay(2000);