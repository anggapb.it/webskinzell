var isEmpty = function(idKey){
	var key = '';
	if(Ext.getCmp(idKey))
		key = Ext.getCmp(idKey).getValue();
	return ((key == '')||(key == null));
}
var keyAndName = function(){
	var sArray = [];
	for (var i = 0; i < arguments.length; i++) {
		var fArray = arguments[i];
		sArray.push({ key: fArray[0], name: fArray[1]});
	}
	return new Object(sArray);
};
var searchObj = new Object({
		//id			:	'fp.search-bar',
		idCombo			: 	'cb.search-bar',
		idText			:	'tf.search-bar',
		idButton		:	'bt.search-bar',
		store			:	new Ext.data.JsonStore(),
		params			: 	new Object(),
		comboWidth		: 	150,
		textWidth		:	150,
});
var searchObj_init = function(){
	searchObj.idCombo = 'cb.search-bar';
	searchObj.idText = 'tf.search-bar';
	searchObj.idButton = 'bt.search-bar';
	searchObj.store = new Ext.data.JsonStore();
	searchObj.params = new Object();
	searchObj.comboWidth = 150;
	searchObj.textWidth = 150;
};
var getKeyPar = function(){
	idKey = searchObj.idCombo;
	var keyPar = '';
	if(Ext.getCmp(idKey))
		keyPar = Ext.getCmp(idKey).getValue();
	return keyPar
};
var getValPar = function(){
	idVal = searchObj.idText;
	var valPar = '';
	if(Ext.getCmp(idVal))
		valPar = Ext.getCmp(idVal).getValue();
	return valPar
}
var searchBar = function(){
	//var idForm = searchObj.id;
	var idCombo = searchObj.idCombo;
	var idText = searchObj.idText;
	var idButton = searchObj.idButton;
	var comboWidth = searchObj.comboWidth;
	var textWidth = searchObj.textWidth;
	var containerWidth = comboWidth + textWidth + 50;
	var comboData = searchObj.params;
	var store = searchObj.store;

	var comboStore = new Ext.data.JsonStore({
		autoDestroy: true,
		fields: ['key', 'name'],
		data : comboData,
	});
	var searchBar = {
		xtype:'form', //id: idForm,
		border:false, frame:false,
		bodyStyle: 'background:transparent; padding:1px;',
		defaults:{ labelAlign:'right', lebelWidth:100},
		items:[{
			xtype:'container', layout:'hbox',
			//fieldLabel:'Pencarian',
			width: containerWidth,
			items:[{
				xtype:'combo', id: idCombo,
				width: comboWidth, emptyText:'Cari berdasarkan ...',
				store: comboStore, valueField:'key', displayField:'name',
				editable: false, triggerAction: 'all',
				forceSelection: true, submitValue: true, mode: 'local',
			},{

				xtype:'textfield', id: idText,
				margins:'0 0 0 5', width: textWidth,
			},{
				xtype:'button', iconCls:'silk-zoom', margins:'0 0 0 5',
				id: idButton,
				//handler: function() { reloadGridStore(); }
				handler: function() { reloadGridStore() }
			}]
		}]
	};

	function reloadGridStore(){
		if(isEmpty(idCombo)) return;
		store.reload({
			params: {
				key: Ext.getCmp(idCombo).getValue(),
				value: Ext.getCmp(idText).getValue(),
				start: 0,
				limit: 20,
			},
		});
	}

	return searchBar;
}


