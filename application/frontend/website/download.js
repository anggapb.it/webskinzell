function download() {
	var ds_stpublish = store_stpublish();
	var file = "";
	var kdjurnal = "";
p = new Ext.Panel({
		border: true,
		frame: true,
		style: 'margin-left: 50px;margin-bottom: 30px',
		width: 120,
		height: 135,
		html: '<p align="center"><i>IMAGE</i></p>'
	});
	
//VARIABEL DATA STORE//	
	var ds_ejurnal = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'e_library/c_jurnal/grid_list_jurnal',
			method: 'POST'
		}),
		root: 'data',
		totalProperty: 'results',
		id: 'photoid',
		remoteSort: true,
		autoLoad: true,
		waitMsg: 'Mengunduh..',
		baseParams: {
			
		},
		fields: [{
			name: "kdjurnal",
			mapping: "kdjurnal"
		},
		{
			name: "idjnsjurnal",
			mapping: "idjnsjurnal"
		},
		{
			name: "judul",
			mapping: "judul"
		},
		{
			name: "deskripsi",
			mapping: "deskripsi"
		},
		{
			name: "file",
			mapping: "file"
		},
		{
			name: "download",
			mapping: "download"
		},
		{
			name: "url",
			mapping: "url"
		},
		]
	});

//LISTENER STATUS PUBLISH
	var fnSelectStPublish = function(combo, record){
		var idstpublish = record.data['idstpublish'];
		updateStPublish(idstpublish);
	};
	
//VARIABEL KOLOM//	
	var cm = new Ext.grid.ColumnModel({
		// specify any defaults for each column
		defaults: {
			sortable: true // columns are not sortable by default           
		},
		columns: [
		new Ext.grid.RowNumberer(),
		{
			header: RH.h3('Judul'),
			width: 150,
			dataIndex: 'judul',
			sortable: true
		},
		{
			header: RH.h3('Deskripsi'),
			width: 150,
			dataIndex: 'deskripsi',
			sortable: true
		},
		{
			header: RH.h3('Download'),
			width: 75,
			dataIndex: 'download',
			align: 'right',
			sortable: true,
			renderer: function(value, p, r)
						{return r.data['download'] + ' Kali'}
		}]
	});
	var vw = new Ext.grid.GridView({
		emptyText: '< Jurnal Belum Dipilih  >'
	});
	var sm_nya = new Ext.grid.CheckboxSelectionModel({
		singleSelect:true,
		listeners: {
			//	rowselect: select_action,
			//	rowdeselect: deselect_action
		}
	});
	var cari_data = [new Ext.ux.grid.Search({
		iconCls: 'btn_search',
		minChars: 1,
		autoFocus: true,
		autoHeight: true,
		position: 'top',
		mode: 'local',
		width: 200
	})];
	var paging = new Ext.PagingToolbar({
		pageSize: 15,
		store: ds_ejurnal,
		displayInfo: true,
		displayMsg: 'Data Jurnal Dari {0} - {1} of {2}',
		emptyMsg: 'Jurnal Belum Dipilih.'
	});

//VARIABEL GRID//	
	var grid_nya = new Ext.grid.EditorGridPanel({
		store: ds_ejurnal,
		frame: true,
		//height: 500,
		autoScroll: true,
		autoHeight: true,
		autoWidth: true,
		//plugins: cari_data,
		id: 'grid_det_product',
		buttonAlign: 'left',
		forceFit: true,
		tbar: [
		{
			xtype: 'button',
			text: ' Add ',
			iconCls: 'silk-add',
			id: 'btn_add',
			width: 5,
			style: 'margin-bottom: 100px',
			handler:function() {
						fnAddJurnal(); 
					}
		},
		{
            text: 'Edit', id: 'btn_edit', iconCls: 'silk-edit',
			style: 'margin-bottom: 100px',
            handler:function(){
                        if(sm_nya.getCount() > 0){
                            var module_id = sm_nya.getSelected().data['kdjurnal'];
                            download_form(module_id,ds_ejurnal);
							Ext.getCmp('kdprodi').enable();
                        }
                    }
        },
		{
            text: 'Delete', id: 'btn_delete', iconCls: 'silk-delete',
			style: 'margin-bottom: 100px',
            handler:function(){
                        if(sm_nya.getCount() > 0){
                            var delete_id = sm_nya.getSelected().data['kdjurnal'];
                            Ext.MessageBox.show({
                                title: "Konfirmasi",
                                msg: "Anda Yakin Untuk menghapus Data ini?",
                                buttons: Ext.MessageBox.YESNO,
                                fn: function(btn) {
                                    if (btn == 'yes') {
                                        Ext.Ajax.request({
                                            url: BASE_URL + 'e_library/c_jurnal/delete',
                                            method: 'POST',
                                            success: function() {
                                                Ext.MessageBox.alert("Informasi", "Hapus Data Berhasil");
                                                 ds_ejurnal.load();
                                            },
                                            failure:function(result){
                                                        Ext.MessageBox.alert("Informasi", "Hapus Data Gagal");
                                                    },
                                            params: { hapus_id: delete_id }
                                        });
                                    }
                                }
                            });
                        }
                    }
        }
		],
		sm: sm_nya,
		vw: vw,
		cm: cm,
		bbar: paging,
		clicksToEdit: 1,
		listeners: {
			rowclick: function rowClick(grid, rowIdx) {
				var rec = ds_ejurnal.getAt(rowIdx);
				RH.setCompValue('frm.kdjurnal', rec.data['kdjurnal']);
				RH.setCompValue('frm.issn', rec.data['issn']);
				RH.setCompValue('frm.kdjnsjurnal', rec.data['nmjnsjurnal']);
				RH.setCompValue('frm.judul', rec.data['judul']);
				RH.setCompValue('frm.penulis', rec.data['penulis']);
				RH.setCompValue('frm.warganegara', rec.data['warganegara']);
				RH.setCompValue('frm.tglterbit', rec.data['tglterbit']);
				RH.setCompValue('frm.fakultas', rec.data['nmfakultas']);
				RH.setCompValue('frm.prodi', rec.data['nmprodi']);
				RH.setCompValue('frm.deskripsi', rec.data['deskripsi']);
				isi_gambar_jurnal(rec.data['cover']);
				Ext.getCmp('bdown').enable();
				file = rec.data["file"]; 
				kdjurnal = rec.data["kdjurnal"]; 
			}
		}
	});

//DISPLAY FORM PANEL//
	var DispPanel = new Ext.form.FormPanel({
		id: 'fp.mhsFDisp',
		fileUpload: true,
		border: false,
		autoScroll:true,
		bodyStyle: 'padding:5px 5px 0',
		frame: true,
		labelAlign: 'top',
		layout: 'anchor',
		forceFit:true,
		items: [
		{
			layout: 'form',
			defaultType: 'textfield',
			defaults: { readOnly: true },
			items: [
			p
			,
			{ 	id: 'frm.kdjurnal', fieldLabel: 'Kode' ,width: 225, height:10, hidden: true}
			,
			{ 	id: 'frm.judul', fieldLabel: 'Judul' ,width: 225, height:50, xtype: 'textarea'}
			,
			{ 	id: 'frm.deskripsi', fieldLabel: 'Deskripsi' ,width: 225, height:500, xtype: 'textarea'}
            ,
			{
                xtype: 'button',
                id: 'bdown',
                text: 'Download',
				disabled: true,
                width: 75,
                handler: function() {
						 window.open('../resources/jurnal/' + file, '_blank');
						 Ext.Ajax.request({
                                    url: BASE_URL + 'e_library/c_jurnal/update_download',
                                    method: 'POST',
									params: {
                                                kdjurnal:kdjurnal,
												pengguna:USERNAME
                                            },
                                    success: function(response) {   
										   ds_ejurnal.reload();
                                    }
                            });
				}
                
            }
            ]
		}]
	});	

//FUNCTION//
	//add//
	function fnAddJurnal(){
		 download_form('',ds_ejurnal);
	}	

/**FUNCTIONS UPDATE GRID KELAS*/	
function updateStPublish(idstpublish){
		Ext.Ajax.request({
			url: BASE_URL + 'e_library/c_jurnal/update_stpublish',
			params: {
                kdjurnal	: RH.getCompValue('frm.kdjurnal'),
                idstpublish : idstpublish,
			},
			success: function() {
				Ext.Msg.alert("Info", "Ubah Berhasil");
				ds_ejurnal.reload();
			},
			failure: function() {
				Ext.Msg.alert("Info", "Ubah Data Gagal");
			}
		});
	}	
	
//=================================================== AWAL//
	var jurnalPanel = new Ext.Panel({
		layout: 'border',
		defaults: {
			collapsible: true,
			split: true
		},
		items: [{
			collapsible: false,
			title: "E-Jurnal",
			region: 'center',
			id: 'images-view',
			items: [grid_nya],
		},
		{
			region: 'east', 
			xtype: 'panel',
			title: 'Detail Jurnal', border: true,
			layout:'fit', width:270,
			collapsible: true, collapseMode: 'header',
			titleCollapse: true, titleAlign: 'center',
			items: [DispPanel],
			collapsed: false
		}],
		listeners: {
			'afterrender': function() {
		}
		}
	});
	
	get_content(jurnalPanel);
}