function f_lowongankerja_form(id_module,ds) {
	Ext.QuickTips.init();
	var ds_perusahaan = store_perusahaan();
	var ds_posisi = store_posisi();
	
	var form_bp_general = new Ext.form.FormPanel({
        border: false,
		id: 'form_bp_general',
		labelAlign: 'left',
		buttonAlign: 'left',
		bodyStyle: 'padding:10px 3px 3px 5px', // atas, kanan, bawah, kiri
		monitorValid: true,
		height: 400,
		width: 700,
              	layout: 'form',
				autoScroll: true,
                reader: new Ext.data.JsonReader ({
                    root: 'data',
                    totalProperty: 'results',
                    id: 'id',
                    fields: [
                        {
							name: "idlowongan",
							mapping: "idlowongan"
						},{
							name: "kdlowongan",
							mapping: "kdlowongan"
						},{
							name: "idperusahaan",
							mapping: "idperusahaan"
						},{
							name: "nmperusahaan",
							mapping: "nmperusahaan"
						},{
							name: "idposisi",
							mapping: "idposisi"
						},{
							name: "nmposisi",
							mapping: "nmposisi"
						},{
							name: "kualifikasi",
							mapping: "kualifikasi"
						},{
							name: "bataslamaran",
							mapping: "bataslamaran"
						},{
							name: "deskripsi",
							mapping: "deskripsi"
						}
                    ]
                }),
                items: [     
                            {   
					xtype:'fieldset',
					title: '',
					width:670,
					items :[{
                                    xtype: 'textfield',
                                    fieldLabel: '',
									id: 'idlowongan',
                                    name: 'idlowongan',
									hidden: true,
                            },
							{
                                    xtype: 'textfield',
                                    fieldLabel: 'Kode',
									id: 'kdlowongan',
                                    name: 'kdlowongan'			
                            },
                            {
                                    xtype: 'combo',
                                    width: 300,
                                    height: 50,
                                    allowBlank: false,
                                    store: ds_perusahaan,
                                    fieldLabel: 'Perusahaan',
                                    id: 'idperusahaan',
                                    triggerAction: 'all',
                                    editable: false,
                                    valueField: 'idperusahaan',
                                    displayField: 'nmperusahaan',
                                    forceSelection: true,
                                    submitValue: true,
                                    hiddenName: 'h_kdperusahaan',
                                    listeners: {},

                                    typeAhead: true,
                                    mode: 'local',
                                    emptyText:'Pilih...',
                                    selectOnFocus:true

                             },
							 {
                                    xtype: 'combo',
                                    width: 300,
                                    height: 50,
                                    allowBlank: false,
                                    store: ds_posisi,
                                    fieldLabel: 'Posisi',
                                    id: 'idposisi',
                                    triggerAction: 'all',
                                    editable: false,
                                    valueField: 'idposisi',
                                    displayField: 'nmposisi',
                                    forceSelection: true,
                                    submitValue: true,
                                    hiddenName: 'h_kdposisi',
                                    listeners: {},

                                    typeAhead: true,
                                    mode: 'local',
                                    emptyText:'Pilih...',
                                    selectOnFocus:true

                             },
							 {
                                    xtype: 'htmleditor',
                                    fieldLabel: 'Kualifikasi',
                                    anchor:'97% 40%',
                                    height:100,
                                    width:true,
                                    name: 'kualifikasi',		
									plugins         : [new Ext.ux.form.HtmlEditor.Table()],									
							 },
							 {
                                    xtype: 'datefield',
                                    //labelStyle: 'width:140px',
                                    fieldLabel: 'Batas Lamaran',
									id: 'bataslamaran',
                                    name: 'bataslamaran'
                            },
							{
                                    xtype: 'htmleditor',
                                    fieldLabel: 'Deskripsi',
                                    anchor:'97% 40%',
                                    height:100,
                                    name: 'deskripsi',
									plugins         : [new Ext.ux.form.HtmlEditor.Table()],
                            }]
							
							}
               

                     ],

                buttons: [{
                    id:'btn_simpan',
                    text: 'Simpan',
                    handler: function() {
                        if(id_module!=''){
                            ubah_grid('form_bp_general');
                        }else{
                            simpan_grid('form_bp_general');
                        }
                            
                    }
                }, 
                    {
                    text: 'Kembali',
                    handler: function() {
                        win.close();
                    }
                }
                    ],listeners:{
            afterrender: module_afterrender
        }
    });
    function module_afterrender(){
	Ext.getCmp("kdlowongan").setReadOnly(false);
        if(id_module!=""){
            form_bp_general.getForm().load({
                url: BASE_URL + 'website/c_lowongankerja/grid',
                params:{
                    id_module:id_module
                },
                success: function(form, action){
					Ext.getCmp("kdlowongan").setReadOnly(true);
                },
                failure: function(form, action){
                Ext.MessageBox.alert('Failure', 'Fail to get data');
                },
                waitMsg:'Loading..'
            });
        }
		Ext.getCmp('bataslamaran').setValue(SYS_DATE);
    }
        
	var win = new Ext.Window({
			title: 'Lowongan Kerja',
			modal: true,
			items: [form_bp_general]
		}).show();
    
    function simpan_grid(namaForm) {
		var form_nya = Ext.getCmp(namaForm);
		form_nya.getForm().submit({
			url: BASE_URL + 'website/c_lowongankerja/save',
			method: 'POST',
                        params:{
                           //mslider:Ext.getCmp('slideratas').getValue(),
                           pengguna:USERNAME
                        },
			success: function() {
				Ext.MessageBox.alert("Informasi", "Simpan Data Berhasil");
				ds.load();
				form_bp_general.getForm().reset();
				Ext.getCmp('form_bp_general').enable();
				Ext.getCmp('btn_simpan').enable();
//				Ext.getCmp('btn_hapus').disable();
//				Ext.getCmp('btn_ubah').disable();
			},
			failure: function() {
				Ext.MessageBox.alert("Informasi", "Simpan Data Gagal");
			}
		});
	}
	
	function ubah_grid(namaForm) {
//           var msgplus;
//            if(kodex!=Ext.getCmp('kode').getValue()){
//               msgplus='<center>Semua field dapat berubah terkecuali field "KODE"!? </br> </center>';
//            }else{
               var msgplus='';
//            }
		var form_nya = Ext.getCmp(namaForm);
		Ext.MessageBox.show({
			title: "Konfirmasi",
                        width: 350,
			msg: msgplus +"<center> Anda Yakin Untuk Mengubah Data ini?</center>",
			buttons: Ext.MessageBox.YESNO,
			fn: function(btn) {
				if (btn == 'yes') {
					form_nya.getForm().submit({
						url: BASE_URL + 'website/c_lowongankerja/update',
                                                params:{
                                                 // mslider:Ext.getCmp('slideratas').getValue(),
                                                  pengguna:USERNAME
                                                },
						method: 'POST',
						success: function() {
							Ext.MessageBox.alert("Informasi", "Ubah Data Berhasil");
							ds.load();
							form_bp_general.getForm().reset();
							Ext.getCmp('form_bp_general').enable();
							Ext.getCmp('btn_simpan').enable();
							win.close();
						},
						failure: function() {
							Ext.MessageBox.alert("Informasi", "Ubah Data Gagal");
						}
					});
				}
			}
		});
	}
}