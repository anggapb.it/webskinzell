function download_form(id_module,ds) {
//VARIABEL SESSION//
	var userid = USERID;
	var ds_jnsjurnal = store_jnsjurnal();
	var ds_stpublish = store_stpublish();
	var ds_fakultas = store_fakultas();
	var ds_prodi_jurnal = store_prodi_jurnal();

	               
	var form_bp_general = new Ext.form.FormPanel({
        border: false,
		autoScroll: true,
		id: 'form_bp_general',
		labelAlign: 'left',
		buttonAlign: 'right',
		fileUpload: true,		
		bodyStyle: 'padding:10px 3px 3px 5px', // atas, kanan, bawah, kiri
		monitorValid: true,
        height: 570,
		width: 800,
              	layout: 'form',
                reader: new Ext.data.JsonReader ({
                    root: 'data',
                    totalProperty: 'results',
                    id: 'id',
                    fields: [
                        {
							name: "kdjurnal",
							mapping: "kdjurnal"
						},
						{
							name: "idjnsjurnal",
							mapping: "idjnsjurnal"
						},
						{
							name: "judul",
							mapping: "judul"
						},
						{
							name: "deskripsi",
							mapping: "deskripsi"
						},
						{
							name: "file",
							mapping: "file"
						},
						{
							name: "download",
							mapping: "download"
						},
						{
							name: "url",
							mapping: "url"
						}
						
                    ]
                }),
                items: [    {
                                    xtype: 'textfield',
                                    fieldLabel: 'Download',
									hidden:true,
									readOnly:true,
                                    name: 'download'
                            },
							{
                                    xtype: 'textfield',
                                    fieldLabel: 'Kode',
									hidden:true,
                                    name: 'kdjurnal'			
                            },
							{
                                    xtype: 'textarea',
                                    fieldLabel: 'Judul',
                                    width:500,
									height:50,
                                    name: 'judul', allowBlank: false
                            },
							{
									xtype: 'htmleditor',			
									fieldLabel: 'Deskripsi',
									height:200,
									anchor:'99%', 
									name: 'deskripsi', 
									allowBlank: false
							},
							{
									xtype: 'fileuploadfield',
									emptyText: 'Select an file to upload...',
									fieldLabel: 'File',
									width: 230,
									buttonText: 'Browse',
									name: 'file', 
									id: 'file'
							} 
							
                    ],

                buttons: [{
                    id:'btn_simpan',
                    text: 'Simpan',
                    iconCls: 'silk-save',
                    handler: function() {
                        if(id_module!=''){
                            ubah_grid('form_bp_general');
                        }else{
                            simpan_grid('form_bp_general');
                        }
                            
                    }
                }, 
                    {
                    text: 'Kembali',
                    handler: function() {
                        win.close();
                    }
                }
                    ],listeners:{
            afterrender: module_afterrender
        }
    });
    function module_afterrender(){
        if(id_module!=""){
            form_bp_general.getForm().load({
                url: BASE_URL + 'e_library/c_jurnal/grid',
                params:{
                    id_module:id_module
                },
                success: function(form, action){

                },
                failure: function(form, action){
                Ext.MessageBox.alert('Failure', 'Fail to get data');
                },
                waitMsg:'Loading..'
            });
        }
    }
        
	var win = new Ext.Window({
			title: 'E-Jurnal',
			modal: true,
			items: [form_bp_general]
		}).show();
    
	function Right(str, n){
		if (n <= 0)
			return "";
		else if (n > String(str).length)
			return str;
		else {
			var iLen = String(str).length;
			return String(str).substring(iLen, iLen - n);
		}
	}
	
    function simpan_grid(namaForm) {
		var x3File = Right(Ext.getCmp('file').getValue(),3);
		var x4File = Right(Ext.getCmp('file').getValue(),4);
		var x3Cover = Right(Ext.getCmp('cover').getValue(),3);
		var x4Cover = Right(Ext.getCmp('cover').getValue(),4);
		var form_nya = Ext.getCmp(namaForm);
		
		if (x3File=="" || x4File=="" || x3File=="doc" || x4File=="docx" || x3File=="pdf" || x3File=="rar" || x3File=="zip" || x3File=="DOC" || x4File=="DOCX" || x3File=="PDF" || x3File=="RAR" || x3File=="ZIP") {
			if (x3Cover=="" || x4Cover=="" || x3Cover=="jpg" || x3Cover=="png" || x4Cover=="jpeg" || x3Cover=="JPG" || x3Cover=="PNG" || x4Cover=="JPEG") {
				
				form_nya.getForm().submit({
					url: BASE_URL + 'e_library/c_jurnal/upload_save',
					method: 'POST',
								params:{
								
								},
					success: function(form_bp_general, o) {
						Ext.MessageBox.alert('Informasi', 'Upload Data Berhasil');
							ds.load();
							win.close();
						
					},
					failure: function() {
						Ext.MessageBox.alert("Informasi", "<center>Upload Data Gagal<br>Periksa Kembali Data Anda Atau File Gambar Anda Lebih Dari 800kb</center>");
					}
				});
			}else{
				Ext.MessageBox.alert('Informasi', '<center>Upload Gambar Gagal<br>File Gambar Yang Di Izinkan (jpg|png|jpeg|JPG|PNG|JPEG)</center>');
			}
		}else{
			Ext.MessageBox.alert('Informasi', '<center>Upload Dokumen Gagal<br>File Dokumen Yang Di Izinkan (doc|docx|pdf|rar|zip|DOC|DOCX|PDF|RAR|ZIP)</center>');
		}
	}
	
	
	
	function ubah_grid(namaForm) {
        var msgplus='';
		var form_nya = Ext.getCmp(namaForm);
		var x3File = Right(Ext.getCmp('file').getValue(),3);
		var x4File = Right(Ext.getCmp('file').getValue(),4);
		var x3Cover = Right(Ext.getCmp('cover').getValue(),3);
		var x4Cover = Right(Ext.getCmp('cover').getValue(),4);
		
		if (x3File=="" || x4File=="" || x3File=="doc" || x4File=="docx" || x3File=="pdf" || x3File=="rar" || x3File=="zip" || x3File=="DOC" || x4File=="DOCX" || x3File=="PDF" || x3File=="RAR" || x3File=="ZIP") {
			if (x3Cover=="" || x4Cover=="" || x3Cover=="jpg" || x3Cover=="png" || x4Cover=="jpeg" || x3Cover=="JPG" || x3Cover=="PNG" || x4Cover=="JPEG") {
			
				Ext.MessageBox.show({
					title: "Konfirmasi",
								width: 350,
					msg: msgplus +"<center> Anda Yakin Mengubah Data ini?</center>",
					buttons: Ext.MessageBox.YESNO,
					fn: function(btn) {
						if (btn == 'yes') {
							form_nya.getForm().submit({
								url: BASE_URL + 'e_library/c_jurnal/upload_update',
														params:{ 
														
														},
								method: 'POST',
								success: function() {
									Ext.MessageBox.alert("Informasi", "Ubah Data Berhasil");
									ds.load();
									win.close();
								},
								failure: function() {
									Ext.MessageBox.alert("Informasi", "<center>Upload Data Gagal<br>Periksa Kembali Data Anda Atau File Gambar Anda Lebih Dari 800kb</center>");
								}
							});
						}
					}
				});
				
			}else{
				Ext.MessageBox.alert('Informasi', '<center>Upload Gambar Gagal<br>File Gambar Yang Di Izinkan (jpg|png|jpeg|JPG|PNG|JPEG)</center>');
			}
		}else{
			Ext.MessageBox.alert('Informasi', '<center>Upload Dokumen Gagal<br>File Dokumen Yang Di Izinkan (doc|docx|pdf|rar|zip|DOC|DOCX|PDF|RAR|ZIP)</center>');
		}
	}
	
}