function f_hub_kami_info() {
	var id_module = '0';
	var form_bp_general = new Ext.form.FormPanel({
		border: false,
		id: 'form_bp_general',
		labelAlign: 'top',
		buttonAlign: 'left',
		bodyStyle: 'padding:5px 5px 5px 5px',
		// atas, kanan, bawah, kiri
		monitorValid: true,
		title: 'Hubungi Kami (Informasi)',
		tbar: [{
			text: 'Simpan',
			iconCls: 'silk-save',
			handler: function() {
				ubah_grid('form_bp_general');
			}
		}],
		layout: 'form',
		//  autoScroll: true,
		reader: new Ext.data.JsonReader({
			root: 'data',
			totalProperty: 'results',
			id: 'id',
			fields: [{
				name: "idhubinfo",
				mapping: "idhubinfo"
			},
			{
				name: "deskripsi",
				mapping: "deskripsi"
			},
			{
				name: "judul",
				mapping: "judul"
			}]
		}),
		items: [{
			layout: 'form',
			items: [
			{
				xtype: 'textfield',
				name: 'judul',
				id: 'judul',
				width: 500,
			},
			{
				xtype: 'htmleditor',
				height: 500,
				anchor: '100% 100%',
				name: 'deskripsi',
				id: 'deskripsi'
			},
			{
				xtype: 'textfield',
				name: 'idhubinfo',
				hidden: true //,
			}]
		}],
		listeners: {
			afterrender: module_afterrender
		}
	});

	function module_afterrender() {
		if (id_module != "") {
			form_bp_general.getForm().load({
				url: BASE_URL + 'website/c_hub_kami_info/grid',
				params: {
					id_module: id_module
				},
				success: function(form, action) {},
				failure: function(form, action) {
					Ext.MessageBox.alert('Failure', 'Fail to get data');
				},
				waitMsg: 'Loading..'
			});
		}
	}

	function ubah_grid(namaForm) {
		var msgplus = '';
		var form_nya = Ext.getCmp(namaForm);
		Ext.MessageBox.show({
			title: "Konfirmasi",
			width: 350,
			msg: msgplus + "<center> Anda Yakin Untuk Mengubah Data ini?</center>",
			buttons: Ext.MessageBox.YESNO,
			fn: function(btn) {
				if (btn == 'yes') {
					form_nya.getForm().submit({
						url: BASE_URL + 'website/c_hub_kami_info/update',
						method: 'POST',
						success: function() {
							Ext.MessageBox.alert("Informasi", "Ubah Data Berhasil");
							f_hub_kami_info();
							form_bp_general.getForm().reset();
							Ext.getCmp('form_bp_general').enable();
							Ext.getCmp('btn_simpan').enable();
						},
						failure: function() {
							Ext.MessageBox.alert("Informasi", "Ubah Data Gagal");
						}
					});
				}
			}
		});
	}
	get_content(form_bp_general);
}