function stposting() {
	var cm = new Ext.grid.ColumnModel({
        // specify any defaults for each column
        defaults: {
            sortable: true // columns are not sortable by default           
        },
        columns: [
		{
			header: 'Kode',
			width: 50,
			dataIndex: 'kdstposting',
			sortable: true
		},
		{
			header: 'Nama',
			width: 150,
			dataIndex: 'nmstposting',
			sortable: true
		},
		{
                xtype: 'actioncolumn',
                width: 40,
				header: 'Edit',
				align:'center',
                items: [{
                    icon   : '../resources/img/icons/fam/application_edit.png',
					tooltip: 'Edit',
					iconCls: 'mousepointereditor',
                    handler: function(grid, rowIndex) {
						var rec = ds_grid.getAt(rowIndex);
						var data_id = rec.data["idstposting"];
						 stposting_form(data_id,ds_grid);
                    }
                }]
        },
		{
                xtype: 'actioncolumn',
                width: 40,
				header: 'Delete',
				align:'center',
                items: [{
                    icon   : '../resources/img/icons/fam/delete.gif',
					tooltip: 'Delete',
					iconCls: 'mousepointereditor',
                    handler: function(grid, rowIndex) {
						var rec = ds_grid.getAt(rowIndex);
						var data_id = rec.data["idstposting"];
						Ext.MessageBox.show({
                                        title: "Konfirmasi",
                                        msg: "Anda Yakin Untuk menghapus Data ini?",
                                        buttons: Ext.MessageBox.YESNO,
                                        fn: function(btn) {
                                                if (btn == 'yes') {
                                                       Ext.Ajax.request({
                                                          url: BASE_URL + 'website/c_stposting/delete',
                                                            method: 'POST',
                                                            success: function() {
                                                                    Ext.MessageBox.alert("Informasi", "Hapus Data Berhasil");
                                                                    ds_grid.load();
                                                            },
                                                            failure: function(result){
                                                                    Ext.MessageBox.alert("Informasi", "Hapus Data Gagal");
                                                            },
                                                           params: { hapus_id: data_id }
                                                        });
                                                }
                                            }
                                    });
                    }
                }]
        }]
    });
        
	var ds_grid = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'website/c_stposting/grid',
			method: 'POST'
		}),
		params: {
			start: 0,
			limit: 5
		},
		root: 'data',
		totalProperty: 'results',
		autoLoad: true,
		fields: [{
			name: "idstposting",
			mapping: "idstposting"
		},{
			name: "kdstposting",
			mapping: "kdstposting"
		},{
			name: "nmstposting",
			mapping: "nmstposting"
		}]
	});
        var vw = new Ext.grid.GridView({emptyText:'< Status Posting Belum Dipilih  >'});

	var sm_nya = new Ext.grid.CheckboxSelectionModel({
		listeners: {
		//	rowselect: select_action,
		//	rowdeselect: deselect_action
		}
	});
	var cari_data = [new Ext.ux.grid.Search({
		iconCls: 'btn_search',
		minChars: 1,
		autoFocus: true,
		autoHeight: true,
		position: 'top',
		mode: 'remote',
		width: 200
	})];
        
        var paging = new Ext.PagingToolbar({
                    pageSize: 50,
                    store: ds_grid,
                    displayInfo: true,
                    displayMsg: 'Data Status Posting Dari {0} - {1} of {2}',
                    emptyMsg: 'Status Posting Belum Dipilih.'
            });
        
	var grid_nya = new Ext.grid.EditorGridPanel({
		store: ds_grid,
		frame: true,
		//width: 1140,
		autoHeight: true,
		autoWidth: true,
		plugins: cari_data,
		id: 'grid_det_product',
               // autoWidth:true,
//                autoSizeColumns:true,
//                enableColumnResize: true,
//                enableColumnHide: false,
//                enableColumnMove: false,
//                enableHdMenu: false,
//                columnLines: true,
//                loadMask: true,
                buttonAlign: 'left',
                defaults : {
                    anchor : '-10'
                },
               forceFit: true,
		tbar: [{
                            text: 'Add', id: 'btn_mahasiswa_add', iconCls: 'silk-add',handler: btn_menu_add

                        },'-','->'],
		sm:sm_nya,
                vw:vw,
		autoScroll: true,
		cm:cm,
		bbar: paging,
                //autoExpandColumn: 'common',
                clicksToEdit: 1,
		listeners: {
			rowdblclick: function rowdblClick(grid, rowIdx) {
				var rec = ds_grid.getAt(rowIdx);
				
				
			}
		}
	});
	                    
	var form_bp_general = new Ext.form.FormPanel({
		id: 'form_bp_general_id',
		region: 'center',
		autoScroll: true,
		buttonAlign: 'left',
		bodyStyle: 'padding: 5px',
		border: false,
		disabled: true,
		waitMsg: 'Waiting...',
		maskDisabled: false,
		monitorValid: true,
		items: [{
			layout: 'form',
			border: false,
			items: [grid_nya]
		}]
	});
        
	var o_m_menu = new Ext.Panel({
		bodyStyle: 'padding: 5px',
		title: 'Status Posting',
		defaults: {
			anchor: '-10'
		},
		border: true,
		margins: '0 0 5 0',
		plain: true,
		layout: 'border',
		items: [form_bp_general]
	});
        
        function btn_menu_add(){
            stposting_form('',ds_grid);
        }
	get_content(o_m_menu);
}