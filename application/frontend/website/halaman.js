function f_halaman() {
        var ds_grid = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'website/c_halaman/grid',
			method: 'POST'
		}),
		params: {
			start: 0,
			limit: 5
		},
		root: 'data',
		totalProperty: 'results',
		autoLoad: true,
		fields: [
		{
			name: "idhalaman",
			mapping: "idhalaman"
		},{
			name: "kdhalaman",
			mapping: "kdhalaman"
		},{
			name: "idktghalaman",
			mapping: "idktghalaman"
		},{
			name: "nmktghalaman",
			mapping: "nmktghalaman"
		},{
			name: "judulind",
			mapping: "judulind"
		},{
			name: "idstpublish",
			mapping: "idstpublish"
		},{
			name: "nmstpublish",
			mapping: "nmstpublish"
		},{
			name: "tglpublish",
			mapping: "tglpublish"
		},{
			name: "userid",
			mapping: "userid"
		},{
			name: "pengguna",
			mapping: "pengguna"
		},{
			name: "idstposting",
			mapping: "idstposting"
		},{
			name: "nmstposting",
			mapping: "nmstposting"
		},{
			name: "dilihat",
			mapping: "dilihat"
		},{
			name: "jampublish",
			mapping: "jampublish"
		},{
			name: "tglagenda",
			mapping: "tglagenda"
		},{
			name: "jamagenda",
			mapping: "jamagenda"
		},{
			name: "gambar",
			mapping: "gambar"
		}]
	});
        
                var cm = new Ext.grid.ColumnModel({
                // specify any defaults for each column
                defaults: {
                    sortable: true // columns are not sortable by default           
                },
                columns: [{
			header: 'Kode',
			width: 50,
			dataIndex: 'kdhalaman',
			hidden : true,
			sortable: true
		},
		{
			header: 'Judul',
			width: 350,
			dataIndex: 'judulind',
			sortable: true
		},
		{
			header: 'Kategori Halaman',
			width: 100,
			dataIndex: 'nmktghalaman',
			sortable: true
		},
		{
			header: 'Status Publish',
			width: 80,
			dataIndex: 'nmstpublish',
			sortable: true
		},
		{
			header: 'Tgl. Publish',
			width: 80,
			dataIndex: 'tglpublish',
			sortable: true
		},
		{
			header: 'Jam Publish',
			width: 80,
			dataIndex: 'jampublish',
			sortable: true
		},
		{
			header: 'Tgl. Agenda',
			width: 80,
			dataIndex: 'tglagenda',
			sortable: true,
			hidden: true
		},
		{
			header: 'Jam Agenda',
			width: 80,
			dataIndex: 'jamagenda',
			sortable: true,
			hidden: true
		},
		{
			header: 'Pengguna',
			width: 100,
			dataIndex: 'pengguna',
			sortable: true
		},
		{
			header: 'Status Posting',
			width: 100,
			dataIndex: 'nmstposting',
			sortable: true,
			hidden: true,
		},
		{
			header: 'Dilihat',
			width: 50,
			dataIndex: 'dilihat',
			sortable: true
		},
		{
                xtype: 'actioncolumn',
                width: 40,
				header: 'Edit',
				align:'center',
                items: [{
                    icon   : '../resources/img/icons/fam/application_edit.png',
					tooltip: 'Edit',
					iconCls: 'mousepointereditor',
                    handler: function(grid, rowIndex) {
						var rec = ds_grid.getAt(rowIndex);
						var data_id = rec.data["idhalaman"];
						f_halaman_form(true,data_id,ds_grid);
                    }
                }]
        },
		{
                xtype: 'actioncolumn',
                width: 40,
				header: 'Delete',
				align:'center',
                items: [{
                    icon   : '../resources/img/icons/fam/delete.gif',
					tooltip: 'Delete',
					iconCls: 'mousepointereditor',
                    handler: function(grid, rowIndex) {
						var rec = ds_grid.getAt(rowIndex);
						var data_id = rec.data["idhalaman"];
						Ext.MessageBox.show({
                                        title: "Konfirmasi",
                                        msg: "Anda Yakin Untuk menghapus Data ini?",
                                        buttons: Ext.MessageBox.YESNO,
                                        fn: function(btn) {
                                                if (btn == 'yes') {
                                                       Ext.Ajax.request({
                                                          url: BASE_URL + 'website/c_halaman/delete',
                                                            method: 'POST',
                                                            success: function() {
                                                                    Ext.MessageBox.alert("Informasi", "Hapus Data Berhasil");
                                                                    ds_grid.load();
                                                            },
                                                            failure: function(result){
                                                                    Ext.MessageBox.alert("Informasi", "Hapus Data Gagal");
                                                            },
                                                           params: { hapus_id: data_id }
                                                        });
                                                }
                                            }
                                    });
                    }
                }]
        }]
            });
        
	var vw = new Ext.grid.GridView({emptyText:'< Halaman Belum Dipilih  >'});

	var sm_nya = new Ext.grid.RowSelectionModel({
		singleSelect: true,
		listeners: {}
	});
        
	var cari_data = [new Ext.ux.grid.Search({
		iconCls: 'btn_search',
		minChars: 1,
		autoFocus: true,
		autoHeight: true,
		position: 'top',
		mode: 'remote',
		width: 200
	})];
        
        var paging = new Ext.PagingToolbar({
                    pageSize: 50,
                    store: ds_grid,
                    displayInfo: true,
                    displayMsg: 'Data Halaman Website Dari {0} - {1} of {2}',
                    emptyMsg: 'Menu Halaman Belum Dipilih.'
            });
        
	var grid_nya = new Ext.grid.EditorGridPanel({
		store: ds_grid,
		frame: true,
		//width: 1140,
		autoScroll: true,
		autoWidth: true,
		height: 525,
		loadMask:true,
		plugins: cari_data,
		id: 'grid_det_product',
               // autoWidth:true,
//                autoSizeColumns:true,
//                enableColumnResize: true,
//                enableColumnHide: false,
//                enableColumnMove: false,
//                enableHdMenu: false,
//                columnLines: true,
//                loadMask: true,
                buttonAlign: 'left',
                defaults : {
                    anchor : '-10'
                },
               forceFit: true,
		tbar: [{
                            text: 'Add', id: 'btn_add', iconCls: 'silk-add',handler: btn_menu_add

                        }/* ,'-',{
                            text: 'Edit', id: 'btn_edit', iconCls: 'silk-edit',
                            handler: function(){
                                if(sm_nya.getCount() > 0){
                                    var module_id = sm_nya.getSelected().data['idhalaman'];
                                   //form_gallery(gallery_id);
                                   // alert(module_id);
                                    f_halaman_form(module_id,ds_grid);
                                }
                            }
                        },'-',{
                            text: 'Delete', id: 'btn_delete', iconCls: 'silk-delete',
                            handler: function(){
                                if(sm_nya.getCount() > 0){
                                    var delete_id = sm_nya.getSelected().data['idhalaman'];
                                    Ext.MessageBox.show({
                                        title: "Konfirmasi",
                                        msg: "Anda Yakin Untuk menghapus Data ini?",
                                        buttons: Ext.MessageBox.YESNO,
                                        fn: function(btn) {
                                                if (btn == 'yes') {
                                                       Ext.Ajax.request({
                                                          url: BASE_URL + 'website/c_halaman/delete',
                                                            method: 'POST',
                                                            success: function() {
                                                                    Ext.MessageBox.alert("Informasi", "Hapus Data Berhasil");
                                                                    ds_grid.load();
                                                            },
                                                            failure: function(result){
                                                                    Ext.MessageBox.alert("Informasi", "Hapus Data Gagal");
                                                            },
                                                           params: { hapus_id: delete_id }
                                                        });
                                                }
                                            }
                                    });
                                }
                            }
                        } */,'-','->'
                       ],
		sm:sm_nya,
                vw:vw,
		autoScroll: true,
		cm:cm,
		bbar: paging,
                //autoExpandColumn: 'common',
                clicksToEdit: 1,
		listeners: {
			rowclick: function rowClick(grid, rowIdx) {
				var rec = ds_grid.getAt(rowIdx);
				//alert(rec.data["kdhalaman"]);
			}
		}
	});
	                    
	var form_bp_general = new Ext.form.FormPanel({
		id: 'form_bp_general_id',
		region: 'center',
		buttonAlign: 'left',
		bodyStyle: 'padding: 5px',
		border: false,
		disabled: true,
		waitMsg: 'Waiting...',
		maskDisabled: false,
		monitorValid: true,
		items: [{
			layout: 'form',
			border: false,
			items: [grid_nya]
		}]
	});
        
	var o_m_menu = new Ext.Panel({
		bodyStyle: 'padding: 5px',
		title: 'Halaman',
		defaults: {
			anchor: '-10'
		},
		border: true,
		margins: '0 0 5 0',
		plain: true,
		layout: 'border',
		items: [form_bp_general]
	});
        
        function btn_menu_add(){
            f_halaman_form(false,null,ds_grid);
        }
	get_content(o_m_menu);
}