function f_menuwebsite_form(id_module,ds) {
	var ds_hierarki = store_hierarki();   
	var ds_status = store_status();
	var form_bp_general = new Ext.form.FormPanel({
                border: false,
		id: 'form_bp_general',
		labelAlign: 'left',
		buttonAlign: 'left',
		bodyStyle: 'padding:10px 3px 3px 5px', // atas, kanan, bawah, kiri
		monitorValid: true,
                height: 430,
		width: 670,
              	layout: 'form',
				autoScroll: true,
                reader: new Ext.data.JsonReader ({
                    root: 'data',
                    totalProperty: 'results',
                    id: 'id',
                    fields: [
                        {
						name: "idmenuweb",
						mapping: "idmenuweb"
					},{
						name: "kdmenuweb",
						mapping: "kdmenuweb"
					},{
						name: "nmmenuind",
						mapping: "nmmenuind"
					},{
						name: "nmmenueng",
						mapping: "nmmenueng"
					},{
						name: "idjnshirarki",
						mapping: "idjnshirarki"
					},{
						name: "nmjnshirarki",
						mapping: "nmjnshirarki"
					},{
						name: "idstatus",
						mapping: "idstatus"
					},{
						name: "nmstatus",
						mapping: "nmstatus"
					},{
						name: "men_idmenuweb",
						mapping: "men_idmenuweb"
					},{
						name: "men_idmenuweb_nm",
						mapping: "men_idmenuweb_nm"
					},{
						name: "menuatas",
						mapping: "menuatas",type: 'bool'
					},{
						name: "menukiri",
						mapping: "menukiri",type: 'bool'
					},{
						name: "menukanan",
						mapping: "menukanan",type: 'bool'
					},{
						name: "menubawah",
						mapping: "menubawah",type: 'bool'
					},{
						name: "url",
						mapping: "url"
					},{
						name: "idhalaman",
						mapping: "idhalaman"
					},{
						name: "judulind",
						mapping: "judulind"
					}
                    ]
                }),
                items: [{   
					xtype:'fieldset',
					title: '',
					width:640,
					items :[{
                                    xtype: 'textfield', //labelStyle: 'width:140px',
                                    fieldLabel: ' ',
                                    id: 'idmenuweb',	
                                    name: 'idmenuweb',
									hidden: true
                            },
							{
                                    xtype: 'textfield', //labelStyle: 'width:140px',
                                    fieldLabel: 'Kode ',
                                    id: 'kdmenuweb',	
                                    name: 'kdmenuweb'			
                            },
                            {
                                    xtype: 'combo',
                                    width: 125,
                                    height: 50,
                                    allowBlank: false,
                                    store: ds_hierarki,
                                    fieldLabel: 'Hierarki',
                                    id: 'idjnshirarki',
                                    triggerAction: 'all',
                                    editable: false,
                                    valueField: 'idjnshirarki',
                                    displayField: 'nmjnshirarki',
                                    forceSelection: true,
                                    submitValue: true,
                                    hiddenName: 'h_hier',
                                    listeners: {},

                                    typeAhead: true,
                                    mode: 'local',
                                    emptyText:'Pilih...',
                                    selectOnFocus:true

                            },
                            {
                                    xtype: 'combo',
                                    width: 125,
                                    height: 50,
                                    allowBlank: false,
                                    store: ds_status,
                                    fieldLabel: 'Status',
                                    id: 'idstatus',
                                    triggerAction: 'all',
                                    editable: false,
                                    valueField: 'idstatus',
                                    displayField: 'nmstatus',
                                    forceSelection: true,
                                    submitValue: true,
                                    hiddenName: 'h_status',
                                    listeners: {},

                                    typeAhead: true,
                                    mode: 'local',
                                    emptyText:'Pilih...',
                                    selectOnFocus:true

                            },
                            {
                                    xtype: 'compositefield',
                                    name: 'comp_submenu',
                                    fieldLabel: 'Parent',
                                    //labelStyle: 'width:160px',
                                    id: 'comp_submenu',
                                    items: [{
                                            xtype: 'textfield',
                                            id: 'men_idmenuweb',
                                            name: 'men_idmenuweb',
											hidden: true
									},
									{
                                            xtype: 'textfield',
                                            id: 'men_idmenuweb_nm',
                                            name: 'men_idmenuweb_nm',
                                            readOnly: true,
                                            width: 200,
                                            emptyText:'Pilih...',
                                            listeners: {
                                                    'render': function(c) {
                                                            c.getEl().on('keypress', function(e) {
                                                            //        if (e.getKey() == 13) Ext.getCmp('btn_simpan_product').focus();
                                                            }, c);
                                                    }
                                            }
                                    },
                                    {
                                            xtype: 'button',
                                            text: ' ... ',
                                            id: 'btn_data_submenu',
                                            width: 3,
                                            handler: function() {
                                                    find_x("Data Parent Web", 2);
                                            }
                                    }]
                            },
							{
                                    xtype: 'compositefield',
                                    name: 'comp_url',
                                    fieldLabel: 'URL',
                                    id: 'comp_url',
                                    items: [{
											xtype: 'radio',
											fieldLabel: '',
											id: 'rurl',
											name: 'rurl',
											checked: true,
											listeners: {
											check : function(cb, value) {
																if (value) {
																	Ext.getCmp('btn_data_halaman').disable();
																	Ext.getCmp('idhalaman').setValue('');
																	Ext.getCmp('judulind').setValue('Pilih...');
																	Ext.getCmp('rhalaman').setValue(false);
																} else {
																	Ext.getCmp('btn_data_halaman').enable();
																}
														   }
											}
										},
										{
											xtype: 'textfield',
											fieldLabel: '',
											id: 'url',
											name: 'url'
										}]
                            },
							{
                                    xtype: 'compositefield',
                                    name: 'comp_halaman',
                                    fieldLabel: 'Halaman',
                                    id: 'comp_halaman',
                                    items: [{
											xtype: 'radio',
											fieldLabel: '',
											id: 'rhalaman',
											name: 'rhalaman',
											checked: false,
											listeners: {
											check : function(cb, value) {
																if (value) {
																	Ext.getCmp('url').disable();
																	Ext.getCmp('url').setValue('');
																	Ext.getCmp('rurl').setValue(false);
																} else {
																	Ext.getCmp('url').enable();
																}
														   }
											}
									},
									{
                                            xtype: 'textfield',
                                            id: 'idhalaman',
                                            name: 'idhalaman',
											hidden: true
									},
									{
                                            xtype: 'textfield',
                                            id: 'judulind',
                                            name: 'judulind',
                                            readOnly: true,
                                            width: 200,
                                            emptyText:'Pilih...',
                                            listeners: {
                                                    'render': function(c) {
                                                            c.getEl().on('keypress', function(e) {
                                                            //        if (e.getKey() == 13) Ext.getCmp('btn_simpan_product').focus();
                                                            }, c);
                                                    }
                                            }
                                    },
                                    {
                                            xtype: 'button',
                                            text: ' ... ',
                                            id: 'btn_data_halaman',
                                            width: 3,
                                            handler: function() {
                                                    find_x("Data Halaman Web", 8);
                                            }
                                    }]
                            },
							 {
                                xtype: 'compositefield',border:false,fieldLabel: 'Posisi Menu',
                                items: [
                                    {xtype:'checkbox',boxLabel: 'Atas', name: 'menuatas', id: 'menuatas',value: '1'},
                                    {xtype:'checkbox',boxLabel: 'Bawah', name: 'menubawah', id: 'menubawah',value: '1'},
                                    {xtype:'checkbox',boxLabel: 'Kanan', name: 'menukanan', id: 'menukanan',value: '1'},
                                    {xtype:'checkbox',boxLabel: 'Kiri', name: 'menukiri', id: 'menukiri',value: '1',hidden:true}
                                ]
							}]
						
						},
						{   
						xtype:'fieldset',
						title: '',
						width:640,
						items :[
							{
                                    xtype: 'textfield',
                                    fieldLabel: 'Nama ',
                                    width:505,
                                    name: 'nmmenuind',
                                    id: 'nmmenuind'
                            },
                            {	
                                    xtype: 'htmleditor',
                                    fieldLabel: 'Deskripsi ',
                                    height:80,
                                    
                                    id:'deskripsiind',
                                    nama:'deskripsiind'
                            }
							]
						},
						{   
						xtype:'fieldset',
						title: 'Bahasa Inggris', hidden: true,
						width:640,
						items :[
                            {
                                    xtype: 'textfield',
                                    fieldLabel: 'Nama ',
									width:505,
									id: 'nmmenueng',
                                    name: 'nmmenueng'
                            },
                            {	
                                    xtype: 'htmleditor',
                                    fieldLabel: 'Deskripsi ',
                                    height:80,
                                    
                                    id:'deskripsieng',
                                    nama:'deskripsieng'
							}
							]
						}],

                buttons: [{
                    id:'btn_simpan',
                    text: 'Simpan',
                    handler: function() {
                        if(id_module!=''){
                            ubah_grid('form_bp_general');
                        }else{
                            simpan_grid('form_bp_general');
                        }
                            
                    }
                }, 
                    {
                    text: 'Kembali',
                    handler: function() {
                        win.close();
                    }
                }
                    ],listeners:{
            afterrender: module_afterrender
        }
    });
    function module_afterrender(){
	Ext.getCmp("kdmenuweb").setReadOnly(false);
	Ext.getCmp("rurl").setValue(true);
	Ext.getCmp('btn_data_halaman').disable();
	
        if(id_module!=""){
            form_bp_general.getForm().load({
                url: BASE_URL + 'website/c_menuweb/grid',
                params:{
                    id_module:id_module
                },
                success: function(form, action){
					Ext.getCmp("kdmenuweb").setReadOnly(true);
						if (Ext.getCmp("url").getValue() !='') {
							Ext.getCmp("rurl").setValue(true);
						}
						if (Ext.getCmp("idhalaman").getValue() !='') {
							Ext.getCmp("rhalaman").setValue(true);
						}
                },
                failure: function(form, action){
                Ext.MessageBox.alert('Failure', 'Fail to get data');
                },
                waitMsg:'Loading..'
            });
        }
    }
        
	var win = new Ext.Window({
			title: 'Menu Website',
			modal: true,
			items: [form_bp_general]
		}).show();
    
    function simpan_grid(namaForm) {
		var form_nya = Ext.getCmp(namaForm);
		form_nya.getForm().submit({
			url: BASE_URL + 'website/c_menuweb/save',
			method: 'POST',
                        params:{
                            matas:Ext.getCmp('menuatas').getValue(),
                            mbawah:Ext.getCmp('menubawah').getValue(),
                            mkanan:Ext.getCmp('menukanan').getValue(),
                            mkiri:Ext.getCmp('menukiri').getValue(),
							
							url:Ext.getCmp('url').getValue()
                        },
			success: function() {
				Ext.MessageBox.alert("Informasi", "Simpan Data Berhasil");
				ds.load();
				form_bp_general.getForm().reset();
				Ext.getCmp('form_bp_general').enable();
				Ext.getCmp('btn_simpan').enable();
//				Ext.getCmp('btn_hapus').disable();
//				Ext.getCmp('btn_ubah').disable();
			},
			failure: function() {
				Ext.MessageBox.alert("Informasi", "Simpan Data Gagal");
			}
		});
	}
	
	function ubah_grid(namaForm) {
//           var msgplus;
//            if(kodex!=Ext.getCmp('kode').getValue()){
//               msgplus='<center>Semua field dapat berubah terkecuali field "KODE"!? </br> </center>';
//            }else{
               var msgplus='';
//            }
		var form_nya = Ext.getCmp(namaForm);
		Ext.MessageBox.show({
			title: "Konfirmasi",
                        width: 350,
			msg: msgplus +"<center> Anda Yakin Untuk Mengubah Data ini?</center>",
			buttons: Ext.MessageBox.YESNO,
			fn: function(btn) {
				if (btn == 'yes') {
					form_nya.getForm().submit({
						url: BASE_URL + 'website/c_menuweb/update',
                                                params:{
                                                    matas:Ext.getCmp('menuatas').getValue(),
                                                    mbawah:Ext.getCmp('menubawah').getValue(),
                                                    mkanan:Ext.getCmp('menukanan').getValue(),
                                                    mkiri:Ext.getCmp('menukiri').getValue(),
													
													url:Ext.getCmp('url').getValue()
                                                },
						method: 'POST',
						success: function() {
							Ext.MessageBox.alert("Informasi", "Ubah Data Berhasil");
							ds.load();
							form_bp_general.getForm().reset();
							Ext.getCmp('form_bp_general').enable();
							Ext.getCmp('btn_simpan').enable();
							win.close();
						},
						failure: function() {
							Ext.MessageBox.alert("Informasi", "Ubah Data Gagal");
						}
					});
				}
			}
		});
	}
}