function kerjasama_form(id_module,ds) {
	RH.startTimer('jampublish');
	RH.startTimer('jamagenda');
	Ext.QuickTips.init();
      p = new Ext.Panel({
                bodyBorder:false,
                width: 170,
                height:160,
                html: '<p><i>PHOTO</i></p>'
            });
		var ds_kategori_halaman = store_kategori_halaman();
		var ds_stpublish = store_stpublish();
    	var form_bp_general = new Ext.form.FormPanel({
                border: false,
		id: 'form_bp_general',
		labelAlign: 'left',
		buttonAlign: 'left',
		bodyStyle: 'padding:10px 3px 3px 5px', // atas, kanan, bawah, kiri
		monitorValid: true,
                height: 550,
		width: 700,
              	layout: 'form',
                autoScroll: true,
                reader: new Ext.data.JsonReader ({
                    root: 'data',
                    totalProperty: 'results',
                    id: 'id',
                    fields: [
                        {
						name: "idkerjasama",
						mapping: "idkerjasama"
					},{
						name: "kdkerjasama",
						mapping: "kdkerjasama"
					},{
						name: "idjnskerjasama",
						mapping: "idjnskerjasama"
					},{
						name: "idstatus",
						mapping: "idstatus"
					},{
						name: "nmstatus",
						mapping: "nmstatus"
					},{
						name: "gambar",
						mapping: "gambar"
					}]
                }),
                items: [ {   
					xtype:'fieldset',
					title: '',
					width:670,
					items :[
                            {
                                    xtype: 'textfield', //labelStyle: 'width:140px',
                                    fieldLabel: 'id',
                                    id: 'idkerjasama',
                                    name: 'idkerjasama',
									hidden: true,
									readOnly: true
                            },
							{
                                    xtype: 'textfield', //labelStyle: 'width:140px',
                                    fieldLabel: 'Kode ',
                                    id: 'kdkerjasama',
                                    name: 'kdkerjasama'			
                            },
                            {
                                    xtype: 'textfield', //labelStyle: 'width:140px',
                                    fieldLabel: 'Nama ',
                                    id: 'nmkerjasama',
                                    name: 'nmkerjasama'			
                            },
                            {
                                    xtype: 'combo',
                                    width: 125,
                                    height: 50,
                                    allowBlank: false,
                                    store: ds_kategori_halaman,
                                    fieldLabel: 'Jenis Kerjasama',
                                    id: 'idjnskerjasama',
                                    triggerAction: 'all',
                                    editable: false,
                                    valueField: 'idjnskerjasama',
                                    displayField: 'nmjnskerjasama',
                                    forceSelection: true,
                                    submitValue: true,
                                    hiddenName: 'h_kerjasama',
                                    listeners: {},

                                    typeAhead: true,
                                    mode: 'local',
                                    emptyText:'Pilih...',
                                    selectOnFocus:true

                            }]},                         
                    ],

                buttons: [{
                    id:'btn_simpan',
                    text: 'Simpan',
                    iconCls: 'silk-save',
                    handler: function() {
                        if(id_module!=''){
                            ubah_grid('form_bp_general');
                        }else{
                            simpan_grid('form_bp_general');
                        }
                            
                    }
                }, 
                    {
                    text: 'Kembali',
                    handler: function() {
                        win.close();
                    }
                }
                    ],listeners:{
            afterrender: module_afterrender
        }
    });
    
    function module_afterrender(){
	Ext.getCmp("kdkerjasama").setReadOnly(false);
        if(id_module!=""){
            form_bp_general.getForm().load({
                url: BASE_URL + 'website/c_kerjasama/grid',
                params:{
                    id_module:id_module
                },
                success: function(form, action){
					Ext.getCmp("kdkerjasama").setReadOnly(true);
                    isi_foto_ori( Ext.getCmp("gambar").getValue());
					
                },
                failure: function(form, action){
                Ext.MessageBox.alert('Failure', 'Fail to get data');
                },
                waitMsg:'Loading..'
            });
			
        }
		//Ext.getCmp('tglpublish').setValue(SYS_DATE);
		//Ext.getCmp('tglagenda').setValue(SYS_DATE);
    }
        
    var win = new Ext.Window({
			title: 'Kerjasama',
			layout: "fit",
                        modal: true,
                        resizable: true,
                       	maximizable:true,
			items: [form_bp_general]
		}).show();
    
    function simpan_grid(namaForm) {
		var form_nya = Ext.getCmp(namaForm);
		form_nya.getForm().submit({
			url: BASE_URL + 'website/c_kerjasama/save',
			method: 'POST',
                        params:{
							userid:USERID
                        },
			success: function() {
				Ext.MessageBox.alert("Informasi", "Simpan Data Berhasil");
				ds.load();
				form_bp_general.getForm().reset();
				Ext.getCmp('form_bp_general').enable();
				Ext.getCmp('btn_simpan').enable();

                                win.close();
			},
			failure: function() {
				Ext.MessageBox.alert("Informasi", "Simpan Data Gagal");
			}
		});
	}
	
	function ubah_grid(namaForm) {

        var msgplus='';

		var form_nya = Ext.getCmp(namaForm);
		Ext.MessageBox.show({
			title: "Konfirmasi",
                        width: 350,
			msg: msgplus +"<center> Anda Yakin Untuk Mengubah Data ini?</center>",
			buttons: Ext.MessageBox.YESNO,
			fn: function(btn) {
				if (btn == 'yes') {
					form_nya.getForm().submit({
						url: BASE_URL + 'website/c_kerjasama/update',
                                                params:{
                                                    userid:USERID
                                                },
						method: 'POST',
						success: function() {
							Ext.MessageBox.alert("Informasi", "Ubah Data Berhasil");
							ds.load();
							form_bp_general.getForm().reset();
							Ext.getCmp('form_bp_general').enable();
							Ext.getCmp('btn_simpan').enable();
                            win.close();
						},
						failure: function() {
							Ext.MessageBox.alert("Informasi", "Ubah Data Gagal");
						}
					});
				}
			}
		});
	}
 }