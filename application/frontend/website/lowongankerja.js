function f_lowongankerja() {
        var ds_grid = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'website/c_lowongankerja/grid',
			method: 'POST'
		}),
		params: {
			start: 0,
			limit: 5
		},
		root: 'data',
		totalProperty: 'results',
		autoLoad: true,
		fields: [
		{
			name: "idlowongan",
			mapping: "idlowongan"
		},{
			name: "kdlowongan",
			mapping: "kdlowongan"
		},{
			name: "idperusahaan",
			mapping: "idperusahaan"
		},{
			name: "nmperusahaan",
			mapping: "nmperusahaan"
		},{
			name: "idposisi",
			mapping: "idposisi"
		},{
			name: "nmposisi",
			mapping: "nmposisi"
		},{
			name: "kualifikasi",
			mapping: "kualifikasi"
		},{
			name: "bataslamaran",
			mapping: "bataslamaran"
		},{
			name: "deskripsi",
			mapping: "deskripsi"
		}]
	});
        
        var cm = new Ext.grid.ColumnModel({
        // specify any defaults for each column
        defaults: {
            sortable: true // columns are not sortable by default           
        },
        columns: [{
			header: 'Kode',
			width: 50,
			dataIndex: 'kdlowongan',
			sortable: true
		},
		{
			header: 'Perusahaan',
			width: 150,
			dataIndex: 'nmperusahaan',
			sortable: true
		},
		{
			header: 'Posisi',
			width: 150,
			dataIndex: 'nmposisi',
			sortable: true
		},
		{
			header: 'Kualifikasi',
			width: 150,
			dataIndex: 'kualifikasi',
			sortable: true
		},
		{
			header: 'Batas Lamaran',
			width: 150,
			dataIndex: 'bataslamaran',
			sortable: true
		},
		{
			header: 'Deskripsi',
			width: 150,
			dataIndex: 'deskripsi',
			sortable: true
		}]
    });
        
	var vw = new Ext.grid.GridView({emptyText:'< Lowongan Kerja Belum Dipilih  >'});

	var sm_nya = new Ext.grid.CheckboxSelectionModel({
		listeners: {
		//	rowselect: select_action,
		//	rowdeselect: deselect_action
		}
	});
	var cari_data = [new Ext.ux.grid.Search({
		iconCls: 'btn_search',
		minChars: 1,
		autoFocus: true,
		autoHeight: true,
		position: 'top',
		mode: 'remote',
		width: 200
	})];
        
        var paging = new Ext.PagingToolbar({
                    pageSize: 50,
                    store: ds_grid,
                    displayInfo: true,
                    displayMsg: 'Data Lowongan Kerja Dari {0} - {1} of {2}',
                    emptyMsg: 'Lowongan Kerja Belum Dipilih.'
            });
        
	var grid_nya = new Ext.grid.EditorGridPanel({
		store: ds_grid,
		frame: true,
		//width: 1140,
		autoHeight: true,
		autoWidth: true,
		plugins: cari_data,
		id: 'grid_det_product',
               // autoWidth:true,
//                autoSizeColumns:true,
//                enableColumnResize: true,
//                enableColumnHide: false,
//                enableColumnMove: false,
//                enableHdMenu: false,
//                columnLines: true,
//                loadMask: true,
                buttonAlign: 'left',
                defaults : {
                    anchor : '-10'
                },
               forceFit: true,
		tbar: [{
                            text: 'Add', id: 'btn_add', iconCls: 'silk-add',handler: btn_menu_add

                        },'-',{
                            text: 'Edit', id: 'btn_edit', iconCls: 'silk-edit',
                            handler: function(){
                                if(sm_nya.getCount() > 0){
                                    var module_id = sm_nya.getSelected().data['idlowongan'];
                                   //form_gallery(gallery_id);
                                    //alert(module_id);
                                    f_lowongankerja_form(module_id,ds_grid);
                                }
                            }
                        },'-',{
                            text: 'Delete', id: 'btn_delete', iconCls: 'silk-delete',
                            handler: function(){
                                if(sm_nya.getCount() > 0){
                                    var delete_id = sm_nya.getSelected().data['idlowongan'];
                                    Ext.MessageBox.show({
                                        title: "Konfirmasi",
                                        msg: "Anda Yakin Untuk menghapus Data ini?",
                                        buttons: Ext.MessageBox.YESNO,
                                        fn: function(btn) {
                                                if (btn == 'yes') {
                                                       Ext.Ajax.request({
                                                          url: BASE_URL + 'website/c_lowongankerja/delete',
                                                            method: 'POST',
                                                            success: function() {
                                                                    Ext.MessageBox.alert("Informasi", "Hapus Data Berhasil");
                                                                    ds_grid.load();
                                                            },
                                                            failure: function(result){
                                                                    Ext.MessageBox.alert("Informasi", "Hapus Data Gagal");
                                                            },
                                                           params: { hapus_id: delete_id }
                                                        });
                                                }
                                            }
                                    });
                                }
                            }
                        },'-','->'
                       ],
		sm:sm_nya,
                vw:vw,
		autoScroll: true,
		cm:cm,
		bbar: paging,
                //autoExpandColumn: 'common',
                clicksToEdit: 1,
		listeners: {
			rowdblclick: function rowdblClick(grid, rowIdx) {
				var rec = ds_grid.getAt(rowIdx);
				//alert(rec.data["kdmenu"] + ', ' + Ext.getCmp('combox').getValue());
                             
				
			}
		}
	});
	                    
	var form_bp_general = new Ext.form.FormPanel({
		id: 'form_bp_general_id',
		region: 'center',
		autoScroll: true,
		buttonAlign: 'left',
		bodyStyle: 'padding: 5px',
		border: false,
		disabled: true,
		waitMsg: 'Waiting...',
		maskDisabled: false,
		monitorValid: true,
		items: [{
			layout: 'form',
			border: false,
			items: [grid_nya]
		}]
	});
        
	var o_m_menu = new Ext.Panel({
		bodyStyle: 'padding: 5px',
		title: 'Lowongan Kerja',
		defaults: {
			anchor: '-10'
		},
		border: true,
		margins: '0 0 5 0',
		plain: true,
		layout: 'border',
		items: [form_bp_general]
	});
        
        function btn_menu_add(){
            f_lowongankerja_form('',ds_grid);
        }
	get_content(o_m_menu);
}