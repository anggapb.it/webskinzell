function f_hub_kami_inbox() {
    function status(val) {
        if (val == "Belum") {
			return '<img src="../resources/img/icons/fam/exclamation.gif">';
        } else if (val == "Terkirim") {
            return '<img src="../resources/img/icons/fam/accept.png">';
        }
        return val;
    }
	
	var sm_nya = new Ext.grid.CheckboxSelectionModel({
		listeners: {
		//	rowselect: select_action,
		//	rowdeselect: deselect_action
		}
	});

	var cm = new Ext.grid.ColumnModel({
        // specify any defaults for each column
        defaults: {
            sortable: true // columns are not sortable by default           
        },
        columns: [
		sm_nya, new Ext.grid.RowNumberer(),
		{
			header: RH.h3('Id'),
			width: 150,
			dataIndex: 'idpesan',
			sortable: true,
			hidden: true
		},
		{
			header: RH.h3('Tanggal/Jam'),
			width: 130,
			dataIndex: 'tglpsnmsk',
			align: 'center',
			sortable: true
		},
		{
			header: RH.h3('Nama Pengirim'),
			width: 200,
			dataIndex: 'nama',
			sortable: true
		},
		{
			header: RH.h3('Email'),
			width: 200,
			dataIndex: 'email',
			sortable: true
		},
		{
			header: RH.h3('Telepon'),
			width: 120,
			dataIndex: 'tlp',
			sortable: true
		},
		{
			header: RH.h3('Pesan Masuk'),
			width: 360,
			dataIndex: 'pesanmsk',
			sortable: true
		},
		{
			header: RH.h3('Status'),
			width: 50,
			align: 'center',
			dataIndex: 'status',
			renderer : status,
			sortable: true
		}]
    });
        
	var ds_grid = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'website/c_hub_kami_inbox/grid',
			method: 'POST'
		}),
		params: {
			start: 0,
			limit: 5
		},
		root: 'data',
		totalProperty: 'results',
		autoLoad: true,
		fields: [{
			name: "idpesan",
			mapping: "idpesan"
		},{
			name: "nama",
			mapping: "nama"
		},{
			name: "email",
			mapping: "email"
		},{
			name: "tlp",
			mapping: "tlp"
		},{
			name: "pesanmsk",
			mapping: "pesanmsk"
		},{
			name: "status",
			mapping: "status"
		},{
			name: "tglpsnmsk",
			mapping: "tglpsnmsk"
		}]
	});
        var vw = new Ext.grid.GridView({emptyText:'< Pesan Belum Dipilih  >'});

	
	var cari_data = [new Ext.ux.grid.Search({
		iconCls: 'btn_search',
		minChars: 1,
		autoFocus: true,
		autoHeight: true,
		position: 'top',
		mode: 'remote',
		width: 200
	})];
        
        var paging = new Ext.PagingToolbar({
                    pageSize: 50,
                    store: ds_grid,
                    displayInfo: true,
                    displayMsg: 'Data Pesan Dari {0} - {1} of {2}',
                    emptyMsg: 'Pesan Belum Dipilih.'
            });
    
	var DelMsg = new Ext.Toolbar({
			items:[/* {
					text: 'Add',
					iconCls: 'silk-add',
					handler: function(){
								var select = grid_nya.getSelectionModel().getSelections();
								if(select.length > 0){				
									Ext.MessageBox.confirm('Message', 'Anda Yakin Menambah Data?', add);		 
								}else{
									Ext.MessageBox.alert('Message', 'Data Belum Di Pilih!');
								}						
					}
			}, */
						{
                            text: 'Delete', id: 'btn_delete', iconCls: 'silk-delete',
                            handler: function(){
								var select = grid_nya.getSelectionModel().getSelections();
								if(select.length > 0){				
									Ext.MessageBox.confirm('Message', 'Yakin Menghapus Pesan Ini?', del);		 
								}else{
									Ext.MessageBox.alert('Message', 'Pilih Pesan Yang Dihapus');
								}						
							}
							
							
							
							/* function(){
                                if(sm_nya.getCount() > 0){
                                    var delete_id = sm_nya.getSelected().data['idpesan'];
                                    Ext.MessageBox.show({
                                        title: "Konfirmasi",
                                        msg: "Anda Yakin Untuk menghapus Data ini?",
                                        buttons: Ext.MessageBox.YESNO,
                                        fn: function(btn) {
                                                if (btn == 'yes') {
                                                       Ext.Ajax.request({
                                                          url: BASE_URL + 'website/c_hub_kami_inbox/delete',
                                                            method: 'POST',
                                                            success: function() {
                                                                    Ext.MessageBox.alert("Informasi", "Hapus Data Berhasil");
                                                                    ds_grid.load();
                                                            },
                                                            failure: function(result){
                                                                    Ext.MessageBox.alert("Informasi", "Hapus Data Gagal");
                                                            },
                                                           params: { hapus_id: delete_id }
                                                        });
                                                }
                                            }
                                    });
                                }
                            } */
                        }]
	});
	
	var grid_nya = new Ext.grid.EditorGridPanel({
		store: ds_grid,
		frame: true,
		//width: 1140,
		height: 550,
		//autoHeight: true,
		autoWidth: true,
		autoScroll: true,
		plugins: cari_data,
		id: 'grid_det_product',
               // autoWidth:true,
//                autoSizeColumns:true,
//                enableColumnResize: true,
//                enableColumnHide: false,
//                enableColumnMove: false,
//                enableHdMenu: false,
//                columnLines: true,
//                loadMask: true,
                buttonAlign: 'left',
                defaults : {
                    anchor : '-10'
                },
               forceFit: true,
		tbar: [DelMsg,'-',
						{
                            text: 'Reply', id: 'btn_edit', iconCls: 'silk-edit',
                            handler: function(){
                                if(sm_nya.getCount() == 1){
                                    var module_id = sm_nya.getSelected().data['idpesan'];
                                   //form_gallery(gallery_id);
                                    //alert(module_id);
                                    f_hub_kami_inbox_form(module_id,ds_grid);
									Ext.getCmp('pesanmsk').show();
                                }else{
									Ext.MessageBox.alert("Informasi", "Pilih Satu Pesan");
								}
                            }
                        },'-',{
                            text: 'Setting Admin', id: 'btn_admin', iconCls: 'silk-add',
                            handler: function(){
                                    showSettingAdmin();
                            }
                        },'->'
                       ],
		sm:sm_nya,
                vw:vw,
		autoScroll: true,
		cm:cm,
		bbar: paging,
                //autoExpandColumn: 'common',
                clicksToEdit: 1,
		listeners: {
			rowdblclick: function rowdblClick(grid, rowIdx) {
				var rec = ds_grid.getAt(rowIdx);
				
				
			}
		}
	});
	
	function del(btn, grid, rowIdx){
		console.log(btn);
		if(btn == 'yes'){			
			var m = grid_nya.getSelectionModel().getSelections();
			//var store = ds_lookup;
			var store = grid_nya.getStore();	
			var record = store.getAt(rowIdx);
			
			for(var i=0; i< m.length; i++){
				var rec = m[i];
				console.log(rec);
				if(rec){
					var delete_id = rec.data['idpesan'];	
					//INSERT NEW DOSENWALIDET RECORD (Mahasiswa)
					 Ext.Ajax.request({
							url: BASE_URL + 'website/c_hub_kami_inbox/delete',
                            method: 'POST',
                            success: function() {
                                        Ext.MessageBox.alert("Informasi", "Hapus Data Berhasil");
                                        ds_grid.load();
                                     },
                            failure: function(result){
                                        Ext.MessageBox.alert("Informasi", "Hapus Data Gagal");
                                     },
                            params: { hapus_id: delete_id }
                    });
				}
			}			
		}
	}
	                    
	var form_bp_general = new Ext.form.FormPanel({
		id: 'form_bp_general_id',
		region: 'center',
		autoScroll: true,
		buttonAlign: 'left',
		bodyStyle: 'padding: 5px',
		border: false,
		disabled: true,
		waitMsg: 'Waiting...',
		maskDisabled: false,
		monitorValid: true,
		items: [{
			layout: 'form',
			border: false,
			items: [grid_nya]
		}]
	});
        
	var o_m_menu = new Ext.Panel({
		bodyStyle: 'padding: 5px',
		title: 'Hubungi Kami Inbox',
		defaults: {
			anchor: '-10'
		},
		border: true,
		margins: '0 0 5 0',
		plain: true,
		layout: 'border',
		items: [form_bp_general]
	});
        
        function btn_menu_add(){
            f_hub_kami_inbox_form('',ds_grid);
        }
	get_content(o_m_menu);
	
	
	function showSettingAdmin(){
		
	// -- FORM PANEL (DISPLAY) SETTING ADMIN
		var fpSettingAdmin = new Ext.form.FormPanel({ 
			id: 'fp.setadm',
			name: 'fp.setadm',
			layout: 'form',
			forceFit: true,
			maskDisabled: false,
			monitorValid: true,
			autoScroll: true,
			margin: '0 0 10',
			frame: true,
			 reader: new Ext.data.JsonReader ({
                    root: 'data',
                    totalProperty: 'results',
                    id: 'id',
                    fields: [
                        {
                                name: "emailadmin",
                                mapping: "emailadmin"
                        },{
                                name: "passadmin",
                                mapping: "passadmin"
                        }
                    ]
                }),
			items: 
			[
				{
                    xtype: 'textfield',
                    vtype:'email',
                    id: 'emailadmin',
                    fieldLabel: 'Email (G-Mail)',
                    width: 250, 
                    allowBlank: false,
                    name: "emailadmin"
                },			
				{
                    xtype: 'textfield',
                    inputType: 'password',
					id: 'passadmin',
                    fieldLabel: 'Password Email',
                    width: 250, 
                    allowBlank: false,
                    name: "passadmin"
                },
			],
			buttons: [
				{
                    id:'btn_simpan',
                    iconCls: 'silk-save',
                    text: 'Simpan',
                    handler: function() {
                        simpan('fp.setadm');
                    }
				}, 
                {
                    text: 'Tutup',
                    handler: function() {
                        wSetAdm.close();
                    }
                }
			],
			listeners:{
				afterrender: module_afterrender
			}
		});
		
	function module_afterrender(){
            fpSettingAdmin.getForm().load({
                url : BASE_URL + 'website/c_hub_kami_info/settingadmin',  
                success: function(form, action){
                },
                failure: function(form, action){
                Ext.MessageBox.alert('Failure', 'Fail to get data');
                },
                waitMsg:'Loading..'
            });
    }
	
	function simpan(namaForm) {
		var form_nya = Ext.getCmp(namaForm);
		form_nya.getForm().submit({
			url: BASE_URL + 'website/c_hub_kami_info/update_settingadmin',
			method: 'POST',
			success: function() {
				Ext.MessageBox.alert("Informasi", "Simpan Data Berhasil");
				 wSetAdm.close();
			},
			failure: function() {
				Ext.MessageBox.alert("Informasi", "Simpan Data Gagal");
			}
		});
	}
		
		var wSetAdm = new Ext.Window({
			title: 'Setting Email Admin', 
			modal: true, 
			layout: 'fit',
			width:400, height:150,
			items: [fpSettingAdmin]
		}).show();
	}
	
	
	
	
	
	
	
	
}