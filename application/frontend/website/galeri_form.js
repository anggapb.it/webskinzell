function f_galeri_form(id_module, ds) {
	var hapusfotox = 0;
	var ds_jgaleri = store_jgaleri();
	var ds_klpgaleri = store_klpgaleri();
	var ds_stpublish = store_stpublish();
	
	var form_bp_general = new Ext.form.FormPanel({
		border: false,
		fileUpload: true,
		id: 'form_bp_general',
		labelAlign: 'left',
		buttonAlign: 'left',
		bodyStyle: 'padding:10px 3px 3px 5px',
		// atas, kanan, bawah, kiri
		monitorValid: true,
		height: 470,
		width: 670,
		layout: 'form',
		autoScroll: true,
		reader: new Ext.data.JsonReader({
			root: 'data',
			totalProperty: 'results',
			id: 'id',
			fields: [{
				name: "kdgaleri",
				mapping: "kdgaleri"
			},
			{
				name: "nmgaleriind",
				mapping: "nmgaleriind"
			},
			{
				name: "idklpgaleri",
				mapping: "idklpgaleri"
			},
			{
				name: "nmklpgaleri",
				mapping: "nmklpgaleri"
			},
			{
				name: "idjnsgaleri",
				mapping: "idjnsgaleri"
			},
			{
				name: "nmjnsgaleri",
				mapping: "nmjnsgaleri"
			},
			{
				name: "pengguna",
				mapping: "pengguna"
			},
			{
				name: "dilihat",
				mapping: "dilihat"
			},
			{
				name: "deskripsiind",
				mapping: "deskripsiind"
			},
			{
				name: "photo-path",
				mapping: "file"
			},
			{
				name: "temp_foto",
				mapping: "file"
			},
			{
				name: "idstpublish",
				mapping: "idstpublish"
			},
			{
				name: "nmstpublish",
				mapping: "nmstpublish"
			},
			{
				name: "url",
				mapping: "url"
			},
			{
				name: "slideratas",
				mapping: "slideratas",
				type: 'bool'
			},
			{
				name: "sliderbawah",
				mapping: "sliderbawah",
				type: 'bool',
			},
			{
				name: "tglpublish",
				mapping: "tglpublish"
			},
			{
				name: "deskripsieng",
				mapping: "deskripsieng"
			},
			{
				name: "nmgalerieng",
				mapping: "nmgalerieng"
			}]
		}),
		items: [
		{   
			xtype:'fieldset',
			title: '',
			width:640,
			items :[
		{
			xtype: 'textfield',
			fieldLabel: 'Kode ',
			id: 'kdgaleri',
			name: 'kdgaleri',
			hidden: true,
			//value: "NULL"
		},
		{
			xtype: 'combo',
			width: 125,
			height: 50,
			allowBlank: false,
			store: ds_jgaleri,
			fieldLabel: 'Jenis Galeri',
			id: 'idjnsgaleri',
			triggerAction: 'all',
			editable: false,
			valueField: 'idjnsgaleri',
			displayField: 'nmjnsgaleriind',
			forceSelection: true,
			submitValue: true,
			hiddenName: 'h_kdjnsgaleri',
			listeners: {},
			typeAhead: true,
			mode: 'local',
			emptyText: 'Pilih...',
			selectOnFocus: true
		},
		{
			xtype: 'combo',
			width: 150,
			height: 50,
			allowBlank: false,
			store: ds_klpgaleri,
			fieldLabel: 'Kelompok Galeri',
			id: 'idklpgaleri',
			triggerAction: 'all',
			editable: false,
			valueField: 'idklpgaleri',
			displayField: 'nmklpgaleriind',
			forceSelection: true,
			submitValue: true,
			hiddenName: 'h_kdklpgaleri',
			listeners: {},
			typeAhead: true,
			mode: 'local',
			emptyText: 'Pilih...',
			selectOnFocus: true
		}]
		},
		{   
			xtype:'fieldset',
			title: '',
			width:640,
			items :[{
						xtype: 'textfield',
						fieldLabel: 'Nama',
						name: 'nmgaleriind'
					},
					{
						xtype: 'htmleditor',
						fieldLabel: 'Deskripsi',
						height: 100,
						name: 'deskripsiind',
					id: 'deskripsiind'
					}]
		},
		{   
			xtype:'fieldset',
			title: 'Bahasa Inggris', hidden: true,
			width:640,
			items :[{
						xtype: 'textfield',
						fieldLabel: 'Nama',
						name: 'nmgalerieng'
					},
					{
						xtype: 'htmleditor',
						fieldLabel: 'Deskripsi',
						height: 100,
						name: 'deskripsieng',
						id: 'deskripsieng'
					}]
		},
		//                            {
		//                                xtype: 'compositefield',
		//                                name: 'comp_file',
		//                                fieldLabel: 'File ',
		//                                //labelStyle: 'width:160px',
		//                                id: 'comp_file',
		//                                width: 200,
		//                                items: [{
		//                                        xtype: 'textfield',
		//                                        id: 'file',
		//                                        listeners: {
		//                                                'change': function(c) {
		//                                                //	isi_foto();
		//                                                }
		//                                        }
		//                                },
		//                                {
		//                                        xtype: 'button',
		//                                        text: ' ... ',
		//                                        id: 'btn_data',
		//                                        width: 3,
		//                                        handler: function() {
		//                                                find_x("Data Gambar Halaman", 1);
		//
		//                                        }
		//                                }]
		//
		//                            },
		{   
			xtype:'fieldset',
			title: '',
			width:640,
			items :[
		{
			xtype: 'fileuploadfield',
			id: 'photo-path',
			emptyText: 'Select an image',
			fieldLabel: 'Photo',
			allowBlank:false,
			name: 'photo-path',
			buttonText: '',
			anchor: '90%',
			buttonCfg: {
				iconCls: 'silk-image'
			},
			listeners: {
				valid: function() {
					hapusfotox = 1;
				}
			}
		},
		{
			xtype: 'textfield',
			fieldLabel: '',
			hidden:true,
			id: 'temp_foto',
			name: 'temp_foto',
			width: 175
		},
		{
			xtype: 'checkboxgroup',
			fieldLabel: 'Set as Banner',
			columns: 2,
			width: 200,
			items: [{
				boxLabel: 'Iya',
				id: 'slideratas',
				name: 'slideratas'
			},
			{
				boxLabel: 'Bawah',
				id: 'sliderbawah',
				name: 'sliderbawah',
				hidden: true
			},{
				boxLabel: 'Galeri',
				id: 'tbhgaleri',
				name: 'idpublishgal',
				hidden: true
			}],
			listeners: {}
		},
		{
			xtype: 'combo',
			width: 125,
			height: 50,
			allowBlank: false,
			store: ds_stpublish,
			fieldLabel: 'Status Publish',
			//fieldLabel: 'Tambahkan ke Galeri',
			id: 'idstpublish',
			triggerAction: 'all',
			editable: false,
			valueField: 'idstpublish',
			displayField: 'nmstpublish',
			forceSelection: true,
			submitValue: true,
			hiddenName: 'h_status',
			listeners: {},
			typeAhead: true,
			mode: 'local',
			emptyText: 'Pilih...',
			selectOnFocus: true
		},
		{
			xtype: 'datefield',
			fieldLabel: 'Tgl. Publish ',
			id: 'tglpublish',
			name: 'tglpublish',
			allowBlank: false,
			hidden: true
		}]
		},
		{
			xtype: 'textfield',
			id: 'pengguna',
			name: 'pengguna',
			hidden: true
		}],
		buttons: [{
			id: 'btn_simpan',
			iconCls: 'silk-save',
			text: 'Simpan',
			handler: function() {
				if (id_module != '') {
					//   alert(Ext.getCmp('slideratasx').getValue());
					ubah_grid('form_bp_general');
				} else {
					simpan_grid('form_bp_general');
				}
			}
		},
		{
			text: 'Kembali',
			handler: function() {
				win.close();
			}
		}],
		listeners: {
			afterrender: module_afterrender
		}
	});

	function module_afterrender() {
	Ext.getCmp('tglpublish').setValue(SYS_DATE);
	Ext.getCmp("kdgaleri").setReadOnly(false);
	
		if (id_module != "") {
			form_bp_general.getForm().load({
				url: BASE_URL + 'website/c_galeri/grid',
				params: {
					id_module: id_module
				},
				success: function() {
					Ext.getCmp('btn_simpan').setText('Edit');
				},
				failure: function(form, action) {
					Ext.MessageBox.alert('Failure', 'Fail to get data');
				},
				waitMsg: 'Loading..'
			});
			
			Ext.getCmp("kdgaleri").setReadOnly(true);
		}
		//   Ext.getCmp('pengguna').setValue(USERNAME);
	}
	var win = new Ext.Window({
		title: 'Galeri',
		modal: true,
		items: [form_bp_general]
	}).show();

	function simpan_grid(namaForm) {
		
		var form_nya = Ext.getCmp(namaForm);
		form_nya.getForm().submit({
			url: BASE_URL + 'website/c_galeri/upload',
			method: 'POST',
			params: {
				penggunax: USERID,
				slideratasx:Ext.getCmp('slideratas').getValue(),
				sliderbawahx:Ext.getCmp('sliderbawah').getValue(),
				tbhgaleri:Ext.getCmp('tbhgaleri').getValue()
			},
			//waitMsg: 'Sedang mengunggah foto...',
			success: function(form_bp_general, o) {
				if (o.result.success == 'true') {
					Ext.MessageBox.alert('Informasi', 'File ' + o.result.nama + o.result.data);
					ds.load();
					win.close();
				} else if (o.result.success == 'false') {
					Ext.MessageBox.alert('Informasi', 'File ' + o.result.nama + o.result.data);
				}
			}
		});
	}

	function ubah_grid(namaForm) {
		
		var msgplus = '';
		var form_nya = Ext.getCmp(namaForm);
		Ext.MessageBox.show({
			title: "Konfirmasi",
			width: 350,
			msg: msgplus + "<center> Anda Yakin Untuk Mengubah Data ini?</center>",
			buttons: Ext.MessageBox.YESNO,
			fn: function(btn) {
				if (btn == 'yes') {
					form_nya.getForm().submit({
						url: BASE_URL + 'website/c_galeri/upload_update',
						params: {
							id: id_module,
							hapusfoto: hapusfotox,
							get_foto:Ext.getCmp('photo-path').getValue(),
							slideratasx:Ext.getCmp('slideratas').getValue(),
							sliderbawahx:Ext.getCmp('sliderbawah').getValue(),
							tbhgaleri:Ext.getCmp('tbhgaleri').getValue()
						},
						method: 'POST',
						success: function(form_bp_general, o) {
							if (o.result.success == 'true') {
								Ext.MessageBox.alert('Informasi', 'Ubah Data Berhasil' + o.result.nama + o.result.data);
								ds.load();
								win.close();
							} else if (o.result.success == 'false') {
								Ext.MessageBox.alert('Informasi', 'Ubah Data Gagal, File ' + o.result.nama + ' gagal di upload' + o.result.data);
							}
						}
					});
				}
			}
		});
	}
}