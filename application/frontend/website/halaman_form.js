function f_halaman_form(isUpdate,id_module,ds) {
	//RH.startTimer('jampublish');
	//RH.startTimer('jamagenda');
	Ext.QuickTips.init();
      p = new Ext.Panel({
                bodyBorder:false,
                width: 170,
                height:160,
                html: '<p><i>PHOTO</i></p>'
            });
		var ds_kategori_halaman = store_kategori_halaman();
		var ds_stpublish = store_stpublish();
        var ds_stposting = store_stposting();
    	var form_bp_general = new Ext.form.FormPanel({
                border: false,
		id: 'form_bp_general',
		labelAlign: 'left',
		buttonAlign: 'left',
		bodyStyle: 'padding:10px 3px 3px 5px', // atas, kanan, bawah, kiri
		monitorValid: true,
                height: 550,
		width: 700,
              	layout: 'form',
                autoScroll: true,
                reader: new Ext.data.JsonReader ({
                    root: 'data',
                    totalProperty: 'results',
                    id: 'id',
                    fields: [
                        {
						name: "idhalaman",
						mapping: "idhalaman"
					},{
						name: "kdhalaman",
						mapping: "kdhalaman"
					},{
						name: "idktghalaman",
						mapping: "idktghalaman"
					},{
						name: "nmktghalaman",
						mapping: "nmktghalaman"
					},{
						name: "judulind",
						mapping: "judulind"
					},{
						name: "sinopsisind",
						mapping: "sinopsisind"
					},{
						name: "deskripsiind",
						mapping: "deskripsiind"
					},{
						name: "juduleng",
						mapping: "juduleng"
					},{
						name: "sinopsiseng",
						mapping: "sinopsiseng"
					},{
						name: "deskripsieng",
						mapping: "deskripsieng"
					},{
						name: "idstpublish",
						mapping: "idstpublish"
					},{
						name: "nmstpublish",
						mapping: "nmstpublish"
					},{
						name: "tglpublish",
						mapping: "tglpublish"
					},{
						name: "userid",
						mapping: "userid"
					},{
						name: "pengguna",
						mapping: "pengguna"
					},{
						name: "idstposting",
						mapping: "idstposting"
					},{
                        name: "nmstposting",
                        mapping: "nmstposting"
                    },{
                        name: "dilihat",
                        mapping: "dilihat"
                    },{
						name: "jampublish",
						mapping: "jampublish"
					},{
						name: "tglagenda",
						mapping: "tglagenda"
					},{
						name: "jamagenda",
						mapping: "jamagenda"
					},{
						name: "gambar",
						mapping: "gambar"
					}]
                }),
                items: [ {   
					xtype:'fieldset',
					title: '',
					autoWidth:true,
					items :[
                            {
                                    xtype: 'textfield', //labelStyle: 'width:140px',
                                    fieldLabel: 'id',
                                    id: 'idhalaman',
                                    name: 'idhalaman',
									hidden: true,
									readOnly: true
                            },
							{
                                    xtype: 'textfield', //labelStyle: 'width:140px',
                                    fieldLabel: 'Kode ',
                                    id: 'kdhalaman',
                                    name: 'kdhalaman',
									hidden: true
									
                            },
                            {
                                    xtype: 'combo',
                                    width: 125,
                                    height: 50,
                                    allowBlank: false,
                                    store: ds_kategori_halaman,
                                    fieldLabel: 'Kategori Halaman',
                                    id: 'idktghalaman',
                                    triggerAction: 'all',
                                    editable: false,
                                    valueField: 'idktghalaman',
                                    displayField: 'nmktghalamanind',
                                    forceSelection: true,
                                    submitValue: true,
                                    hiddenName: 'h_ktghalaman',
                                    listeners: {},

                                    typeAhead: true,
                                    mode: 'local',
                                    emptyText:'Pilih...',
                                    selectOnFocus:true

                            }]},                         
					{   
					xtype:'fieldset',
					title: '',
					autoWidth:true,
					items :[{
                                    xtype: 'textfield',
                                    fieldLabel: 'Judul',
                                    id: 'judulind',
                                    name: 'judulind',
									width: 500
                            },
                            {	
                                    xtype: 'htmleditor',
                                    fieldLabel: 'Sinopsis',
                                    height:200,
                                    width:550,
                                    anchor:'97% 20%',// autoScroll :true,
                                    name:'sinopsisind',
                                    id:'sinopsisind'
                            },
                            {	
                                    xtype: 'htmleditor',
                                    fieldLabel: 'Deskripsi',
                                    height:400,
                                    width:550,
                                    anchor:'97% 75%',
                                    plugins         : [new Ext.ux.form.HtmlEditor.Table()],
                                    name:'deskripsiind',
                                    id:'deskripsiind'
                            }]},
                    {   
					xtype:'fieldset',
					title: 'Bahasa Inggris', hidden: true,
					autoWidth:true,
					items :[{
                                    xtype: 'textfield',
                                    fieldLabel: 'Judul',
                                    id: 'juduleng',
                                    name: 'juduleng',
									width: 500
                            },
                            {	
                                    xtype: 'htmleditor',
                                    fieldLabel: 'Sinopsis',
                                    height:200,
                                    width:550,
                                    anchor:'97% 20%',
                                    name: 'sinopsiseng',
                                    id: 'sinopsiseng'
                            },
                            {	
                                    xtype: 'htmleditor',
                                    fieldLabel: 'Deskripsi',
                                    height:500,
                                    width:550,
                                    anchor:'97% 75%',
                                    name: 'deskripsieng',
                                    id: 'deskripsieng'
                            }]},
                     {   
					xtype:'fieldset',
					title: '',
					autoWidth:true,
					items :[{
                                    xtype: 'compositefield',
                                    name: 'comp_file_gambar',
                                    fieldLabel: 'File Gambar',
                                    //labelStyle: 'width:160px',
                                    id: 'comp_file_gambar',
                                    width: 200,
                                    items: [{
                                            xtype: 'textfield',
                                            id: 'gambar',
											name: 'gambar',
											readOnly: true,
                                            listeners: {
                                                    'change': function(c) {
                                                            isi_foto_ori( Ext.getCmp("gambar").getValue());
                                                    }
                                            }
                                    },
                                    {
                                            xtype: 'button',
                                            text: ' ... ',
                                            id: 'btn_data_gambar',
                                            width: 3,
                                            handler: function() {
                                                    find_x("Data Gambar Halaman", 1);

                                            }
                                    }]
                            },{
                                layout: 'form',
                                width:300,
                                bodyStyle: 'padding:0px 3px 0px 100px', //atas kanan bawah kiri
                                border: false,
                                items: 
                                    [{
                                        xtype: 'fieldset',
                                        id:'file_gambar',
                                        title: 'Foto',
                                        height: 160,
                                        width: 130, 
                                        items:[p]
                                    }]
                            },
                            {
                                    xtype: 'combo',
                                    width: 125,
                                    height: 50,
                                    allowBlank: false,
                                    store: ds_stpublish,
                                    fieldLabel: 'Status Publish',
                                    id: 'idstpublish',
                                    triggerAction: 'all',
                                    editable: false,
                                    valueField: 'idstpublish',
                                    displayField: 'nmstpublish',
                                    forceSelection: true,
                                    submitValue: true,
                                    hiddenName: 'h_status',
                                    listeners: {},

                                    typeAhead: true,
                                    mode: 'local',
                                    emptyText:'Pilih...',
                                    selectOnFocus:true

                            },/*{
                                    xtype: 'combo',
                                    width: 125,
                                    height: 50,
                                    allowBlank: false,
                                    store: ds_stposting,
                                    fieldLabel: 'Status Posting',
                                    id: 'idstposting',
                                    triggerAction: 'all',
                                    editable: false,
                                    valueField: 'idstposting',
                                    displayField: 'nmstposting',
                                    forceSelection: true,
                                    submitValue: true,
                                    hiddenName: 'h_stposting',
                                    listeners: {},

                                    typeAhead: true,
                                    mode: 'local',
                                    emptyText:'Pilih...',
                                    selectOnFocus:true,
                                    hidden: true,

                            },*/{
                                    xtype: 'datefield',
                                    fieldLabel: 'Tgl.Publish',
                                    name: 'tglpublish',
                                    id: 'tglpublish'
                            },{
                                    xtype: 'textfield',
                                    fieldLabel: 'Jam Publish',
                                    name: 'jampublish',
                                    id: 'jampublish'
                            },{
                                    xtype: 'datefield',
                                    fieldLabel: 'Tgl.Agenda',
                                    name: 'tglagenda',
                                    id: 'tglagenda',
                                    hidden: true
                            },{
                                    xtype: 'textfield',
                                    fieldLabel: 'Jam Agenda',
                                    name: 'jamagenda',
                                    id: 'jamagenda',
                                    hidden: true
                            }]
							
						}
                    ],

                buttons: [{
                    id:'btn_simpan',
                    text: 'Simpan',
                    iconCls: 'silk-save',
                    handler: function() {
                        if(id_module){
                            ubah_grid('form_bp_general');
                        }else{
                            simpan_grid('form_bp_general');
                        }
                            
                    }
                }, 
                    {
                    text: 'Kembali',
                    handler: function() {
						clearInterval();
                        win.close();
                    }
                }
                    ],listeners:{
            afterrender: module_afterrender
        }
    });
    
    function module_afterrender(){
	Ext.getCmp("kdhalaman").setReadOnly(false);
        if(isUpdate){
			
            form_bp_general.getForm().load({
                url: BASE_URL + 'website/c_halaman/grid',
                params:{
                    id_module:id_module
                },
                success: function(form, action){
					Ext.getCmp("kdhalaman").setReadOnly(true);
                    isi_foto_ori( Ext.getCmp("gambar").getValue());

                },
                failure: function(form, action){
                Ext.MessageBox.alert('Failure', 'Fail to get data');
                },
                waitMsg:'Loading..'
            });
			
        } else {
			Ext.getCmp('tglpublish').setValue(SYS_DATE);
			Ext.getCmp('tglagenda').setValue(SYS_DATE);
			
			setInterval(function(){
				var d=new Date();
				var t=d.toLocaleTimeString();
				if(Ext.getCmp('jampublish')) {
					RH.setCompValue('jampublish',t);
				}
				if(Ext.getCmp('jamagenda')) {
					RH.setCompValue('jamagenda',t);		
				}
			},1000);
		}
    }
        
    var win = new Ext.Window({
			title: 'Halaman',
			layout: "fit",
                        modal: true,
                        resizable: true,
                       	maximizable:true,
			items: [form_bp_general]
		}).show();
    
    function simpan_grid(namaForm) {
		var form_nya = Ext.getCmp(namaForm);
		form_nya.getForm().submit({
			url: BASE_URL + 'website/c_halaman/save',
			method: 'POST',
                        params:{
							userid:USERID
                        },
			success: function() {
				Ext.MessageBox.alert("Informasi", "Simpan Data Berhasil");
				ds.load();
				form_bp_general.getForm().reset();
				Ext.getCmp('form_bp_general').enable();
				Ext.getCmp('btn_simpan').enable();

                                win.close();
			},
			failure: function() {
				Ext.MessageBox.alert("Informasi", "Simpan Data Gagal");
			}
		});
	}
	
	function ubah_grid(namaForm) {

        var msgplus='';

		var form_nya = Ext.getCmp(namaForm);
		Ext.MessageBox.show({
			title: "Konfirmasi",
                        width: 350,
			msg: msgplus +"<center> Anda Yakin Untuk Mengubah Data ini?</center>",
			buttons: Ext.MessageBox.YESNO,
			fn: function(btn) {
				if (btn == 'yes') {
					form_nya.getForm().submit({
						url: BASE_URL + 'website/c_halaman/update',
                                                params:{
                                                    userid:USERID
                                                },
						method: 'POST',
						success: function() {
							Ext.MessageBox.alert("Informasi", "Ubah Data Berhasil");
							ds.load();
							form_bp_general.getForm().reset();
							Ext.getCmp('form_bp_general').enable();
							Ext.getCmp('btn_simpan').enable();
                            win.close();
						},
						failure: function() {
							Ext.MessageBox.alert("Informasi", "Ubah Data Gagal");
						}
					});
				}
			}
		});
	}
 }