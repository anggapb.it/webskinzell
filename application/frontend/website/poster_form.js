function f_poster(id_module,ds) {
      p = new Ext.Panel({
                bodyBorder:false,
                width: 170,
                height:160,
                html: '<p><i>PHOTO</i></p>'
            });
        var ds_kategori_halaman = store_kategori_halaman();
        var ds_stpublish = store_stpublish();
 
		var ds_status = store_status();

		var form_bp_general = new Ext.form.FormPanel({
                border: false,
        id: 'form_bp_general',
        labelAlign: 'left',
        buttonAlign: 'left',
        bodyStyle: 'padding:10px 3px 3px 5px', // atas, kanan, bawah, kiri
        monitorValid: true,
                height: 400,
        width: 380,
                layout: 'form',
                autoScroll: true,
                reader: new Ext.data.JsonReader ({
                    root: 'data',
                    totalProperty: 'results',
                    id: 'id',
                    fields: [
                    {
                        name: "idposter",
                        mapping: "idposter"
                    },{
                        name: "nmposter",
                        mapping: "nmposter"
                    },{
						name: "idstatus",
						mapping: "idstatus"
					},{
						name: "nmstatus",
						mapping: "nmstatus"
					},{
                        name: "gambar",
                        mapping: "gambar"
                    }]
                }),
                items: [ {
                                    xtype: 'textfield', //labelStyle: 'width:140px',
                                    fieldLabel: 'Id ',
                                    id: 'idposter',
                                    name: 'idposter',
                                    hidden: true,          
                            },{
                                    xtype: 'textfield', //labelStyle: 'width:140px',
                                    fieldLabel: 'Nama Poster ',
                                    id: 'nmposter',
                                    name: 'nmposter'          
                            },{
                                    xtype: 'combo',
                                    width: 125,
                                    height: 50,
                                    allowBlank: false,
                                    store: ds_status,
                                    fieldLabel: 'Status',
                                    id: 'idstatus',
                                    triggerAction: 'all',
                                    editable: false,
                                    valueField: 'idstatus',
                                    displayField: 'nmstatus',
                                    forceSelection: true,
                                    submitValue: true,
                                    hiddenName: 'h_status',
                                    listeners: {},

                                    typeAhead: true,
                                    mode: 'local',
                                    emptyText:'Pilih...',
                                    selectOnFocus:true

                            },{
                                    xtype: 'compositefield',
                                    name: 'comp_file_gambar',
                                    fieldLabel: 'File Gambar',
                                    //labelStyle: 'width:160px',
                                    id: 'comp_file_gambar',
                                    width: 200,
                                    items: [{
                                            xtype: 'textfield',
                                            id: 'gambar',
                                            name: 'gambar',
                                            readOnly: true,
                                            listeners: {
                                                    'change': function(c) {
                                                            isi_foto_ori( Ext.getCmp("gambar").getValue());
                                                    }
                                            }
                                    },
                                    {
                                            xtype: 'button',
                                            text: ' ... ',
                                            id: 'btn_data_gambar',
                                            width: 3,
                                            handler: function() {
                                                    find_x("Data Gambar Halaman", 1);

                                            }
                                    }]
                            },{
                                layout: 'form',
                                width:300,
                                bodyStyle: 'padding:0px 3px 0px 100px', //atas kanan bawah kiri
                                border: false,
                                items: 
                                    [{
                                        xtype: 'fieldset',
                                        id:'file_gambar',
                                        title: 'Foto',
                                        height: 160,
                                        width: 130, 
                                        items:[p]
                                    }]
                            }
                    ],

                buttons: [{
                    id:'btn_simpan',
                    text: 'Simpan',
                    iconCls: 'silk-save',
                    handler: function() {
                        if(id_module!=''){
                            ubah_grid('form_bp_general');
                        }else{
                            simpan_grid('form_bp_general');
                        }
                            
                    }
                }, 
                    {
                    text: 'Kembali',
                    handler: function() {
                        win.close();
                    }
                }
                    ],listeners:{
            afterrender: module_afterrender
        }
    });
    
    function module_afterrender(){
    Ext.getCmp("idposter").setReadOnly(true);
        if(id_module!=""){
            form_bp_general.getForm().load({
                url: BASE_URL + 'website/c_poster/grid',
                params:{
                    id_module:id_module
                },
                success: function(form, action){
                    Ext.getCmp("idposter").setReadOnly(true);
                    isi_foto_ori( Ext.getCmp("gambar").getValue());
                    
                },
                failure: function(form, action){
                Ext.MessageBox.alert('Failure', 'Fail to get data');
                },
                waitMsg:'Loading..'
            });
            
        }
       
    }
        
    var win = new Ext.Window({
            title: 'Poster',
            layout: "fit",
                        modal: true,
                        resizable: true,
                        maximizable:true,
            items: [form_bp_general]
        }).show();
    
    function simpan_grid(namaForm) {
        var form_nya = Ext.getCmp(namaForm);
        form_nya.getForm().submit({
            url: BASE_URL + 'website/c_poster/save',
            method: 'POST',
                        params:{
                            userid:USERID
                        },
            success: function() {
                Ext.MessageBox.alert("Informasi", "Simpan Data Berhasil");
                ds.load();
                form_bp_general.getForm().reset();
                Ext.getCmp('form_bp_general').enable();
                Ext.getCmp('btn_simpan').enable();

                                win.close();
            },
            failure: function() {
                Ext.MessageBox.alert("Informasi", "Simpan Data Gagal");
            }
        });
    }
    
    function ubah_grid(namaForm) {

        var msgplus='';

        var form_nya = Ext.getCmp(namaForm);
        Ext.MessageBox.show({
            title: "Konfirmasi",
                        width: 350,
            msg: msgplus +"<center> Anda Yakin Untuk Mengubah Data ini?</center>",
            buttons: Ext.MessageBox.YESNO,
            fn: function(btn) {
                if (btn == 'yes') {
                    form_nya.getForm().submit({
                        url: BASE_URL + 'website/c_poster/update',
                                                params:{
                                                    userid:USERID
                                                },
                        method: 'POST',
                        success: function() {
                            Ext.MessageBox.alert("Informasi", "Ubah Data Berhasil");
                            ds.load();
                            form_bp_general.getForm().reset();
                            Ext.getCmp('form_bp_general').enable();
                            Ext.getCmp('btn_simpan').enable();
                            win.close();
                        },
                        failure: function() {
                            Ext.MessageBox.alert("Informasi", "Ubah Data Gagal");
                        }
                    });
                }
            }
        });
    }
 }