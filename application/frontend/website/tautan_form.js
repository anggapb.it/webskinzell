function f_tautan_form(id_module,ds) {
	
	 p = new Ext.Panel({
                bodyBorder:false,
                width: 170,
                height:160,
                html: '<p><i>PHOTO</i></p>'
            });
			
	var ds_klptautan = store_klptautan();
	var ds_stpublish = store_stpublish();

	var form_bp_general = new Ext.form.FormPanel({
        border: false,
		id: 'form_bp_general',
		labelAlign: 'left',
		buttonAlign: 'left',
		bodyStyle: 'padding:10px 3px 3px 5px', // atas, kanan, bawah, kiri
		monitorValid: true,
                height: 550,
		width: 700,
              	layout: 'form',
				autoScroll: true,
                reader: new Ext.data.JsonReader ({
                    root: 'data',
                    totalProperty: 'results',
                    id: 'id',
                    fields: [
                        {
                                name: "idtautan",
                                mapping: "idtautan"
                        },{
								name: "kdtautan",
								mapping: "kdtautan"
						},{
                                name: "nmtautanind",
                                mapping: "nmtautanind"
                        },{
                                name: "idklptautan",
                                mapping: "idklptautan"
                        },{
                                name: "nmklptautan",
                                mapping: "nmklptautan"
                        },{
                                name: "userid",
                                mapping: "userid"
                        },{
                                name: "pengguna",
                                mapping: "pengguna"
                        },{
                                name: "dilihat",
                                mapping: "dilihat"
                        },{
                                name: "deskripsiind",
                                mapping: "deskripsiind"
                        },{
                                name: "gambar",
                                mapping: "gambar"
                        },{
                                name: "idstpublish",
                                mapping: "idstpublish"
                        },{
                                name: "nmstpublish",
                                mapping: "nmstpublish"
                        },{
                                name: "url",
                                mapping: "url"
                        },{
                                name: "tglpublish",
                                mapping: "tglpublish"
                        },{
                                name: "deskripsieng",
                                mapping: "deskripsieng"
                        },{
                                name: "nmtautaneng",
                                mapping: "nmtautaneng"
                        }
                    ]
                }),
                items: [     
                            {   
					xtype:'fieldset',
					title: '',
					width:670,
					items :[{
                                    xtype: 'textfield',
                                    fieldLabel: '',
									id: 'idtautan',
                                    name: 'idtautan',
									hidden: true
                            },{
                                    xtype: 'textfield',
                                    fieldLabel: 'Kode',
									id: 'kdtautan',
                                    name: 'kdtautan'			
                            },
                            {
                                    xtype: 'combo',
                                    width: 125,
                                    height: 50,
                                    allowBlank: false,
                                    store: ds_klptautan,
                                    fieldLabel: 'Kelompok Tautan',
                                    id: 'idklptautan',
                                    triggerAction: 'all',
                                    editable: false,
                                    valueField: 'idklptautan',
                                    displayField: 'nmklptautanind',
                                    forceSelection: true,
                                    submitValue: true,
                                    hiddenName: 'h_kdklptautan',
                                    listeners: {},

                                    typeAhead: true,
                                    mode: 'local',
                                    emptyText:'Pilih...',
                                    selectOnFocus:true

                             }]},   
                             {   
					xtype:'fieldset',
					title: 'Bahasa Indonesia',
					width:670,
					items :[{
                                    xtype: 'textfield',
                                    fieldLabel: 'Nama',
                                    name: 'nmtautanind'			
                            },
                            {
                                    xtype: 'htmleditor',
                                    fieldLabel: 'Deskripsi',
                                    border:false,
                                    //anchor:'98%',
                                    //layout:'fit',
                                    height:100,
                                    autoWidth:true,
                                    name: 'deskripsiind'			
                             }]},   
                             {   
					xtype:'fieldset',
					title: 'Bahasa Inggris',
					width:670,
					items :[{
                                    xtype: 'textfield',
                                    fieldLabel: 'Nama',
                                    name: 'nmtautaneng'
                            },
                            {
                                    xtype: 'htmleditor',
                                    fieldLabel: 'Deskripsi',
                                    //anchor:'98%',
                                    height:100,
                                     autoWidth:true,
                                    name: 'deskripsieng'
                            }]},   
                             {   
					xtype:'fieldset',
					title: '',
					width:670,
					items :[{
                                    xtype: 'compositefield',
                                    items: [

                                    {
                                            xtype: 'textfield',
                                            fieldLabel: 'Gambar',
                                            name:'gambar',
                                            id:'gambar',
											readOnly: true,
                                            labelAlign: 'right',
											listeners: {
                                                    'change': function(c) {
                                                            isi_foto_ori( Ext.getCmp("gambar").getValue());
                                                    }
                                            }
                                    },

                                    {
                                    xtype: 'button',
                                    text: '...',
                                    id: 'btn_data',
                                    width: 3,
                                    handler: function() {
                                            find_x("Data Gambar Halaman", 1);

                                    }
                                    }]
                            },
							{
                                layout: 'form',
                                width:300,
                                bodyStyle: 'padding:0px 3px 0px 100px', //atas kanan bawah kiri
                                border: false,
                                items: 
                                    [{
                                        xtype: 'fieldset',
                                        id:'file_gambar',
                                        title: 'Foto',
                                        height: 160,
                                        width: 130, 
                                        items:[p]
                                    }]
                            },
                            {
                                    xtype: 'textfield',
                                    fieldLabel:'URL',
                                    name:'url'			
                            },
                            {
                                    xtype: 'combo',
                                    width: 125,
                                    height: 50,
                                    allowBlank: false,
                                    store: ds_stpublish,
                                    fieldLabel: 'Status Publish',
                                    id: 'idstpublish',
                                    triggerAction: 'all',
                                    editable: false,
                                    valueField: 'idstpublish',
                                    displayField: 'nmstpublish',
                                    forceSelection: true,
                                    submitValue: true,
                                    hiddenName: 'h_status',
                                    listeners: {},

                                    typeAhead: true,
                                    mode: 'local',
                                    emptyText:'Pilih...',
                                    selectOnFocus:true

                            },
                            {
                                    xtype: 'datefield',
                                    fieldLabel: 'Tgl.Publish',
									id: 'tglpublish',
                                    name: 'tglpublish'
                            }
               

                     ]}],

                buttons: [{
                    id:'btn_simpan',
                    text: 'Simpan',
                    handler: function() {
                        if(id_module!=''){
                            ubah_grid('form_bp_general');
                        }else{
                            simpan_grid('form_bp_general');
                        }
                            
                    }
                }, 
                    {
                    text: 'Kembali',
                    handler: function() {
                        win.close();
                    }
                }
                    ],listeners:{
            afterrender: module_afterrender
        }
    });
    function module_afterrender(){
	Ext.getCmp('tglpublish').setValue(SYS_DATE);
	Ext.getCmp("kdtautan").setReadOnly(false);
        if(id_module!=""){
            form_bp_general.getForm().load({
                url: BASE_URL + 'website/c_tautan/grid',
                params:{
                    id_module:id_module
                },
                success: function(form, action){
					Ext.getCmp("kdtautan").setReadOnly(true);
					isi_foto_ori( Ext.getCmp("gambar").getValue());
                },
                failure: function(form, action){
                Ext.MessageBox.alert('Failure', 'Fail to get data');
                },
                waitMsg:'Loading..'
            });
        }
    }
        
	var win = new Ext.Window({
			title: 'Tautan',
			modal: true,
			items: [form_bp_general]
		}).show();
    
    function simpan_grid(namaForm) {
		var form_nya = Ext.getCmp(namaForm);
		form_nya.getForm().submit({
			url: BASE_URL + 'website/c_tautan/save',
			method: 'POST',
                        params:{
                           userid:USERID
                        },
			success: function() {
				Ext.MessageBox.alert("Informasi", "Simpan Data Berhasil");
				ds.load();
				form_bp_general.getForm().reset();
				Ext.getCmp('form_bp_general').enable();
				Ext.getCmp('btn_simpan').enable();
			},
			failure: function() {
				Ext.MessageBox.alert("Informasi", "Simpan Data Gagal");
			}
		});
	}
	
	function ubah_grid(namaForm) {

        var msgplus='';

		var form_nya = Ext.getCmp(namaForm);
		Ext.MessageBox.show({
			title: "Konfirmasi",
                        width: 350,
			msg: msgplus +"<center> Anda Yakin Untuk Mengubah Data ini?</center>",
			buttons: Ext.MessageBox.YESNO,
			fn: function(btn) {
				if (btn == 'yes') {
					form_nya.getForm().submit({
						url: BASE_URL + 'website/c_tautan/update',
                                                params:{
                                                  userid:USERID
                                                },
						method: 'POST',
						success: function() {
							Ext.MessageBox.alert("Informasi", "Ubah Data Berhasil");
							ds.load();
							form_bp_general.getForm().reset();
							Ext.getCmp('form_bp_general').enable();
							Ext.getCmp('btn_simpan').enable();
							win.close();
						},
						failure: function() {
							Ext.MessageBox.alert("Informasi", "Ubah Data Gagal");
						}
					});
				}
			}
		});
	}
}