function f_galeri() {
	var id;
	var imgName = '',
		imgId = '';
	var tg_gallery = false;
	var tg_galeri = false;
	var tg_slideratas = false;
	var tg_sliderbawah = false;
	var tg_album = false;
	var tg_thumb = false;
	var file = "";
	
	var ds_photo_list = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'website/c_galeri/grid',
			method: 'POST'
		}),
		root: 'data',
		totalProperty: 'results',
		id: 'photoid',
		remoteSort: true,
		autoLoad: true,
		waitMsg: 'Mengunduh..',
		baseParams: {
			start: 0,
			limit: 50,
			gallery: tg_gallery,
			slideratas: tg_slideratas,
			//sliderbawah:tg_sliderbawah,
			album: tg_album,
			thumb: tg_thumb
		},
		fields: [{
			name: "idgaleri",
			mapping: "idgaleri"
		},{
			name: "kdgaleri",
			mapping: "kdgaleri"
		},
		{
			name: "name",
			mapping: "nmgaleriind"
		},
		{
			name: "tglupload",
			mapping: "tglinput"
		},
		{
			name: "file",
			mapping: "file"
		},
		{
			name: "url",
			mapping: "url"
		},
		{
			name: "status",
			mapping: "idstpublish"
		},
		{
			name: "nmstatus",
			mapping: "nmstpublish"
		},
		{
			name: "album",
			mapping: "slideratas"
		},
		{
			name: "deskripsiind",
			mapping: "deskripsiind"
		},
		{
			name: "idklpgaleri",
			mapping: "idklpgaleri"
		},
		{
			name: "nmklpgaleri",
			mapping: "nmklpgaleri"
		},
		{
			name: "idjnsgaleri",
			mapping: "idjnsgaleri"
		},
		{
			name: "nmjnsgaleri",
			mapping: "nmjnsgaleri"
		},
		{
			name: "tglpublish",
			mapping: "tglpublish"
		},
		{
			name: "userid",
			mapping: "userid"
		},
		{
			name: "pengguna",
			mapping: "pengguna"
		},
		{
			name: "dilihat",
			mapping: "dilihat"
		}]
	});
	
	var tpl = new Ext.XTemplate('<tpl for=".">', 
	'<div class="thumb-wrap" id="{name}">', 
	'<div class="thumb"><img src="../resources/img/ori/{file}" title="{name}"></div>', 
	'</div>', 
	'</tpl>', 
	'<div class="x-clear"></div>');
	
	var tplDetail = new Ext.XTemplate('<div class="details">', 
	'<tpl for=".">', '<table border="0" cellspacing="5">', 
	'<tr>  <td align="center" ><img src="../resources/img/ori/{file}" align="center" width="185" height="174"><div class="details-info"> </td> </tr>', 
	'<tr><td align="justify" > <br><b>Nama Gambar:</b> <br>', 
	'<span> {name}</span> <br>', 
	'<br><b>Deskripsi:</b> <br> </td></tr>', 
	'<tr><td align="center" ><div class="det-font"><span><textarea readonly cols="37" rows="9">{deskripsiind}</textarea> </span> </div>', 
	'</div>', 
	'</td></tr>', 
	'</table>', 
	'</tpl>', 
	'</div>');
	
	var tplDetailClear = new Ext.XTemplate();
	
	var paging_photo_list = new Ext.PagingToolbar({
		pageSize: 50,
		store: ds_photo_list,
		displayInfo: true,
		displayMsg: 'Displaying Photo list {0} - {1} of {2}',
		emptyMsg: 'No data to display'
	});
	var grid_photo_list = new Ext.DataView({
		store: ds_photo_list,
		tpl: tpl,
		//autoHeight:true,
		height: 495,
		multiSelect: true,
		overClass: 'x-view-over',
		itemSelector: 'div.thumb-wrap',
		emptyText: 'No images to display',
		loadMask: true,
		autoScroll: true,
		id: 'dataview_ok',
		//height: 400,
		autoWidth: true,
		//style: 'border:1px solid #99BBE8; border-top-width: 0',
		prepareData: function(data) {
			data.shortName = Ext.util.Format.ellipsis(data.name, 15);
			//data.sizeString = Ext.util.Format.fileSize(data.size);
			//data.dateString = data.lastmod.format("m/d/Y g:i a");
			return data;
		},
		listeners: {
			afterrender: function() {
				panelRightBottom.expand(true);
				//panelRightBottom.collapse(true);
				//panelCart.collapse(true)
			},
			selectionchange: {
				fn: function(dv, nodes) {
					var l = nodes.length;
					var s = l != 1 ? 's' : '';
					panelRightBottom.expand(true);
					//panelCart.collapse(true);
				}
			},
			click: {
				fn: function() {
					var selNode = grid_photo_list.getSelectedRecords();
					if (selNode.length != 0) {
						for (var i = 0; i < selNode.length; i++) {
							imgName = selNode[i].data.name;
							imgId = selNode[i].data.idgaleri;
							file = selNode[i].data.file;
						}
						panelRightBottom.expand(true);
						// panelRightBottom.collapse(true);
						// alert(imgId);
						tplDetail.overwrite(panelRightBottom.body, selNode[0].data);
						
						Ext.getCmp('btn_download').show();
						
					}
				}
			},
			dblclick: function(dv, idx, n, e) {
				var rec = ds_photo_list.getAt(idx);
				f_galeri_form(rec.data["idgaleri"], ds_photo_list);
			}
		}
	});

	function toggleGaleri(item, pressed) {
		tg_galeri = pressed;
		ds_photo_list.setBaseParam('idpublishgal', tg_galeri);
		ds_photo_list.load();
	}

	function toggleSlideratas(item, pressed) {
		tg_slideratas = pressed;
		ds_photo_list.setBaseParam('slideratas', tg_slideratas);
		ds_photo_list.load();
	}

	function toggleSliderbawah(item, pressed) {
		tg_sliderbawah = pressed;
		ds_photo_list.setBaseParam('sliderbawah', tg_sliderbawah);
		ds_photo_list.load();
	}

	function btn_photo_delete() {
		Ext.MessageBox.show({
			title: "Konfirmasi",
			msg: 'Anda Yakin Untuk menghapus gambar ' + imgName + '?',
			buttons: Ext.MessageBox.YESNO,
			fn: function(btn) {
				if (btn == 'yes') {
					Ext.Ajax.request({
						url: BASE_URL + 'website/c_galeri/delete',
						success: function() {
							tplDetailClear.overwrite(panelRightBottom.body, '');
							Ext.getCmp('btn_download').hide();
							ds_photo_list.load();
						},
						failure: function() {},
						params: {
							id: imgId,
							filex: file
						}
					});
				}
			}
		})
	}
	var panelRightBottom = new Ext.Panel({});
	//=================================================== AWAL
	var galleri = new Ext.Panel({
		layout: 'border',
		// autoLoad :true,
		defaults: {
			collapsible: true,
			split: true
		},
		items: [{
			collapsible: false,
			title: "Galeri",
			//bodyStyle: 'padding:0px',
			region: 'center',
			//cmargins: '5 0 0 0',
			id: 'images-view',
			//frame: true,
			//width: 640,
			//height:495,
			items: [grid_photo_list],
			tbar: [{
				text: 'Upload',
				id: 'btn_add',
				iconCls: 'silk-add',
				handler: function() {
					// photo_upload("photo",ds_photo_list);
					f_galeri_form('', ds_photo_list);
				}
			},
			{
				text: 'Delete',
				id: 'btn_photo_delete',
				iconCls: 'silk-delete',
				handler: btn_photo_delete
			}, '->',
			//                            {
			//                                text: 'Gallery',
			//                                enableToggle: true,
			//                                toggleHandler: toggleGallery,
			//                                pressed: false,
			//                                hidden:true
			//                            },'-',
			{
				text: 'Galeri',
				enableToggle: true,
				toggleHandler: toggleGaleri,
				pressed: false
			}, '-',{
				text: 'Banner Atas',
				enableToggle: true,
				toggleHandler: toggleSlideratas,
				pressed: false
			}, '-',
			{
				text: 'Slider Bawah',
				enableToggle: true,
				toggleHandler: toggleSliderbawah,
				pressed: false,
				hidden: true
			}
			//                            ,{
			//                                text: 'Album',
			//                                enableToggle: true,
			//                                toggleHandler: toggleAlbum,
			//                                pressed: false,
			//                                hidden:true
			//                            },
			//                            '-',{
			//                                text: 'Thumbnail',
			//                                enableToggle: true,
			//                                toggleHandler: toggleThumb,
			//                                pressed: false,
			//                                hidden:true
			//                            },'-'
			],
			bbar: [paging_photo_list]
		},
		{
			region: 'east',
			minSize: 75,
			maxSize: 250,
			cmargins: '5 0 0 0',
			frame: true,
			width: 350,
			id: 'images-view2',
			name: 'images-view2',
			title: 'Informasi Gambar',
			items: [panelRightBottom,
			{
				buttons: [{
                    id:'btn_download',
					name:'btn_download',
                    text: 'DOWNLOAD',
					hidden:true,
                    handler: function() {
						 
						 window.open('../resources/img/ori/' + file,'_blank');

						}
					}]
				}],
			collapsed: false
		}],
		listeners: {
			'afterrender': function() {
				//  Ext.getCmp('images-view2').collapse(false);
			}
		}
	});
	get_content(galleri);
}