function f_hub_kami_inbox_form(id_module,ds) {
	//var nmcom = CLIENT; //var nmcom = window.location.hostname;
	var form_bp_general = new Ext.form.FormPanel({
		waitMsgTarget: true,
        border: false,
		id: 'form_bp_general',
		labelAlign: 'left',
		buttonAlign: 'right',
		bodyStyle: 'padding:10px 3px 3px 5px', // atas, kanan, bawah, kiri
		monitorValid: true,
        height: 500,
		width: 800,
              	layout: 'form',
                reader: new Ext.data.JsonReader ({
                    root: 'data',
                    totalProperty: 'results',
                    id: 'id',
                    fields: [
                        {
                                name: "idpesan", mapping: "idpesan"
                        },{
                                name: "email",mapping: "email"
                        },{
                                name: "pesanmsk",mapping: "pesanmsk"
                        },{
                                name: "pesanklr",mapping: "pesanklr"
                        },{
                                name: "status",mapping: "status"
                        },{
                                name: "subject",mapping: "subject"
                        },{
                                name: "cc",mapping: "cc"
                        }
                    ]
                }),
                items: [     
							{
                                    xtype: 'textfield',
                                    fieldLabel: 'Id',
                                    labelStyle: 'width:140px',
                                    width:200,
                                    name: 'idpesan',
									hidden: true
                            },
							{
                                    xtype: 'textfield',
									vtype:'email',
                                    fieldLabel: 'To',
                                    labelStyle: 'width:100px',
                                    width:200,
                                    name: 'email',
									readOnly: true,
									allowBlank: false									
                            },
							{
                                    xtype: 'textfield',
									vtype:'email',
                                    fieldLabel: 'Cc',
                                    labelStyle: 'width:100px',
                                    width:200,
                                    name: 'cc'			
                            },
							{
                                    xtype: 'textfield',
                                    fieldLabel: 'Subject',
                                    labelStyle: 'width:100px',
                                    width:500,
                                    name: 'subject'
                            },
							{
                                    xtype: 'textarea',
                                    fieldLabel: 'Pesan Masuk',
                                    width:680,
									height:150,
									id: 'pesanmsk',
                                    name: 'pesanmsk', 
									readOnly: true,
									hidden: true
                            },
							{
									xtype: 'htmleditor',			
									fieldLabel: 'Tulis Pesan',
									height:200,
									anchor:'99%', 
									name: 'pesanklr', 
									allowBlank: false
							},
							/* {
								xtype: 'textfield',
								fieldLabel: 'Nama Komputer',
								hidden:true,
								readOnly:true,
								id: 'nmcom',
								name: 'nmcom',
								value:nmcom
							}, */
                    ],

                buttons: [{
                    id:'btn_kirim',
                    text: 'Kirim',
                    iconCls: 'silk-save',
                    handler: function() {
						Ext.getCmp('idbtn_batal').disable();
                        if(id_module!=''){
                            ubah_grid('form_bp_general');
                        }else{
                            simpan_grid('form_bp_general');
                        }
                            
                    }
                }, 
                    {
                    text: 'Batal',
					id: 'idbtn_batal',
                    handler: function() {
                        win.close();
                    }
                }
                    ],listeners:{
            afterrender: module_afterrender
        }
    });
    function module_afterrender(){
        if(id_module!=""){
            form_bp_general.getForm().load({
                url: BASE_URL + 'website/c_hub_kami_inbox/grid',
                params:{
                    id_module:id_module
                },
                success: function(form, action){

                },
                failure: function(form, action){
                Ext.MessageBox.alert('Failure', 'Fail to get data');
                },
                waitMsg:'Loading..'
            });
        }
    }
        
	var win = new Ext.Window({
			title: 'Email',
			modal: true,
			items: [form_bp_general]
		}).show();
    
    function simpan_grid(namaForm) {
		var form_nya = Ext.getCmp(namaForm);
		//var waitmsg = Ext.MessageBox.wait('Proses mengirim pesan...', 'Info');
		form_nya.getForm().submit({
			url: BASE_URL + 'website/c_hub_kami_inbox/save',
			method: 'POST',
                        params:{
                        },
			success: function() {
				//waitmsg.hide();
				Ext.MessageBox.alert("Informasi", "Kirim Email Berhasil");
				ds.load();
				form_bp_general.getForm().reset();
				Ext.getCmp('form_bp_general').enable();
				Ext.getCmp('btn_kirim').enable();
				win.close();
			},
			failure: function() {
				Ext.MessageBox.alert("Informasi", "Kirim Email Gagal");
			},
			waitMsg:'Loading..'
		});
	}
	
	function ubah_grid(namaForm) {
        var msgplus='';
		var form_nya = Ext.getCmp(namaForm);
		Ext.MessageBox.show({
			title: "Konfirmasi",
            width: 250,
			msg: msgplus +"<center> Anda Yakin Mengirim Email Ini?</center>",
			buttons: Ext.MessageBox.YESNO,
			fn: function(btn) {
				if (btn == 'yes') {
					form_nya.getForm().submit({
						url: BASE_URL + 'website/c_hub_kami_inbox/sendmail',
                                                params:{

                                                },
						method: 'POST',
						success: function() {
							form_bp_general.getForm().load({waitMsg:'Loading'});
							Ext.MessageBox.alert("Informasi", "Kirim Email Berhasil");
							ds.load();
							form_bp_general.getForm().reset();
							Ext.getCmp('form_bp_general').enable();
							Ext.getCmp('btn_kirim').enable();
							Ext.getCmp('idbtn_batal').enable();
							win.close();
						},
						failure: function() {
							Ext.MessageBox.alert("Informasi", "Kirim Email Gagal");
							Ext.getCmp('idbtn_batal').enable();
						},
						waitMsg:'Loading..'
					});
				}
			}
		});
	}
}