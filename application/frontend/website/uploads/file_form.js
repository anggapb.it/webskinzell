function file_form(id,ds){
    var ds_cb_album = new Ext.data.JsonStore({
        autoLoad: true,
        proxy: new Ext.data.HttpProxy({
            url: site_url+'admin/uploads/files/get_album',
            method: 'POST'
        }),
        totalProperty: 'results',
        root: 'data',
        fields: [
            {name: 'album', mapping: 'album'}
        ]
    });
    
    var form_file = new Ext.FormPanel({
        fileUpload: false,
        width: 600,
        frame: false,
        autoHeight: true,
        border:false,
        bodyStyle: 'padding: 10px 10px 0 10px;',
        monitorValid :true,
        labelWidth: 50,
        defaults: {
            anchor: '95%',
            allowBlank: false,
            msgTarget: 'side'
        },reader: new Ext.data.JsonReader ({
            root: 'data',
            totalProperty: 'results',
            id: 'id',
            fields: [
                {name:'id',mapping:'id'},
                {name:'nama',mapping:'nama'},
                {name:'information',mapping:'information'},
                {name:'album',mapping:'album'},
                {name:'url',mapping:'url'},
                {name:'url_link',mapping:'url_link'},
                {name:'active',mapping:'active'}
            ]
        }),
        items: [
            {
                layout:'column',
                border:false,
                items:[
                    {
                        columnWidth:0.5,
                        layout: 'form',
                        border:false,
                        items: [
                            {xtype: 'textfield',fieldLabel: 'ID',id:'id',name:'id',anchor:'80%',
                                allowBlank:true,hidden:true},
                            {xtype: 'textfield',fieldLabel: 'Name',id:'nama',name:'nama',anchor:'80%',
                                allowBlank:false}
                        ]
                    },
                    {
                        columnWidth:0.5,
                        layout: 'form',
                        border:false,
                        items: [
                            {
                                xtype: 'combo', name: 'album', fieldLabel: 'Album',
                                store: ds_cb_album, mode: 'local', triggerAction: 'all',
                                editable: true, valueField: 'album', displayField: 'album',
                                forceSelection: false, submitValue: true,anchor:'80%'
                            },
                            {xtype: 'textfield',fieldLabel: 'Url Link',id:'url_link',name:'url_link',
                                anchor:'100%'}
                        ]
                    }
                ]
            },
        {xtype: 'htmleditor',fieldLabel: 'Info',id:'information',name:'information',anchor:'100%'},
        {xtype: 'compositefield',border:false,fieldLabel: 'Active',
            items: [
                {xtype:'checkbox',name: 'active',id: 'active',hidden:false}
            ]
        }
        ],
        buttons: [{
            text: 'Save',
            handler: function(){
                if(form_file.getForm().isValid()){
                    form_file.getForm().submit({
                        url: base_url+'admin/uploads/files/save',
                        waitMsg: 'Sedang mengunggah file...',
                        params:{
                            created_by:USERNAME
                        },
                        success: function(form_file, o){
                            Ext.MessageBox.alert('', 'Data berhasil disimpan..');
                            ds.load();
                            f_file.close();
                        }
                    });
                }
            }
        }],listeners:{
            afterrender: form_file_afterrender
        }
    });

    function form_file_afterrender(){
        form_file.getForm().load({
            url: base_url+'admin/uploads/files/getRow.do',
            params:{id: id},
            success: function(form, action){

            },
            failure: function(form, action){
            Ext.MessageBox.alert('Failure', 'Fail to get data');
            },
            waitMsg:'Loading..'
        });
    }
    function save_data(){
        form_file.getForm().submit({
            url: site_url+'admin/files/file/save',
            method: 'POST',
            waitMsg:"Uploading...",
            params:{
                username:USERNAME
            },
            success: function(form, action){
                f_file.close();
                ds.load();
                Ext.MessageBox.alert('Selesai', 'Data berhasil disimpan..');
            },
            failure: function(f, a){
                Ext.MessageBox.alert('Selesai', 'Data gagal disimpan..');
            }
        });
    }
    var f_file = new Ext.Window({
        title: "Upload File",
        layout: "fit",
        modal: true,
        resizable: false,
        maximizable: false,
        closeAction: "close",
        hideMode: "offsets",
        constrainHeader: true,
        items:[
            form_file
        ]
    }).show();
}