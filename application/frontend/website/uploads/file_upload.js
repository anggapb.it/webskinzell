function file_upload(status,ds){
    var ds_cb_album = new Ext.data.JsonStore({
        autoLoad: true,
        proxy: new Ext.data.HttpProxy({
            url: site_url+'admin/uploads/files/get_album',
            method: 'POST'
        }),
        totalProperty: 'results',
        root: 'data',
        fields: [
            {name: 'album', mapping: 'album'}
        ]
    });
    
    var form_file = new Ext.FormPanel({
        fileUpload: true,
        width: 600,
        frame: false,
        autoHeight: true,
        border:false,
        bodyStyle: 'padding: 10px 10px 0 10px;',
        monitorValid :true,
        labelWidth: 50,
        defaults: {
            anchor: '95%',
            allowBlank: false,
            msgTarget: 'side'
        },
        items: [
            {
                layout:'column',
                border:false,
                items:[
                    {
                        columnWidth:0.5,
                        layout: 'form',
                        border:false,
                        items: [
                            {xtype: 'textfield',fieldLabel: 'Name',id:'name',name:'name',anchor:'80%',
                                allowBlank:false},
                            {
                                xtype: 'fileuploadfield',id: 'file-path',emptyText: 'Select an file',
                                fieldLabel: 'file',name: 'file-path',buttonText: '',anchor:'90%',
                                buttonCfg: {
                                    iconCls: 'silk-image'
                                },allowBlank:false
                            }
                        ]
                    },
                    {
                        columnWidth:0.5,
                        layout: 'form',
                        border:false,
                        items: [
                            {
                                xtype: 'combo', name: 'album', fieldLabel: 'Album',
                                store: ds_cb_album, mode: 'local', triggerAction: 'all',
                                editable: true, valueField: 'album', displayField: 'album',
                                forceSelection: false, submitValue: true,anchor:'80%'
                            },
                            {xtype: 'textfield',fieldLabel: 'Url Link',id:'url_link',name:'url_link',
                                anchor:'100%'}
                        ]
                    }
                ]
            },
        {xtype: 'htmleditor',fieldLabel: 'Info',id:'information',name:'information',anchor:'100%'},
        {xtype: 'compositefield',border:false,fieldLabel: 'Active',
            items: [
                {xtype:'checkbox',name: 'active',id: 'active',hidden:false}
            ]
        }
        ],
        buttons: [{
            text: 'Save',
            handler: function(){
                if(form_file.getForm().isValid()){
	                form_file.getForm().submit({
	                    url: base_url+'admin/uploads/files/upload',
	                    waitMsg: 'Sedang mengunggah file...',
	                    success: function(form_file, o){
	                        Ext.MessageBox.alert('Confirm', o.result.message);
                                ds.load();
                                f_file.close();
	                    }
//                            ,failure:function(form_file,o){
//                                Ext.MessageBox.alert('Upload File Berhasil', o.result.message);
//                            }
	                });
                }
            }
        },{
            text: 'Reset',
            handler: function(){
                form_file.getForm().reset();
            }
        }]
    });

    function get_file(){
        form_file.getForm().load({
            url: base_url+'admin/files/file/get_file',
            params:{status: status},
            success: function(form, action){

            },
            failure: function(form, action){
            Ext.MessageBox.alert('Failure', 'Fail to get data');
            },
            waitMsg:'Loading..'
        });
    }
    function save_data(){
        form_file.getForm().submit({
            url: site_url+'admin/files/file/save',
            method: 'POST',
            waitMsg:"Uploading...",
            params:{
                username:USERNAME
            },
            success: function(form, action){
                f_file.close();
                var res = action.result.data.resval;
                switch(res){
                    case '0' : Ext.MessageBox.alert('Save','Saving data failed'); break;
                    case '1' : Ext.MessageBox.alert('Save','Saving data success');
                }
                Ext.MessageBox.hide();
                Ext.MessageBox.alert('Selesai', 'Data sudah tersimpan..');
                ds.load();
                f_file.close();
            },
            failure: function(f, a){
                Ext.MessageBox.hide();
                Ext.MessageBox.alert('Selesai', 'Data sudah gagal tersimpan..');
            }
        });
    }
    var f_file = new Ext.Window({
        title: "Upload File",
        layout: "fit",
        modal: true,
        resizable: false,
        maximizable: false,
        closeAction: "close",
        hideMode: "offsets",
        constrainHeader: true,
        items:[
            form_file
        ]
    }).show();
}