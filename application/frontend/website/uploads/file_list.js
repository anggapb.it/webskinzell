function file_list(){
    var ds_file_list = new Ext.data.JsonStore({
        proxy: new Ext.data.HttpProxy({
            url: site_url+'admin/uploads/files/grid.do',
            method: 'POST'
        }),
        baseParams:{start:0, limit:50,status:1},
        root: 'data',
        totalProperty: 'results',
        id:'fileid',
        remoteSort: false,
        autoLoad:true,
        fields: [
            {name: 'id'   , mapping: 'id'},
            {name: 'nama'   , mapping: 'name'},
            {name: 'album'   , mapping: 'album'},
            {name: 'url'   , mapping: 'url'},
            {name: 'file_type'   , mapping: 'file_type'},
            {name: 'created_by'   , mapping: 'created_by'},
            {name: 'created_at'   , mapping: 'created_at'},
            {name: 'active'    , mapping: 'active'}
        ]
    });
    var cm_file_list = new Ext.grid.ColumnModel([
        {id:"id",header: 'ID', dataIndex: 'id',width:100,hidden:true},
        {header: 'Name', dataIndex: 'nama',width:100,hidden:false},
        {header: 'Album', dataIndex: 'album',width:100,hidden:false},
        {header: 'URL', dataIndex: 'url',width:100,hidden:false},
        {header: 'File Type', dataIndex: 'file_type',width:100,hidden:false},
        {header: 'Created By', dataIndex: 'created_by',width:100,hidden:false},
        {header: 'Created At', dataIndex: 'created_at',width:100,hidden:false},
        {header: 'Active', dataIndex: 'active',width:50,hidden:false, 
                renderer : clStatus, 
                sortable : true}
    ]);
    var sm_file_list = new Ext.grid.RowSelectionModel({singleSelect: true});
    var vw_file_list = new Ext.grid.GridView({
        emptyText: '< No data to display >', forceFit: true
    });
    var paging_file_list = new Ext.PagingToolbar({
        pageSize: 50,
        store: ds_file_list,
        displayInfo: true,
        displayMsg: 'Displaying file list {0} - {1} of {2}',
        emptyMsg: 'No data to display'
    });
    var grid_file_list = new Ext.grid.EditorGridPanel({
            title:'file List',
            id: 'grid_file_list',
            ds: ds_file_list, border: false,
            cm: cm_file_list,
            sm: sm_file_list,
            view: vw_file_list,
            height: 500,
            autoSizeColumns:true,
            enableColumnResize: true,
            enableColumnHide: false,
            enableColumnMove: false,
            enableHdMenu: false,
            columnLines: true,
            loadMask: true,
            buttonAlign: 'left',
            defaults : {
                anchor : '-10'
            },
            tbar:[{
                text: 'Add', id: 'btn_file_add', iconCls: 'silk-add',
                handler: btn_file_add

            },'-',{
                text: 'Edit', id: 'btn_file_edit', iconCls: 'silk-cog',
                handler: btn_file_edit
            },'-',{
                text: 'Delete', id: 'btn_file_delete', iconCls: 'silk-delete',
                handler: btn_file_delete
            },'->'],
            bbar:[paging_file_list]
    });
    
    function btn_file_add(){
        file_upload("",ds_file_list);
    }
    function btn_file_edit(){
        if(sm_file_list.getCount() > 0){
            var id = sm_file_list.getSelected().data['id'];
            file_form(id,ds_file_list);
        }
    }
    function btn_file_delete(){
        if(sm_file_list.getCount() > 0){
            var id = sm_file_list.getSelected().data['id'];
            function showResult(btn){
                if(btn=="yes"){
                    Ext.Ajax.request({
                       url: base_url+'admin/uploads/files/delete',
                       success: function(){
                           ds_file_list.load();
                       },
                       failure: function(){},
                       params: { id: id}
                    });
                }else{
                    ds_file_list.load();
                }
            }
            Ext.MessageBox.confirm('Confirm', "Data akan di hapus ?..", showResult);
        }
    }
    
    get_content(grid_file_list);
}
file_list();