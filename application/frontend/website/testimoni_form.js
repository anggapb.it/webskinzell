function testimoni_form(id_module,ds) {
	RH.startTimer('jampublish');
	//RH.startTimer('jamagenda');
		
		var ds_stpublish = store_stpublish();
    	var form_bp_general = new Ext.form.FormPanel({
                border: false,
		id: 'form_bp_general',
		labelAlign: 'left',
		buttonAlign: 'left',
		bodyStyle: 'padding:10px 3px 3px 5px', // atas, kanan, bawah, kiri
		monitorValid: true,
                height: 350,
		width: 480,
              	layout: 'form',
                autoScroll: true,
                reader: new Ext.data.JsonReader ({
                    root: 'data',
                    totalProperty: 'results',
                    id: 'id',
                    fields: [
                        {
						name: "idtestimoni",
						mapping: "idtestimoni"
					},{
						name: "dari",
						mapping: "dari"
					},{
						name: "deskripsi",
						mapping: "deskripsi"
					},{
						name: "idstpublish",
						mapping: "idstpublish"
					},{
						name: "nmstpublish",
						mapping: "nmstpublish"
					},{
						name: "tglpublish",
						mapping: "tglpublish"
					},{
						name: "jampublish",
						mapping: "jampublish"
					}]
                }),
                items: [ {   
					xtype:'fieldset',
					title: '',
					width:460,
					items :[
                            {
                                    xtype: 'textfield', //labelStyle: 'width:140px',
                                    fieldLabel: 'id',
                                    id: 'idtestimoni',
                                    name: 'idtestimoni',
									hidden: true,
									readOnly: true
                            },
							{
                                    xtype: 'textfield', //labelStyle: 'width:140px',
                                    fieldLabel: 'Dari ',
                                    id: 'dari',
                                    name: 'dari'			
                            },
                            {
                                    xtype: 'textarea', //labelStyle: 'width:140px',
                                    fieldLabel: 'Deskripsi ',
                                    id: 'deskripsi',
                                    name: 'deskripsi',
                                    width: 300			
                            },
                            {
                                    xtype: 'combo',
                                    width: 125,
                                    height: 50,
                                    allowBlank: false,
                                    store: ds_stpublish,
                                    fieldLabel: 'Status Publish',
                                    id: 'idstpublish',
                                    triggerAction: 'all',
                                    editable: false,
                                    valueField: 'idstpublish',
                                    displayField: 'nmstpublish',
                                    forceSelection: true,
                                    submitValue: true,
                                    hiddenName: 'h_status',
                                    listeners: {},

                                    typeAhead: true,
                                    mode: 'local',
                                    emptyText:'Pilih...',
                                    selectOnFocus:true

                            },
                            {
                                    xtype: 'datefield',
                                    fieldLabel: 'Tgl.Publish',
                                    name: 'tglpublish',
                                    id: 'tglpublish'
                            },{
                                    xtype: 'textfield',
                                    fieldLabel: 'Jam Publish',
                                    name: 'jampublish',
                                    id: 'jampublish'
                            }]}
                    ],

                buttons: [{
                    id:'btn_simpan',
                    text: 'Simpan',
                    iconCls: 'silk-save',
                    handler: function() {
                        if(id_module!=''){
                            ubah_grid('form_bp_general');
                        }else{
                            simpan_grid('form_bp_general');
                        }
                            
                    }
                }, 
                    {
                    text: 'Kembali',
                    handler: function() {
                        win.close();
                    }
                }
                    ],listeners:{
            afterrender: module_afterrender
        }
    });
    
    function module_afterrender(){
	Ext.getCmp("dari").setReadOnly(false);
        if(id_module!=""){
            form_bp_general.getForm().load({
                url: BASE_URL + 'website/c_testimoni/grid',
                params:{
                    id_module:id_module
                },
                success: function(form, action){
					Ext.getCmp("dari").setReadOnly(true);
                    //isi_foto_ori( Ext.getCmp("gambar").getValue());
					
                },
                failure: function(form, action){
                Ext.MessageBox.alert('Failure', 'Fail to get data');
                },
                waitMsg:'Loading..'
            });
			
        }
		Ext.getCmp('tglpublish').setValue(SYS_DATE);
		//Ext.getCmp('tglagenda').setValue(SYS_DATE);
    }
        
    var win = new Ext.Window({
			title: 'Testimoni',
			layout: "fit",
                        modal: true,
                        resizable: true,
                       	maximizable:true,
			items: [form_bp_general]
		}).show();
    
    function simpan_grid(namaForm) {
		var form_nya = Ext.getCmp(namaForm);
		form_nya.getForm().submit({
			url: BASE_URL + 'website/c_testimoni/save',
			method: 'POST',
                        params:{
							userid:USERID
                        },
			success: function() {
				Ext.MessageBox.alert("Informasi", "Simpan Data Berhasil");
				ds.load();
				form_bp_general.getForm().reset();
				Ext.getCmp('form_bp_general').enable();
				Ext.getCmp('btn_simpan').enable();

                                win.close();
			},
			failure: function() {
				Ext.MessageBox.alert("Informasi", "Simpan Data Gagal");
			}
		});
	}
	
	function ubah_grid(namaForm) {

        var msgplus='';

		var form_nya = Ext.getCmp(namaForm);
		Ext.MessageBox.show({
			title: "Konfirmasi",
                        width: 350,
			msg: msgplus +"<center> Anda Yakin Untuk Mengubah Data ini?</center>",
			buttons: Ext.MessageBox.YESNO,
			fn: function(btn) {
				if (btn == 'yes') {
					form_nya.getForm().submit({
						url: BASE_URL + 'website/c_testimoni/update',
                                                params:{
                                                    userid:USERID
                                                },
						method: 'POST',
						success: function() {
							Ext.MessageBox.alert("Informasi", "Ubah Data Berhasil");
							ds.load();
							form_bp_general.getForm().reset();
							Ext.getCmp('form_bp_general').enable();
							Ext.getCmp('btn_simpan').enable();
                            win.close();
						},
						failure: function() {
							Ext.MessageBox.alert("Informasi", "Ubah Data Gagal");
						}
					});
				}
			}
		});
	}
 }