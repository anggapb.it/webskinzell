function daerah_form(id_module,ds) {
	               
	var form_bp_general = new Ext.form.FormPanel({
                border: false,
		id: 'form_bp_general',
		labelAlign: 'left',
		buttonAlign: 'right',
		bodyStyle: 'padding:10px 3px 3px 5px', // atas, kanan, bawah, kiri
		monitorValid: true,
                height: 240,
		width: 380,
              	layout: 'form',
                reader: new Ext.data.JsonReader ({
                    root: 'data',
                    totalProperty: 'results',
                    id: 'id',
                    fields: [
                        {
                                name: "iddaerah", mapping: "iddaerah"
                        },{
                                name: "nmdaerah", mapping: "nmdaerah"
                        },{
                                name: "kelurahan",mapping: "kelurahan"
                        },{
                                name: "kecamatan",mapping: "kecamatan"
                        },{
                                name: "kotkab",mapping: "kotkab"
                        },{
                                name: "provinsi",mapping: "provinsi"
                        }
                    ]
                }),
                items: [{
								xtype:'fieldset',
								title: '',
								items :[ 
                            {
                                    xtype: 'textfield',
                                    fieldLabel: 'Nama Daerah',
                                    labelStyle: 'width:100px',
                                    width:240,
                                    name: 'nmdaerah'			
                            },{
                                    xtype: 'textfield',
                                    fieldLabel: 'Kelurahan',
                                    labelStyle: 'width:100px',
                                    width:240,
                                    name: 'kelurahan'			
                            },{
                                    xtype: 'textfield',
                                    fieldLabel: 'Kecamatan',
                                    labelStyle: 'width:100px',
                                    width:240,
                                    name: 'kecamatan'			
                            },{
                                    xtype: 'textfield',
                                    fieldLabel: 'Kota / Kab',
                                    labelStyle: 'width:100px',
                                    width:240,
                                    name: 'kotkab'			
                            },{
                                    xtype: 'textfield',
                                    fieldLabel: 'Provinsi',
                                    labelStyle: 'width:100px',
                                    width:240,
                                    name: 'provinsi'			
                            }]
							},
                            {
                                    xtype: 'textfield',
                                    fieldLabel: '',
									hidden:true,
                                    labelStyle: 'width:100px',
                                    width:240,
                                    name: 'iddaerah'			
                            }
                    ],

                buttons: [{
                    id:'btn_simpan',
                    text: 'Simpan',
                    handler: function() {
                        if(id_module!=''){
                            ubah_grid('form_bp_general');
                        }else{
                            simpan_grid('form_bp_general');
                        }
                            
                    }
                }, 
                    {
                    text: 'Kembali',
                    handler: function() {
                        win.close();
                    }
                }
                    ],listeners:{
            afterrender: module_afterrender
        }
    });
    function module_afterrender(){
        if(id_module!=""){
            form_bp_general.getForm().load({
                url: BASE_URL + 'website/c_daerah/grid',
                params:{
                    id_module:id_module
                },
                success: function(form, action){

                },
                failure: function(form, action){
                Ext.MessageBox.alert('Failure', 'Fail to get data');
                },
                waitMsg:'Loading..'
            });
        }
    }
        
	var win = new Ext.Window({
			title: 'Daerah',
			modal: true,
			items: [form_bp_general]
		}).show();
    
    function simpan_grid(namaForm) {
		var form_nya = Ext.getCmp(namaForm);
		form_nya.getForm().submit({
			url: BASE_URL + 'website/c_daerah/save',
			method: 'POST',
                        params:{
//                            matas:Ext.getCmp('menuatas').getValue(),
//                            mbawah:Ext.getCmp('menubawah').getValue(),
//                            mkanan:Ext.getCmp('menukanan').getValue(),
//                            mkiri:Ext.getCmp('menukiri').getValue()
                        },
			success: function() {
				Ext.MessageBox.alert("Informasi", "Simpan Data Berhasil");
				ds.load();
				form_bp_general.getForm().reset();
				Ext.getCmp('form_bp_general').enable();
				Ext.getCmp('btn_simpan').enable();
//				Ext.getCmp('btn_hapus').disable();
//				Ext.getCmp('btn_ubah').disable();
			},
			failure: function() {
				Ext.MessageBox.alert("Informasi", "Simpan Data Gagal");
			}
		});
	}
	
	function ubah_grid(namaForm) {
//           var msgplus;
//            if(kodex!=Ext.getCmp('kode').getValue()){
//               msgplus='<center>Semua field dapat berubah terkecuali field "KODE"!? </br> </center>';
//            }else{
               var msgplus='';
//            }
		var form_nya = Ext.getCmp(namaForm);
		Ext.MessageBox.show({
			title: "Konfirmasi",
                        width: 350,
			msg: msgplus +"<center> Anda Yakin Untuk Mengubah Data ini?</center>",
			buttons: Ext.MessageBox.YESNO,
			fn: function(btn) {
				if (btn == 'yes') {
					form_nya.getForm().submit({
						url: BASE_URL + 'website/c_daerah/update',
                                                params:{
//                                                    matas:Ext.getCmp('menuatas').getValue(),
//                                                    mbawah:Ext.getCmp('menubawah').getValue(),
//                                                    mkanan:Ext.getCmp('menukanan').getValue(),
//                                                    mkiri:Ext.getCmp('menukiri').getValue()
                                                },
						method: 'POST',
						success: function() {
							Ext.MessageBox.alert("Informasi", "Ubah Data Berhasil");
							ds.load();
							form_bp_general.getForm().reset();
							Ext.getCmp('form_bp_general').enable();
							Ext.getCmp('btn_simpan').enable();
							win.close();
						},
						failure: function() {
							Ext.MessageBox.alert("Informasi", "Ubah Data Gagal");
						}
					});
				}
			}
		});
	}
}