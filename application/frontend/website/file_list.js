function f_file(){
        var ds_grid = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'website/c_galeri/grid',
			method: 'POST'
		}),
		params: {
			start: 0,
			limit: 5
		},
                root: 'data',
		totalProperty: 'results',
		autoLoad: true,
		fields: [
		{
			name: "kdgaleri",
			mapping: "kdgaleri"
		},{
			name: "nmgaleriind",
			mapping: "nmgaleriind"
		},{
			name: "idklpgaleri",
			mapping: "idklpgaleri"
		},{
			name: "idjnsgaleri",
			mapping: "idjnsgaleri"
		},{
			name: "idstpublish",
			mapping: "idstpublish"
		},{
			name: "tglpublish",
			mapping: "tglpublish"
		},{
			name: "pengguna",
			mapping: "pengguna"
		},{
			name: "dilihat",
			mapping: "dilihat"
		}]
	});
        
        var cm = new Ext.grid.ColumnModel({
        // specify any defaults for each column
        defaults: {
            sortable: true // columns are not sortable by default           
        },
        columns: [{
			header: 'Kode',
			width: 50,
			dataIndex: 'kdgaleri',
			sortable: true
		},
		{
			header: 'Nama',
			width: 150,
			dataIndex: 'nmgaleriind',
			sortable: true
		},
		{
			header: 'Kelompok Galeri',
			width: 150,
			dataIndex: 'idklpgaleri',
			sortable: true
		},
		{
			header: 'Jenis Galeri',
			width: 150,
			dataIndex: 'idjnsgaleri',
			sortable: true
		},
		{
			header: 'Status Publish',
			width: 150,
			dataIndex: 'idstpublish',
			sortable: true
		},
		{
			header: 'TGL. Publish',
			width: 150,
			dataIndex: 'tglpublish',
			sortable: true
		},
		{
			header: 'Pengguna',
			width: 150,
			dataIndex: 'pengguna',
			sortable: true
		},
		{
			header: 'Dilihat',
			width: 150,
			dataIndex: 'dilihat',
			sortable: true
		}]
    });
        
	var vw = new Ext.grid.GridView({emptyText:'< Galeri Belum Dipilih  >'});

	var sm_nya = new Ext.grid.CheckboxSelectionModel({
		listeners: {
		//	rowselect: select_action,
		//	rowdeselect: deselect_action
		}
	});
	var cari_data = [new Ext.ux.grid.Search({
		iconCls: 'btn_search',
		minChars: 1,
		autoFocus: true,
		autoHeight: true,
		position: 'top',
		mode: 'remote',
		width: 200
	})];
        
        var paging = new Ext.PagingToolbar({
                    pageSize: 50,
                    store: ds_grid,
                    displayInfo: true,
                    displayMsg: 'Data Galeri Website Dari {0} - {1} of {2}',
                    emptyMsg: 'Menu Galeri Belum Dipilih.'
            });
        
	var grid_nya = new Ext.grid.EditorGridPanel({
		store: ds_grid,
		frame: true,
		//width: 1140,
		height: 595,
		plugins: cari_data,
		id: 'grid_det_product',
               // autoWidth:true,
//                autoSizeColumns:true,
//                enableColumnResize: true,
//                enableColumnHide: false,
//                enableColumnMove: false,
//                enableHdMenu: false,
//                columnLines: true,
//                loadMask: true,
                buttonAlign: 'left',
                defaults : {
                    anchor : '-10'
                },
               forceFit: true,
		tbar: [{
                            text: 'Add', id: 'btn_add', iconCls: 'silk-add',handler: btn_menu_add

                        },'-',{
                            text: 'Edit', id: 'btn_edit', iconCls: 'silk-edit',
                            handler: function(){
                                if(sm_nya.getCount() > 0){
                                    var module_id = sm_nya.getSelected().data['kdgaleri'];
                                   //form_gallery(gallery_id);
                                    //alert(module_id);
                                    f_galeri_form(module_id,ds_grid);
                                }
                            }
                        },'-',{
                            text: 'Delete', id: 'btn_delete', iconCls: 'silk-delete',
                            handler: function(){
                                if(sm_nya.getCount() > 0){
                                    var delete_id = sm_nya.getSelected().data['kdgaleri'];
                                    Ext.MessageBox.show({
                                        title: "Konfirmasi",
                                        msg: "Anda Yakin Untuk menghapus Data ini?",
                                        buttons: Ext.MessageBox.YESNO,
                                        fn: function(btn) {
                                                if (btn == 'yes') {
                                                       Ext.Ajax.request({
                                                          url: BASE_URL + 'website/c_galeri/delete',
                                                            method: 'POST',
                                                            success: function() {
                                                                    Ext.MessageBox.alert("Informasi", "Hapus Data Berhasil");
                                                                    ds_grid.load();
                                                            },
                                                            failure: function(result){
                                                                    Ext.MessageBox.alert("Informasi", "Hapus Data Gagal");
                                                            },
                                                           params: { hapus_id: delete_id }
                                                        });
                                                }
                                            }
                                    });
                                }
                            }
                        },'-','->'
                       ],
		sm:sm_nya,
                vw:vw,
		autoScroll: true,
		cm:cm,
		bbar: paging,
                //autoExpandColumn: 'common',
                clicksToEdit: 1,
		listeners: {
			rowdblclick: function rowdblClick(grid, rowIdx) {
				var rec = ds_grid.getAt(rowIdx);
				//alert(rec.data["kdmenu"] + ', ' + Ext.getCmp('combox').getValue());
                             
				
			}
		}
	});
	                    
	var form_bp_general = new Ext.form.FormPanel({
		id: 'form_bp_general_id',
		region: 'center',
		autoScroll: true,
		buttonAlign: 'left',
		bodyStyle: 'padding: 5px',
		border: false,
		disabled: true,
		waitMsg: 'Waiting...',
		maskDisabled: false,
		title: 'Upload File',
		monitorValid: true,
		items: [{
			layout: 'form',
			border: false,
			items: [grid_nya]
		}]
	});
        
	function btn_menu_add(){
            f_galeri_form('',ds_grid);
        }
	get_content(form_bp_general);
}