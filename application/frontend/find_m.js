function find_x(vtitle, pilih, i_mx) {
	if (pilih == 1) {
		var ds_1 = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url: BASE_URL + 'get_images/get_photox ',
				method: 'POST'
			}),
			autoLoad: true,baseParams: {
				start: 0,
				limit: 50
			},
			root: 'images',
			totalProperty: 'results',
                        fields: ['name', 'url',
			{
				name: 'size',
				type: 'float'
			},
			{
				name: 'lastmod',
				type: 'date',
				dateFormat: 'timestamp'
			}, 'thumb_url', 'nama_produk', 'harga', 'deskripsi']
		});
		var cm_1 = new Ext.grid.ColumnModel([{
			header: 'Nama',
			dataIndex: 'name',
			width: 300
		}, ]);
		var sm_1 = new Ext.grid.RowSelectionModel({
			singleSelect: true
		});
		var vw_1 = new Ext.grid.GridView({
			emptyText: '< Belum ada Data >'
		});
		var paging_1 = new Ext.PagingToolbar({
			pageSize: 50,
			store: ds_1,
			displayInfo: true,
			displayMsg: 'Data Gambar Dari {0} - {1} of {2}',
			emptyMsg: 'No data to display'
		});
		var cari_1 = [new Ext.ux.grid.Search({
			iconCls: 'btn_search',
			minChars: 1,
			autoFocus: true,
			position: 'top',
			mode: 'local',
			width: 200
		})];
		var grid_find_1 = new Ext.grid.GridPanel({
			ds: ds_1,
			cm: cm_1,
			sm: sm_1,
			view: vw_1,
			height: 350,
			width: 400,
			plugins: cari_1,
			autoSizeColumns: true,
			enableColumnResize: true,
			enableColumnHide: false,
			enableColumnMove: false,
			enableHdaccess: false,
			columnLines: true,
			loadMask: true,
			buttonAlign: 'left',
			layout: 'anchor',
			anchorSize: {
				width: 400,
				height: 400
			},
			tbar: [],
			bbar: paging_1,
			listeners: {
				rowdblclick: klik2kali
			}
		});
		var win_find_1 = new Ext.Window({
			title: vtitle,
			modal: true,
			items: [grid_find_1]
		}).show();
	} else if (pilih == 2) {
            var datastorex, dataindex1, dataindex2;
		if(vtitle!="Data Parent Web"){
                    var ds_2 = new Ext.data.JsonStore({
                            proxy: new Ext.data.HttpProxy({
                                    url: BASE_URL + 'c_mastah/gridS_menu',
                                    method: 'POST'
                            }),
                            autoLoad: true,
                            root: 'data',
                            totalProperty: 'results',
                            fields: [{
                                    name: 'idmenu',
                                    mapping: 'idmenu'
                            },
                            {
                                    name: 'nmmenu',
                                    mapping: 'nmmenu'
                            }]
                    });
                    datastorex = ds_2;
                    dataindex1 = 'idmenu';
                    dataindex2 = 'nmmenu';
                }else{
                    var ds_21 = new Ext.data.JsonStore({
                            proxy: new Ext.data.HttpProxy({
                            url: BASE_URL + 'c_mastah/gridS_menuweb',
                            method: 'POST'
                            }),
                            params: {
                                    start: 0,
                                    limit: 5
                            },
                            root: 'data',
                            totalProperty: 'results',
                            autoLoad: true,
                            fields: [
                            {
                                    name: "idmenuweb",
                                    mapping: "idmenuweb"
                            },{
                                    name: "nmmenuind",
                                    mapping: "nmmenuind"
                            }]
		   });
                   datastorex = ds_21;
                   dataindex1 = 'idmenuweb';
                   dataindex2 = 'nmmenuind';
                } 
                
                
		var cm_2 = new Ext.grid.ColumnModel([
                    {
                        hidden:true,
			header: 'ID Menu',
			dataIndex: dataindex1,
			width: 75
                    },{
			header: 'Nama Menu',
			dataIndex: dataindex2,
			width: 300
                    }
                ]);
		var sm_2 = new Ext.grid.RowSelectionModel({
			singleSelect: true
		});
		var vw_2 = new Ext.grid.GridView({
			emptyText: '< Belum ada Data >'
		});
		var paging_2 = new Ext.PagingToolbar({
			pageSize: 50,
			store: ds_2,
			displayInfo: true,
			displayMsg: 'Data Menu Dari {0} - {1} of {2}',
			emptyMsg: 'No data to display'
		});
		var cari_2 = [new Ext.ux.grid.Search({
			iconCls: 'btn_search',
			minChars: 1,
			autoFocus: true,
			position: 'top',
			mode: 'local',
			width: 200
		})];
		var grid_find_2 = new Ext.grid.GridPanel({
			ds: datastorex,
			cm: cm_2,
			sm: sm_2,
			view: vw_2,
			height: 350,
			width: 400,
			plugins: cari_2,
			autoSizeColumns: true,
			enableColumnResize: true,
			enableColumnHide: false,
			enableColumnMove: false,
			enableHdaccess: false,
			columnLines: true,
			loadMask: true,
			buttonAlign: 'left',
			layout: 'anchor',
			anchorSize: {
				width: 400,
				height: 400
			},
			tbar: [],
			bbar: paging_2,
			listeners: {
				rowdblclick: klik2kali
			}
		});
		var win_find_2 = new Ext.Window({
			title: vtitle,
			modal: true,
			items: [grid_find_2]
		}).show();
	} else if (pilih == 3) {
		var ds_3 = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
                                url: BASE_URL + 'c_utility/g_PG',
				method: 'POST'
			}),
			autoLoad: true,
			root: 'data',
			totalProperty: 'results',
                        fields: [{
                                name: 'userid',
                                mapping: 'userid'
                        },
                        {
                                name: 'nmlengkap',
                                mapping: 'nmlengkap'
                        },
                        {
                                name: 'idstatus',
                                mapping: 'idstatus'
                        }]
		});
		var cm_3 = new Ext.grid.ColumnModel([
                    {
                      //  hidden:true,
			header: 'User ID',
			dataIndex: 'userid',
			width: 115
                    },{
			header: 'Nama Lengkap',
			dataIndex: 'nmlengkap',
			width: 200
                    },{
			header: 'Status',
			dataIndex: 'idstatus',
			width: 80
                    }
                ]);
		var sm_3 = new Ext.grid.RowSelectionModel({
			singleSelect: true
		});
		var vw_3 = new Ext.grid.GridView({
			emptyText: '< Belum ada Data >'
		});
		var paging_3 = new Ext.PagingToolbar({
			pageSize: 50,
			store: ds_3,
			displayInfo: true,
			displayMsg: 'Data Pengguna Dari {0} - {1} of {2}',
			emptyMsg: 'No data to display'
		});
		var cari_3 = [new Ext.ux.grid.Search({
			iconCls: 'btn_search',
			minChars: 1,
			autoFocus: true,
			position: 'top',
			mode: 'local',
			width: 200
		})];
		var grid_find_3 = new Ext.grid.GridPanel({
			ds: ds_3,
			cm: cm_3,
			sm: sm_3,
			view: vw_3,
			height: 350,
			width: 400,
			plugins: cari_3,
			autoSizeColumns: true,
			enableColumnResize: true,
			enableColumnHide: false,
			enableColumnMove: false,
			enableHdaccess: false,
			columnLines: true,
			loadMask: true,
			buttonAlign: 'left',
			layout: 'anchor',
			anchorSize: {
				width: 400,
				height: 400
			},
			tbar: [],
			bbar: paging_3,
			listeners: {
				rowdblclick: klik2kali
			}
		});
		var win_find_3 = new Ext.Window({
			title: vtitle,
			modal: true,
			items: [grid_find_3]
		}).show();
	}else if (pilih == 4) {
		var ds_4 = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
                                url: BASE_URL + 'c_mastah/grid_kotkabasal',
				method: 'POST'
			}),
			autoLoad: true,
			root: 'data',
                        totalProperty: 'results',
		
			fields: [
                        {
                                name: 'kdkabtbpro',
                                mapping: 'kdkabtbpro'
                        },{
                                name: 'nmprotbpro',
                                mapping: 'nmprotbpro'
                        },
                        {
                                name: 'nmkabtbpro',
                                mapping: 'nmkabtbpro'
                        }]
		});
		var cm_4 = new Ext.grid.ColumnModel([
                    {   hidden:true,
			header: 'kdkabtbpro',
			dataIndex: 'kdkabtbpro',
			width: 200
                    },{
			header: 'Propinsi',
			dataIndex: 'nmprotbpro',
			width: 80
                    },{
			header: 'Kab/Kota',
			dataIndex: 'nmkabtbpro',
			width: 200
                    }
                ]);
		var sm_4 = new Ext.grid.RowSelectionModel({
			singleSelect: true
		});
		var vw_4 = new Ext.grid.GridView({
			emptyText: '< Belum ada Data >'
		});
		var paging_4 = new Ext.PagingToolbar({
			pageSize: 50,
			store: ds_4,
			displayInfo: true,
			displayMsg: 'Data Kab/Kota Dari {0} - {1} of {2}',
			emptyMsg: 'No data to display'
		});
		var cari_4 = [new Ext.ux.grid.Search({
			iconCls: 'btn_search',
			minChars: 1,
			autoFocus: true,
			position: 'top',
			mode: 'remote',
			width: 200
		})];
		var grid_find_4 = new Ext.grid.GridPanel({
			ds: ds_4,
			cm: cm_4,
			sm: sm_4,
			view: vw_4,
			height: 350,
			width: 400,
			plugins: cari_4,
			autoSizeColumns: true,
			enableColumnResize: true,
			enableColumnHide: false,
			enableColumnMove: false,
			enableHdaccess: false,
			columnLines: true,
			loadMask: true,
			buttonAlign: 'left',
			layout: 'anchor',
			anchorSize: {
				width: 400,
				height: 400
			},
			tbar: [],
			bbar: paging_4,
			listeners: {
				rowdblclick: klik2kali
			}
		});
		var win_find_4 = new Ext.Window({
			title: vtitle,
			modal: true,
			items: [grid_find_4]
		}).show();
	}else if (pilih == 5) {
		var ds_5 = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
                                url: BASE_URL + 'c_mastah/grid_ptasal',
				method: 'POST'
			}),
			autoLoad: true,
			root: 'data',
			totalProperty: 'results',
                        fields: [
                        {
                                name: 'kdptitbpti',
                                mapping: 'kdptitbpti'
                        },{
                                name: 'nmptitbpti',
                                mapping: 'nmptitbpti'
                        }]
		});
		var cm_5 = new Ext.grid.ColumnModel([
                    {
			header: 'Kode',
			dataIndex: 'kdptitbpti',
			width: 80
                    },{
			header: 'Nama',
			dataIndex: 'nmptitbpti',
			width: 200
                    }
                ]);
		var sm_5 = new Ext.grid.RowSelectionModel({
			singleSelect: true
		});
		var vw_5 = new Ext.grid.GridView({
			emptyText: '< Belum ada Data >'
		});
		var paging_5 = new Ext.PagingToolbar({
			pageSize: 50,
			store: ds_5,
			displayInfo: true,
			displayMsg: 'Data PT Dari {0} - {1} of {2}',
			emptyMsg: 'No data to display'
		});
		var cari_5 = [new Ext.ux.grid.Search({
			iconCls: 'btn_search',
			minChars: 1,
			autoFocus: true,
			position: 'top',
			mode: 'remote',
			width: 200
		})];
		var grid_find_5 = new Ext.grid.GridPanel({
			ds: ds_5,
			cm: cm_5,
			sm: sm_5,
			view: vw_5,
			height: 350,
			width: 400,
			plugins: cari_5,
			autoSizeColumns: true,
			enableColumnResize: true,
			enableColumnHide: false,
			enableColumnMove: false,
			enableHdaccess: false,
			columnLines: true,
			loadMask: true,
			buttonAlign: 'left',
			layout: 'anchor',
			anchorSize: {
				width: 400,
				height: 400
			},
			tbar: [],
			bbar: paging_5,
			listeners: {
				rowdblclick: klik2kali
			}
		});
		var win_find_5 = new Ext.Window({
			title: vtitle,
			modal: true,
			items: [grid_find_5]
		}).show();
	}else if (pilih == 6) {
            if(vtitle!="Data Program Studi Asal"){
		var ds_6 = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
                                url: BASE_URL + 'c_mastah/grid_prodi',
				method: 'POST'
			}),
			autoLoad: true,
			root: 'data',
			totalProperty: 'results',
                        fields: [
                        {
                                name: 'kdprodi',
                                mapping: 'kdprodi'
                        },{
                                name: 'nmprodi',
                                mapping: 'nmprodi'
                        }]
		});
           }else{
               var ds_6 = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
                                url: BASE_URL + 'c_mastah/grid_prodiasal',
				method: 'POST'
			}),
			autoLoad: true,
			root: 'data',
			totalProperty: 'results',
                        fields: [
                        {
                                name: 'kdprodi',
                                mapping: 'kdprodi'
                        },{
                                name: 'nmprodi',
                                mapping: 'nmprodi'
                        }]
		});
           }
		var cm_6 = new Ext.grid.ColumnModel([
                    {
			header: 'Kode Prodi',
			dataIndex: 'kdprodi',
			width: 80
                    },{
			header: 'Nama Prodi',
			dataIndex: 'nmprodi',
			width: 200
                    }
                ]);
		var sm_6 = new Ext.grid.RowSelectionModel({
			singleSelect: true
		});
		var vw_6 = new Ext.grid.GridView({
			emptyText: '< Belum ada Data >'
		});
		var paging_6 = new Ext.PagingToolbar({
			pageSize: 50,
			store: ds_6,
			displayInfo: true,
			displayMsg: 'Data Program Studi Dari {0} - {1} of {2}',
			emptyMsg: 'No data to display'
		});
		var cari_6 = [new Ext.ux.grid.Search({
			iconCls: 'btn_search',
			minChars: 1,
			autoFocus: true,
			position: 'top',
			mode: 'local',
			width: 200
		})];
		var grid_find_6 = new Ext.grid.GridPanel({
			ds: ds_6,
			cm: cm_6,
			sm: sm_6,
			view: vw_6,
			height: 350,
			width: 400,
			plugins: cari_6,
			autoSizeColumns: true,
			enableColumnResize: true,
			enableColumnHide: false,
			enableColumnMove: false,
			enableHdaccess: false,
			columnLines: true,
			loadMask: true,
			buttonAlign: 'left',
			layout: 'anchor',
			anchorSize: {
				width: 400,
				height: 400
			},
			tbar: [],
			bbar: paging_6,
			listeners: {
				rowdblclick: klik2kali
			}
		});
		var win_find_6 = new Ext.Window({
			title: vtitle,
			modal: true,
			items: [grid_find_6]
		}).show();
	}else if (pilih == 7) {
                 var ds_7 = new Ext.data.JsonStore({
                            proxy: new Ext.data.HttpProxy({
                                    url: BASE_URL + 'c_mastah/griddosen',
                                    method: 'POST'
                            }),
                            autoLoad: true,
                            root: 'data',
                            totalProperty: 'results',
                            fields: [{
                                    name: 'nidu',
                                    mapping: 'nidu'
                            },
                            {
                                    name: 'nmdosdgngelar',
                                    mapping: 'nmdosdgngelar'
                            }]
                    });
                                   
		var cm_7 = new Ext.grid.ColumnModel([
                    {
                        hidden:true,
			header: 'NIDN',
			dataIndex: 'nidu',
			width: 75
                    },{
			header: 'Nama Dosen',
			dataIndex: 'nmdosdgngelar',
			width: 300
                    }
                ]);
		var sm_7 = new Ext.grid.RowSelectionModel({
			singleSelect: true
		});
		var vw_7 = new Ext.grid.GridView({
			emptyText: '< Belum ada Data >'
		});
		var paging_7 = new Ext.PagingToolbar({
			pageSize: 50,
			store: ds_7,
			displayInfo: true,
			displayMsg: 'Data Dosen Dari {0} - {1} of {2}',
			emptyMsg: 'No data to display'
		});
		var cari_7 = [new Ext.ux.grid.Search({
			iconCls: 'btn_search',
			minChars: 1,
			autoFocus: true,
			position: 'top',
			mode: 'local',
			width: 200
		})];
		var grid_find_7 = new Ext.grid.GridPanel({
			ds: ds_7,
			cm: cm_7,
			sm: sm_7,
			view: vw_7,
			height: 350,
			width: 400,
			plugins: cari_7,
			autoSizeColumns: true,
			enableColumnResize: true,
			enableColumnHide: false,
			enableColumnMove: false,
			enableHdaccess: false,
			columnLines: true,
			loadMask: true,
			buttonAlign: 'left',
			layout: 'anchor',
			anchorSize: {
				width: 400,
				height: 400
			},
			tbar: [],
			bbar: paging_7,
			listeners: {
				rowdblclick: klik2kali
			}
		});
		var win_find_7 = new Ext.Window({
			title: vtitle,
			modal: true,
			items: [grid_find_7]
		}).show();
	}else if (pilih == 8) {
                 var ds_8 = new Ext.data.JsonStore({
                            proxy: new Ext.data.HttpProxy({
                                    url: BASE_URL + 'c_mastah/gridS_halaman',
                                    method: 'POST'
                            }),
                            autoLoad: true,
                            root: 'data',
                            totalProperty: 'results',
                            fields: [{
                                    name: 'idhalaman',
                                    mapping: 'idhalaman'
                            },
                            {
                                    name: 'judulind',
                                    mapping: 'judulind'
                            }]
                    });
                                   
		var cm_8 = new Ext.grid.ColumnModel([
                    {
                        hidden:true,
			header: 'No',
			dataIndex: 'idhalaman',
			width: 75
                    },{
			header: 'Judul',
			dataIndex: 'judulind',
			width: 300
                    }
                ]);
		var sm_8 = new Ext.grid.RowSelectionModel({
			singleSelect: true
		});
		var vw_8 = new Ext.grid.GridView({
			emptyText: '< Belum ada Data >'
		});
		var paging_8 = new Ext.PagingToolbar({
			pageSize: 50,
			store: ds_8,
			displayInfo: true,
			displayMsg: 'Data Halaman Dari {0} - {1} of {2}',
			emptyMsg: 'No data to display'
		});
		var cari_8 = [new Ext.ux.grid.Search({
			iconCls: 'btn_search',
			minChars: 1,
			autoFocus: true,
			position: 'top',
			mode: 'local',
			width: 200
		})];
		var grid_find_8 = new Ext.grid.GridPanel({
			ds: ds_8,
			cm: cm_8,
			sm: sm_8,
			view: vw_8,
			height: 350,
			width: 400,
			plugins: cari_8,
			autoSizeColumns: true,
			enableColumnResize: true,
			enableColumnHide: false,
			enableColumnMove: false,
			enableHdaccess: false,
			columnLines: true,
			loadMask: true,
			buttonAlign: 'left',
			layout: 'anchor',
			anchorSize: {
				width: 400,
				height: 400
			},
			tbar: [],
			bbar: paging_8,
			listeners: {
				rowdblclick: klik2kali
			}
		});
		var win_find_8 = new Ext.Window({
			title: vtitle,
			modal: true,
			items: [grid_find_8]
		}).show();
	} else if (pilih == 9) {
         var ds_9 = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'registrasi/c_bayarmember/grid',
			method: 'POST'
		}),
		params: {
			start: 0,
			limit: 5
		},
		root: 'data',
		totalProperty: 'results',
		autoLoad: true,
		fields: [{
			name: "nomember",
			mapping: "nomember"
		},
		{
			name: "nama",
			mapping: "nama"
		},
		{
			name: "notelp",
			mapping: "notelp"
		},
		{
			name: "jmlbayar",
			mapping: "jmlbayar"
		},
		{
			name: "nokuitansi",
			mapping: "nokuitansi"
		},
		{
			name: "idcarabyr",
			mapping: "idcarabyr"
		},
		{
			name: "nmcarabyr",
			mapping: "nmcarabyr"
		},
		{
			name: "noref",
			mapping: "noref"
		},
		{
			name: "jmlbayar",
			mapping: "jmlbayar"
		},
		{
			name: "password",
			mapping: "password"
		}]
        });
                                   
		var cm_9 = new Ext.grid.ColumnModel([
		
		{
			header: '<center>No. Member</center>',
			width: 75,
			dataIndex: 'nopmb'
		},
		{
			header: '<center>Password</center>',
			width: 200,
			dataIndex: 'password',
			sortable: true
		},
		{
			header: '<center>Nama</center>',
			width: 150,
			dataIndex: 'nama',
			sortable: true
			
		},
		{
			header: '<center>No Telp</center>',
			width: 100,
			dataIndex: 'notelp',
			sortable: true
			
		},
		{
			header: '<center>Pembayaran</center>',
			width: 70,
			//dataIndex: 'nmsetbiayapmb',
			sortable: true
			
		},
		{
			header: '<center>Nominal</center>',
			width: 70,
			dataIndex: 'jmlbayar',
			sortable: true,
			xtype: 'numbercolumn', 
			format:'0,000', 
			align:'right'
			
		},
		{
			header: '<center>No Kuitansi</center>',
			width: 200,
			dataIndex: 'nokuitansi',
			sortable: true
		},
		{
			header: '<center>No Ref</center>',
			width: 200,
			dataIndex: 'noref',
			sortable: true
		}
                ]);
		var sm_9 = new Ext.grid.RowSelectionModel({
			singleSelect: true
		});
		var vw_9 = new Ext.grid.GridView({
			emptyText: '< Belum ada Data >'
		});
		var paging_9 = new Ext.PagingToolbar({
			pageSize: 50,
			store: ds_9,
			displayInfo: true,
			displayMsg: 'Data Member Dari {0} - {1} of {2}',
			emptyMsg: 'No data to display'
		});
		var cari_9 = [new Ext.ux.grid.Search({
			iconCls: 'btn_search',
			minChars: 1,
			autoFocus: true,
			position: 'top',
			mode: 'local',
			width: 200
		})];
		var grid_find_9 = new Ext.grid.GridPanel({
			ds: ds_9,
			cm: cm_9,
			sm: sm_9,
			view: vw_9,
			height: 350,
			width: 900,
			plugins: cari_9,
			autoSizeColumns: true,
			enableColumnResize: true,
			enableColumnHide: false,
			enableColumnMove: false,
			enableHdaccess: false,
			columnLines: true,
			loadMask: true,
			buttonAlign: 'left',
			layout: 'anchor',
			anchorSize: {
				width: 400,
				height: 400
			},
			tbar: [],
			bbar: paging_9,
			listeners: {
				rowdblclick: klik2kali
			}
		});
		var win_find_9 = new Ext.Window({
			title: vtitle,
			modal: true,
			items: [grid_find_9]
		}).show();
	} else if (pilih == 10) {
                 var ds_10 = new Ext.data.JsonStore({
                            proxy: new Ext.data.HttpProxy({
                                    url: BASE_URL + 'c_mastah/grid_matakuliah',
                                    method: 'POST'
                            }),
                            autoLoad: true,
                            root: 'data',
                            totalProperty: 'results',
                            fields: [{
                                    name: 'idmk',
                                    mapping: 'idmk'
                            },
                            {
                                    name: 'kdmk',
                                    mapping: 'kdmk'
                            },
							{
                                    name: 'nmmk',
                                    mapping: 'nmmk'
                            }]
                    });
                                   
		var cm_10 = new Ext.grid.ColumnModel([
                    {
                        hidden:true,
			header: 'No',
			dataIndex: 'idmk',
			width: 75
                    },{
			header: 'Mata Kuliah',
			dataIndex: 'nmmk',
			width: 300
                    }
                ]);
		var sm_10 = new Ext.grid.RowSelectionModel({
			singleSelect: true
		});
		var vw_10 = new Ext.grid.GridView({
			emptyText: '< Belum ada Data >'
		});
		var paging_10 = new Ext.PagingToolbar({
			pageSize: 50,
			store: ds_10,
			displayInfo: true,
			displayMsg: 'Data Mata Kuliah Dari {0} - {1} of {2}',
			emptyMsg: 'No data to display'
		});
		var cari_10 = [new Ext.ux.grid.Search({
			iconCls: 'btn_search',
			minChars: 1,
			autoFocus: true,
			position: 'top',
			mode: 'local',
			width: 200
		})];
		var grid_find_10 = new Ext.grid.GridPanel({
			ds: ds_10,
			cm: cm_10,
			sm: sm_10,
			view: vw_10,
			height: 350,
			width: 400,
			plugins: cari_10,
			autoSizeColumns: true,
			enableColumnResize: true,
			enableColumnHide: false,
			enableColumnMove: false,
			enableHdaccess: false,
			columnLines: true,
			loadMask: true,
			buttonAlign: 'left',
			layout: 'anchor',
			anchorSize: {
				width: 400,
				height: 400
			},
			tbar: [],
			bbar: paging_10,
			listeners: {
				rowdblclick: klik2kali
			}
		});
		var win_find_10 = new Ext.Window({
			title: vtitle,
			modal: true,
			items: [grid_find_10]
		}).show();
	}
     
	 //katalog//pengarang//
	else if (pilih == 11) {
                 var ds_11 = new Ext.data.JsonStore({
                            proxy: new Ext.data.HttpProxy({
                                    url: BASE_URL + 'c_mastah/grid_pengarang',
                                    method: 'POST'
                            }),
                            autoLoad: true,
                            root: 'data',
                            totalProperty: 'results',
                            fields: [{
                                    name: 'id_pengarang',
                                    mapping: 'id_pengarang'
                            },
                            {
                                    name: 'kd_pengarang',
                                    mapping: 'kd_pengarang'
                            },
							{
                                    name: 'nama_pengarang',
                                    mapping: 'nama_pengarang'
                            }]
                    });
                                   
		var cm_11 = new Ext.grid.ColumnModel([
                    {
                        hidden:true,
			header: 'No',
			dataIndex: 'id_pengarang',
			width: 75
                    },{
			header: 'Pengarang Buku',
			dataIndex: 'nama_pengarang',
			width: 300
                    }
                ]);
		var sm_11 = new Ext.grid.RowSelectionModel({
			singleSelect: true
		});
		var vw_11 = new Ext.grid.GridView({
			emptyText: '< Belum ada Data >'
		});
		var paging_11 = new Ext.PagingToolbar({
			pageSize: 50,
			store: ds_11,
			displayInfo: true,
			displayMsg: 'Data Pengarang Dari {0} - {1} of {2}',
			emptyMsg: 'No data to display'
		});
		var cari_11 = [new Ext.ux.grid.Search({
			iconCls: 'btn_search',
			minChars: 1,
			autoFocus: true,
			position: 'top',
			mode: 'local',
			width: 200
		})];
		var grid_find_11 = new Ext.grid.GridPanel({
			ds: ds_11,
			cm: cm_11,
			sm: sm_11,
			view: vw_11,
			height: 350,
			width: 400,
			plugins: cari_11,
			autoSizeColumns: true,
			enableColumnResize: true,
			enableColumnHide: false,
			enableColumnMove: false,
			enableHdaccess: false,
			columnLines: true,
			loadMask: true,
			buttonAlign: 'left',
			layout: 'anchor',
			anchorSize: {
				width: 400,
				height: 400
			},
			tbar: [],
			bbar: paging_11,
			listeners: {
				rowdblclick: klik2kali
			}
		});
		var win_find_11= new Ext.Window({
			title: vtitle,
			modal: true,
			items: [grid_find_11]
		}).show();
	}
	
	
	//katalog//kategori//
	else if (pilih == 12) {
                 var ds_12 = new Ext.data.JsonStore({
                            proxy: new Ext.data.HttpProxy({
                                    url: BASE_URL + 'c_mastah/grid_kategori',
                                    method: 'POST'
                            }),
                            autoLoad: true,
                            root: 'data',
                            totalProperty: 'results',
                            fields: [{
                                    name: 'id_kategori',
                                    mapping: 'id_kategori'
                            },
                            {
                                    name: 'kd_kategori',
                                    mapping: 'kd_kategori'
                            },
							{
                                    name: 'nama_kategori',
                                    mapping: 'nama_kategori'
                            }]
                    });
                                   
		var cm_12 = new Ext.grid.ColumnModel([
                    {
                        hidden:true,
			header: 'No',
			dataIndex: 'id_kategori',
			width: 75
                    },{
			header: 'Kategori Buku',
			dataIndex: 'nama_kategori',
			width: 300
                    }
                ]);
		var sm_12 = new Ext.grid.RowSelectionModel({
			singleSelect: true
		});
		var vw_12 = new Ext.grid.GridView({
			emptyText: '< Belum ada Data >'
		});
		var paging_12 = new Ext.PagingToolbar({
			pageSize: 50,
			store: ds_12,
			displayInfo: true,
			displayMsg: 'Data Kategori Dari {0} - {1} of {2}',
			emptyMsg: 'No data to display'
		});
		var cari_12 = [new Ext.ux.grid.Search({
			iconCls: 'btn_search',
			minChars: 1,
			autoFocus: true,
			position: 'top',
			mode: 'local',
			width: 200
		})];
		var grid_find_12 = new Ext.grid.GridPanel({
			ds: ds_12,
			cm: cm_12,
			sm: sm_12,
			view: vw_12,
			height: 350,
			width: 400,
			plugins: cari_12,
			autoSizeColumns: true,
			enableColumnResize: true,
			enableColumnHide: false,
			enableColumnMove: false,
			enableHdaccess: false,
			columnLines: true,
			loadMask: true,
			buttonAlign: 'left',
			layout: 'anchor',
			anchorSize: {
				width: 400,
				height: 400
			},
			tbar: [],
			bbar: paging_12,
			listeners: {
				rowdblclick: klik2kali
			}
		});
		var win_find_12= new Ext.Window({
			title: vtitle,
			modal: true,
			items: [grid_find_12]
		}).show();
	}//katalog//pengarang//
	else if (pilih == 13) {
                 var ds_11 = new Ext.data.JsonStore({
                            proxy: new Ext.data.HttpProxy({
                                    url: BASE_URL + 'c_mastah/grid_pengarang',
                                    method: 'POST'
                            }),
                            autoLoad: true,
                            root: 'data',
                            totalProperty: 'results',
                            fields: [{
                                    name: 'id_pengarang',
                                    mapping: 'id_pengarang'
                            },
                            {
                                    name: 'kd_pengarang',
                                    mapping: 'kd_pengarang'
                            },
							{
                                    name: 'nama_pengarang',
                                    mapping: 'nama_pengarang'
                            }]
                    });
                                   
		var cm_11 = new Ext.grid.ColumnModel([
                    {
                        hidden:true,
			header: 'No',
			dataIndex: 'id_pengarang',
			width: 75
                    },{
			header: 'Pengarang Buku',
			dataIndex: 'nama_pengarang',
			width: 300
                    }
                ]);
		var sm_11 = new Ext.grid.RowSelectionModel({
			singleSelect: true
		});
		var vw_11 = new Ext.grid.GridView({
			emptyText: '< Belum ada Data >'
		});
		var paging_11 = new Ext.PagingToolbar({
			pageSize: 50,
			store: ds_11,
			displayInfo: true,
			displayMsg: 'Data Pengarang Dari {0} - {1} of {2}',
			emptyMsg: 'No data to display'
		});
		var cari_11 = [new Ext.ux.grid.Search({
			iconCls: 'btn_search',
			minChars: 1,
			autoFocus: true,
			position: 'top',
			mode: 'local',
			width: 200
		})];
		var grid_find_11 = new Ext.grid.GridPanel({
			ds: ds_11,
			cm: cm_11,
			sm: sm_11,
			view: vw_11,
			height: 350,
			width: 400,
			plugins: cari_11,
			autoSizeColumns: true,
			enableColumnResize: true,
			enableColumnHide: false,
			enableColumnMove: false,
			enableHdaccess: false,
			columnLines: true,
			loadMask: true,
			buttonAlign: 'left',
			layout: 'anchor',
			anchorSize: {
				width: 400,
				height: 400
			},
			tbar: [],
			bbar: paging_11,
			listeners: {
				rowdblclick: klik2kali
			}
		});
		var win_find_11= new Ext.Window({
			title: vtitle,
			modal: true,
			items: [grid_find_11]
		}).show();
	}
	
	
	//katalog//kategori//
	else if (pilih == 14) {
                 var ds_12 = new Ext.data.JsonStore({
                            proxy: new Ext.data.HttpProxy({
                                    url: BASE_URL + 'c_mastah/grid_kategori',
                                    method: 'POST'
                            }),
                            autoLoad: true,
                            root: 'data',
                            totalProperty: 'results',
                            fields: [{
                                    name: 'id_kategori',
                                    mapping: 'id_kategori'
                            },
                            {
                                    name: 'kd_kategori',
                                    mapping: 'kd_kategori'
                            },
							{
                                    name: 'nama_kategori',
                                    mapping: 'nama_kategori'
                            }]
                    });
                                   
		var cm_12 = new Ext.grid.ColumnModel([
                    {
                        hidden:true,
			header: 'No',
			dataIndex: 'id_kategori',
			width: 75
                    },{
			header: 'Kategori Buku',
			dataIndex: 'nama_kategori',
			width: 300
                    }
                ]);
		var sm_12 = new Ext.grid.RowSelectionModel({
			singleSelect: true
		});
		var vw_12 = new Ext.grid.GridView({
			emptyText: '< Belum ada Data >'
		});
		var paging_12 = new Ext.PagingToolbar({
			pageSize: 50,
			store: ds_12,
			displayInfo: true,
			displayMsg: 'Data Kategori Dari {0} - {1} of {2}',
			emptyMsg: 'No data to display'
		});
		var cari_12 = [new Ext.ux.grid.Search({
			iconCls: 'btn_search',
			minChars: 1,
			autoFocus: true,
			position: 'top',
			mode: 'local',
			width: 200
		})];
		var grid_find_12 = new Ext.grid.GridPanel({
			ds: ds_12,
			cm: cm_12,
			sm: sm_12,
			view: vw_12,
			height: 350,
			width: 400,
			plugins: cari_12,
			autoSizeColumns: true,
			enableColumnResize: true,
			enableColumnHide: false,
			enableColumnMove: false,
			enableHdaccess: false,
			columnLines: true,
			loadMask: true,
			buttonAlign: 'left',
			layout: 'anchor',
			anchorSize: {
				width: 400,
				height: 400
			},
			tbar: [],
			bbar: paging_12,
			listeners: {
				rowdblclick: klik2kali
			}
		});
		var win_find_12= new Ext.Window({
			title: vtitle,
			modal: true,
			items: [grid_find_12]
		}).show();
	}
	
	//e-jurnal//jenisjurnal//
	else if (pilih == 15) {
                 var ds_15 = new Ext.data.JsonStore({
                            proxy: new Ext.data.HttpProxy({
                                    url: BASE_URL + 'c_mastah/grid_jnsjurnal',
                                    method: 'POST'
                            }),
                            autoLoad: true,
                            root: 'data',
                            totalProperty: 'results',
                            fields: [{
                                    name: 'kdjnsjurnal',
                                    mapping: 'kdjnsjurnal'
                            },
							{
                                    name: 'nmjnsjurnal',
                                    mapping: 'nmjnsjurnal'
                            }]
                    });
                                   
		var cm_15 = new Ext.grid.ColumnModel([
            {
				header: 'Kode',
				dataIndex: 'kdjnsjurnal',
				width: 50
            },
			{
				header: 'Jenis Jurnal',
				dataIndex: 'nmjnsjurnal',
				width: 245
            }
        ]);
		var sm_15 = new Ext.grid.RowSelectionModel({
			singleSelect: true
		});
		var vw_15 = new Ext.grid.GridView({
			emptyText: '< Belum ada Data >'
		});
		var paging_15 = new Ext.PagingToolbar({
			pageSize: 10,
			store: ds_15,
			displayInfo: true,
			displayMsg: 'Data Jenis Jurnal Dari {0} - {1} of {2}',
			emptyMsg: 'No data to display'
		});
		var cari_15 = [new Ext.ux.grid.Search({
			iconCls: 'btn_search',
			minChars: 1,
			autoFocus: true,
			position: 'top',
			mode: 'local',
			width: 200
		})];
		var grid_find_15 = new Ext.grid.GridPanel({
			ds: ds_15,
			cm: cm_15,
			sm: sm_15,
			view: vw_15,
			height: 350,
			width: 300,
			plugins: cari_15,
			autoSizeColumns: true,
			enableColumnResize: true,
			enableColumnHide: false,
			enableColumnMove: false,
			enableHdaccess: false,
			columnLines: true,
			loadMask: true,
			buttonAlign: 'left',
			layout: 'anchor',
			anchorSize: {
				width: 400,
				height: 400
			},
			tbar: [],
			bbar: paging_15,
			listeners: {
				rowdblclick: klik2kali
			}
		});
		var win_find_15 = new Ext.Window({
			title: vtitle,
			modal: true,
			items: [grid_find_15]
		}).show();
	}
	
	//e-library//jenis buku//
	else if (pilih == 16) {
                 var ds_16 = new Ext.data.JsonStore({
                            proxy: new Ext.data.HttpProxy({
                                    url: BASE_URL + 'c_mastah/grid_jnsbuku',
                                    method: 'POST'
                            }),
                            autoLoad: true,
                            root: 'data',
                            totalProperty: 'results',
                            fields: [{
                                    name: 'KDJNSBUKU',
                                    mapping: 'KDJNSBUKU'
                            },
							{
                                    name: 'NMJNSBUKU',
                                    mapping: 'NMJNSBUKU'
                            }]
                    });
                                   
		var cm_16 = new Ext.grid.ColumnModel([
            {
				header: 'Kode',
				dataIndex: 'KDJNSBUKU',
				width: 50
            },
			{
				header: 'Jenis Buku',
				dataIndex: 'NMJNSBUKU',
				width: 245
            }
        ]);
		var sm_16 = new Ext.grid.RowSelectionModel({
			singleSelect: true
		});
		var vw_16 = new Ext.grid.GridView({
			emptyText: '< Belum ada Data >'
		});
		var paging_16 = new Ext.PagingToolbar({
			pageSize: 10,
			store: ds_16,
			displayInfo: true,
			displayMsg: 'Data Jenis Buku Dari {0} - {1} of {2}',
			emptyMsg: 'No data to display'
		});
		var cari_16 = [new Ext.ux.grid.Search({
			iconCls: 'btn_search',
			minChars: 1,
			autoFocus: true,
			position: 'top',
			mode: 'local',
			width: 200
		})];
		var grid_find_16 = new Ext.grid.GridPanel({
			ds: ds_16,
			cm: cm_16,
			sm: sm_16,
			view: vw_16,
			height: 350,
			width: 300,
			plugins: cari_16,
			autoSizeColumns: true,
			enableColumnResize: true,
			enableColumnHide: false,
			enableColumnMove: false,
			enableHdaccess: false,
			columnLines: true,
			loadMask: true,
			buttonAlign: 'left',
			layout: 'anchor',
			anchorSize: {
				width: 400,
				height: 400
			},
			tbar: [],
			bbar: paging_16,
			listeners: {
				rowdblclick: klik2kali
			}
		});
		var win_find_16 = new Ext.Window({
			title: vtitle,
			modal: true,
			items: [grid_find_16]
		}).show();
	}
	 
	 
	    
	function klik2kali(grid, rowIdx) {
		                
		switch (pilih) {
		case 1:
                        var rec_1 = ds_1.getAt(rowIdx);
			var var_1 = rec_1.data["name"];
			
                     if(vtitle=="Data Gambar Halaman"){
                        Ext.getCmp('gambar').focus();
			Ext.getCmp("gambar").setValue(var_1); 
                     }else{
                        Ext.getCmp('file_gambar').focus();
			Ext.getCmp("file_gambar").setValue(var_1);
                     }
                        
                        win_find_1.close();
			break;
                case 2:
                    var rec_2, var_22;
                        if(vtitle=="Data Parent Web"){
                            rec_2 = ds_21.getAt(rowIdx);
                            var_22 = rec_2.data["idmenuweb"];
							var_23 = rec_2.data["nmmenuind"];
                        
                            Ext.getCmp('men_idmenuweb_nm').focus()
                            Ext.getCmp("men_idmenuweb").setValue(var_22);
							Ext.getCmp("men_idmenuweb_nm").setValue(var_23);
                        }else{
                            rec_2 = ds_2.getAt(rowIdx);
                            var_22 = rec_2.data["idmenu"];
							var_23 = rec_2.data["nmmenu"];
                        
                            Ext.getCmp('submenu').focus()
                            Ext.getCmp("idsubmenu").setValue(var_22);
							Ext.getCmp("submenu").setValue(var_23);
                        }
			
                        win_find_2.close();
			break;
                case 3:
                        var rec_3 = ds_3.getAt(rowIdx);
			var var_31 = rec_3.data["nmlengkap"];
			var var_32 = rec_3.data["userid"]
			
                        if(vtitle=="Cari Pengguna"){
                            
                            Ext.getCmp('namapengguna').focus()
                            Ext.getCmp("namapengguna").setValue(var_31);
                            win_find_3.close();
                        }else{
                            Ext.getCmp('pengguna').focus()
                            Ext.getCmp("pengguna").setValue(var_31);
                            win_find_3.close();
                        
                        }
			break;
                
                 case 4:
                        var rec_4 = ds_4.getAt(rowIdx);
			var var_41 = rec_4.data["kdkabtbpro"];
			var var_42 = rec_4.data["nmkabtbpro"];
			var var_43 = rec_4.data["nmprotbpro"];
			
			//Ext.getCmp('kdkotkabasal').focus()
                        if(vtitle=='Data Kota/Kabupaten SLTA'){
                            Ext.getCmp("kdkotkabslta").setValue(var_41);
                            Ext.getCmp("nmkotkabsltatampil").setValue(var_42);
                            Ext.getCmp("nmpropinsisltatampil").setValue(var_43);
                        }else if(vtitle=='Data Kota/Kabupaten Perguruan Tinggi'){
                            Ext.getCmp("kdkotkabptasal").setValue(var_41);
                            Ext.getCmp("nmkotkabpttampil").setValue(var_42);
                            Ext.getCmp("nmpropinsipttampil").setValue(var_43);
                        }else{
                            Ext.getCmp("kdkotkabasal").setValue(var_41);
                            Ext.getCmp("nmkotkabtampil").setValue(var_42);
                            Ext.getCmp("nmpropinsitampil").setValue(var_43);   //data pribadi
                        }
                        win_find_4.close();
			break;
		case 5:
                        var rec_5 = ds_5.getAt(rowIdx);
			var var_51 = rec_5.data["kdptitbpti"];
			var var_52 = rec_5.data["nmptitbpti"];
			    Ext.getCmp("kdptasal").setValue(var_51);
                            Ext.getCmp("nmptasal").setValue(var_52);
                        
                        win_find_5.close();
			break;
                        
                case 6:
                        var rec_6 = ds_6.getAt(rowIdx);
			var var_61 = rec_6.data["kdprodi"];
			var var_62 = rec_6.data["nmprodi"];
			    Ext.getCmp("kdprodiasal").setValue(var_61);
                            Ext.getCmp("nmprodiasal").setValue(var_62);
                        
                        win_find_6.close();
			break;
		
                case 7:
                        var rec_7 = ds_7.getAt(rowIdx);
			var var_71 = rec_7.data["nidu"];
			var var_72 = rec_7.data["nmdosdgngelar"];
			
				if(vtitle=="Data Dosen Penguji"){
					Ext.getCmp("cmbpenguji").setValue(var_72);
				} else {
					Ext.getCmp("nidu").setValue(var_71);
					Ext.getCmp("dosen").setValue(var_72);
				}
                        
                        win_find_7.close();
			break;
			
			case 8:
                        var rec_8 = ds_8.getAt(rowIdx);
			var var_81 = rec_8.data["idhalaman"];
			var var_82 = rec_8.data["judulind"];
			    Ext.getCmp("idhalaman").setValue(var_81);
				Ext.getCmp("judulind").setValue(var_82);
                        
                        win_find_8.close();
			break;
                        
      		 case 9:
                        var rec_9 = ds_9.getAt(rowIdx);
			var var_91 = rec_9.data["nopmb"];
			var var_92 = rec_9.data["nmjadwalpmb"];
			var var_93 = rec_9.data["idstmskmhs"];
			var var_94 = rec_9.data["nama"];
			var var_95 = rec_9.data["notelp"];
			var var_96 = rec_9.data["idsetbiayapmb"];
			var var_97 = rec_9.data["nominal"];
			var var_98 = rec_9.data["nokuitansipmb"];
			var var_99 = rec_9.data["tglkuitansipmb"];
			var var_100 = rec_9.data["jamkuitansi"];
			var var_101 = rec_9.data["idcarabyr"];
			var var_102 = rec_9.data["noref"];
			var var_103 = rec_9.data["jmlbayar"];
			var var_104 = rec_9.data["password"];
                        
			Ext.getCmp("nopmb").setValue(var_91);
                        Ext.getCmp("nmjadwalpmb").setValue(var_92);
                        Ext.getCmp("statuspendaftar").setValue(var_93);
                        Ext.getCmp("namapendaftar").setValue(var_94);
                        Ext.getCmp("notelp").setValue(var_95);
						Ext.getCmp("untukpembayaran").setValue(var_96);
                        Ext.getCmp("nominal").setValue(var_97);
						Ext.getCmp("nokuitansi").setValue(var_98);
						Ext.getCmp("tglkuitansi").setValue(var_99);
						Ext.getCmp("timekuitansi").setValue(var_100);
						Ext.getCmp("carabayar").setValue(var_101);
						Ext.getCmp("noref").setValue(var_102);
						Ext.getCmp("uangditerima").setValue(var_103);
                        Ext.getCmp("password").setValue(var_104);
						
						Ext.getCmp('simpan').disable();
						Ext.getCmp('cetak').enable();
						Ext.getCmp('batal').enable();
                        win_find_9.close();
			break;
			
			case 10:
                        var rec_10 = ds_10.getAt(rowIdx);
				var var_101 = rec_10.data["idmk"];
				var var_102 = rec_10.data["nmmk"];
			    Ext.getCmp("idmk").setValue(var_101);
				Ext.getCmp("nmmk").setValue(var_102);
                        
                        win_find_10.close();
			break;
            
			//katalog//pengarang//
			case 11:
                        var rec_11 = ds_11.getAt(rowIdx);
			var var_110 = rec_11.data["nama_pengarang"];
			    Ext.getCmp("id_pengarang").setValue(var_110);
                        
                        win_find_11.close();
			break;
			//katalog//kategori//
			case 12:
                        var rec_12 = ds_12.getAt(rowIdx);
			var var_112 = rec_12.data["nama_kategori"];
			    Ext.getCmp("id_kategori").setValue(var_112);
                        
                        win_find_12.close();
			break;
			//e-jurnal//jnsjurnal//
			case 15:
            var rec_15 = ds_15.getAt(rowIdx);
			var var_115 = rec_15.data["kdjnsjurnal"];
			var var_116 = rec_15.data["nmjnsjurnal"];
			Ext.getCmp("idjjurnal").setValue(var_115);
			Ext.getCmp("jjurnal").setValue(var_116);
            win_find_15.close();
			break;
			
			//e-library//buku//
			case 16:
            var rec_16 = ds_16.getAt(rowIdx);
			var var_117 = rec_16.data["KDJNSBUKU"];
			var var_118 = rec_16.data["NMJNSBUKU"];
			Ext.getCmp("kdjbuku").setValue(var_117);
			Ext.getCmp("jbuku").setValue(var_118);
            win_find_16.close();
			break;
			          
      		default:
			break;
		}
	}
}