var f2Fields = new Object([
	{ name: "kode", mapping: "kode" }
	, { name: "nama", mapping: "nama" }
]);

var f3Fields = new Object([
	{ name: "id", mapping: "id" }
	, { name: "kode", mapping: "kode" }
	, { name: "nama", mapping: "nama" }
]);

var f4Fields = new Object([
	{ name: "id", mapping: "id" }
	, { name: "kode", mapping: "kode" }
	, { name: "nama", mapping: "nama" }
	, { name: "deskripsi", mapping: "deskripsi" }
]);

function getMasterJsonStore(fieldsObj, strMethod){
	var masterUrl = BASE_URL + 'data_controller/' + strMethod;
	var jsonStore = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: masterUrl,
			method: 'POST'
		}),
		autoLoad: true,  root: 'data', fields: fieldsObj,
	});

	return jsonStore;
}
/**
======== DATA STORES (for COMBO) ===============
*/
function store_kelmatkul(){
	return getMasterJsonStore(f2Fields, 'get_kelmatkul');
}
function store_availability(){
	var ds = new Ext.data.JsonStore({ //static data
		//autoDestroy: true,
		fields: f2Fields,
		data: [
		{	kode:"Y",nama:"Ada"},
		{	kode:"T",nama:"Tidak Ada"},
		]
	});	return ds;
}
function store_persen_static(){
	var ds = new Ext.data.JsonStore({ //static data
		//autoDestroy: true,
		fields: f2Fields,
		data: [
		{	"kode":"0","nama":"semua"},
		{	"kode":"25","nama":"25 %"},
		{	"kode":"50","nama":"50 %"},
		{	"kode":"80","nama":"80 %"},
		{	"kode":"100","nama":"100 %"}
		]
	}); return ds;
}
// GENDER
function store_gender_static(){
	var ds = new Ext.data.JsonStore({ //static data
		autoDestroy: true,
		fields: f2Fields,
		data: [
		{	"kode":"L","nama":"Laki-laki"},
		{	"kode":"P","nama":"Perempuan"}
		]
	}); return ds;
}
function store_gender(){
	return getMasterJsonStore(f2Fields, 'get_gender');
}
//RELIGION
function store_religion_static(){
	var ds = new Ext.data.JsonStore({ //static data
	autoDestroy: true,
    fields: f3Fields,
	data: [
		{"idx":'1',"kode":"I","nama":"Islam"},
		{"idx":'2',"kode":"K","nama":"Katolik"},
		{"idx":'3',"kode":"P","nama":"Protestan"},
		{"idx":'4',"kode":"B","nama":"Budha"},
		{"idx":'5',"kode":"H","nama":"Hindu"},
		{"idx":'9',"kode":"L","nama":"Lainnya"}
	]});
	return ds;
}

function store_religion(){
	return getMasterJsonStore(f3Fields, 'get_religion');
}
function store_goldarah(){
	return getMasterJsonStore(f3Fields, 'get_bloodtype');	//ds_bloodtype
}
function store_wn(){
	return getMasterJsonStore(f2Fields, 'get_nationality'); //ds_wn
}
function store_parentjob(){
	return getMasterJsonStore(f4Fields, 'get_parentjob');
}
function store_parentedu(){
	return getMasterJsonStore(f4Fields, 'get_parentedu');
}
// STATUS SMESTER
function store_stsmt(){
	var ds_stsmt = new Ext.data.JsonStore({ //from database
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'data_controller/get_stsmt ',
			method: 'POST'
		}),
		autoLoad: true,
		root: 'data',
		fields: [
		{ name: "kdstsemester", mapping: "kdstsemester" }
		, { name: "nmthnakademik", mapping: "nmthnakademik" }
		, { name: "nmjnssemester", mapping: "nmjnssemester" }
		, { name: "nmsmt", mapping: "nmsmt" }
		]
	});
	return ds_stsmt;
}
// PRODI INTERNAL
function store_prodireg() {
	var ds_prodireg = new Ext.data.JsonStore({ //from database
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'data_controller/get_prodireg ',
			method: 'POST'
		}),
		autoLoad: true,
		root: 'data',
		fields: [
		{ name: "kdprodi", mapping: "kdprodi" }
		, { name: "nourutprodi", mapping: "nourutprodi" }
		, { name: "nmprodi", mapping: "nmprodi" }
		, { name: "lnmprodi", mapping: "lnmprodi" }
		, { name: "kdfakultas", mapping: "kdfakultas" }
		, { name: "nmfakultas", mapping: "nmfakultas" }
		]
	});
	return ds_prodireg;
};

function store_shift(){
	return getMasterJsonStore(f2Fields, 'get_shift');
}
function store_class(){
	return getMasterJsonStore(f4Fields, 'get_class');
}
function store_klsmhs(){
	var ds = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'data_controller/get_klsmhs',
			method: 'POST'
		}),
		autoLoad: true,
		root: 'data',
		fields: [
		{ name: "idklsmhs" }, { name: "kdklsmhs" },
		,{ name: "nmklsmhs" }, { name: "deskripsi" }
		]
	});
	return ds;
}
function store_jkls(){
	var ds_jkls = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'data_controller/get_jkls',
			method: 'POST'
		}),
		autoLoad: true,
		root: 'data',
		fields: [
		{ name: "idjnskls", mapping: "idjnskls" },
		{ name: "nmjnskls", mapping: "nmjnskls" }
		]
	});
	return ds_jkls;
}
function store_jklsplusall(){
	var ds_jkls = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'data_controller/get_jkls',
			method: 'POST'
		}),
		autoLoad: true,
		root: 'data',
		fields: [
		{ name: "idjnskls", mapping: "idjnskls" },
		{ name: "nmjnskls", mapping: "nmjnskls" }
		],
		listeners : {
			load : function() {
			var rec = new this.recordType({idjnskls:3, nmjnskls:'ALL'});
			rec.commit();
			this.add(rec);
			}
		},
	});
	return ds_jkls;
}
function store_tahun(){
	return getMasterJsonStore(f2Fields, 'get_years');
}
function store_stawal(){
	return getMasterJsonStore(f2Fields, 'get_stfirst');
}
function store_staktiv(){
	return getMasterJsonStore(f2Fields, 'get_staktiv');
}
function store_jslta(){
	return getMasterJsonStore(f4Fields, 'get_jslta');
}
function store_stakrslta(){
// STATUS AKREDITASI SLTA
	return getMasterJsonStore(f4Fields, 'get_stakrslta');
}
function store_jpt(){
	return getMasterJsonStore(f4Fields, 'get_jpt');
}
function store_stakrpt(){
// STATUS AKREDITASI PT
	return getMasterJsonStore(f4Fields, 'get_stakrpt');
}
function store_kuakrpt(){
// KUALIFIKASI AKREDITASI PT
	return getMasterJsonStore(f4Fields, 'get_kuakrpt');
}
function store_mhsjob(){
// PEKERJAAN MAHASISWA
	return getMasterJsonStore(f4Fields, 'get_mhsjob');
}
function store_biayastudi(){
	return getMasterJsonStore(f2Fields, 'get_biaya');
}

//DOSEN
function store_status_dos(){
	return getMasterJsonStore(f2Fields, 'get_status_dos');
}
function store_jabak(){
	return getMasterJsonStore(f2Fields, 'get_jabak');	//ds_jabak
}
function store_pdktop(){
	return getMasterJsonStore(f2Fields, 'get_pdktop');	//ds_pdktop
}
function sore_jstudi(){
	return getMasterJsonStore(f2Fields, 'get_jenjangstudi');
}
function store_stikj(){
	return getMasterJsonStore(f2Fields, 'get_stikj');	//ds_stikj
}
function store_staktivdos(){
	return getMasterJsonStore(f2Fields, 'get_staktivdos');	//ds_staktivdos
}
/** KEUANGAN */
function store_carabyr(){
	var ds_carabyr = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'data_controller/get_carabyr',
			method: 'POST'
		}),
		autoLoad: true,
		root: 'data',
		fields: [
		{ name: "idcarabyr", mapping: "idcarabyr" }
		, { name: "kdcarabyr", mapping: "kdcarabyr" }
		, { name: "nmcarabyr", mapping: "nmcarabyr" }
		//, { name: "deskripsi", mapping: "deskripsi" }
		]
	});
	return ds_carabyr;
}
function store_jbiaya(){
	var ds_jbiaya = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'data_controller/get_jbiaya',
			method: 'POST'
		}),
		autoLoad: true,
		root: 'data',
		fields: [
		{ name: "idjnsbiaya", mapping: "idjnsbiaya" }
		, { name: "kdjnsbiaya", mapping: "kdjnsbiaya" }
		, { name: "nmjnsbiaya", mapping: "nmjnsbiaya" }
		//, { name: "deskripsi", mapping: "deskripsi" }
		]
	});
	return ds_jbiaya;
}

//kelompok setting
function dm_kelompoksetting(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url : BASE_URL + 'kelompoksetting_controller/get_klpsetting', 
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idklpset',
				mapping: 'idklpset'
			},{
				name: 'kdklpset',
				mapping: 'kdklpset'
			},{
				name: 'nmklpset',
				mapping: 'nmklpset'
			},{
				name: 'deskripsi',
				mapping: 'deskripsi'
			}]
		});
		return master;
	}

//setting
function dm_setting(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'setting_controller/get_setting',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idset',
				mapping: 'idset'
			},{
				name: 'idklpset',
				mapping: 'idklpset'
			},{
				name: 'nmklpset',
				mapping: 'nmklpset'
			},{
				name: 'kdset',
				mapping: 'kdset'
			},{
				name: 'nmset',
				mapping: 'nmset'
			},{
				name: 'ketset',
				mapping: 'ketset'
			},{
				name: 'nilai',
				mapping: 'nilai'
			},{
				name: 'nourut',
				mapping: 'nourut'
			}]
		});
		return master;
	}

/*=========== added by NAFHUL =====================*/

/* ======================== KEUANGAN ========================== */

