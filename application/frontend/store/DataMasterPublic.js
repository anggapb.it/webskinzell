// DATA SOURCE (for COMBO)
//Status Aktif-Tidak Aktif
function store_status() {
        var ds_status = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_mastah/gridM_status',
			method: 'POST'
		}),
		totalProperty: 'results',
		root: 'data',
                autoLoad: true,
		fields: [{
			name: 'idstatus',
			mapping: 'idstatus'
		},{
			name: 'kdstatus',
			mapping: 'kdstatus'
		},
		{
			name: 'nmstatus',
			mapping: 'nmstatus'
		}]
	});
	return ds_status;
}

function store_jkerja() {
        var ds_status = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_mastah/gridM_jkerja',
			method: 'POST'
		}),
		totalProperty: 'results',
		root: 'data',
                autoLoad: true,
		fields: [{
			name: 'idjnskerjasama',
			mapping: 'idjnskerjasama'
		},{
			name: 'kdjnskerjasama',
			mapping: 'kdjnskerjasama'
		},
		{
			name: 'nmjnskerjasama',
			mapping: 'nmjnskerjasama'
		}]
	});
	return ds_status;
}

function store_jabatan() {
        var ds_status = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_mastah/gridM_jabatan',
			method: 'POST'
		}),
		totalProperty: 'results',
		root: 'data',
                autoLoad: true,
		fields: [{
			name: 'idjabatan',
			mapping: 'idjabatan'
		},{
			name: 'kdjabatan',
			mapping: 'kdjabatan'
		},
		{
			name: 'nmjabatan',
			mapping: 'nmjabatan'
		}]
	});
	return ds_status;
}

function store_negaratujuan() {
        var ds_status = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_mastah/gridM_negaratujuan',
			method: 'POST'
		}),
		totalProperty: 'results',
		root: 'data',
                autoLoad: true,
		fields: [{
			name: 'idnegaratujuan',
			mapping: 'idnegaratujuan'
		},{
			name: 'kdnegaratujuan',
			mapping: 'kdnegaratujuan'
		},
		{
			name: 'nmnegaratujuan',
			mapping: 'nmnegaratujuan'
		}]
	});
	return ds_status;
}

function store_periode() {
        var ds_status = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_mastah/gridM_periode',
			method: 'POST'
		}),
		totalProperty: 'results',
		root: 'data',
                autoLoad: true,
		fields: [{
			name: 'idperiode',
			mapping: 'idperiode'
		},{
			name: 'kdperiode',
			mapping: 'kdperiode'
		},
		{
			name: 'nmperiode',
			mapping: 'nmperiode'
		}]
	});
	return ds_status;
}

function store_provinsi() {
        var ds_status = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_mastah/gridM_provinsi',
			method: 'POST'
		}),
		totalProperty: 'results',
		root: 'data',
                autoLoad: true,
		fields: [{
			name: 'idprovinsi',
			mapping: 'idprovinsi'
		},{
			name: 'kdprovinsi',
			mapping: 'kdprovinsi'
		},
		{
			name: 'nmprovinsi',
			mapping: 'nmprovinsi'
		}]
	});
	return ds_status;
}

function store_stposting() {
        var ds_status = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_mastah/gridM_stposting',
			method: 'POST'
		}),
		totalProperty: 'results',
		root: 'data',
                autoLoad: true,
		fields: [{
			name: 'idstposting',
			mapping: 'idstposting'
		},{
			name: 'kdstposting',
			mapping: 'kdstposting'
		},
		{
			name: 'nmstposting',
			mapping: 'nmstposting'
		}]
	});
	return ds_status;
}

function store_jhewan() {
        var ds_status = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_mastah/gridM_jhewan',
			method: 'POST'
		}),
		totalProperty: 'results',
		root: 'data',
                autoLoad: true,
		fields: [{
			name: 'idjnshewan',
			mapping: 'idjnshewan'
		},{
			name: 'kdjnshewan',
			mapping: 'kdjnshewan'
		},
		{
			name: 'nmjnshewan',
			mapping: 'nmjnshewan'
		}]
	});
	return ds_status;
}

function store_klshewan() {
        var ds_status = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_mastah/gridM_klshewan',
			method: 'POST'
		}),
		totalProperty: 'results',
		root: 'data',
                autoLoad: true,
		fields: [{
			name: 'idklshewan',
			mapping: 'idklshewan'
		},{
			name: 'kdklshewan',
			mapping: 'kdklshewan'
		},
		{
			name: 'nmklshewan',
			mapping: 'nmklshewan'
		}]
	});
	return ds_status;
}

function store_program() {
        var ds_status = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_mastah/gridM_program',
			method: 'POST'
		}),
		totalProperty: 'results',
		root: 'data',
                autoLoad: true,
		fields: [{
			name: 'idprogram',
			mapping: 'idprogram'
		},{
			name: 'kdprogram',
			mapping: 'kdprogram'
		},
		{
			name: 'nmprogram',
			mapping: 'nmprogram'
		}]
	});
	return ds_status;
}

// Hierarki Head-Detail
function store_hierarki() {
        var ds_hierarki = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_mastah/gridM_hierarki',
			method: 'POST'
		}),
		totalProperty: 'results',
		root: 'data',
		autoLoad: true,
		fields: [{

			name: 'idjnshirarki',
			mapping: 'idjnshirarki'
		},
		{
			name: 'nmjnshirarki',
			mapping: 'nmjnshirarki'
		}]
	});
	return ds_hierarki;
}

function store_dashboard() {
        var ds_dashboard = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_mastah/gridM_dash',
			method: 'POST'
		}),
		totalProperty: 'results',
		root: 'data',
                autoLoad: true,
		fields: [{

			name: 'idjnsdashboard',
			mapping: 'idjnsdashboard'
		},
		{
			name: 'nmjnsdashboard',
			mapping: 'nmjnsdashboard'
		}]
	});

	return ds_dashboard; 
}
    
function store_otoritas() {    
       var ds_otoritas= new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_utility/g_JKP',
			method: 'POST'
		}),
		params: {
			start: 0,
			limit: 5
		},
		root: 'data',
		totalProperty: 'results',
		autoLoad: true,
		fields: [{
			name: "idklppengguna",
			mapping: "idklppengguna"
		},
		{
			name: "kdklppengguna",
			mapping: "kdklppengguna"
		},
		{
			name: "nmklppengguna",
			mapping: "nmklppengguna"
		}]
	});

	return ds_otoritas;
}   

function store_stsemester() {       
        var ds_stsemester = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_mastah/grid_stsemester',
			method: 'POST'
		}),
		totalProperty: 'results',
		root: 'data',
                autoLoad: true,
		fields: [{
			name: 'kdstsemester',
			mapping: 'kdstsemester'
		}

            ]
	});

	return ds_stsemester;
}
  
function store_jadwalpmb() {    
        var ds_jadwalpmb = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_mastah/grid_jadwalpmb',
			method: 'POST'
		}),
		totalProperty: 'results',
		root: 'data',
		autoLoad: true,
		fields: [{
			name: 'idjadwalpmb',
			mapping: 'idjadwalpmb'
		},
		{
			name: 'kdjadwalpmb',
			mapping: 'kdjadwalpmb'
		},
		{
			name: 'nmjadwalpmb',
			mapping: 'nmjadwalpmb'
		}]
	});

	return ds_jadwalpmb;
}
 
function store_carabyr() {  
        var ds_carabyr = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_mastah/grid_carabyr',
			method: 'POST'
		}),
		totalProperty: 'results',
		root: 'data',
                autoLoad: true,
		
		fields: [{
			name: 'idcarabyr',
			mapping: 'idcarabyr'
		},
		{
			name: 'nmcarabyr',
			mapping: 'nmcarabyr'
		}]
	});    


	return ds_carabyr;
}	
 
function store_pekerjaanortu() {  
        var ds_pekerjaanortu = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_mastah/grid_pekerjaanortu',
			method: 'POST'
		}),
		totalProperty: 'results',
		root: 'data',
		autoLoad: true,
		fields: [{
			name: 'idpekerjaanortu',
			mapping: 'idpekerjaanortu'
		},
		{
			name: 'nmpekerjaanortu',
			mapping: 'nmpekerjaanortu'
		}]
	});

 	return ds_pekerjaanortu;
}	

function store_pekerjaanmhs() {  
        var ds_pekerjaanmhs = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_mastah/grid_pekerjaanmhs',
			method: 'POST'
		}),
		totalProperty: 'results',
		root: 'data',
		autoLoad: true,
		fields: [{
			name: 'idpekerjaanmhs',
			mapping: 'idpekerjaanmhs'
		},
		{
			name: 'nmpekerjaanmhs',
			mapping: 'nmpekerjaanmhs'
		}]
	});

 	return ds_pekerjaanmhs;
}	

function store_stmskmhs() {         
        var ds_stmskmhs = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_mastah/grid_stmskmhs',
			method: 'POST'
		}),
		totalProperty: 'results',
		root: 'data',
		autoLoad: true,
		fields: [{
			name: 'idstmskmhs',
			mapping: 'idstmskmhs'
		},
		{
			name: 'nmstmskmhs',
			mapping: 'nmstmskmhs'
		}]
	});

 	return ds_stmskmhs;
}      

function store_jusm() { 
		var ds_jusm = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_mastah/grid_jusm',
			method: 'POST'
		}),
		totalProperty: 'results',
		root: 'data',
		autoLoad: true,
		fields: [{
			name: 'idjnsusm',
			mapping: 'idjnsusm'
		},
		{
			name: 'nmjnsusm',
			mapping: 'nmjnsusm'
		}]
	});

 	return ds_jusm;
}     

function store_stmskmhs_pmb() {	
        var ds_stmskmhs_pmb = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_mastah/grid_stmskmhs_pmb',
			method: 'POST'
		}),
		totalProperty: 'results',
		root: 'data',
		autoLoad: true,
		fields: [{
			name: 'idstmskmhs',
			mapping: 'idstmskmhs'
		},
		{
			name: 'nmstmskmhs',
			mapping: 'nmstmskmhs'
		}]
	});

  	return ds_stmskmhs_pmb;
}     
 
function store_prodi() {	 
    var ds_prodi = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_mastah/grid_prodi',
			method: 'POST'
		}),
		totalProperty: 'results',
		root: 'data',
		autoLoad: true,
		fields: [{
			name: 'kdprodi',
			mapping: 'kdprodi'
		},
		{
			name: 'nmprodi',
			mapping: 'nmprodi'
		}]


	});//grid_klsmhs
  	return ds_prodi;
}   

function store_prodiall() {	
	var ds_prodiall = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_mastah/grid_prodiall',
			method: 'POST'
		}),
		totalProperty: 'results',
		root: 'data',
		autoLoad: true,
		fields: [{
			name: 'kdprodi',
			mapping: 'kdprodi'
		},
		{
			name: 'nmprodi',
			mapping: 'nmprodi'
		}]
	});

  	return ds_prodiall;
}   
 
function store_klsmhs() { 
    var ds_klsmhs = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_mastah/grid_klsmhs',
			method: 'POST'
		}),
		totalProperty: 'results',
		root: 'data',
		autoLoad: true,
		fields: [{
			name: 'idklsmhs',
			mapping: 'idklsmhs'
		},
		{
			name: 'nmklsmhs',
			mapping: 'nmklsmhs'
		}]
	});

   	return ds_klsmhs;
} 
 
function store_jenpdkpendaftar() { 
        var ds_jenpdkpendaftar = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_mastah/grid_jenpdkpendaftar',
			method: 'POST'
		}),
		totalProperty: 'results',
		root: 'data',
		autoLoad: true,
		fields: [{
			name: 'idjenpdkpendaftar',
			mapping: 'idjenpdkpendaftar'
		},
		{
			name: 'nmjenpdkpendaftar',
			mapping: 'nmjenpdkpendaftar'
		}]
	});

    return ds_jenpdkpendaftar;
} 
 
function store_stakreditasi() {  
        var ds_stakreditasi = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_mastah/grid_stakreditasi',
			method: 'POST'
		}),
		totalProperty: 'results',
		root: 'data',
		autoLoad: true,
		fields: [{
			name: 'idstakreditasi',
			mapping: 'idstakreditasi'
		},
		{
			name: 'nmstakreditasi',
			mapping: 'nmstakreditasi'
		}]
	}); 

    return ds_stakreditasi;
} 

function store_sbrinfo() {        
        var ds_sbrinfo = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_mastah/grid_sbrinfo',
			method: 'POST'
		}),
		totalProperty: 'results',
		root: 'data',
		autoLoad: true,
		fields: [{
			name: 'idsbrinfo',
			mapping: 'idsbrinfo'
		},
		{
			name: 'nmsbrinfo',
			mapping: 'nmsbrinfo'
		}]
	});//

    return ds_sbrinfo;
} 
 
function store_rekpmb() {  
        var ds_rekpmb = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_mastah/grid_rekpmb',
			method: 'POST'
		}),
		totalProperty: 'results',
		root: 'data',
		autoLoad: true,
		fields: [{
			name: 'idrekpmb',
			mapping: 'idrekpmb'
		},
		{
			name: 'nmrekpmb',
			mapping: 'nmrekpmb'
		}]
	});//

    return ds_rekpmb;
} 
 
function store_alasanpendaftar() {  
        var ds_alasanpendaftar = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_mastah/grid_alasanpendaftar',
			method: 'POST'
		}),
		totalProperty: 'results',
		root: 'data',
		autoLoad: true,
		fields: [{
			name: 'idalasanpendaftar',
			mapping: 'idalasanpendaftar'
		},
		{
			name: 'nmalasanpendaftar',
			mapping: 'nmalasanpendaftar'
		}]

	});

    return ds_alasanpendaftar;
} 

function store_jgaleri() { 
        var ds_jgaleri = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_mastah/grid_jgaleri',
			method: 'POST'
		}),
		totalProperty: 'results',
		root: 'data',
		autoLoad: true,
		fields: [{
			name: 'idjnsgaleri',
			mapping: 'idjnsgaleri'
		},
		{
			name: 'kdjnsgaleri',
			mapping: 'kdjnsgaleri'
		},
		{
			name: 'nmjnsgaleriind',
			mapping: 'nmjnsgaleriind'
		}]
	});
     
	return ds_jgaleri;
}       
 
function store_klpgaleri() {
        var ds_klpgaleri = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_mastah/grid_klpgaleri',
			method: 'POST'
		}),
		totalProperty: 'results',
		root: 'data',
		autoLoad: true,
		fields: [{
			name: 'idklpgaleri',
			mapping: 'idklpgaleri'
		},
		{
			name: 'kdklpgaleri',
			mapping: 'kdklpgaleri'
		},
		{
			name: 'nmklpgaleriind',
			mapping: 'nmklpgaleriind'
		}]
	});
 
 return ds_klpgaleri;
}    

function store_stpublish() {  
        var ds_stpublish = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_mastah/grid_stpublish',
			method: 'POST'
		}),
		totalProperty: 'results',
		root: 'data',
		autoLoad: true,
		fields: [{
			name: 'idstpublish',
			mapping: 'idstpublish'
		},
		{
			name: 'kdstpublish',
			mapping: 'kdstpublish'
		},
		{
			name: 'nmstpublish',
			mapping: 'nmstpublish'
		}]
	});

    return ds_stpublish;
}

function store_kategori_halaman() {
        var ds_kategori_halaman = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_mastah/grid_kategorihalaman',
			method: 'POST'
		}),
		totalProperty: 'results',
		root: 'data',
		autoLoad: true,
		fields: [{
			name: 'idktghalaman',
			mapping: 'idktghalaman'
		},
		{
			name: 'kdktghalaman',
			mapping: 'kdktghalaman'
		},
		{
			name: 'nmktghalamanind',
			mapping: 'nmktghalamanind'
		}]
	});

	return ds_kategori_halaman;
}

function store_klptautan() {	
    var ds_klptautan = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_mastah/grid_klptautan',
			method: 'POST'
		}),
		totalProperty: 'results',
		root: 'data',
		autoLoad: true,
		fields: [{
			name: 'idklptautan',
			mapping: 'idklptautan'
		},
		{
			name: 'kdtautan',
			mapping: 'kdtautan'
		},
		{
			name: 'nmklptautanind',
			mapping: 'nmklptautanind'
		}]
	});

	return ds_klptautan;
}
	
function store_jpengguna() { 
	var ds_jpengguna = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_mastah/grid_jpengguna',
			method: 'POST'
		}),
		totalProperty: 'results',
		root: 'data',
		autoLoad: true,
		fields: [{
			name: 'idjnspengguna',
			mapping: 'idjnspengguna'
		},
		{
			name: 'nmjnspengguna',
			mapping: 'nmjnspengguna'
		}]
	});    

    return ds_jpengguna;
}
 
function store_vsemester() {  
	var ds_vsemester = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_mastah/grid_vstsemester2',
			method: 'POST'
		}),
		totalProperty: 'results',
		root: 'data',
		autoLoad: true,
		fields: [{
			name: 'kdstsemester',
			mapping: 'kdstsemester'
		}]
	});   

    return ds_vsemester;
}
 
function store_vsemester3() { 
       var ds_vsemester3 = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_mastah/grid_vstsemester3',
			method: 'POST'
		}),
		totalProperty: 'results',
		root: 'data',
		autoLoad: true,
		fields: [{
			name: 'kdstsemester',
			mapping: 'kdstsemester'
		}]
	});   

    return ds_vsemester3;
}
 
function store_stusm() {  
        var ds_stusm = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_mastah/gridM_stusm',
			method: 'POST'
		}),
		totalProperty: 'results',
		root: 'data',
                autoLoad: true,
		fields: [{
			name: 'idstusm',
			mapping: 'idstusm'
		},
		{
			name: 'nmstusm',
			mapping: 'nmstusm'
		}]
	});

    return ds_stusm;
}    
 
function store_thnakademik() { 
        var ds_thnakademik = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_mastah/grid_thnakademik',
			method: 'POST'
		}),
		totalProperty: 'results',
		root: 'data',
                autoLoad: true,
		fields: [{
			name: 'idthnakademik',
			mapping: 'idthnakademik'
		},
		{
			name: 'kdthnakademik',
			mapping: 'kdthnakademik'
		}]
	});

     return ds_thnakademik;
} 

function store_stsemester_private() {        
        var ds_stsemester_private = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_mastah/grid_vstsemester3',
			method: 'POST'
		}),
		totalProperty: 'results',
		root: 'data',
                autoLoad: true,
		fields: [{
			name: 'idjnssemester',
			mapping: 'idjnssemester'
		},
		{
			name: 'nmjnssemester',
			mapping: 'nmjnssemester'
		}]
	});

    return ds_stsemester_private;
} 

function store_setbiayapmb() {  
        var ds_setbiayapmb = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_mastah/grid_setbiayapmb',
			method: 'POST'
		}),
		totalProperty: 'results',
		root: 'data',
                autoLoad: true,
		fields: [{
			name: 'idsetbiayapmb',
			mapping: 'idsetbiayapmb'
		},
		{
			name: 'nmsetbiayapmb',
			mapping: 'nmsetbiayapmb'
		}]
	});

    return ds_setbiayapmb;
}

function store_perusahaan() {		
	 var ds_perusahaan = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_mastah/grid_perusahaan',
			method: 'POST'
		}),
		totalProperty: 'results',
		root: 'data',
                autoLoad: true,
		fields: [{
			name: 'idperusahaan',
			mapping: 'idperusahaan'
		},
		{
			name: 'kdperusahaan',
			mapping: 'kdperusahaan'
		},
		{
			name: 'nmperusahaan',
			mapping: 'nmperusahaan'
		}]
	});
	
    return ds_perusahaan;
} 

function store_posisi() {		
	var ds_posisi = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_mastah/grid_posisi',
			method: 'POST'
		}),
		totalProperty: 'results',
		root: 'data',
                autoLoad: true,
		fields: [{
			name: 'idposisi',
			mapping: 'idposisi'
		},
		{
			name: 'kdposisi',
			mapping: 'kdposisi'
		},
		{
			name: 'nmposisi',
			mapping: 'nmposisi'
		}]
	});
	
    return ds_posisi;
} 

function store_sesi() {	
	var ds_sesi = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_mastah/grid_sesi',
			method: 'POST'
		}),
		totalProperty: 'results',
		root: 'data',
                autoLoad: true,
		fields: [{
			name: 'idsesi',
			mapping: 'idsesi'
		},
		{
			name: 'kdsesi',
			mapping: 'kdsesi'
		},
		{
			name: 'nmsesi',
			mapping: 'nmsesi'
		}]
	});
	
    return ds_sesi;
} 	 

function store_thnakademik2() {
	 var ds_thnakademik2 = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_mastah/grid_thnakademik2',
			method: 'POST'
		}),
		totalProperty: 'results',
		root: 'data',
                autoLoad: true,
		fields: [{
			name: 'kdstsemester',
			mapping: 'kdstsemester'
		},{
			name: 'kdthnakademik',
			mapping: 'kdthnakademik'
		},{
			name: 'nmthnakademik',
			mapping: 'nmthnakademik'
		}]
	});

    return ds_thnakademik2;
}    

function store_tbpro() {    
    var ds_tbpro = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_mastah/grid_tbpro',
			method: 'POST'
		}),
		totalProperty: 'results',
		root: 'data',
		autoLoad: true,
		fields: [{
			name: 'kdkotkabasal',
			mapping: 'kdkotkabasal'

		},
		{
			name: 'nmkabtbpro',
			mapping: 'nmkabtbpro'
		}]
	});

    return ds_tbpro;
}   

function store_jmlbiaya() { 
	var ds_jmlbiaya = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_mastah/grid_jmlbiaya',
			method: 'POST'
		}),
		totalProperty: 'results',
		root: 'data',
                autoLoad: true,
		fields: [{
			name: 'nominal',
			mapping: 'nominal'
		},{
			name: 'idsetbiaya',
			mapping: 'idsetbiaya'
		}]
	});

	return ds_jmlbiaya;
}  

function store_tahun() { 
	var ds_tahun = new Ext.data.JsonStore({
	proxy: new Ext.data.HttpProxy({
		url: BASE_URL + 'c_data_store/get_tahun',
		method: 'POST'
	}),

	autoLoad: true,
	root: 'data',

	fields: [
	  { name: "tahun", mapping: "tahun" }
	]
	});

	return ds_tahun;
}  

function store_penguji() { 
	var ds_penguji = new Ext.data.JsonStore({
	proxy: new Ext.data.HttpProxy({
		url: BASE_URL + 'c_mastah/griddosen',
		method: 'POST'
	}),

	autoLoad: true,
	root: 'data',

	fields: [

	  { name: "nidu", mapping: "nidu" },


	  { name: "nmdosdgngelar", mapping: "nmdosdgngelar" }

	]
	});

	return ds_penguji;
} 
function store_klppengguna_mhs_dosen() {
	var ds_klppengguna_mhs_dosen = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_mastah/grid_pengguna_mhs_dos',
			method: 'POST'
		}),
		totalProperty: 'results',
		root: 'data',
                autoLoad: true,
		
		fields: [{
			name: 'idklppengguna',
			mapping: 'idklppengguna'
		},
		{
			name: 'nmklppengguna',
			mapping: 'nmklppengguna'
		}]
	});  

	return ds_klppengguna_mhs_dosen;
} 
function store_stkrs() {
	var ds_stkrs = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_mastah/gridstkrs',
			method: 'POST'
		}),
		totalProperty: 'results',
		root: 'data',
                autoLoad: true,
		
		fields: [{
			name: 'idstkrs',
			mapping: 'idstkrs'
		},
		{
			name: 'kdstkrs',
			mapping: 'kdstkrs'
		},
		{
			name: 'nmstkrs',
			mapping: 'nmstkrs'
		}]
	});
	return ds_stkrs;
}   	

function store_jnsjurnal() {  
        var ds_jnsjurnal = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_mastah/grid_jnsjurnal',
			method: 'POST'
		}),
		totalProperty: 'results',
		root: 'data',
		autoLoad: true,
		fields: [{
			name: 'idjnsjurnal',
			mapping: 'idjnsjurnal'
		},
		{
			name: 'nmjnsjurnal',
			mapping: 'nmjnsjurnal'
		}]
	});

    return ds_jnsjurnal;
}

function store_fakultas() {  
        var ds_fakultas = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_mastah/grid_fakultas',
			method: 'POST'
		}),
		totalProperty: 'results',
		root: 'data',
		autoLoad: true,
		fields: [{
			name: 'kdfakultas',
			mapping: 'kdfakultas'
		},
		{
			name: 'nmfakultas',
			mapping: 'nmfakultas'
		}]
	});

    return ds_fakultas;
}

function store_prodi_jurnal() {  
        var ds_prodi_jurnal = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_mastah/grid_prodi_jurnal',
			method: 'POST'
		}),
		totalProperty: 'results',
		root: 'data',
		autoLoad: true,
		fields: [{
			name: 'kdprodi',
			mapping: 'kdprodi'
		},
		{
			name: 'nmprodi',
			mapping: 'nmprodi'
		}]
	});

    return ds_prodi_jurnal;
}

//=============================================================================
 var data, x;
 var p;
 var tpl;
 
        function user_foto_thumbs(x){
           //    alert(x);
              tpl = new Ext.Template(
                                            "<img src='"+ BASE_PATH +"/resources/img/thumbs/t_user/thumb_"+ x + "' width='110' height='125'/>"
                                        );
                                        tpl.overwrite(p.body, data);
                                        p.body.highlight('#c3daf9', {block:true});
           
         //   return tpl; 
         }
		 
		function user_foto_ori(x){
           //    alert(x);
              tpl = new Ext.Template(
                                            "<img src='"+ BASE_PATH +"/resources/img/ori/o_user/"+ x + "' width='110' height='125'/>"
                                        );
                                        tpl.overwrite(p.body, data);
                                        p.body.highlight('#c3daf9', {block:true});
           
         //   return tpl; 
         }
		 
		function pmb_foto_thumbs(x){
           //    alert(x);
              tpl = new Ext.Template(
                                            "<img src='"+ BASE_PATH +"/resources/img/thumbs/t_pmb/thumb_"+ x + "' width='110' height='125'/>"
                                        );
                                        tpl.overwrite(p.body, data);
                                        p.body.highlight('#c3daf9', {block:true});
           
         //   return tpl; 
         }
		 
		function pmb_foto_ori(x){
           //    alert(x);
              tpl = new Ext.Template(
                                            "<img src='"+ BASE_PATH +"/resources/img/ori/o_pmb/"+ x + "' width='110' height='125'/>"
                                        );
                                        tpl.overwrite(p.body, data);
                                        p.body.highlight('#c3daf9', {block:true});
           
         //   return tpl; 
         }
		 
		 //========================================================
		 
		 function isi_foto_ori_private(x){
           //    alert(x);
		   var addslash = ''; 
				for (i=0;i<BASE_PATH.length;i++){
					if (BASE_PATH.charAt(i)=='/'){
						addslash = addslash + BASE_PATH.charAt(i); // fungsi menghilangkan url website ini dengan menambahkan karakter slash untu==
					}
				}
				
				    var ds_url = new Ext.data.JsonStore({ //ambil url foto di private
						proxy: new Ext.data.HttpProxy({
							url: BASE_URL + 'c_mastah/get_url_private',
							method: 'POST'
						}),
						root: 'data',
						totalProperty: 'results',
						autoLoad: true,
						fields: [
						{
							name: "nilai",
							mapping: "nilai"
						}],
						listeners: {
							load: function(store, records, options) {
									var vurl = 0;
									ds_url.each(function (rec) { 
										vurl = rec.get('nilai');
									});
									tpl = new Ext.Template(
                                            "<img src='"+ addslash + vurl + x + "' width='110' height='125'/>"
                                        );
                                        tpl.overwrite(p.body, data);
                                        p.body.highlight('#c3daf9', {block:true});
							}
						}
					});						
           
         //   return tpl; 
         }
		 
		 //========================================================

		  function isi_foto_icons(x){
           //    alert(x);
              tpl = new Ext.Template(
                                            "<img src='"+ BASE_PATH +"/resources/img/icons/"+ x + "' width='110' height='125'/>"
                                        );
                                        tpl.overwrite(p.body, data);
                                        p.body.highlight('#c3daf9', {block:true});
           
         //   return tpl; 
         }
		 
		 //========================================================
         
          function isi_foto_ori(x){
           //    alert(x);
              tpl = new Ext.Template(
                                            "<img src='"+ BASE_PATH +"/resources/img/ori/"+ x + "' width='110' height='125'/>"
                                        );
                                        tpl.overwrite(p.body, data);
                                        p.body.highlight('#c3daf9', {block:true});
           
         //   return tpl; 
         }
         
         function kosong(){
             tpl = new Ext.Template(
                                            "<p><i>PHOTO</i></p>"
                                        );

                                        tpl.overwrite(p.body, data);
                                        p.body.highlight('#c3daf9', {block:true});
          //  return tpl;                                
        } 
        
        function toMoney(v) {
	return formatmoney(v);
}

function jumlah(a, b, c) {
		var tmpsatu1, tmpsatu2, tmpdua1, tmpdua2;
		tmpsatu1 = Ext.getCmp(a).getValue();
		tmpdua1 = Ext.getCmp(b).getValue();
		for (var r = 1; r < 10; r++) {
			tmpsatu1 = tmpsatu1.replace('.', '');
			tmpdua1 = tmpdua1.replace('.', '');
		}
		tmpsatu2 = tmpsatu1;
		tmpdua2 = tmpdua1;
		var hasil = Ext.getCmp(c);
		var tiga;
		tiga = parseInt(tmpsatu2) + parseInt(tmpdua2);
		hasil.setValue(tiga.toString());
	}

function kurang(a, b, c) {
		var tmpsatu1, tmpsatu2, tmpdua1, tmpdua2;
		tmpsatu1 = Ext.getCmp(a).getValue();
		tmpdua1 = Ext.getCmp(b).getValue();
		for (var r = 1; r < 10; r++) {
			tmpsatu1 = tmpsatu1.replace('.', '');
			tmpdua1 = tmpdua1.replace('.', '');
		}
		tmpsatu2 = tmpsatu1;
		tmpdua2 = tmpdua1;
		var hasil = Ext.getCmp(c);
		var tiga;
		tiga = parseInt(tmpsatu2) - parseInt(tmpdua2);
		hasil.setValue(tiga.toString());
	}
        
function cek_input_number(component_id) {
        var id = Ext.getCmp(component_id);
        if (isNaN(id.getValue()) && id.getValue() != '') {
                Ext.Msg.alert('Validasi Input', 'Input Harus Bilangan atau Numeric');
                id.setValue('');
        } else {
                id.setValue(formatmoney(id.getValue()));
        }
}

function formatmoney(v){
   // v = v.replace('.','');
    v = (Math.round((v-0)*100))/100;
    v = (v == Math.floor(v)) ? v + "" : ((v*10 == Math.floor(v*10)) ? v + "0" : v);
    v = String(v);
    var ps = v.split('.');
    var whole = ps[0];
    var sub = ps[1] ? '.'+ ps[1] : '';
    var r = /(\d+)(\d{3})/;
    while (r.test(whole)) {
            whole = whole.replace(r, '$1' + '.' + '$2');
    }
    v = whole + sub;
    if(v.charAt(0) == '-'){
        return  '-'+v.substr(1);
    }
    return v ;
}

function formatmoney_grid(v){
    v = (Math.round((v-0)*100))/100;
    v = (v == Math.floor(v)) ? v + "" : ((v*10 == Math.floor(v*10)) ? v + "0" : v);
    v = String(v);
    var ps = v.split('.');
    var whole = ps[0];
    var sub = ps[1] ? '.'+ ps[1] : '';
    var r = /(\d+)(\d{3})/;
    while (r.test(whole)) {
            whole = whole.replace(r, '$1' + '.' + '$2');
    }
    v = whole + sub;
    if(v.charAt(0) == '-'){
        return  '-'+v.substr(1);
    }
    return '<div style="text-align: right;">' + v + '</div>' ;
}

function terbilang(bilangan) {
	bilangan = String(bilangan);
	var angka = new Array('0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
	var kata = new Array('', 'Satu', 'Dua', 'Tiga', 'Empat', 'Lima', 'Enam', 'Tujuh', 'Delapan', 'Sembilan');
	var tingkat = new Array('', ' Ribu', ' Juta', ' Milyar', ' Triliun');
	var panjang_bilangan = bilangan.length; /* pengujian panjang bilangan */
	if (panjang_bilangan > 15) {
		kaLimat = "Diluar Batas";
		return kaLimat;
	} /* mengambil angka-angka yang ada dalam bilangan, dimasukkan ke dalam array */
	for (i = 1; i <= panjang_bilangan; i++) {
		angka[i] = bilangan.substr(-(i), 1);
	}
	i = 1;
	j = 0;
	kaLimat = ""; /* mulai proses iterasi terhadap array angka */
	while (i <= panjang_bilangan) {
		subkaLimat = "";
		kata1 = "";
		kata2 = "";
		kata3 = ""; /* untuk Ratusan */
		if (angka[i + 2] != "0") {
			if (angka[i + 2] == "1") {
				kata1 = "Seratus ";
			} else {
				kata1 = kata[angka[i + 2]] + " Ratus";
			}
		} /* untuk Puluhan atau Belasan */
		if (angka[i + 1] != "0") {
			if (angka[i + 1] == "1") {
				if (angka[i] == "0") {
					kata2 = "Sepuluh";
				} else if (angka[i] == "1") {
					kata2 = "Sebelas";
				} else {
					kata2 = kata[angka[i]] + " Belas";
				}
			} else {
				kata2 = kata[angka[i + 1]] + " Puluh";
			}
		} /* untuk Satuan */
		if (angka[i] != "0") {
			if (angka[i + 1] != "1") {
				kata3 = kata[angka[i]];
			}
		} /* pengujian angka apakah tidak nol semua, lalu ditambahkan tingkat */
		if ((angka[i] != "0") || (angka[i + 1] != "0") || (angka[i + 2] != "0")) {
			subkaLimat = kata1 + "" + kata2 + "" + kata3 + "" + tingkat[j] + "";
		} /* gabungkan variabe sub kaLimat (untuk Satu blok 3 angka) ke variabel kaLimat */
		kaLimat = subkaLimat + kaLimat;
		i = i + 3;
		j = j + 1;
	} /* mengganti Satu Ribu jadi Seribu jika diperlukan */
	if ((angka[5] == "0") && (angka[6] == "0")) {
		kaLimat = kaLimat.replace("Satu Ribu", "Seribu ");
	}
	return kaLimat + " Rupiah";
}
var layout_main;
//var Tree;
var menuTreeLoader;
var tree;
var tab_center;
var KDSTSEMESTER,KDTHNAKADEMIK,IDJNSSEMESTER,NMJNSSEMESTER;
        Ext.Ajax.request({
                url:BASE_URL + 'c_mastah/grid_vstsemester',
                method:'POST',
                success: function(response){
                    var r = Ext.decode(response.responseText);
                    KDSTSEMESTER = r.kdstsemester;
                    KDTHNAKADEMIK = r.kdthnakademik;
                    IDJNSSEMESTER = r.idjnssemester;
                    NMJNSSEMESTER = r.nmjnssemester;
                    
                }
//kdstsemester, kdthnakademik, idjnssemester, nmjnssemester
            });
var SYS_DATE = new Date();


function isi_gambar_jurnal(x){
           //    alert(x);
              tpl = new Ext.Template(
                                            "<img src='"+ BASE_PATH +"/resources/jurnal/"+ x + "' width='110' height='125'/>"
                                        );
                                        tpl.overwrite(p.body, data);
                                        p.body.highlight('#c3daf9', {block:true});
           
         //   return tpl; 
         }
		 
function isi_gambar_buku(x){
    //alert(x);
    tpl = new Ext.Template( 
			"<img src='data:"+ BASE_PATH +"/resources/blob/jpeg;base64, {"+ x + "}' width='110' height='125'/>"
    );
    tpl.overwrite(p.body, data);
    p.body.highlight('#c3daf9', {block:true});
    //return tpl; 
}