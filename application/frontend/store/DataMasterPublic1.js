// DATA SOURCE (for COMBO)
//Status Aktif-Tidak Aktif
        var ds_status = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_mastah/gridM_status',
			method: 'POST'
		}),
		totalProperty: 'results',
		root: 'data',
                autoLoad: true,
		fields: [{
			name: 'idstatus',
			mapping: 'idstatus'
		},{
			name: 'kdstatus',
			mapping: 'kdstatus'
		},
		{
			name: 'nmstatus',
			mapping: 'nmstatus'
		}]
	});

// Hierarki Head-Detail
        var ds_hierarki = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_mastah/gridM_hierarki',
			method: 'POST'
		}),
		totalProperty: 'results',
		root: 'data',
		autoLoad: true,
		fields: [{
			name: 'kdjnshirarki',
			mapping: 'kdjnshirarki'
		},
		{
			name: 'nmjnshirarki',
			mapping: 'nmjnshirarki'
		}]
	});

        var ds_dashboard = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_mastah/gridM_dash',
			method: 'POST'
		}),
		totalProperty: 'results',
		root: 'data',
                autoLoad: true,
		fields: [{
			name: 'kdjnsdashboard',
			mapping: 'kdjnsdashboard'
		},
		{
			name: 'nmjnsdashboard',
			mapping: 'nmjnsdashboard'
		}]
	});
        
       var ds_otoritas= new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_utility/g_JKP',
			method: 'POST'
		}),
		params: {
			start: 0,
			limit: 5
		},
		root: 'data',
		totalProperty: 'results',
		autoLoad: true,
		fields: [{
			name: "idklppengguna",
			mapping: "idklppengguna"
		},
		{
			name: "kdklppengguna",
			mapping: "kdklppengguna"
		},
		{
			name: "nmklppengguna",
			mapping: "nmklppengguna"
		}]
	});
        
        var ds_stsemester = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_mastah/grid_stsemester',
			method: 'POST'
		}),
		totalProperty: 'results',
		root: 'data',
                autoLoad: true,
		fields: [{
			name: 'kdstsemester',
			mapping: 'kdstsemester'
		}

            ]
	});
       
        var ds_jadwalpmb = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_mastah/grid_jadwalpmb',
			method: 'POST'
		}),
		totalProperty: 'results',
		root: 'data',
		autoLoad: true,
		fields: [{
			name: 'kdjadwalpmb',
			mapping: 'kdjadwalpmb'
		},
		{
			name: 'nmjadwalpmb',
			mapping: 'nmjadwalpmb'
		}]
	});
        
        var ds_carabyr = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_mastah/grid_carabyr',
			method: 'POST'
		}),
		totalProperty: 'results',
		root: 'data',
                autoLoad: true,
		
		fields: [{
			name: 'idcarabyr',
			mapping: 'idcarabyr'
		},
		{
			name: 'nmcarabyr',
			mapping: 'nmcarabyr'
		}]
	});    
    
        
        var ds_pekerjaanortu = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_mastah/grid_pekerjaanortu',
			method: 'POST'
		}),
		totalProperty: 'results',
		root: 'data',
		autoLoad: true,
		fields: [{
			name: 'idpekerjaanortu',
			mapping: 'idpekerjaanortu'
		},
		{
			name: 'nmpekerjaanortu',
			mapping: 'nmpekerjaanortu'
		}]
	});
        
        var ds_stmskmhs = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_mastah/grid_stmskmhs',
			method: 'POST'
		}),
		totalProperty: 'results',
		root: 'data',
		autoLoad: true,
		fields: [{
			name: 'idstmskmhs',
			mapping: 'idstmskmhs'
		},
		{
			name: 'nmstmskmhs',
			mapping: 'nmstmskmhs'
		}]
	});
      

var ds_jusm = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_mastah/grid_jusm',
			method: 'POST'
		}),
		totalProperty: 'results',
		root: 'data',
		autoLoad: true,
		fields: [{
			name: 'idjnsusm',
			mapping: 'idjnsusm'
		},
		{
			name: 'nmjnsusm',
			mapping: 'nmjnsusm'
		}]
	});
	
        var ds_stmskmhs_pmb = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_mastah/grid_stmskmhs_pmb',
			method: 'POST'
		}),
		totalProperty: 'results',
		root: 'data',
		autoLoad: true,
		fields: [{
			name: 'idstmskmhs',
			mapping: 'idstmskmhs'
		},
		{
			name: 'nmstmskmhs',
			mapping: 'nmstmskmhs'
		}]
	});
        
    var ds_prodi = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_mastah/grid_prodi',
			method: 'POST'
		}),
		totalProperty: 'results',
		root: 'data',
		autoLoad: true,
		fields: [{
			name: 'kdprodi',
			mapping: 'kdprodi'
		},
		{
			name: 'nmprodi',
			mapping: 'nmprodi'
		}]
	});
	
	var ds_prodiall = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_mastah/grid_prodiall',
			method: 'POST'
		}),
		totalProperty: 'results',
		root: 'data',
		autoLoad: true,
		fields: [{
			name: 'kdprodi',
			mapping: 'kdprodi'
		},
		{
			name: 'nmprodi',
			mapping: 'nmprodi'
		}]
	});
           
    var ds_klsmhs = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_mastah/grid_klsmhs',
			method: 'POST'
		}),
		totalProperty: 'results',
		root: 'data',
		autoLoad: true,
		fields: [{
			name: 'idklsmhs',
			mapping: 'idklsmhs'
		},
		{
			name: 'nmklsmhs',
			mapping: 'nmklsmhs'
		}]
	});
        
        var ds_jenpdkpendaftar = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_mastah/grid_jenpdkpendaftar',
			method: 'POST'
		}),
		totalProperty: 'results',
		root: 'data',
		autoLoad: true,
		fields: [{
			name: 'idjenpdkpendaftar',
			mapping: 'idjenpdkpendaftar'
		},
		{
			name: 'kdjenpdkpendaftar',
			mapping: 'kdjenpdkpendaftar'
		},
		{
			name: 'nmjenpdkpendaftar',
			mapping: 'nmjenpdkpendaftar'
		}]
	});
        
        var ds_stakreditasi = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_mastah/grid_stakreditasi',
			method: 'POST'
		}),
		totalProperty: 'results',
		root: 'data',
		autoLoad: true,
		fields: [{
			name: 'idstakreditasi',
			mapping: 'idstakreditasi'
		},
		{
			name: 'nmstakreditasi',
			mapping: 'nmstakreditasi'
		}]
	}); 
        
        var ds_sbrinfo = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_mastah/grid_sbrinfo',
			method: 'POST'
		}),
		totalProperty: 'results',
		root: 'data',
		autoLoad: true,
		fields: [{
			name: 'idsbrinfo',
			mapping: 'idsbrinfo'
		},
		{
			name: 'nmsbrinfo',
			mapping: 'nmsbrinfo'
		}]
	});//
        
         var ds_rekpmb = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_mastah/grid_rekpmb',
			method: 'POST'
		}),
		totalProperty: 'results',
		root: 'data',
		autoLoad: true,
		fields: [{
			name: 'idrekpmb',
			mapping: 'idrekpmb'
		},
		{
			name: 'nmrekpmb',
			mapping: 'nmrekpmb'
		}]
	});//
        
        var ds_alasanpendaftar = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_mastah/grid_alasanpendaftar',
			method: 'POST'
		}),
		totalProperty: 'results',
		root: 'data',
		autoLoad: true,
		fields: [{
			name: 'idalasanpendaftar',
			mapping: 'idalasanpendaftar'
		},
		{
			name: 'nmalasanpendaftar',
			mapping: 'nmalasanpendaftar'
		}]
	});//
        
        var ds_jgaleri = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_mastah/grid_jgaleri',
			method: 'POST'
		}),
		totalProperty: 'results',
		root: 'data',
		autoLoad: true,
		fields: [{
			name: 'kdjnsgaleri',
			mapping: 'kdjnsgaleri'
		},
		{
			name: 'nmjnsgaleriind',
			mapping: 'nmjnsgaleriind'
		}]
	});
        
        var ds_klpgaleri = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_mastah/grid_klpgaleri',
			method: 'POST'
		}),
		totalProperty: 'results',
		root: 'data',
		autoLoad: true,
		fields: [{
			name: 'kdklpgaleri',
			mapping: 'kdklpgaleri'
		},
		{
			name: 'nmklpgaleriind',
			mapping: 'nmklpgaleriind'
		}]
	});
        
        var ds_stpublish = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_mastah/grid_stpublish',
			method: 'POST'
		}),
		totalProperty: 'results',
		root: 'data',
		autoLoad: true,
		fields: [{
			name: 'kdstpublish',
			mapping: 'kdstpublish'
		},
		{
			name: 'nmstpublish',
			mapping: 'nmstpublish'
		}]
	});
        
        var ds_kategori_halaman = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_mastah/grid_kategorihalaman',
			method: 'POST'
		}),
		totalProperty: 'results',
		root: 'data',
		autoLoad: true,
		fields: [{
			name: 'kdktghalaman',
			mapping: 'kdktghalaman'
		},
		{
			name: 'nmktghalamanind',
			mapping: 'nmktghalamanind'
		}]
	});
        
    var ds_klptautan = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_mastah/grid_klptautan',
			method: 'POST'
		}),
		totalProperty: 'results',
		root: 'data',
		autoLoad: true,
		fields: [{
			name: 'kdtautan',
			mapping: 'kdtautan'
		},
		{
			name: 'nmklptautanind',
			mapping: 'nmklptautanind'
		}]
	});
    
	var ds_jpengguna = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_mastah/grid_jpengguna',
			method: 'POST'
		}),
		totalProperty: 'results',
		root: 'data',
		autoLoad: true,
		fields: [{
			name: 'idjnspengguna',
			mapping: 'idjnspengguna'
		},
		{
			name: 'nmjnspengguna',
			mapping: 'nmjnspengguna'
		}]
	});    
        
	var ds_vsemester = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_mastah/grid_vstsemester2',
			method: 'POST'
		}),
		totalProperty: 'results',
		root: 'data',
		autoLoad: true,
		fields: [{
			name: 'kdstsemester',
			mapping: 'kdstsemester'
		}]
	});   
        
       var ds_vsemester3 = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_mastah/grid_vstsemester3',
			method: 'POST'
		}),
		totalProperty: 'results',
		root: 'data',
		autoLoad: true,
		fields: [{
			name: 'kdstsemester',
			mapping: 'kdstsemester'
		}]
	});   
        
        var ds_stusm = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_mastah/gridM_stusm',
			method: 'POST'
		}),
		totalProperty: 'results',
		root: 'data',
                autoLoad: true,
		fields: [{
			name: 'idstusm',
			mapping: 'idstusm'
		},
		{
			name: 'nmstusm',
			mapping: 'nmstusm'
		}]
	});
        
        var ds_thnakademik = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_mastah/grid_thnakademik',
			method: 'POST'
		}),
		totalProperty: 'results',
		root: 'data',
                autoLoad: true,
		fields: [{
			name: 'idthnakademik',
			mapping: 'idthnakademik'
		},
		{
			name: 'kdthnakademik',
			mapping: 'kdthnakademik'
		}]
	});
        
        var ds_stsemester_private = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_mastah/grid_vstsemester3',
			method: 'POST'
		}),
		totalProperty: 'results',
		root: 'data',
                autoLoad: true,
		fields: [{
			name: 'idjnssemester',
			mapping: 'idjnssemester'
		},
		{
			name: 'nmjnssemester',
			mapping: 'nmjnssemester'
		}]
	});
        
        var ds_setbiayapmb = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_mastah/grid_setbiayapmb',
			method: 'POST'
		}),
		totalProperty: 'results',
		root: 'data',
                autoLoad: true,
		fields: [{
			name: 'idsetbiayapmb',
			mapping: 'idsetbiayapmb'
		},
		{
			name: 'nmsetbiayapmb',
			mapping: 'nmsetbiayapmb'
		}]
	});
	
	 var ds_perusahaan = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_mastah/grid_perusahaan',
			method: 'POST'
		}),
		totalProperty: 'results',
		root: 'data',
                autoLoad: true,
		fields: [{
			name: 'kdperusahaan',
			mapping: 'kdperusahaan'
		},
		{
			name: 'nmperusahaan',
			mapping: 'nmperusahaan'
		}]
	});
	
	var ds_posisi = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_mastah/grid_posisi',
			method: 'POST'
		}),
		totalProperty: 'results',
		root: 'data',
                autoLoad: true,
		fields: [{
			name: 'kdposisi',
			mapping: 'kdposisi'
		},
		{
			name: 'nmposisi',
			mapping: 'nmposisi'
		}]
	});
	
	var ds_sesi = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_mastah/grid_sesi',
			method: 'POST'
		}),
		totalProperty: 'results',
		root: 'data',
                autoLoad: true,
		fields: [{
			name: 'kdsesi',
			mapping: 'kdsesi'
		},
		{
			name: 'nmsesi',
			mapping: 'nmsesi'
		}]
	});
	
	
	 var ds_thnakademik2 = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_mastah/grid_thnakademik2',
			method: 'POST'
		}),
		totalProperty: 'results',
		root: 'data',
                autoLoad: true,
		fields: [{
			name: 'kdstsemester',
			mapping: 'kdstsemester'
		},{
			name: 'kdthnakademik',
			mapping: 'kdthnakademik'
		},{
			name: 'nmthnakademik',
			mapping: 'nmthnakademik'
		}]
	});
	
	var ds_jmlbiaya = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_mastah/grid_jmlbiaya',
			method: 'POST'
		}),
		totalProperty: 'results',
		root: 'data',
                autoLoad: true,
		fields: [{
			name: 'nominal',
			mapping: 'nominal'
		},{
			name: 'idsetbiaya',
			mapping: 'idsetbiaya'
		}]
	});
	
	 var ds_tahun = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_mastah/grid_tahun',
			method: 'POST'
		}),
		totalProperty: 'results',
		root: 'data',
                autoLoad: true,
		fields: [{
			name: 'kode',
			mapping: 'kode'
		},{
			name: 'nama',
			mapping: 'nama'
		}]
	});
	
	 var ds_kelas = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_mastah/grid_jkls',
			method: 'POST'
		}),
		totalProperty: 'results',
		root: 'data',
                autoLoad: true,
		fields: [{
			name: 'idjnskls',
			mapping: 'idjnskls'
		},{
			name: 'nmjnskls',
			mapping: 'nmjnskls'
		}]
	});
	
	var ds_klppengguna_mhs_dosen = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_mastah/grid_pengguna_mhs_dos',
			method: 'POST'
		}),
		totalProperty: 'results',
		root: 'data',
                autoLoad: true,
		
		fields: [{
			name: 'idklppengguna',
			mapping: 'idklppengguna'
		},
		{
			name: 'nmklppengguna',
			mapping: 'nmklppengguna'
		}]
	});  
	
	var ds_stkrs = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_mastah/gridstkrs',
			method: 'POST'
		}),
		totalProperty: 'results',
		root: 'data',
                autoLoad: true,
		
		fields: [{
			name: 'idstkrs',
			mapping: 'idstkrs'
		},
		{
			name: 'kdstkrs',
			mapping: 'kdstkrs'
		},
		{
			name: 'nmstkrs',
			mapping: 'nmstkrs'
		}]
	});  
//=============================================================================
 var data, x;
 var p;
 var tpl;
 
        function user_foto_thumbs(x){
           //    alert(x);
              tpl = new Ext.Template(
                                            "<img src='"+ BASE_PATH +"/resources/img/thumbs/t_user/thumb_"+ x + "' width='110' height='125'/>"
                                        );
                                        tpl.overwrite(p.body, data);
                                        p.body.highlight('#c3daf9', {block:true});
           
         //   return tpl; 
         }
		 
		 function user_foto_ori(x){
           //    alert(x);
              tpl = new Ext.Template(
                                            "<img src='"+ BASE_PATH +"/resources/img/ori/o_user/"+ x + "' width='110' height='125'/>"
                                        );
                                        tpl.overwrite(p.body, data);
                                        p.body.highlight('#c3daf9', {block:true});
           
         //   return tpl; 
         }
		 
		  function isi_foto_thumbs(x){
           //    alert(x);
              tpl = new Ext.Template(
                                            "<img src='"+ BASE_PATH +"/resources/img/thumbs/thumb_"+ x + "' width='110' height='125'/>"
                                        );
                                        tpl.overwrite(p.body, data);
                                        p.body.highlight('#c3daf9', {block:true});
           
         //   return tpl; 
         }
		 
		 function isi_foto_ori(x){
           //    alert(x);
              tpl = new Ext.Template(
                                            "<img src='"+ BASE_PATH +"/resources/img/ori/"+ x + "' width='110' height='125'/>"
                                        );
                                        tpl.overwrite(p.body, data);
                                        p.body.highlight('#c3daf9', {block:true});
           
         //   return tpl; 
         }
		 
		 function isi_foto_ori_private(x){
           //    alert(x);
              tpl = new Ext.Template(
                                            "<img src='"+ BASE_PATH +"/resources/img/ori/"+ x + "' width='110' height='125'/>"
                                        );
                                        tpl.overwrite(p.body, data);
                                        p.body.highlight('#c3daf9', {block:true});
           
         //   return tpl; 
         }
		  function isi_foto_icons(x){
           //    alert(x);
              tpl = new Ext.Template(
                                            "<img src='"+ BASE_PATH +"/resources/img/icons/"+ x + "' width='110' height='125'/>"
                                        );
                                        tpl.overwrite(p.body, data);
                                        p.body.highlight('#c3daf9', {block:true});
           
         //   return tpl; 
         }
         
          function isi_fotokaryawan(x){
           //    alert(x);
              tpl = new Ext.Template(
                                            "<img src='"+ BASE_PATH +"/resources/img/thumbs/"+ x + "' width='110' height='125'/>"
                                        );
                                        tpl.overwrite(p.body, data);
                                        p.body.highlight('#c3daf9', {block:true});
           
         //   return tpl; 
         }
         
         function kosong(){
             tpl = new Ext.Template(
                                            "<p><i>PHOTO</i></p>"
                                        );

                                        tpl.overwrite(p.body, data);
                                        p.body.highlight('#c3daf9', {block:true});
          //  return tpl;                                
        } 
        
        function toMoney(v) {
	return formatmoney(v);
}

function jumlah(a, b, c) {
		var tmpsatu1, tmpsatu2, tmpdua1, tmpdua2;
		tmpsatu1 = Ext.getCmp(a).getValue();
		tmpdua1 = Ext.getCmp(b).getValue();
		for (var r = 1; r < 10; r++) {
			tmpsatu1 = tmpsatu1.replace('.', '');
			tmpdua1 = tmpdua1.replace('.', '');
		}
		tmpsatu2 = tmpsatu1;
		tmpdua2 = tmpdua1;
		var hasil = Ext.getCmp(c);
		var tiga;
		tiga = parseInt(tmpsatu2) + parseInt(tmpdua2);
		hasil.setValue(tiga.toString());
	}

function kurang(a, b, c) {
		var tmpsatu1, tmpsatu2, tmpdua1, tmpdua2;
		tmpsatu1 = Ext.getCmp(a).getValue();
		tmpdua1 = Ext.getCmp(b).getValue();
		for (var r = 1; r < 10; r++) {
			tmpsatu1 = tmpsatu1.replace('.', '');
			tmpdua1 = tmpdua1.replace('.', '');
		}
		tmpsatu2 = tmpsatu1;
		tmpdua2 = tmpdua1;
		var hasil = Ext.getCmp(c);
		var tiga;
		tiga = parseInt(tmpsatu2) - parseInt(tmpdua2);
		hasil.setValue(tiga.toString());
	}
        
function cek_input_number(component_id) {
        var id = Ext.getCmp(component_id);
        if (isNaN(id.getValue()) && id.getValue() != '') {
                Ext.Msg.alert('Validasi Input', 'Input Harus Bilangan atau Numeric');
                id.setValue('');
        } else {
                id.setValue(formatmoney(id.getValue()));
        }
}

function formatmoney(v){
   // v = v.replace('.','');
    v = (Math.round((v-0)*100))/100;
    v = (v == Math.floor(v)) ? v + "" : ((v*10 == Math.floor(v*10)) ? v + "0" : v);
    v = String(v);
    var ps = v.split('.');
    var whole = ps[0];
    var sub = ps[1] ? '.'+ ps[1] : '';
    var r = /(\d+)(\d{3})/;
    while (r.test(whole)) {
            whole = whole.replace(r, '$1' + '.' + '$2');
    }
    v = whole + sub;
    if(v.charAt(0) == '-'){
        return  '-'+v.substr(1);
    }
    return v ;
}

function formatmoney_grid(v){
    v = (Math.round((v-0)*100))/100;
    v = (v == Math.floor(v)) ? v + "" : ((v*10 == Math.floor(v*10)) ? v + "0" : v);
    v = String(v);
    var ps = v.split('.');
    var whole = ps[0];
    var sub = ps[1] ? '.'+ ps[1] : '';
    var r = /(\d+)(\d{3})/;
    while (r.test(whole)) {
            whole = whole.replace(r, '$1' + '.' + '$2');
    }
    v = whole + sub;
    if(v.charAt(0) == '-'){
        return  '-'+v.substr(1);
    }
    return '<div style="text-align: right;">' + v + '</div>' ;
}

function terbilang(bilangan) {
	bilangan = String(bilangan);
	var angka = new Array('0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
	var kata = new Array('', 'Satu', 'Dua', 'Tiga', 'Empat', 'Lima', 'Enam', 'Tujuh', 'Delapan', 'Sembilan');
	var tingkat = new Array('', ' Ribu', ' Juta', ' Milyar', ' Triliun');
	var panjang_bilangan = bilangan.length; /* pengujian panjang bilangan */
	if (panjang_bilangan > 15) {
		kaLimat = "Diluar Batas";
		return kaLimat;
	} /* mengambil angka-angka yang ada dalam bilangan, dimasukkan ke dalam array */
	for (i = 1; i <= panjang_bilangan; i++) {
		angka[i] = bilangan.substr(-(i), 1);
	}
	i = 1;
	j = 0;
	kaLimat = ""; /* mulai proses iterasi terhadap array angka */
	while (i <= panjang_bilangan) {
		subkaLimat = "";
		kata1 = "";
		kata2 = "";
		kata3 = ""; /* untuk Ratusan */
		if (angka[i + 2] != "0") {
			if (angka[i + 2] == "1") {
				kata1 = "Seratus ";
			} else {
				kata1 = kata[angka[i + 2]] + " Ratus";
			}
		} /* untuk Puluhan atau Belasan */
		if (angka[i + 1] != "0") {
			if (angka[i + 1] == "1") {
				if (angka[i] == "0") {
					kata2 = "Sepuluh";
				} else if (angka[i] == "1") {
					kata2 = "Sebelas";
				} else {
					kata2 = kata[angka[i]] + " Belas";
				}
			} else {
				kata2 = kata[angka[i + 1]] + " Puluh";
			}
		} /* untuk Satuan */
		if (angka[i] != "0") {
			if (angka[i + 1] != "1") {
				kata3 = kata[angka[i]];
			}
		} /* pengujian angka apakah tidak nol semua, lalu ditambahkan tingkat */
		if ((angka[i] != "0") || (angka[i + 1] != "0") || (angka[i + 2] != "0")) {
			subkaLimat = kata1 + "" + kata2 + "" + kata3 + "" + tingkat[j] + "";
		} /* gabungkan variabe sub kaLimat (untuk Satu blok 3 angka) ke variabel kaLimat */
		kaLimat = subkaLimat + kaLimat;
		i = i + 3;
		j = j + 1;
	} /* mengganti Satu Ribu jadi Seribu jika diperlukan */
	if ((angka[5] == "0") && (angka[6] == "0")) {
		kaLimat = kaLimat.replace("Satu Ribu", "Seribu ");
	}
	return kaLimat + " Rupiah";
}
var layout_main;
//var Tree;
var menuTreeLoader;
var tree;
var tab_center;
var KDSTSEMESTER,KDTHNAKADEMIK,IDJNSSEMESTER,NMJNSSEMESTER;
        Ext.Ajax.request({
                url:BASE_URL + 'c_mastah/grid_vstsemester',
                method:'POST',
                success: function(response){
                    var r = Ext.decode(response.responseText);
                    KDSTSEMESTER = r.kdstsemester;
                    KDTHNAKADEMIK = r.kdthnakademik;
                    IDJNSSEMESTER = r.idjnssemester;
                    NMJNSSEMESTER = r.nmjnssemester;
                    
                }
//kdstsemester, kdthnakademik, idjnssemester, nmjnssemester
            });
var SYS_DATE = new Date();