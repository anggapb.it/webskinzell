function f_ppstudi() {
	var ds_grid = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_utility/g_PG',
			method: 'POST'
		}),
		params: {
			start: 0,
			limit: 5
		},
		root: 'data',
		totalProperty: 'results',
		autoLoad: true,
		fields: [{
			name: "userid",
			mapping: "userid"
		},
		{
			name: "password",
			mapping: "password"
		},
		{
			name: "nmlengkap",
			mapping: "nmlengkap"
		},
		{
			name: "email",
			mapping: "email"
		},
		{
			name: "nohp",
			mapping: "nohp"
		},
		{
			name: "noref",
			mapping: "noref"
		},
		{
			name: "nmklppengguna",
			mapping: "nmklppengguna"
		},
		{
			name: "nmjnspengguna",
			mapping: "nmjnspengguna"
		},
		{
			name: "foto",
			mapping: "foto"
		},
		{
			name: "nmstatus",
			mapping: "nmstatus"
		},
		{
			name: "tgldaftar",
			mapping: "tgldaftar"
		}]
	});

	var cm = new Ext.grid.ColumnModel({
		// specify any defaults for each column
		defaults: {
			sortable: true // columns are not sortable by default           
		},
		columns:  [{
			header: 'User Id',
			width: 50,
			dataIndex: 'userid',
			sortable: true
		},
		{
			header: 'Password',
			width: 150,
			dataIndex: 'password',
			sortable: true
		},
		{
			header: 'Nama Lengkap',
			width: 150,
			dataIndex: 'nmlengkap',
			sortable: true
		},
		{
			header: 'ID Kel Pengguna',
			width: 80,
			dataIndex: 'nmklppengguna',
			sortable: true
		},
		{
			header: 'ID Jenis Pengguna',
			width: 80,
			dataIndex: 'nmjnspengguna',
			sortable: true
		},
		{
			header: 'Status',
			width: 80,
			dataIndex: 'nmstatus',
			sortable: true
		},
		{
			header: 'Tanggal Daftar',
			width: 90,
			dataIndex: 'tgldaftar',
			sortable: true
		}]
	});
	var vw = new Ext.grid.GridView({
		emptyText: '< Materi Belum Dipilih  >'
	});
	var sm_nya = new Ext.grid.CheckboxSelectionModel({
		singleSelect:true,
		listeners: {
			//	rowselect: select_action,
			//	rowdeselect: deselect_action
		}
	});
	var cari_data = [new Ext.ux.grid.Search({
		iconCls: 'btn_search',
		minChars: 1,
		autoFocus: true,
		autoHeight: true,
		position: 'top',
		mode: 'local',
		width: 200
	})];
	var paging = new Ext.PagingToolbar({
		pageSize: 50,
		store: ds_grid,
		displayInfo: true,
		displayMsg: 'Data Materi Dari {0} - {1} of {2}',
		emptyMsg: 'Materi Belum Dipilih.'
	});
	
	// ================================================
	
	var ds_grid2 = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_utility/g_PPS',
			method: 'POST'
		}),
		root: 'data',
		totalProperty: 'results',
		autoLoad: true,
		fields: [{
			name: "kdprodi",
			mapping: "kdprodi"
		},
		{
			name: "nmprodi",
			mapping: "nmprodi"
		},
		{
			name: "nmjenjangstudi",
			mapping: "nmjenjangstudi"
		},
		{
			name: "pilih",
			mapping: "pilih",
			type: 'bool'
		}]
	});
	

	var cm2 = new Ext.grid.ColumnModel({
		// specify any defaults for each column
		defaults: {
			sortable: true // columns are not sortable by default           
		},
		columns: [
		//new Ext.grid.RowNumberer(),
		{
			header: 'Kode Prodi',
			width: 70,
			dataIndex: 'kdprodi',
			sortable: true
		},
		{
			header: 'Nama Prodi',
			width: 250,
			dataIndex: 'nmprodi',
			sortable: true
		},
		{
			header: 'Jenjang Studi',
			width: 90,
			dataIndex: 'nmjenjangstudi',
			sortable: true
		},
		{
			xtype: 'checkcolumn',
			header: '<center>Pilih</center>',
			width: 50,
			dataIndex: 'pilih',
			sortable: true
			,
                        editor:{
                                    xtype: 'checkbox',
                                    id:'cbpilih',
                                    name:'cbpilih',
                                    listeners: {
                                        change: function() {
                                                                                           
                                        }
                                    }
                        }
		}]
	});
	var vw2 = new Ext.grid.GridView({
		emptyText: '< Prodi Belum Dipilih  >'
	});
	var sm_nya2 = new Ext.grid.CheckboxSelectionModel({
		listeners: {
			//	rowselect: select_action,
			//	rowdeselect: deselect_action
		}
	});
	var cari_data2 = [new Ext.ux.grid.Search({
		iconCls: 'btn_search',
		minChars: 1,
		autoFocus: true,
		autoHeight: true,
		position: 'top',
		mode: 'local',
		width: 200
	})];
	var paging2 = new Ext.PagingToolbar({
		pageSize: 50,
		store: ds_grid2,
		displayInfo: true,
		displayMsg: 'Data Prodi Dari {0} - {1} of {2}',
		emptyMsg: 'Prodi Belum Dipilih.'
	});
	
	// =========================================================
	var grid_nya = new Ext.grid.EditorGridPanel({
		store: ds_grid,
		frame: true,
		//width: 1140,
		autoScroll: true,
		height: 480,
		autoWidth: true,
		plugins: cari_data,
		id: 'grid_det_product',
		buttonAlign: 'left',
		defaults: {
			anchor: '-10'
		},
		forceFit: true,
		tbar: ['->'],
		sm: sm_nya,
		vw: vw,
		cm: cm,
		bbar: paging,
		//autoExpandColumn: 'common',
		clicksToEdit: 1,
		listeners: {
			rowclick: function rowClick(grid, rowIdx) {
				var rec = ds_grid.getAt(rowIdx);
					isi =rec.data["userid"];
					if(isi!=''){
					Ext.getCmp('btn_simpan_prodi').enable(); 
                    Ext.getCmp('btn_selectall').enable(); 
                    Ext.getCmp('btn_deselectall').enable(); 
					ds_grid2.setBaseParam('userid', isi);
					ds_grid2.load();
					} else {
					Ext.getCmp('btn_simpan_prodi').disable(); 
                    Ext.getCmp('btn_selectall').disable(); 
                    Ext.getCmp('btn_deselectall').disable(); 
					}
			}
		}
	});
	// ================================================================
		var grid_nya2 = new Ext.grid.EditorGridPanel({
		store: ds_grid2,
		frame: true,
		//width: 1140,
		autoScroll: true,
		height: 480,
		autoWidth: true,
		plugins: cari_data2,
		id: 'grid_det_product2',
		buttonAlign: 'left',
		defaults: {
			anchor: '-10'
		},
		forceFit: true,
		tbar: [
		{
			text: 'Simpan',
			id: 'btn_simpan_prodi',
			iconCls: 'silk-save',
			disabled:true,
			handler: function() {
			if (sm_nya.getCount() > 0) {
				simpanpenggunaprodi()
			} else {
				Ext.MessageBox.alert("Informasi", "Pilih Salah Satu Pengguna");
			}		
			}
		}, 
        {
			text: 'Select All',
			id: 'btn_selectall',
			iconCls: 'silk-accept',
            disabled:true,
            handler: function()  {
				                    selectall()
			                     }
			
		},
        {
			text: 'Deselect All',
			id: 'btn_deselectall',
			iconCls: 'silk-delete',
            disabled:true,
            handler: function()  {
				                    deselectall()
			                     }
			
		}, '->'],
		sm: sm_nya2,
		vw: vw2,
		cm: cm2,
		bbar: paging2,
		//autoExpandColumn: 'common',
		clicksToEdit: 1,
		listeners: {
			rowdblclick: function rowdblClick(grid, rowIdx) {
				var rec = ds_grid2.getAt(rowIdx);
				//alert(rec.data["kdmenu"] + ', ' + Ext.getCmp('combox').getValue());
			}
		}
	});
	
	var form_bp_general = new Ext.form.FormPanel({
		id: 'form_bp_general_id',
		title: 'Pengguna Program Studi',
		region: 'center',
		autoScroll: true,
		buttonAlign: 'left',
		bodyStyle: 'padding: 5px',
		border: false,
		waitMsg: 'Waiting...',
		layout: 'column',
		items: [{
			columnWidth: .55,
			xtype: 'panel',
			border: false,
			bodyStyle: 'padding:0px 0px 0px 0px',
			items: [{
				layout: 'form',
				border: false,
				items: [{
					xtype: 'fieldset',
					title: 'Daftar Pengguna',
					layout: 'form',
					id:'daftarmateri',
					// labelAlign: 'top',
					items: [grid_nya]
						}]
					}]
		
			},
			{
			columnWidth: .45,
			xtype: 'panel',
			border: false,
			bodyStyle: 'padding:0px 0px 0px 0px',
			items: [{
				layout: 'form',
				border: false,
				items: [{
					xtype: 'fieldset',
					title: 'Untuk Program Studi',
					layout: 'form',
					id:'daftarprodi',
					items: [grid_nya2]
						}]
					}]
		
			}]
	});

	
	get_content(form_bp_general);
	
	function get_pengguna_prodi(sm, sm2) {
	var par='';
	var a= 1;
	var c = ';';
	
	var cntrec = sm.getCount();
	var arr = sm.getSelections(); // record grid materi yang dipilih
	
	for (i = 0; i < cntrec; i++) {
		  grid_nya2.getStore().each(function(rec){ // ambil seluruh grid prodi
									var rowData = rec.data; 
									if (a == grid_nya2.getStore().getCount()) {
										c = ''
									}
									var j = (rowData['pilih']) ? 1 : 0;
									
									par += arr[i].data['userid'] + 'x' +
									rowData['kdprodi'] + 'x' +
									j + 'x' + // pilih
									c;
									
									a= a+1;
								});
								return par;
		}
	}
	
	function simpanpenggunaprodi(){

					    var penggunaprodi = get_pengguna_prodi(sm_nya,sm_nya2);
                                            Ext.Ajax.request({
						url: BASE_URL + 'c_utility/s_PPS',
						params: {
							pilihpenggunaprodi: penggunaprodi,
//    
						},
						success: function() {
							Ext.MessageBox.alert("Informasi", "Simpan Data Berhasil");
							ds_grid2.load();
						},
						failure: function() {
							Ext.MessageBox.alert("Informasi", "SImpan Data Gagal");
						}
                                            });
                                        
        }
        
      //select all
    function selectall(){

					    var penggunaprodi = get_pengguna_prodi(sm_nya,sm_nya2);
                                            Ext.Ajax.request({
                        						url: BASE_URL + 'c_utility/sa_PPS',
                        						params: {
                        							pilihpenggunaprodi: penggunaprodi,
                        						},
                        						success: function() {
              							               Ext.MessageBox.alert("Informasi", "Select All Program Studi");
              							               ds_grid2.load();
                  						        },
                  						        failure: function() {
							                         Ext.MessageBox.alert("Informasi", "Select All Program Studi Gagal");
                  						        }
                                            });        
    }
    
    //deselect all
    function deselectall(){

					    var penggunaprodi = get_pengguna_prodi(sm_nya,sm_nya2);
                                            Ext.Ajax.request({
                            						url: BASE_URL + 'c_utility/da_PPS',
                            						params: {
                            							pilihpenggunaprodi: penggunaprodi,
                            						},
                            						success: function() {
              							               Ext.MessageBox.alert("Informasi", "Deselect All Program Studi");
              							               ds_grid2.load();
                  						            },
                  						            failure: function() {
					                                   Ext.MessageBox.alert("Informasi", "Deselect All Program Studi Gagal");
                  						            }
                                            });        
    }
}