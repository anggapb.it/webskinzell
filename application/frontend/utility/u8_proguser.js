function u8() {
	var ds_combo = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_utility/g_JKP',
			method: 'POST'
		}),
		params: {
			start: 0,
			limit: 5
		},
		root: 'data',
		totalProperty: 'results',
		autoLoad: true,
		fields: [{
			name: "kdklppengguna",
			mapping: "kdklppengguna"
		},
		{
			name: "nmklppengguna",
			mapping: "nmklppengguna"
		}]
	});
        
        var cm = new Ext.grid.ColumnModel({
        // specify any defaults for each column
        defaults: {
            sortable: true // columns are not sortable by default           
        },
        columns: [{
			header: 'Kode Program Studi',
			width: 150,
			dataIndex: 'kdmenu',
			sortable: true
		},
		{
			header: 'Program Studi',
			width: 150,
			dataIndex: 'nmmenu',
			sortable: true
		},
		{
			header: 'Fakultas',
			width: 150,
			dataIndex: 'deskripsi',
			sortable: true
		},
		{       xtype: 'checkcolumn',
                        header: 'All',
			dataIndex: 'user_aktif',
                        id:'status_check',
            		width: 150,
			readonly:false,
                        listeners:{
                            rowClick:function(){
                                ds_grid().setBaseParam('dodol','b');
                            }
                        }
			//sortable: true
		}]
    });
        
	var ds_grid = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_utility/g_OT',
			method: 'POST'
		}),
		params: {
			start: 0,
			limit: 5
		},
		root: 'data',
		totalProperty: 'results',
		autoLoad: true,
		fields: [{
			name: "kdmenu",
			mapping: "kdmenu"
		},{
			name: "nmmenu",
			mapping: "nmmenu"
		},{
			name: "deskripsi",
			mapping: "deskripsi"
		},{
			name: "idjnshirarki",
			mapping: "idjnshirarki"
		},{
			name: "idstatus",
			mapping: "idstatus"
		},{
			name: "men_idmenu",
			mapping: "men_idmenu"
		},{
			name: "user_aktif",
			mapping: "user_aktif", type: 'bool'
		}]
	});
        var vw = new Ext.grid.GridView({emptyText:'< Pengguna Belum Dipilih  >'});

	var sm_nya = new Ext.grid.CheckboxSelectionModel({
		listeners: {
		//	rowselect: select_action,
		//	rowdeselect: deselect_action
		}
	});
	var cari_data = [new Ext.ux.grid.Search({
		iconCls: 'btn_search',
		minChars: 1,
		autoFocus: true,
		autoHeight: true,
		position: 'top',
		mode: 'remote',
		width: 200
	})];
        
        var paging = new Ext.PagingToolbar({
                    pageSize: 50,
                    store: ds_grid,
                    displayInfo: true,
                    displayMsg: 'Data Pengguna Program Studi Dari {0} - {1} of {2}',
                    emptyMsg: 'Kelompok Pengguna Belum Dipilih.'
            });
        
	var grid_nya = new Ext.grid.EditorGridPanel({
		store: ds_grid,
		frame: true,
		width: 1140,
		height: 520,
		//plugins: cari_data,
		id: 'grid_det_product',
                forceFit: true,
		tbar: [{
			text: 'Pengguna (Nama Lengkap)',
//			iconCls: 'silk-printer',
			handler: function() {
				
			}
                        },{
                            xtype: 'textfield',
                            width: 125,
                            height: 50,
                            allowBlank: false,
                        //    store: ds_combo,
                            id: 'pengguna',
                            listeners: {
//                                select:function(){
//                                    var isi = Ext.getCmp('combox').getValue();
//                                    ds_grid.setBaseParam('klppengguna', isi);
//                                    ds_grid.load();
//                                }
//                                
                            },

                            typeAhead: true,
                          //  mode: 'remote',
                            emptyText:'Pilih...',
                            selectOnFocus:true
                
                        },{
								xtype: 'button',
								text: 'Pilih...',
								id: 'btn_data_gambar',
								width: 10,
								handler: function() {
									find_x("Data Pengguna", 3);
                                                                        
								}
			}
                    ,'->'],
		//sm:sm_nya,
                vw:vw,
		autoScroll: true,
		cm:cm,
		bbar: paging,
                //autoExpandColumn: 'common',
                clicksToEdit: 1,
		listeners: {
			rowdblclick: function rowdblClick(grid, rowIdx) {
				var rec = ds_grid.getAt(rowIdx);
				//alert(rec.data["kdmenu"] + ', ' + Ext.getCmp('combox').getValue());
                               
                                Ext.Ajax.request({
                                    method: 'POST',
                                    url: BASE_URL + 'c_utility/u_OT',
                                    params: {
                                            bool: rec.data["user_aktif"],
                                            kpengguna: Ext.getCmp('combox').getValue(),
                                            kdmenu: rec.data["kdmenu"]
                                    },
                                    success: function() {
                                            ds_grid.load();
                                            alert('Data telah diupdate!?')
                                    }
			});
				
			}
		}
	});
	                    
	var form_bp_general = new Ext.form.FormPanel({
		id: 'form_bp_general_id',
		region: 'center',
		autoScroll: true,
		buttonAlign: 'left',
		bodyStyle: 'padding: 5px',
		border: false,
		disabled: true,
		waitMsg: 'Waiting...',
		maskDisabled: false,
		monitorValid: true,
		items: [{
			layout: 'form',
			border: false,
			items: [grid_nya]
		}]
	});
        
	var o_m_product = new Ext.Panel({
		bodyStyle: 'padding: 5px',
		title: 'Pengguna Program Studi',
		defaults: {
			anchor: '-10'
		},
		border: true,
		margins: '0 0 5 0',
		plain: true,
		layout: 'border',
		items: [form_bp_general]
	});
        
	function simpan_grid(namaForm) {
		var form_nya = Ext.getCmp(namaForm);
		form_nya.getForm().submit({
			url: BASE_URL + 'c_utility/s_JKP',
			method: 'POST',
			success: function() {
				Ext.MessageBox.alert("Informasi", "Simpan Data Berhasil");
				ds_grid.load();
			},
			failure: function() {
				Ext.MessageBox.alert("Informasi", "Simpan Data Gagal");
			}
		});
	}
	function hapus_grid(namaForm) {
		var form_nya = Ext.getCmp(namaForm);
		Ext.MessageBox.show({
			title: "Konfirmasi",
			msg: "Anda Yakin Untuk menghapus Data ini?",
			buttons: Ext.MessageBox.YESNO,
			fn: function(btn) {
				if (btn == 'yes') {
					form_nya.getForm().submit({
						url: BASE_URL + 'c_utility/d_JKP',
						method: 'POST',
						success: function() {
							Ext.MessageBox.alert("Informasi", "Hapus Data Berhasil");
							ds_grid.load();
						},
						failure: function() {
							Ext.MessageBox.alert("Informasi", "Hapus Data Gagal");
						}
					});
				}
			}
		})
	}
	function ubah_grid(namaForm) {
		var form_nya = Ext.getCmp(namaForm);
		Ext.MessageBox.show({
			title: "Konfirmasi",
			msg: "Anda Yakin Untuk Mengubah Data ini?",
			buttons: Ext.MessageBox.YESNO,
			fn: function(btn) {
				if (btn == 'yes') {
					form_nya.getForm().submit({
						url: BASE_URL + 'c_utility/u_JKP',
						params: {
						//	kriteria: kriteria
						},
						method: 'POST',
						success: function() {
							Ext.MessageBox.alert("Informasi", "Ubah Data Berhasil");
							ds_grid.load();
						},
						failure: function() {
							Ext.MessageBox.alert("Informasi", "Ubah Data Gagal");
						}
					});
				}
			}
		});
	}
	get_content(o_m_product);
}