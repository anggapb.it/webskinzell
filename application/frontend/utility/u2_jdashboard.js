function u2() {
	var kodex;
	var ds_nya = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_utility/g_JD',
			method: 'POST'
		}),
		params: {
			start: 0,
			limit: 5
		},
		root: 'data',
		totalProperty: 'results',
		autoLoad: true,
		fields: [{
			name: "idjnsdashboard",
			mapping: "idjnsdashboard"
		},{
			name: "kdjnsdashboard",
			mapping: "kdjnsdashboard"
		},
		{
			name: "nmjnsdashboard",
			mapping: "nmjnsdashboard"
		},
		{
			name: "deskripsi",
			mapping: "deskripsi"
		}]
	});
	var sm_nya = new Ext.grid.RowSelectionModel({
		singleSelect: true,
		listeners: {}
	});
	var cari_data_nya = [new Ext.ux.grid.Search({
		iconCls: 'btn_search',
		minChars: 1,
		autoFocus: true,
		autoHeight: true,
		position: 'top',
		mode: 'remote',
		width: 200
	})];
	var paging = new Ext.PagingToolbar({
		pageSize: 50,
		store: ds_nya,
		displayInfo: true,
		displayMsg: 'Data Dashboard Dari {0} - {1} of {2}',
		emptyMsg: 'No data to display'
	});
	var grid_nya = new Ext.grid.GridPanel({
		store: ds_nya,
		frame: true,
		height: 530,
		plugins: cari_data_nya,
		id: 'grid_det_nya',
		forceFit: true,
		tbar: [],
		sm: sm_nya,
		autoScroll: true,
		autoSizeColumns: true,
		enableColumnResize: true,
		enableColumnHide: false,
		enableColumnMove: false,
		enableHdaccess: false,
		columnLines: true,
		loadMask: true,
		columns: [{
			header: 'ID',
			width: 50,
                        hidden:true,
			dataIndex: 'idjnsdashboard',
			sortable: true
		},{
			header: 'Kode',
			width: 50,
			dataIndex: 'kdjnsdashboard',
			sortable: true
		},
		{
			header: 'Nama',
			width: 150,
			dataIndex: 'nmjnsdashboard',
			sortable: true
		},
		{
			header: 'Deskripsi',
			width: 150,
			dataIndex: 'deskripsi',
			sortable: true
		}],
		bbar: paging,
		listeners: {
			rowclick: function rowClick(grid, rowIdx) {
				var rec = ds_nya.getAt(rowIdx);
				Ext.getCmp("id").setValue(rec.data["idjnsdashboard"]);
				Ext.getCmp("kode").setValue(rec.data["kdjnsdashboard"]);
				kodex = rec.data["kode"];
				Ext.getCmp("nama").setValue(rec.data["nmjnsdashboard"]);
				Ext.getCmp("deskripsi").setValue(rec.data["deskripsi"]);
				Ext.getCmp('btn_simpan').disable();
				Ext.getCmp('btn_ubah').enable();
				Ext.getCmp('btn_hapus').enable();
				Ext.getCmp('form_bp_general').enable();
			}
		}
	});
	var form_bp_general = new Ext.form.FormPanel({
		id: 'form_bp_general',
		forceFit: true,
		title: 'Jenis Dashboard',
		layout: 'column',
		items: [{
			columnWidth: .42,
			xtype: 'panel',
			border: false,
			bodyStyle: 'padding:3px 3px 3px 3px',
			items: [{
                            layout: 'form',
                            bodyStyle: 'padding:3px 3px 3px 3px',
                            border: false,
                            items: [{
				xtype: 'fieldset',
				title: 'Detail Jenis Dashboard',
                                defaults: {
						labelWidth: 100,
						labelAlign: 'right'
					},
				height: 595,
				items: [{
					columnWidth: 1,
					border: false,
					layout: 'form',
					items: [{
						xtype: 'textfield',
						//fieldLabel: 'ID',
						id: 'id',
						width: 70,
                                                hidden:true,
						//allowBlank: false,
						disabled: true,
						listeners: {}
					},{
						xtype: 'textfield',
						fieldLabel: 'Kode',
						id: 'kode',
						width: 70,
						allowBlank: false,
						disabled: true,
						listeners: {}
					},
					{
						xtype: 'textfield',
						fieldLabel: 'Nama ',
						id: 'nama',
						allowBlank: false,
						width: 250
					},
					{
						xtype: 'textarea',
						fieldLabel: 'Deskripsi',
						id: 'deskripsi',
						width: 300
					},
					{
						columnWidth: 1,
						xtype: 'panel',
						border: false,
						buttonAlign: 'left',
						buttons: [{
							text: 'Baru',
							id: 'btn_baru_nya',
							iconCls: 'silk-add',
							handler: function() {
								form_bp_general.getForm().reset();
								Ext.getCmp('form_bp_general').enable();
								Ext.getCmp('btn_simpan').enable();
								Ext.getCmp('btn_hapus').disable();
								Ext.getCmp('btn_ubah').disable();
								Ext.getCmp('kode').focus();
							}
						}, '->',
						{
							text: 'Simpan',
							id: 'btn_simpan',
							iconCls: 'silk-save',
							handler: function() {
								simpan_grid("form_bp_general");
							}
						},
						{
							text: 'Hapus',
							id: 'btn_hapus',
							iconCls: 'silk-delete',
							handler: function() {
								hapus_grid("form_bp_general");
							}
						},
						{
							text: 'Ubah',
							id: 'btn_ubah',
							iconCls: 'silk-edit',
							handler: function() {
								ubah_grid("form_bp_general");
							}
						}]
					}]
				}]
			}]
                    }]
		},
		{
			columnWidth: .58,
			xtype: 'panel',
			border: false,
			bodyStyle: 'padding:3px 3px 3px 3px',
			items: [{
				layout: 'form',
				//bodyStyle: 'padding:10px 3px 3px 5px',
				border: false,
				items: [grid_nya]
			}]
		}],
		listeners: {
			afterrender: awal
		}
	});

	function awal() {
		form_bp_general.getForm().reset();
		Ext.getCmp('form_bp_general').enable();
		Ext.getCmp('btn_simpan').disable();
		Ext.getCmp('btn_hapus').disable();
		Ext.getCmp('btn_ubah').disable();
	}

	function simpan_grid(namaForm) {
		var form_nya = Ext.getCmp(namaForm);
		form_nya.getForm().submit({
			url: BASE_URL + 'c_utility/s_JD',
			method: 'POST',
			success: function() {
				Ext.MessageBox.alert("Informasi", "Simpan Data Berhasil");
				ds_nya.load();
				form_bp_general.getForm().reset();
				Ext.getCmp('form_bp_general').enable();
				Ext.getCmp('btn_simpan').enable();
				Ext.getCmp('btn_hapus').disable();
				Ext.getCmp('btn_ubah').disable();
			},
			failure: function() {
				Ext.MessageBox.alert("Informasi", "Simpan Data Gagal");
			}
		});
	}

	function hapus_grid(namaForm) {
		var form_nya = Ext.getCmp(namaForm);
		Ext.MessageBox.show({
			title: "Konfirmasi",
			msg: "Anda Yakin Untuk menghapus Data ini?",
			buttons: Ext.MessageBox.YESNO,
			fn: function(btn) {
				if (btn == 'yes') {
					form_nya.getForm().submit({
						url: BASE_URL + 'c_utility/d_JD',
						method: 'POST',
						success: function() {
							Ext.MessageBox.alert("Informasi", "Hapus Data Berhasil");
							ds_nya.load();
							form_bp_general.getForm().reset();
							Ext.getCmp('form_bp_general').enable();
							Ext.getCmp('btn_simpan').enable();
							Ext.getCmp('btn_hapus').disable();
							Ext.getCmp('btn_ubah').disable();
						},
						failure: function() {
							Ext.MessageBox.alert("Informasi", "Hapus Data Gagal");
						}
					});
				}
			}
		})
	}

	function ubah_grid(namaForm) {
		var msgplus;
//		if (kodex != Ext.getCmp('kode').getValue()) {
//			// alert("Kode tidak bisa di ubah!?");
//			msgplus = '<center>Semua field dapat berubah terkecuali field "KODE"!? </br> </center>';
//		} else {
			msgplus = '';
//		}
		var form_nya = Ext.getCmp(namaForm);
		Ext.MessageBox.show({
			title: "Konfirmasi",
			width: 350,
			msg: msgplus + "<center> Anda Yakin Untuk Mengubah Data ini?</center>",
			buttons: Ext.MessageBox.YESNO,
			fn: function(btn) {
				if (btn == 'yes') {
					form_nya.getForm().submit({
						url: BASE_URL + 'c_utility/u_JD',
						params: {
							//	kriteria: kriteria
						},
						method: 'POST',
						success: function() {
							Ext.MessageBox.alert("Informasi", "Ubah Data Berhasil");
							ds_nya.load();
							form_bp_general.getForm().reset();
							Ext.getCmp('form_bp_general').enable();
							Ext.getCmp('btn_simpan').enable();
							Ext.getCmp('btn_hapus').disable();
							Ext.getCmp('btn_ubah').disable();
						},
						failure: function() {
							Ext.MessageBox.alert("Informasi", "Ubah Data Gagal");
						}
					});
				}
			}
		});
	}
	get_content(form_bp_general);
}