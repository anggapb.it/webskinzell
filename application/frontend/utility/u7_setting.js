function u7() {
	var formx = new Ext.form.FormPanel({
		border: false,
		id: 'formx',
		labelAlign: 'left',
		buttonAlign: 'left',
		bodyStyle: 'padding:60px 3px 3px 5px', // atas, kanan, bawah, kiri
		monitorValid: true,
                height: 510,
		width: 470,
		items: [{
			layout: 'column',
			border: false,
			items: [{
				columnWidth: 1,
				border: false,
				layout: 'form',
				items: [{
					xtype: 'fieldset',
					title: 'Detail Jenis Dashboard',
					height: 250,
					items: [{
						columnWidth: 1,
						border: false,
						layout: 'form',
						items: [{
							xtype: 'textfield',
							fieldLabel: 'Kode',
							id: 'kode',
							width: 70,
							allowBlank: false,
							disabled: true,
							listeners: {}
						},
						{
							xtype: 'textfield',
							fieldLabel: 'Nama ',
							id: 'nama',
							allowBlank: false,
							width: 250
						},
						{
							xtype: 'textarea',
							fieldLabel: 'Deskripsi',
							id: 'deskripsi',
                                                        width: 300
						},
						{
							columnWidth: 1,
							xtype: 'panel',
							border: false,
							buttonAlign: 'left',
							buttons: [
                                                        {
								text: 'Baru',
								id: 'btn_baru_product',
								iconCls: 'silk-add',
								handler: function() {
									formx.getForm().reset();
									Ext.getCmp('formx').enable();
									Ext.getCmp('btn_simpan').enable();
									Ext.getCmp('btn_hapus').disable();
									Ext.getCmp('btn_ubah').disable();
									Ext.getCmp('kode').focus();
								}
							},'->',
							{
								text: 'Simpan',
								id: 'btn_simpan',
								iconCls: 'silk-save',
								handler: function() {
									simpan_grid("formx");
								}
							},
							{
								text: 'Hapus',
								id: 'btn_hapus',
								iconCls: 'silk-delete',
								handler: function() {
									hapus_grid("formx");
								}
							},
							{
								text: 'Ubah',
								id: 'btn_ubah',
								iconCls: 'silk-edit',
								handler: function() {
									ubah_grid("formx");
								}
							}]
						}]
					}]
				}]
			}]
		}]
	});
	
	var ds_product = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'c_utility/g_JD',
			method: 'POST'
		}),
		params: {
			start: 0,
			limit: 5
		},
		root: 'data',
		totalProperty: 'results',
		autoLoad: true,
		fields: [{
			name: "kode",
			mapping: "kdjnsdashdoard"
		},
		{
			name: "nama",
			mapping: "nmjnsdashdoard"
		},
		{
			name: "deskripsi",
			mapping: "deskripsi"
		}]
	});
	var sm_product = new Ext.grid.RowSelectionModel({
		singleSelect: true,
		listeners: {}
	});
	var cari_data_product = [new Ext.ux.grid.Search({
		iconCls: 'btn_search',
		minChars: 1,
		autoFocus: true,
		autoHeight: true,
		position: 'top',
		mode: 'local',
		width: 200
	})];
	var grid_product = new Ext.grid.GridPanel({
		store: ds_product,
		frame: true,
		width: 630,
		height: 350,
		plugins: cari_data_product,
		id: 'grid_det_product',
		forceFit: true,
		tbar: [{hidden:true,
			text: 'Print',
			iconCls: 'silk-printer',
			handler: function() {
				var parsing = '';
				parsing = parsing + 'var1';
				parsing = parsing + 'qie' + 'var2';
				parsing = parsing + 'qie' + 'var3';
				parsing = parsing + 'qie' + 'var4';
				var win = window.open();
				win.location.reload();
				win.location = BASE_URL + 'print_topdf/print_Dproducts';
			}
		}, '->'],
		sm: sm_product,
		autoScroll: true,
		autoSizeColumns: true,
		enableColumnResize: true,
		enableColumnHide: false,
		enableColumnMove: false,
		enableHdaccess: false,
		columnLines: true,
		loadMask: true,
		columns: [{
			header: 'Kode',
			width: 50,
			dataIndex: 'kode',
			sortable: true
		},
		{
			header: 'Nama',
			width: 150,
			dataIndex: 'nama',
			sortable: true
		},
		{
			header: 'Deskripsi',
			width: 150,
			dataIndex: 'deskripsi',
			sortable: true
		}],
		bbar: paging_product,
		listeners: {
			rowdblclick: function rowDbClick(grid, rowIdx) {
				var rec = ds_product.getAt(rowIdx);
				Ext.getCmp("kode").setValue(rec.data["kode"]);
				Ext.getCmp("nama").setValue(rec.data["nama"]);
				Ext.getCmp("deskripsi").setValue(rec.data["deskripsi"]);
				
				Ext.getCmp('btn_simpan').disable();
				Ext.getCmp('btn_ubah').enable();
				Ext.getCmp('btn_hapus').enable();
				Ext.getCmp('formx').enable();
				
			}
		}
	});
	var paging_product = new Ext.PagingToolbar({
		pageSize: 50,
		store: ds_product,
		displayInfo: true,
		displayMsg: 'Data Produk Dari {0} - {1} of {2}',
		emptyMsg: 'No data to display'
	});
	var tabs2 = new Ext.Panel({
		forceFit: true,
		layout: 'column',
		items: [{
			columnWidth: .42,
			xtype: 'panel',
			border: false,
			bodyStyle: 'padding:3px 3px 3px 10px',
			items: [formx]
		},
		{
			columnWidth: .56,
			xtype: 'panel',
			border: false,
			bodyStyle: 'padding:20px 3px 3px 10px',
			items: [grid_product]
		}],
		listeners: {
			afterrender: awal
		}
	});
	function awal() {
		formx.getForm().reset();
		Ext.getCmp('formx').enable();
		Ext.getCmp('btn_simpan').disable();
		Ext.getCmp('btn_hapus').disable();
		Ext.getCmp('btn_ubah').disable();
	}
	var form_bp_general = new Ext.form.FormPanel({
		id: 'form_bp_general_id',
		region: 'center',
		autoScroll: true,
		buttonAlign: 'left',
		bodyStyle: 'padding: 5px',
		border: false,
		disabled: true,
		waitMsg: 'Waiting...',
		maskDisabled: false,
		monitorValid: true,
		items: [{
			layout: 'form',
			border: false,
			items: [tabs2]
		}]
	});
	var o_m_product = new Ext.Panel({
		bodyStyle: 'padding: 5px',
		title: 'Jenis Dashboard',
		defaults: {
			anchor: '-10'
		},
		border: true,
		margins: '0 0 5 0',
		plain: true,
		layout: 'border',
		items: [form_bp_general]
	});
	function simpan_grid(namaForm) {
		var form_nya = Ext.getCmp(namaForm);
		form_nya.getForm().submit({
			url: BASE_URL + 'c_utility/s_JD',
			method: 'POST',
			success: function() {
				Ext.MessageBox.alert("Informasi", "Simpan Data Berhasil");
				ds_product.load();
				formx.getForm().reset();
				Ext.getCmp('formx').enable();
				Ext.getCmp('btn_simpan').enable();
				Ext.getCmp('btn_hapus').disable();
				Ext.getCmp('btn_ubah').disable();
			},
			failure: function() {
				Ext.MessageBox.alert("Informasi", "Simpan Data Gagal");
			}
		});
	}
	function hapus_grid(namaForm) {
		var form_nya = Ext.getCmp(namaForm);
		Ext.MessageBox.show({
			title: "Konfirmasi",
			msg: "Anda Yakin Untuk menghapus Data ini?",
			buttons: Ext.MessageBox.YESNO,
			fn: function(btn) {
				if (btn == 'yes') {
					form_nya.getForm().submit({
						url: BASE_URL + 'c_utility/d_JD',
						method: 'POST',
						success: function() {
							Ext.MessageBox.alert("Informasi", "Hapus Data Berhasil");
							ds_product.load();
							formx.getForm().reset();
							Ext.getCmp('formx').enable();
							Ext.getCmp('btn_simpan').enable();
							Ext.getCmp('btn_hapus').disable();
							Ext.getCmp('btn_ubah').disable();
						},
						failure: function() {
							Ext.MessageBox.alert("Informasi", "Hapus Data Gagal");
						}
					});
				}
			}
		})
	}
	function ubah_grid(namaForm) {
		var form_nya = Ext.getCmp(namaForm);
		Ext.MessageBox.show({
			title: "Konfirmasi",
			msg: "Anda Yakin Untuk Mengubah Data ini?",
			buttons: Ext.MessageBox.YESNO,
			fn: function(btn) {
				if (btn == 'yes') {
					form_nya.getForm().submit({
						url: BASE_URL + 'c_utility/u_JD',
						params: {
						//	kriteria: kriteria
						},
						method: 'POST',
						success: function() {
							Ext.MessageBox.alert("Informasi", "Ubah Data Berhasil");
							ds_product.load();
							formx.getForm().reset();
							Ext.getCmp('formx').enable();
							Ext.getCmp('btn_simpan').enable();
							Ext.getCmp('btn_hapus').disable();
							Ext.getCmp('btn_ubah').disable();
						},
						failure: function() {
							Ext.MessageBox.alert("Informasi", "Ubah Data Gagal");
						}
					});
				}
			}
		});
	}
	get_content(o_m_product);
}